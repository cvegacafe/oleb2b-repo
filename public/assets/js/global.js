$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function(){
    $('.delete-form').on('submit',function () {
        event.preventDefault();
        var form = $(this);
        var title = $(this).data('title') || "Warning";
        var text = $(this).data('text') || "Are you sure?";
        var type = $(this).data('type') || "warning";
        var btnCancel = $(this).data('btn-cancel') || "Cancel";
        var btnOk = $(this).data('btn-ok') || "Yes, delete";
        swal({
                title: title,
                text: text,
                type: type,
                showCancelButton    : true,
                cancelButtonText    : btnCancel,
                cancelButtonColor   : "#C62828",
                confirmButtonColor  : "#f44336",
                confirmButtonText   : btnOk,
                closeOnConfirm      : true
            },
            function(){
                form.submit();
            });
    });
});

function success_message(title, text){
    swal({
        title   : title,
        text    : text,
        type    : "success"
    },function(){
        location.reload();
    });
}





function confirm_delete(ruta){
    swal({
            title   : "Warning",
            text    : "Are you sure?",
            type    : "warning",
            showCancelButton    : true,
            cancelButtonText    : "Cancel",
            cancelButtonColor   : "#C62828",
            confirmButtonColor  : "#f44336",
            confirmButtonText   : "Yes, delete",
            closeOnConfirm      : false
        },
        function(){
            $.ajax({
                url:ruta,
                method:'DELETE',
                success: function (response) {
                    swal("Done", "The company has been deleted.", "success");
                },
                error: function (){
                    swal("Eror", "An error was occurred when trying to delete.", "error");
                }
            });

        });
}
$('.validate-delete').on('click',function(e){
    e.preventDefault();
    var delete_route = $(this).prop("href");
    confirm_delete(delete_route);
});