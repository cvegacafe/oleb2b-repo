<?php namespace App\Services;

use Illuminate\Support\Facades\Lang;
use Stichoza\GoogleTranslate\TranslateClient;

/**
 * Class TranslateService
 * @package App\Services
 */
class TranslateService
{

  /**
   * @var null|TranslateClient
   */
  private $trans = null;
  /**
   * @var null
   */
  private $currentLang = null;

  /**
   * TranslateService constructor.
   */
  public function __construct()
    {
        $this->trans = new TranslateClient();
        $this->currentLang = Lang::locale();
    }

  /**
   * @param $lang
   * @return bool
   */
  private function sameLang($lang)
    {
        return $this->currentLang == $lang;
    }

  /**
   * @param $text
   * @return string
   */
  public function transToSpanish($text)
    {
        if ($this->sameLang('es')) return $text;
        return $this->translateWord('es',$text);
    }

  /**
   * @param $text
   * @return string
   */
  public function transToEnglish($text)
    {
        if ($this->sameLang('en')) return $text;
        return $this->translateWord('en',$text);
    }

  /**
   * @param $text
   * @return string
   */
  public function transToRussian($text)
    {
        if ($this->sameLang('ru')) return $text;
        return $this->translateWord('ru',$text);
    }

  /**
   * @param $lang
   * @param $word
   * @return string
   */
  public function translateWord($lang, $word)
    {
        return $this->trans->setSource($this->currentLang)->setTarget($lang)->translate($word);
    }
}
