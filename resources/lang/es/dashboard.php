<?php

return [
    'company_info' => 'Perfil de la Empresa',
    'products' => 'Productos',
    'certificates' => 'Certificados',
    'buying_requests' => 'Solicitudes de Cotización',
    'company_photos' => 'Fotos de la Empresa',
    'advanced' => 'Avanzado',
    'update' => 'Actualizar',
    'submit' => 'Crear',
    'add_products' => 'Agregar',
    'add_photos' => 'Subir Fotos',
    'upgrade' => 'Upgrade',
    'membership' => 'Membresía'
];