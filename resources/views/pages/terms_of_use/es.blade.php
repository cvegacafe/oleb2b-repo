@extends('layouts.pages')
@section('title')
    {{trans('portal.terms_of_use')}}
@endsection
@section('content')
    <div class="container-fluid contact-top">
        <div class="col-xs-12">
            <h1>{{trans('portal.terms_of_use')}}</h1>
        </div>
    </div>
    <section class="contact">
        <div class="container terms-list">
            <div class="row ">
                <h4>¡POR FAVOR LEA ESTOS TÉRMINOS Y CONDICIONES CUIDADOSAMENTE!
                    <small>(Actualizado el 01 de marzo de 2017)</small>
                </h4>
                <p>Bienvenido a <strong>www.oleb2b.com</strong>. Estas Condiciones de Uso describen los términos y condiciones aplicables a su acceso y uso del sitio web en <strong>www.oleb2b.com</strong> ("Sitio"). Este documento es un acuerdo legalmente vinculante entre usted como usuario (s) del Sitio (en adelante, "usted", "su" o de aquí en adelante "Usuario") y la entidad Oleb2b.com que figuran en la cláusula 2.1 a continuación (en adelante, "nosotros", "nuestro" o "Oleb2b.com" o “Oleb2b.com S.A.C.” en lo sucesivo).
                </p>

                <ol>
                    <li>
                        <strong> Aplicación y aceptación de los Términos</strong>
                        <ol>
                            <li> El uso de los Sitios y servicios, software y productos de Oleb2b.com (de forma colectiva como el de aquí en adelante "Servicios") está sujeto a los términos y condiciones contenidos en este documento, así como la política de privacidad, la Política de ficha de producto y cualquier otro reglas y políticas de los sitios que Oleb2b.com se permite la publicación de vez en cuando. Este documento y otras reglas y políticas del sitio se denominan colectivamente en adelante, las "Condiciones". Al acceder al Sitio o utilizar los Servicios, usted se compromete a aceptar y estar obligado por los Términos. Por favor, no use los Servicios o el Sitio si usted no acepta todos los términos.</li>
                            <li>El usuario no puede utilizar los Servicios y no podrá aceptar las Condiciones si (a) no tiene la edad legal para formalizar un contrato vinculante con Oleb2b.com, o (b) que no están autorizados a recibir servicios bajo las leyes de la República de Perú u otros países / regiones, incluyendo el país / región en la que reside o desde el cual utiliza los Servicios.
                            </li>
                            <li>Usted reconoce y acepta que Oleb2b.com podrá modificar las Condiciones en cualquier momento mediante la publicación de la modificación relevante y Condiciones actualizados en los Sitios. Al continuar utilizando los servicios o los sitios, el usuario acepta que los términos modificados se aplicarán a usted.
                            </li>
                            <li> Si Oleb2b.com ha publicado o proporcionado una traducción del español / Versión Inglés / Rusia / idioma portugués de las Condiciones, usted acepta que la traducción se proporciona únicamente para su comodidad y que la versión en idioma Inglés regirán el uso de los servicios o los sitios.
                            </li>
                            <li>Es posible que tenga que entrar en un acuerdo por separado, ya sea en línea o fuera de línea, con Oleb2b.com o nuestro afiliado a un Servicio (“Acuerdos Adicionales"). Si existe algún conflicto o inconsistencia entre los Términos y un Acuerdo adicional, el Acuerdo adicional tendrá precedencia sobre las condiciones sólo en relación con el Servicio de que se trate.
                            </li>
                            <li>Las Condiciones no podrán ser modificados de otra forma, excepto por escrito por un oficial autorizado de Oleb2b.com.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong> Prestación de Servicios</strong>
                        <ol>
                            <li>La entidad contratante Oleb2b.com que se contrata con Perú es Oleb2b.com si usted es un miembro registrado del sitio y registrada o residentes en América Latina y Rusia. Y que usted está de acuerdo que puede facturar por su parte de los Servicios..
                            </li>
                            <li>Debe registrarse como miembro en el Sitio con el fin de acceder y utilizar algunos servicios. Además, Oleb2b.com se reserva el derecho, sin previo aviso, para restringir el acceso o la utilización de ciertos servicios (o funciones dentro de los servicios) para el pago de los usuarios o sujetos a otras condiciones que Oleb2b.com puede imponer en nuestra discreción.

                            </li>
                            <li>Servicios (o cualquier función dentro de los Servicios) pueden variar para las diferentes regiones y países. Ninguna garantía de representación que se da un servicio en particular o característica o función de la misma o el mismo tipo y la extensión del servicio o de las características y funciones de los mismos estarán disponibles para los usuarios. Oleb2b.com podrá, a su discreción, limitar el único, negar o crear diferentes niveles de acceso y la utilización de cualquiera de los Servicios (o cualquier función dentro de los Servicios) con respecto a los diferentes usuarios.
                            </li>
                            <li>Oleb2b.com puede poner en marcha, cambiar, actualizar, imponer condiciones a, suspender o dejar de ofrecer cualquiera de los Servicios (o cualquier función dentro de los Servicios) sin previo aviso, excepto que, en el caso de un servicio de pago, tales cambios afecten negativamente a los usuarios de pago en el uso de ese servicio.
                            </li>
                            <li> Algunos servicios pueden ser proporcionados por filiales de Oleb2b.com en nombre de Oleb2b.com.

                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong> Los usuarios generales</strong>
                        <ol>
                            <li>Como condición para el acceso y uso de los Sitios o Servicios, usted acepta que cumplirá con todas las leyes y reglamentos al usar los Sitios o Servicios correspondientes.</li>
                            <li>Usted se compromete a utilizar el Sitio o los Servicios únicamente para sus propios fines privados e

                                internos. El usuario acepta que (a) no copiar, reproducir, descargar, volver a publicar, vender, distribuir

                                o revender cualquier Servicio o cualquier información, texto, imágenes, gráficos, clips de vídeo, sonido,

                                directorios, archivos, bases de datos o listados, etc. disponibles en o a través del sitio (el &quot;contenido del

                                sitio&quot;), y (b) no copiar, reproducir, descargar, compilar o utilizar cualquier contenido del sitio para el

                                propósito de operar un negocio que compite con Oleb2b.com, o de otro modo la explotación comercial

                                de los contenidos del sitio. La recuperación sistemática de contenido del sitio de los sitios para crear o

                                compilar, directa o indirectamente, una colección, compilación, base de datos o directorio (ya sea a

                                través de robots, arañas, dispositivos automáticos o procesos manuales) sin el permiso escrito de

                                Oleb2b.com está prohibido. Está prohibido el uso de cualquier contenido o materiales en el Sitio para

                                cualquier propósito que no esté expresamente permitido en los Términos.
                            </li>
                            <li>Debe leer la Política de Privacidad de Oleb2b.com que regula la protección y uso de la información

                                personal sobre los usuarios en posesión de Oleb2b.com y nuestros afiliados. Usted acepta los términos

                                de la Política de Privacidad y acepta que el uso de la información personal sobre usted de acuerdo con la

                                política de privacidad.
                            </li>
                            <li>Oleb2b.com puede permitir a los usuarios que tengan acceso a contenidos, productos o servicios

                                ofrecidos por terceros a través de hipervínculos (en forma de enlaces de texto, banners, canales o de

                                otro tipo), API o de otra manera a los sitios web de dichos terceros. Se le advierte de leer los términos y

                                condiciones de dichos sitios web &#39;y / o políticas de privacidad antes de usar el sitio. Usted reconoce que

                                Oleb2b.com no tiene control sobre los sitios web de dichos terceros, no controla dichos sitios web y no

                                será responsable ni estará obligado a cualquiera de dichos sitios web, o cualquier contenido, productos

                                o servicios puestos a disposición en dicha página web sitios.
                            </li>
                            <li>Usted se compromete a no llevar a cabo ninguna acción para socavar la integridad de los sistemas

                                informáticos o redes de Oleb2b.com y / o cualquier otro usuario ni obtener acceso no autorizado a

                                dichos sistemas informáticos o redes.
                            </li>
                            <li>Usted se compromete a no llevar a cabo ninguna acción que pudiera afectar a la integridad del

                                sistema de regeneración de Oleb2b.com, como dejar retroalimentación positiva por sí mismo el uso de

                                identificadores secundarios miembros o a través de terceros o por dejar la regeneración negativa sin

                                fundamento para otro usuario.
                            </li>
                            <li>Por publicar o mostrar cualquier información, contenido o material ( &quot;Contenido de Usuario&quot;) en los

                                Sitios o cualquier tipo de Contenido de Usuario a Oleb2b.com o nuestro representante (s), otorga una

                                licencia irrevocable, perpetua, mundial, libre de regalías, y sub licenciable (a través de múltiples niveles)

                                licencia para Oleb2b.com para mostrar, transmitir, distribuir, reproducir, publicar, reproducir, adaptar,

                                modificar, traducir, crear trabajos derivados, y ningún otro uso o la totalidad del contenido de los

                                usuarios en cualquier forma, medio o tecnología ahora conocidos o no conocidos actualmente, en

                                cualquier forma y para cualquier propósito que puede ser beneficioso para el funcionamiento del sitio,

                                la prestación de cualquiera de los Servicios y / o el negocio del usuario. Usted confirma y garantiza a

                                Oleb2b.com que tiene todos los derechos, poderes y autoridad necesarios para conceder la licencia

                                anteriormente mencionada.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong> Cuenta de Usuario</strong>
                        <ol>
                            <li>El usuario debe estar registrado en el sitio para acceder o utilizar algunos servicios (para miembros

                                registrados también se conoce como un &quot;miembro&quot; más adelante). Excepto con la aprobación de

                                Oleb2b.com, un usuario sólo podrá registrar una cuenta de usuario en el sitio. Oleb2b.com puede

                                cancelar o terminar la cuenta de miembro de un usuario si Oleb2b.com tiene razones para sospechar

                                que el usuario se ha registrado o controlada de dos o más cuentas de usuario al mismo tiempo. Además,

                                Oleb2b.com puede rechazar la solicitud del usuario para su registro por cualquier motivo.
                            </li>
                            <li>Una vez registrado en el Sitio, Oleb2b.com deberá asignar una cuenta y emitir un ID de usuario y

                                contraseña (estos últimos serán elegidos miembros registrados durante el registro) para cada usuario

                                registrado. Una cuenta puede tener una cuenta de correo electrónico basado en web con espacio de

                                almacenamiento limitado para el Miembro de enviar o recibir mensajes de correo electrónico.
                            </li>
                            <li>Un conjunto de ID de usuario y contraseña es única para una sola cuenta. Cada miembro será el

                                único responsable de mantener la confidencialidad y seguridad de su ID de usuario y contraseña y de

                                todas las actividades que ocurran bajo su cuenta. Ningún miembro puede compartir, ceder o permitir el

                                uso de su cuenta de miembro, la identificación o contraseña de otra persona fuera de la propia entidad

                                empresarial del miembro. Miembro acuerda notificar Oleb2b.com inmediatamente si tiene

                                conocimiento de cualquier uso no autorizado de su contraseña o su cuenta o cualquier otra violación de

                                la seguridad de su cuenta.
                            </li>
                            <li>El Usuario acepta que todas las actividades que ocurran bajo su cuenta (incluyendo, sin limitación, la

                                publicación de cualquier información de la compañía o producto, haciendo clic para aceptar el acuerdo o

                                reglas adicionales, la suscripción o hacer cualquier pago por los servicios, el envío de mensajes de correo

                                electrónico utilizando la cuenta de correo electrónico) se considerará que ha sido autorizada por el

                                miembro.
                            </li>
                            <li>Los miembros reconocen que el uso compartido de su cuenta con otras personas, o permitir que

                                múltiples usuarios fuera de su entidad de negocio a utilizar su cuenta (colectivamente, &quot;uso múltiple&quot;),

                                puede causar un daño irreparable a Oleb2b.com u otros usuarios del sitio. Miembro indemnizará

                                Oleb2b.com, nuestros afiliados, directores, empleados, agentes y representantes contra cualquier

                                pérdida o daño (incluyendo pero no limitado a la pérdida de beneficios) sufridos como consecuencia de

                                la utilización múltiple de su cuenta. Miembros también de acuerdo en que, en el caso de la utilización

                                múltiple de su cuenta o el fracaso de un miembro para mantener la seguridad de su cuenta, Oleb2b.com

                                no será responsable por cualquier pérdida o daños derivados de tal infracción y tendrá el derecho de

                                suspender o terminar la cuenta del socio sin responsabilidad para miembros.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Responsabilidades de los Miembros</strong>
                        <ol>
                            <li>Cada miembro representa, garantiza y acepta que (a) usted tiene pleno poder y autoridad para

                                aceptar los términos de la concesión de la licencia y la autorización y para llevar a cabo las obligaciones a

                                continuación; (B) utilizar el Sitio y los Servicios con fines comerciales únicamente; y (c) la dirección que

                                usted proporciona cuando se registra es el centro de actividad principal de la entidad. A los efectos de

                                esta disposición, una rama o una oficina de enlace no serán consideradas una entidad separada y su

                                centro de actividad principal se considerará que la de su oficina central.
                            </li>
                            <li>Miembro estará obligado a proporcionar información o material acerca de su entidad, empresa o sus

                                productos / servicios como parte del proceso de registro en el Sitio o su uso de cualquier servicio o de la

                                cuenta de miembro. Cada miembro representa, garantiza y acepta que (a) dicha información y material

                                sea que se presenten durante el proceso de registro o posteriormente a lo largo de la continuación del

                                uso del Sitio o Servicio es verdadera, precisa, actualizada y completa, y (b) se mantendrá y rápidamente

                                modificar toda la información y materiales para mantenerla verdadera, precisa, actualizada y completa.</li>
                            <li>Al convertirse en miembro, usted autoriza la inclusión de la información de contacto sobre usted en

                                nuestra base de datos del comprador y autorizar Oleb2b.com y nuestros afiliados para compartir la

                                información de contacto con otros Usuarios o de otra manera utilizar su información personal de

                                acuerdo con la política de privacidad.
                            </li>
                            <li>Cada miembro representa, garantiza y acepta que (a) usted será el único responsable de obtener

                                todas las licencias necesarias y permisos de terceros con respecto a cualquier Contenido de Usuario que

                                usted envíe, publique o muestre; (B) cualquier Contenido de Usuario que usted envíe, publique o

                                muestre no infringe o viola cualquiera de los derechos de autor, patentes, marcas comerciales, nombres

                                comerciales, secretos comerciales o cualquier otro derecho personal o de propiedad de terceros y (c)

                                que tiene la derecho y autoridad para vender, intercambiar, distribuir o de exportación u ofrecer a la

                                venta, canje, distribuir o exportar los productos o servicios descritos en el contenido de usuario y dicha

                                venta, comercio, distribución o exportación u oferta no viola ningún derecho de terceros.
                            </li>
                            <li>Los usuarios de OleB2B.com se comprometen a ingresar información sobre su empresa y productos siguiendo las siguientes normas:
                                <ol>
                                    <li>sea verdadera, precisa, completa y legal;
                                    </li>
                                    <li>no sea falsa, equívoca o engañosa;
                                    </li>
                                    <li>no contener información que sea difamatorio, calumnioso, amenazante o de acoso, obsceno,

                                        ofensivo, sexualmente explícito o perjudicial para los menores;
                                    </li>
                                    <li>no contiene información que es discriminatorio o promueva la discriminación basada en la raza, sexo,

                                        religión, nacionalidad, discapacidad, orientación sexual o edad;
                                    </li>
                                    <li>no violar la Política del listado, otras condiciones o en cualquier acuerdo adicionales aplicables
                                    </li>
                                    <li>no violar las leyes y regulaciones (incluyendo, sin limitación, las que rigen el control de las

                                        exportaciones, la protección del consumidor, la competencia desleal, o la publicidad engañosa

                                        aplicables) o promover cualquier actividad que pueda violar las leyes y reglamentos aplicables;</li>
                                    <li>no contiene ningún enlace directa o indirectamente a cualquier otro sitio web que incluye cualquier

                                        contenido que pueda violar los Términos.
                                    </li>
                                </ol>
                            </li>
                            <li>Cada miembro representa además, garantiza y acepta que deberá:
                                <ol>
                                    <li>llevar a cabo sus actividades en el Sitio de conformidad con las leyes y reglamentos aplicables;
                                    </li>
                                    <li>llevar a cabo sus transacciones comerciales con otros usuarios del Sitio de buena fe;</li>
                                    <li>llevar a cabo sus actividades de conformidad con los Términos y cualesquiera acuerdos adicionales

                                        aplicables;</li>
                                    <li>No utilizar los Servicios o del sitio para defraudar a cualquier persona o entidad (incluyendo sin

                                        limitación la venta de artículos robados, uso de tarjetas de crédito / débito robadas);</li>
                                    <li>no hacerse pasar por otra persona o entidad, falsificar sí mismo o su afiliación con cualquier persona

                                        o entidad;</li>
                                    <li>no violar las leyes y regulaciones (incluyendo, sin limitación, las que rigen el control de las

                                        exportaciones, la protección del consumidor, la competencia desleal, o la publicidad engañosa

                                        aplicables) o promover cualquier actividad que pueda violar las leyes y reglamentos aplicables;</li>
                                    <li>No realizar spam o phishing;</li>
                                    <li>No se involucre en cualquier otra actividad ilegal (incluyendo, sin limitación, aquellas que pudieran

                                        constituir un delito penal, etc.) o alentar o instigar cualquier actividad ilegal;
                                    </li>
                                    <li>no implica intentos de copiar, reproducir, explotar o expropiar varios propietarios directorios, bases

                                        de datos y listados de Oleb2b.com;
                                    </li>
                                    <li>no implica ningún virus informático u otros dispositivos y códigos destructivos que tienen el efecto de

                                        dañar, interferir, interceptar o expropiar cualquier sistema de software o hardware, datos o información

                                        personal;
                                    </li>
                                    <li>no implica ningún plan para socavar la integridad de los datos, sistemas o redes utilizadas por

                                        Oleb2b.com y / o de cualquier usuario de los Sitios o de obtener acceso no autorizado a dichos datos,

                                        sistemas o redes;
                                    </li>
                                    <li>no participó en ninguna actividad que de otro modo crear cualquier responsabilidad Oleb2b.com o

                                        nuestros afiliados.</li>
                                </ol>

                            </li>
                            <li>miembros no pueden utilizar los servicios de cuenta de miembro para participar en actividades que

                                son idénticos o similares a los negocios del mercado de comercio electrónico de Oleb2b.com.
                            </li>
                            <li>Si Miembro proporciona un árbitro de negocios, miembros representa, garantiza y acepta que ha

                                obtenido todas las autorizaciones necesarias, aprobaciones y dispensas de sus socios de negocios y

                                asociados para (a) actuar como el árbitro de negocios; (B) publicar y publicar sus datos de contacto e

                                información, cartas de referencia y los comentarios en su nombre; y (c) que terceras personas puedan

                                ponerse en contacto con este tipo de árbitros de negocio para respaldar las afirmaciones o

                                declaraciones hechas por ti. Asimismo, garantiza y acepta que todas las cartas de referencia y los

                                comentarios son verdadera y exacta y terceros puede comunicarse con los árbitros de negocio sin la

                                necesidad de obtener su consentimiento.
                            </li>
                            <li>Miembro se compromete a proporcionar toda la información necesaria, los materiales y la

                                aprobación, y prestar toda la asistencia razonable y la cooperación necesaria para la prestación de los

                                Servicios de Oleb2b.com, evaluar si miembro ha incumplido las condiciones y / o manejo de cualquier

                                queja contra el miembro. Si el fracaso de miembros a hacerlo da lugar a retraso en, o suspensión o de

                                terminación de la prestación de cualquier servicio, Alibaba.com no estará obligado a extender el período

                                de servicio pertinente ni será responsable por cualquier pérdida o daños derivados de la demora,

                                suspensión o terminación.
                            </li>
                            <li>Miembro reconoce y acepta que Oleb2b.com no estará obligado a vigilar activamente ni ejercer

                                ningún control editorial alguno sobre el contenido de cualquier mensaje o material o información

                                creada, obtenida o accesibles mediante los servicios o sitios. Oleb2b.com no respalda, verificar o no

                                certificar el contenido de cualquier comentario u otro material o información formuladas por otros

                                Miembros. Cada usuario es el único responsable por el contenido de sus comunicaciones y puede ser

                                considerada legalmente responsable o responsables por el contenido de sus comentarios u otro

                                material o información
                            </li>
                            <li>Miembro reconoce y acepta que los Servicios sólo puede ser utilizado por las empresas y sus

                                representantes para el uso comercial y no para los consumidores individuales o para uso personal.
                            </li>
                            <li>Miembro reconoce y acepta que cada usuario es el único responsable del cumplimiento de las leyes

                                y reglamentos aplicables en sus respectivas jurisdicciones para asegurar que todo uso del sitio y los

                                servicios están en conformidad con la misma.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong> El incumplimiento de los Miembros</strong>
                        <ol>
                            <li>Oleb2b.com se reserva el derecho a nuestra entera discreción, de eliminar, modificar o rechazar

                                cualquier Contenido de Usuario que usted envíe a, publique o muestre en el sitio que creemos

                                razonablemente que es ilegal, viola los términos, podría estar sujeto a responsabilidad Oleb2b.com, o se

                                encuentra de otro modo inadecuado en opinión de Oleb2b.com.
                            </li>
                            <li>Si cualquier miembro infringe cualquiera de los Términos o si Oleb2b.com tiene motivos razonables

                                para creer que un Miembro ha incumplido alguna de las Condiciones, Oleb2b.com tendrá el derecho de

                                imponer una sanción contra el miembro, o suspender o cancelar la cuenta del miembro o suscripción de

                                cualquier servicio sin responsabilidad alguna para el miembro. Oleb2b.com también tendrá el derecho

                                de restringir, rechazar o prohibir todo uso actual o futuro de cualquier otro servicio que pueda ser

                                proporcionada por Oleb2b.com. Las sanciones que se pueden imponer Oleb2b.com incluyen, entre

                                otros, de advertencia, la eliminación de cualquier listado de productos u otro contenido de usuario que

                                el miembro haya presentado, publique o muestre, la imposición de restricciones sobre el número de

                                fichas de producto que el miembro puede enviar o pantalla, o la imposición de restricciones en el uso

                                del miembro de ninguna función o funciones de cualquier servicio por el período que Oleb2b.com

                                considere adecuada, en nuestra única discreción.
                            </li>
                            <li>Sin limitar la generalidad de lo dispuesto en las Condiciones, un miembro serían considerados como

                                miembros de incumplimiento de las condiciones en cualquiera de las siguientes circunstancias:
                                <ol>
                                    <li>tras la queja o reclamación de terceros, Oleb2b.com tiene motivos razonables para creer que tales

                                        miembros tiene intencionadamente o materialmente no pudo realizar su contrato con dicho tercero

                                        incluyendo, sin limitaciones, en el que el miembro ha de entregar ningún artículo ordenados por tales

                                        terceros después de la recepción del precio de compra, o cuando los productos miembro ha entregado

                                        materialmente no cumplen con los términos y descripciones esbozadas en su contrato con dicho

                                        tercero,
                                    </li>
                                    <li>Oleb2b.com tiene motivos razonables para sospechar que dicha Miembro ha utilizado una tarjeta de

                                        crédito robada u otra información falsa o engañosa en cualquier transacción con una contraparte,
                                    </li>
                                    <li>Oleb2b.com tiene motivos razonables para sospechar que cualquier información proporcionada por el

                                        miembro no está actualizada o completa, o es falsa, inexacta o engañosa, o
                                    </li>
                                    <li>Oleb2b.com cree que las acciones del miembro puedan provocar una pérdida financiera o

                                        responsabilidad legal para Oleb2b.com o nuestros afiliados o de otros usuarios.
                                    </li>
                                </ol>
                            </li>
                            <li>Oleb2b.com se reserva el derecho de cooperar plenamente con las autoridades gubernamentales,

                                investigadores privados y / o terceros dañados en la investigación de cualquier presuntas irregularidades

                                penales o civiles. Además, Oleb2b.com puede revelar información sobre la identidad y el contacto del

                                miembro, a petición de un organismo gubernamental o policial, un tercero perjudicado, o como

                                resultado de una citación u otra acción legal. Oleb2b.com no será responsable de los daños o resultados

                                derivados de dicha divulgación, y Miembro se compromete a no llevar cualquier acción o reclamación

                                contra Oleb2b.com para dicha divulgación.
                            </li>
                            <li>Oleb2b.com podrá, en cualquier momento y según nuestro criterio razonable, imponer límites a,

                                suspender o terminar el uso del miembro de cualquier Servicio o el Sitio sin ser responsable al Miembro

                                si Oleb2b.com ha recibido una notificación de que el Miembro ha incumplido de cualquier acuerdo o

                                compromiso con cualquier filial de Oleb2b.com y dicho incumplimiento implica o se sospecha razonable

                                de la participación de las actividades deshonestas o fraudulentas. Oleb2b.com tendrá el derecho de

                                publicar los registros de tal incumplimiento en el Sitio. No se requerirá Oleb2b.com de investigar dicho

                                incumplimiento o solicitud de confirmación por parte del miembro.
                            </li>
                            <li>Cada Miembro se compromete a indemnizar Oleb2b.com, nuestros afiliados, directores, empleados,

                                agentes y representantes y para no causarles daño, de cualquier y todos los daños, pérdidas,

                                reclamaciones y pasivos (incluyendo los costos legales en una base de plena indemnización) que puedan

                                surgir desde su presentación, publicación o exhibición de cualquier contenido del usuario, de su uso de

                                los Sitios o Servicios, o de su incumplimiento de las condiciones.
                            </li>
                            <li>Cada miembro se compromete, además, que Oleb2b.com no es responsable y no tendrá ninguna

                                responsabilidad hacia usted o cualquier otra persona por cualquier Contenido de Usuario u otro material

                                transmitido a través del Sitio, incluyendo el material fraudulenta, falsa, engañosa, inexacta, difamatoria,

                                ofensiva o ilícita y que el riesgo de daños por tal material es responsabilidad exclusiva de cada miembro.

                                Oleb2b.com se reserva el derecho, a su propio costo, de asumir la defensa exclusiva y control de

                                cualquier asunto sujeto a indemnización por parte del miembro, en cuyo caso el miembro cooperará con

                                Oleb2b.com para hacer valer cualquier defensa disponible.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Las transacciones entre compradores y vendedores</strong>
                        <ol>
                            <li>A través del Sitio, Oleb2b.com proporciona plataformas basadas en web electrónicos para el

                                intercambio de información entre compradores y vendedores de productos y servicios. Sin embargo,

                                para cualquiera de los Servicios, Oleb2b.com no representa el vendedor o el comprador en las

                                transacciones específicas. Oleb2b.com no controla y no es responsable ni responsable de la calidad,

                                seguridad, legalidad o disponibilidad de los productos o servicios que se ofrecen a la venta en el Sitio o

                                la capacidad de los vendedores para completar una venta o la capacidad de los compradores para

                                completar una compra.
                            </li>
                            <li>Los usuarios quedan informados de que puede haber riesgos de tratar con personas que actúan bajo

                                falsas pretensiones. Oleb2b.com utiliza varias técnicas para verificar la exactitud de cierta información a

                                nuestros usuarios de pago nos proporcionan cuando se registran para un servicio de pago de la

                                membresía en el sitio. Sin embargo, debido a la verificación del usuario en Internet es difícil,

                                Oleb2b.com no puede y no confirma la identidad pretendida de cada usuario (incluyendo, sin limitación,

                                los miembros de pago). Nosotros sugerimos que utilice diversos medios, así como el sentido común,

                                para evaluar la integridad de los posibles socios comerciales.</li>
                            <li>Cada Usuario reconoce que está asumiendo plenamente los riesgos de realizar cualquier transacción

                                de compra y venta en relación con el uso del Sitio o los Servicios, y que está asumiendo plenamente los

                                riesgos de responsabilidad civil o daños de ningún tipo en relación con la actividad posterior de

                                cualquier tipo relativa a los productos o servicios que son objeto de transacciones utilizando el sitio.

                                Estos riesgos deben incluir, pero no se limitan a, la falsificación de productos y servicios, maniobras

                                fraudulentas, calidad insatisfactoria, incumplimiento de las especificaciones, productos defectuosos o

                                peligrosos, productos ilegales, retraso o incumplimiento en la entrega o pago, errores de cálculo de

                                costos, incumplimiento de garantía, incumplimiento de contrato y los accidentes de transporte. Estos

                                riesgos incluyen los riesgos que la fabricación, importación, exportación, distribución, oferta, exhibición,

                                compra, venta y / o uso de productos o servicios ofrecidos o mostrados en el Sitio pueden violar o

                                pueden hacerse valer para violar los derechos de terceros, y el riesgo de que usuario puede incurrir en

                                costes de defensa u otros costes en relación con la afirmación de los Derechos de terceros de terceros, o

                                en relación con cualquier reclamación por cualquiera de las partes que tienen derecho a la defensa o

                                indemnización en relación con las afirmaciones de los derechos, demandas o reclamaciones de los

                                demandantes derechos de terceros. Estos riesgos incluyen los riesgos que los consumidores, otros

                                compradores, los usuarios finales de los productos u otras personas que afirman haber sufrido lesiones

                                o daños relacionados con los productos obtenidos originalmente por los usuarios de los Sitios como

                                consecuencia de las transacciones de compra y venta en relación con el uso de los Sitios pueden sufrir

                                daños y / o hacer valer las reclamaciones derivadas de su uso de este tipo de productos. Todos los

                                riesgos antes mencionados se denominan en lo sucesivo como &quot;Riesgos de transacción&quot;. Cada Usuario

                                acepta que Oleb2b.com no será responsable o responsables de los daños, reclamaciones,

                                responsabilidades, costos, daños, inconvenientes, las interrupciones del negocio o gastos de cualquier

                                tipo que pueda surgir a consecuencia de o en relación con cualquier riesgo de transacción.
                            </li>
                            <li>El usuario es el único responsable de todos los términos y condiciones de las transacciones llevadas a

                                cabo en, a través o como resultado del uso del Sitio o los Servicios, incluyendo, sin limitación, los

                                términos respecto al pago, devoluciones, garantías, envío, seguros, honorarios, impuestos, título,

                                licencias, multas, permisos, manejo, transporte y almacenamiento.
                            </li>
                            <li>Usuario se compromete a proporcionar toda la información y los materiales que pueda ser

                                razonablemente requerido por Oleb2b.com en relación con sus transacciones llevadas a cabo en, a

                                través o como resultado del uso del Sitio o los Servicios. Oleb2b.com tiene el derecho de suspender o

                                terminar la cuenta de cualquier usuario si el usuario no proporciona la información y el material

                                requerido.
                            </li>
                            <li>En el caso de que cualquier usuario tiene un conflicto con cualquier parte de una transacción, dicho

                                Usuario se compromete a liberar e indemnizar Oleb2b.com (y nuestros agentes, afiliados, directores,

                                funcionarios y empleados) de todas las reclamaciones, demandas, acciones, procedimientos, costos,

                                gastos y daños (incluyendo, sin limitación, cualquier daño real, especiales, incidentales o consecuentes)

                                que surja de o en conexión con dicha transacción.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Limitación de responsabilidad</strong>
                        <ol>
                            <li>EN LA MEDIDA MÁXIMA PERMITIDA POR LA LEY, LOS SERVICIOS PRESTADOS POR OLEB2B.COM A

                                TRAVÉS DEL SITIO SE OFRECEN &quot;TAL CUAL&quot;, &quot;SEGÚN DISPONIBILIDAD&quot; Y &quot;CON TODAS LAS FALLAS&quot;, Y

                                OLEB2B.COM RENUNCIA EXPRESAMENTE A TODAS LAS GARANTÍAS, expresa o implícita, incluyendo,

                                pero no limitado a, cualquier garantía de la condición, calidad, durabilidad, rendimiento, precisión,

                                fiabilidad, COMERCIALIZACIÓN O IDONEIDAD PARA UN FIN DETERMINADO. Tales garantías,

                                REPRESENTACIONES, CONDICIONES, y las empresas estén quedan excluidas.
                            </li>
                            <li>EN LA MEDIDA MÁXIMA PERMITIDA POR LA LEY, OLEB2B.COM no hace representaciones o garantías

                                acerca de la validez, precisión, exactitud, confiabilidad, calidad, estabilidad, integridad o actualidad de

                                cualquier información proporcionada en OA TRAVÉS DE LOS SITIOS; OLEB2B.COM NO representa ni

                                garantiza que la fabricación, importación, exportación, distribución, OFERTA, DISPLAY, compra, venta y /

                                o uso de productos o servicios ofrecidos o mostrados en el Sitio no viola LOS DERECHOS DE TERCEROS; Y

                                OLEB2B.COM NO REPRESENTA NI GARANTÍAS DE CUALQUIER TIPO RESPECTO A cualquier producto o

                                servicio ofrecido o mostrada en el sitio.
                            </li>
                            <li>Cualquier material descargado u obtenido a través del Sitio se realiza a discreción exclusiva de cada

                                usuario y el riesgo y cada usuario es el único responsable de cualquier daño al sistema informático del

                                Oleb2b.com o pérdida de datos que pueda resultar de la descarga de dicho material. Ningún consejo o

                                información, ya sea oral o escrita, obtenida por cualquier usuario desde Oleb2b.com o a través o desde

                                el sitio creará ninguna garantía que no estén expresamente en el presente documento.
                            </li>
                            <li>El Sitio puede poner a disposición de los usuarios a los servicios o productos ofrecidos por terceros

                                independientes. No hay garantía alguna ni se hace en relación con tales servicios o productos. En ningún

                                caso Oleb2b.com y nuestros afiliados ser declarados responsables de cualquier tipo de servicios o

                                productos.
                            </li>
                            <li>Cada usuario se compromete a indemnizar y Oleb2b.com, nuestros afiliados, directores, funcionarios

                                y empleados, de cualquier y todas las pérdidas, reclamaciones, obligaciones (incluyendo costos legales

                                en una base de plena indemnización) que puedan surgir del uso de dicho Usuario de sitio o los Servicios

                                (incluyendo, pero no limitado a la visualización de información del usuario en el sitio) o de su

                                incumplimiento de cualquiera de los términos y condiciones de los Términos. Cada usuario de este

                                medio también acepta indemnizar y Oleb2b.com, nuestros afiliados, directores, funcionarios y

                                empleados, de cualquier y todas las pérdidas, daños, reclamaciones, obligaciones (incluyendo costos

                                legales en una base de plena indemnización) que puedan surgir del incumplimiento por parte del

                                usuario de cualquier tipo de garantía realizadas por el usuario a Oleb2b.com, incluyendo, pero no

                                limitado a los establecidos en la Sección 5 a continuación.
                            </li>
                            <li>Cada Usuario se compromete además a indemnizar y Oleb2b.com, nuestros afiliados, directores,

                                funcionarios y empleados, de cualquier y todas las pérdidas, daños, reclamaciones, responsabilidades

                                (incluyendo los costos legales en una base de plena indemnización) que puedan surgir, directa o

                                indirectamente, como resultado de las reclamaciones formuladas por los reclamantes derechos de

                                terceros o a otros terceros relativas a los productos ofrecidos o mostrados en el sitio. Cada usuario está

                                de acuerdo en la presente, además, que Oleb2b.com no es responsable y no tendrá ninguna

                                responsabilidad hacia usted, para cualquier material publicado por otros, incluyendo el material

                                difamatorio, ofensivo o ilícito y que el riesgo de daños de este tipo de material es responsabilidad

                                exclusiva de cada usuario. Oleb2b.com se reserva el derecho, a su propio costo, de asumir la defensa

                                exclusiva y control de cualquier asunto sujeto a indemnización por su parte, en cuyo caso usted deberá

                                cooperar con Oleb2b.com para hacer valer cualquier defensa disponible.</li>
                            <li>Oleb2b.com no será responsable de ningún daño especial, directo, indirecto, punitivo, incidental o

                                consecuente o cualquier daño (incluyendo, pero no limitado a daños por pérdida de beneficios o

                                ahorros, interrupción de negocios, pérdida de información), ya sea en contrato, negligencia, agravio,

                                equidad u otro, o cualquier otro daño resultante de cualquiera de los siguientes.
                                <ol>
                                    <li>el uso o la imposibilidad de uso del Sitio o los Servicios;</li>
                                    <li>cualquier defecto en los productos, muestras, datos, información o servicios adquiridos u obtenidos

                                        de un usuario o cualquier otro tercero a través del Sitio;</li>
                                    <li>violación de derechos de terceros o a las reclamaciones o demandas que del usuario fabricación,

                                        importación, exportación, distribución, oferta, exhibición, compra, venta y / o uso de productos o

                                        servicios ofrecidos o mostrados en el Sitio pueden violar o pueden hacerse valer para violar Derechos de

                                        terceros; o reclamos de cualquiera de las partes que tienen derecho a la defensa o indemnización en

                                        relación con las afirmaciones de los derechos, demandas o reclamaciones de los demandantes los

                                        derechos de terceros;
                                    </li>
                                    <li> el acceso no autorizado por parte de terceros a los datos o la información privada de cualquier

                                        usuario;
                                    </li>
                                    <li>declaraciones o conducta de cualquier Usuario del Sitio; o;</li>
                                    <li>cualquier asunto relativo a servicios que pudiera surgir, incluida la negligencia.</li>
                                </ol>
                            </li>
                            <li>Sin perjuicio de las disposiciones anteriores, la responsabilidad total de Oleb2b.com, nuestros

                                empleados, agentes, afiliados, representantes o cualquier persona que actúe en nuestro nombre en

                                relación con cada usuario para todas las reclamaciones derivadas del uso del Sitio o los Servicios en

                                cualquier calendario año se limitará a la cantidad mayor de (a) el importe de los honorarios que el

                                usuario haya pagado a Oleb2b.com o nuestros afiliados durante el año calendario y (b) la cantidad

                                máxima permitida por la ley aplicable. La frase anterior no impedirá la exigencia por parte del Usuario

                                de demostrar daños reales. Todas las reclamaciones derivadas del uso de los Sitios o Servicios deberán

                                ser presentadas dentro de un (1) año desde la fecha en que la causa de la acción o cualquier otro plazo

                                más largo según lo prescrito en virtud de cualquier ley aplicable para este término del uso.
                            </li>
                            <li>Las limitaciones y exclusiones de responsabilidad al que en virtud de las cláusulas no se aplicarán en

                                la medida máxima permitida por la ley y se aplicarán independientemente de que Oleb2b.com ha sido

                                informado o debiera haber tenido conocimiento de la posibilidad de que se produzcan tales pérdidas.</li>
                        </ol>
                    </li>
                    <li>
                        <strong> Fuerza Mayor</strong>
                        <ol>
                            <li>En ningún caso se Oleb2b.com ser declarados responsables de cualquier retraso o fallo o

                                interrupción de los contenidos o servicios entregados a través de los Sitios que resulte directa o

                                indirectamente de actos de la naturaleza, las fuerzas o causas fuera de nuestro control, incluyendo, sin

                                limitación, fallos de Internet , informática, telecomunicaciones o cualquier otro equipos, fallas de

                                suministro eléctrico, huelgas, conflictos laborales, disturbios, insurrecciones, disturbios civiles, escasez

                                de mano de obra o materiales, incendios, inundaciones, tormentas, explosiones, actos de Dios, guerra,

                                acciones gubernamentales, órdenes de los órganos jurisdiccionales nacionales o extranjeras o

                                incumplimiento de terceros.</li>
                        </ol>
                    </li>
                    <li><strong>Derechos de Propiedad Intelectual</strong>
                        <ol>
                            <li>Oleb2b.com es el único propietario o licenciatario legítimo de todos los derechos e intereses en el

                                Sitio y el contenido del sitio. El contenido del sitio encarnan los secretos comerciales y otros derechos de

                                propiedad intelectual protegidos por derechos de autor en todo el mundo y otras leyes. Todos los

                                derechos de título, de propiedad y de propiedad intelectual en los sitios ya sus contenidos

                                permanecerán con Oleb2b.com, nuestros afiliados o licenciatarios del contenido del sitio, como sea el

                                caso. Todos los derechos no reclamados en contrario en las Condiciones o por Oleb2b.com quedan

                                reservados.
                            </li>
                            <li>&quot;OleB2B&quot;, &quot;OLEB2B.COM&quot;, y relacionadas iconos y logotipos son marcas comerciales registradas o

                                marcas de servicio de DRAGON YUAN SAC. La copia no autorizada, modificación, uso o publicación de

                                estas marcas está estrictamente prohibido.
                                <ol>
                                    <li>OleB2B.com es capaz de demostrar que la comunicación, ya sea en persona o de manera electrónica ha sido enviada al usuario en mencion.
                                    </li>
                                    <li>Apenas OleB2B.com postee la información en el portal siendo esta accesible para cualquier usuario sin cargo alguno.
                                    </li>
                                </ol>
                            </li>
                            <li>Oleb2b.com puede tener terceros independientes que participan en la prestación de los servicios

                                (por ejemplo, los proveedores de autenticación y verificación de servicios, servicios de diseño). Usted no

                                puede usar ninguna marca comercial, marca de servicio o logotipo de estas terceras partes

                                independientes sin la previa autorización por escrito de dichas partes.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Avisos</strong>
                        <ol>
                            <li>Todas las notificaciones o demandas legales a Oleb2b.com deberán ser hechas por escrito y

                                enviadas a Oleb2b.com personalmente, por mensajería o correo certificado a la siguiente entidad y

                                dirección: OleB2B.com S.A.C. en calle Francisco Graña 480 Lima 17 Perú de la manera mencionada

                                anteriormente.
                            </li>
                            <li>Todas las notificaciones legales o demandas o en el momento de un usuario serán efectivas si bien

                                entregado personalmente, enviado por mensajería, correo certificado, por fax o por correo electrónico a

                                este último conocido correspondencia fax o dirección de correo electrónico, que facilite el Usuario a

                                Oleb2b.com, o mediante la publicación de dicha notificación ni requerimiento al área del sitio que es

                                accesible al público sin costo alguno. Aviso a un usuario se considerará para ser recibida por dicho

                                Usuario, siempre y cuando.
                                <ol>
                                    <li>Oleb2b.com es capaz de demostrar que la comunicación, ya sea en forma física o electrónica, se ha

                                        enviado a dicho Usuario o
                                    </li>
                                    <li>inmediatamente después de la publicación de dicha Oleb2b.com aviso en un área del sitio que sea

                                        accesible al público gratuitamente.
                                    </li>
                                </ol>
                            </li>
                            <li>Usted acepta que todos los acuerdos, avisos, demandas, divulgaciones y otras comunicaciones que

                                Oleb2b.com envía electrónicamente satisfacen el requisito legal de que dicha comunicación debe ser

                                por escrito.

                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Disposiciones generales</strong>
                        <ol>
                            <li>Sin perjuicio de los acuerdos adicionales, las Condiciones constituyen el acuerdo completo entre

                                usted y Oleb2b.com con respecto a, y rigen el uso del Sitio y los Servicios, reemplaza cualquier acuerdo

                                anterior, oral o escrita en relación con el mismo tema aquí.</li>
                            <li>Oleb2b.com y que son contratistas independientes, y ninguna relación de agencia, asociación,

                                empresa conjunta, empleado-empleador o franquiciador-franquiciado se pretende ni se crea por los

                                Términos.</li>
                            <li>Si cualquier disposición de estas Condiciones se considera inválida o inejecutable, dicha disposición

                                será eliminada y las disposiciones restantes seguirán siendo válidas y ser forzada.</li>
                            <li>Los encabezados son para fines de referencia y de ninguna manera definen, limitan, interpretan o

                                describen el ámbito o alcance de dicha sección.

                                el fracaso de 12,5 Oleb2b.com para hacer valer cualquier derecho o falta de acción con respecto a

                                cualquier incumplimiento por su bajo las condiciones de uso no constituirá una renuncia a tal derecho ni

                                una renuncia al derecho de Oleb2b.com a actuar con respecto a incumplimientos posteriores o

                                similares.</li>
                            <li>Oleb2b.com tendrá derecho a ceder las Condiciones (incluyendo todos los derechos, títulos,

                                beneficios, intereses y obligaciones y funciones en los términos a cualquier persona o entidad

                                (incluyendo cualquier afiliado de Oleb2b.com). Usted no puede asignar, en su totalidad o en parte, las

                                condiciones a cualquier persona o entidad.</li>
                        </ol>
                    </li>
                </ol>
            </div>
        </div>

    </section>
@endsection