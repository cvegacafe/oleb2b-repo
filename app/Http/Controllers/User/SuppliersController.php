<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\Supplier\Supplier;
use App\Services\CertificationService;
use App\Services\CompanyTypeService;
use App\Services\CountryService;
use App\Services\FactorydetailService;
use App\Services\FileService;
use App\Services\LanguageService;
use App\Services\MeasureService;
use App\Services\MembershipService;
use App\Services\PayUService;
use App\Services\SupercategoryService;
use App\Services\SupplierService;
use App\Services\TaxTypeService;
use App\Services\UserService;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

/**
 * Class SuppliersController
 * @package App\Http\Controllers\User
 */
class SuppliersController extends Controller
{
    /**
     * @var SupplierService|null
     */
    private $supplierService = null;

    /**
     * SuppliersController constructor.
     * @param SupplierService $supplierService
     */
    public function __construct(SupplierService $supplierService)
    {
        $this->supplierService = $supplierService;
    }


    /**
     * @param Guard $guard
     * @param CountryService $countryService
     * @param TaxTypeService $taxTypeService
     * @param SupercategoryService $supercategoryService
     * @param MeasureService $measureService
     * @param CompanyTypeService $companyTypeService
     * @param MembershipService $membershipService
     * @param PayUService $payUService
     * @param LanguageService $languageService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(
        Guard $guard,
        CountryService $countryService,
        TaxTypeService $taxTypeService,
        SupercategoryService $supercategoryService,
        MeasureService $measureService,
        CompanyTypeService $companyTypeService,
        MembershipService $membershipService,
        PayUService $payUService,
        LanguageService $languageService
    ) {

        $countries = $countryService->getAllEnableForSuppliers();
        $taxtypes = $taxTypeService->all();
        $supercategories = $supercategoryService->all();
        $typesCompanies = $companyTypeService->all();
        $measurements = $measureService->all();
        $supplier = $guard->user()->supplier;
        $memberships = $membershipService->all();
        $advancedMembership = $memberships->find(2);
        $premiumMembership = $memberships->find(3);
        $payuParameters = $payUService->getParameters();
        $languages = $languageService->all();
        if (!$supplier) {
            return view('user.supplier.create',
                compact('countries', 'taxtypes', 'supercategories', 'typesCompanies', 'measurements',
                    'memberships', 'advancedMembership', 'premiumMembership', 'payuParameters', 'languages'));
        }

        return view('user.supplier.edit',
            compact('supplier', 'countries', 'taxtypes', 'supercategories', 'typesCompanies', 'measurements',
                'memberships', 'advancedMembership', 'premiumMembership', 'payuParameters', 'languages'));
    }

    /**
     * @param Request $request
     * @param SupplierService $supplierService
     * @param FactorydetailService $factorydetailService
     * @param CertificationService $certificationService
     * @param FileService $fileService
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $UserSupplierRequest
     */
    public function store(
        Request $request,
        SupplierService $supplierService,
        FactorydetailService $factorydetailService,
        CertificationService $certificationService,
        FileService $fileService
    ) {
        try {
            $validatorCompany = Validator::make($request->all(), [
                'suppliers_company_name' => 'required|max:250',
                'suppliers_company_description' => 'required|max:700',
                "suppliers_member_trade_organization" => "max:50",
                "suppliers_country_id" => "required|exists:countries,id",
                "suppliers_taxtype_id" => "required|exists:taxtypes,id",
                "suppliers_tax_number" => "required|max:30|unique:suppliers,tax_number",
                "suppliers_company_legal_address" => "required|max:255",
                "suppliers_postal_code" => "max:10",
                "suppliers_website" => "max:150",
                "suppliers_year_company_registered" => "numeric|max:9999",
                "suppliers_company_type" => "required|exists:companyTypes,id",
                "suppliers_number_employees" => "numeric|max:99999999",
                "suppliers_supercategory_id" => "required|exists:supercategories,id",

                /* Account Validation */
                "office_phone" => "required|max:25",
                "mobile_phone" => "required|max:25",
                /*"address" => "required|max:200",
                "city" => "required|max:60",*/
                "postal_code" => "required|max:20",
                "languages" => "required|existsArray:languages,id",
                "other_languages" => "max:100"
            ]);

            if ($validatorCompany->fails()) {
                return redirect()->route('company.profile.show', ['section' => 'details'])
                    ->withErrors($validatorCompany)->withInput();
            }

            $supplierInputs = [
                'company_name' => $request->get('suppliers_company_name'),
                'company_description' => $request->get('suppliers_company_description'),
                "member_trade_organization" => $request->get('suppliers_member_trade_organization'),
                "country_id" => $request->get('suppliers_country_id'),
                "taxtype_id" => $request->get('suppliers_taxtype_id'),
                "tax_number" => $request->get('suppliers_tax_number'),
                "company_legal_address" => $request->get('suppliers_company_legal_address'),
                "postal_code" => $request->get('postal_code'),
                "website" => $request->get('suppliers_website'),
                "year_company_registered" => $request->get('suppliers_year_company_registered'),
                "company_type" => $request->get('suppliers_company_type'),
                "number_employees" => $request->get('suppliers_number_employees', 0),
                "supercategory_id" => $request->get('suppliers_supercategory_id')
            ];

            /*$validatorUpdatedCertification = Validator::make($request->all(), [
                "type_certification" => "max:255",
                "issued_by" => "max:255",
                "start_date" => "date_format:Y-m-d",
                "expiration_date" => "date_format:Y-m-d",
                "image" => "file",
            ]);

           if ($validatorUpdatedCertification->fails()) {

               return redirect()->route('company.profile.show',['section'=>'certifications'])
                   ->withErrors($validatorUpdatedCertification)
                   ->withInput();
           }*/

            $validatorNewCertification = Validator::make($request->all(), [
                "new_certification_type_certification" => "max:255",
                "new_certification_issued_by" => "max:255",
                "new_certification_start_date" => "date_format:Y-m-d",
                "new_certification_expiration_date" => "date_format:Y-m-d"
            ]);

            if ($validatorNewCertification->fails()) {

                return redirect()->route('company.profile.show', ['section' => 'certifications'])
                    ->withErrors($validatorNewCertification)->withInput();
            }

            $newCertificationInputs = [
                "type_certification" => $request->get('new_certification_type_certification'),
                "issued_by" => $request->get('new_certification_issued_by'),
                "start_date" => $request->get('new_certification_start_date'),
                "expiration_date" => $request->get('new_certification_expiration_date'),
            ];

            $validatorFactory = Validator::make($request->all(), [
                "factoryDetails_factory_address" => "max:255",
                "factoryDetails_factory_size" => "numeric|max:999999999",
                "factoryDetails_number_qc_staff" => "numeric|max:999999999",
                "factoryDetails_number_rd_staff" => "numeric|max:9999999",
                "factoryDetails_number_production_lines" => "numeric|max:9999999",
                "factoryDetails_annual_output_value" => "numeric|max:999999999",
                /* "factoryDetails_measure_id" => "exists:measurements,id"*/
            ]);

            if ($validatorFactory->fails()) {
                return redirect()->route('company.profile.show', ['section' => 'factory'])
                    ->withErrors($validatorFactory)->withInput();
            }

            $factoryInputs = [
                "factory_address" => $request->get('factoryDetails_factory_address'),
                "factory_size" => $request->get('factoryDetails_factory_size'),
                "number_qc_staff" => $request->get('factoryDetails_number_qc_staff'),
                "number_rd_staff" => $request->get('factoryDetails_number_rd_staff'),
                "number_production_lines" => $request->get('factoryDetails_number_production_lines'),
                "annual_output_value" => $request->get('factoryDetails_annual_output_value'),
                "measure_id" => $request->get('factoryDetails_measure_id'),
            ];

            DB::beginTransaction();

            if ($request->hasFile('suppliers_image')) {
                $path = $fileService->storeSupplierLogo($request->file('suppliers_image'), Auth::id());
                $supplierInputs['logo'] = $path;
            }

            $supplier = $supplierService->create($supplierInputs);
            Auth::user()->suppliers()->attach($supplier->id, ['is_principal' => true]);

            $userData = [
                'office_phone' => $request->get('office_phone'),
                'mobile_phone' => $request->get('mobile_phone'),
                'address' => $request->get('suppliers_company_legal_address'),
                'company' => $request->get('suppliers_company_name'),
                'postal_code' => $request->get('suppliers_postal_code'),
                'other_languages' => $request->get('other_languages'),
            ];

            (new UserService())->update(Auth::id(), $userData);

            if ($request->has('languages')) {
                Auth::user()->languages()->attach($request->get('languages'));
            }

            if (isset($factoryInputs['factory_address'])) {
                $factorydetailService->createFactoryDetails($factoryInputs, $supplier->id);
            }

            if (isset($newCertificationInputs['issued_by'])) {
                if ($request->hasFile('new_certification_image')) {
                    $path = $fileService->storeSupplierLogo($request->file('new_certification_image'), Auth::id());
                    $newCertificationInputs['image'] = $path;
                    $newCertificationInputs['image_extension'] = $request->file('new_certification_image')->getClientOriginalExtension();
                }
                $certificationService->createIfAllInputsFill($supplier, $newCertificationInputs);
            }

            DB::commit();
            Aviso::actualizado(trans('validation.custom.messages.record_saved'));

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            Aviso::error(trans('validation.custom.messages.record_saved_error'));
        }

        return redirect()->route('user.products.index');
    }

    /**
     * @param Request $request
     * @param Supplier $supplier
     * @param SupplierService $supplierService
     * @param FactorydetailService $factorydetailService
     * @param CertificationService $certificationService
     * @param FileService $fileService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(
        Request $request,
        Supplier $supplier,
        SupplierService $supplierService,
        FactorydetailService $factorydetailService,
        CertificationService $certificationService,
        FileService $fileService
    ) {

        try {
            $this->authorize('update', $supplier);

            $validatorCompany = Validator::make($request->all(), [
                'suppliers_company_name' => 'required|max:250',
                'suppliers_company_description' => 'required|max:800',
                "suppliers_member_trade_organization" => "max:50",
                "suppliers_country_id" => "required|exists:countries,id",
                "suppliers_taxtype_id" => "required|exists:taxtypes,id",
                "suppliers_tax_number" => "required|max:30|unique:suppliers,tax_number," . $supplier->id,
                "suppliers_company_legal_address" => "required|max:255",
                "suppliers_postal_code" => "max:10",
                "suppliers_website" => "max:150",
                "suppliers_year_company_registered" => "numeric|max:9999",
                "suppliers_company_type" => "required|exists:companyTypes,id",
                "suppliers_number_employees" => "numeric|max:99999999",
                "suppliers_supercategory_id" => "required|exists:supercategories,id"
            ]);

            if ($validatorCompany->fails()) {
                return redirect()->route('company.profile.show', ['section' => 'details'])
                    ->withErrors($validatorCompany)->withInput();
            }

            $supplierInputs = [
                'company_name' => $request->get('suppliers_company_name'),
                'company_description' => $request->get('suppliers_company_description'),
                "member_trade_organization" => $request->get('suppliers_member_trade_organization'),
                "country_id" => $request->get('suppliers_country_id'),
                "taxtype_id" => $request->get('suppliers_taxtype_id'),
                "tax_number" => $request->get('suppliers_tax_number'),
                "company_legal_address" => $request->get('suppliers_company_legal_address'),
                "postal_code" => $request->get('suppliers_postal_code'),
                "website" => $request->get('suppliers_website'),
                "year_company_registered" => $request->get('suppliers_year_company_registered'),
                "company_type" => $request->get('suppliers_company_type'),
                "number_employees" => $request->get('suppliers_number_employees', 0),
                "supercategory_id" => $request->get('suppliers_supercategory_id')
            ];


            /*$validatorUpdatedCertification = Validator::make($request->all(), [
                "type_certification" => "max:255",
                "issued_by" => "max:255",
                "start_date" => "date_format:Y-m-d",
                "expiration_date" => "date_format:Y-m-d",
                "image" => "file",
            ]);

           if ($validatorUpdatedCertification->fails()) {

               return redirect()->route('company.profile.show',['section'=>'certifications'])
                   ->withErrors($validatorUpdatedCertification)
                   ->withInput();
           }*/

            $validatorNewCertification = Validator::make($request->all(), [
                "new_certification_type_certification" => "max:255",
                "new_certification_issued_by" => "max:255",
                "new_certification_start_date" => "date_format:Y-m-d",
                "new_certification_expiration_date" => "date_format:Y-m-d"
            ]);

            if ($validatorNewCertification->fails()) {

                return redirect()->route('company.profile.show', ['section' => 'certifications', 'action' => 'create'])
                    ->withErrors($validatorNewCertification)->withInput();
            }

            $newCertificationInputs = [
                "type_certification" => $request->get('new_certification_type_certification'),
                "issued_by" => $request->get('new_certification_issued_by'),
                "start_date" => $request->get('new_certification_start_date'),
                "expiration_date" => $request->get('new_certification_expiration_date'),
            ];

            $validatorFactory = Validator::make($request->all(), [
                "factoryDetails_factory_address" => "max:255",
                "factoryDetails_factory_size" => "numeric|max:9999999",
                "factoryDetails_number_qc_staff" => "numeric|max:9999999",
                "factoryDetails_number_rd_staff" => "numeric|max:9999999",
                "factoryDetails_number_production_lines" => "numeric|max:9999999",
                "factoryDetails_annual_output_value" => "numeric|max:9999999",
                "factoryDetails_measure_id" => "exists:measurements,id"
            ]);

            if ($validatorFactory->fails()) {
                return redirect()->route('company.profile.show', ['section' => 'factory'])
                    ->withErrors($validatorFactory)->withInput();
            }

            $factoryInputs = [
                "factory_address" => $request->get('factoryDetails_factory_address'),
                "factory_size" => $request->get('factoryDetails_factory_size'),
                "number_qc_staff" => $request->get('factoryDetails_number_qc_staff'),
                "number_rd_staff" => $request->get('factoryDetails_number_rd_staff'),
                "number_production_lines" => $request->get('factoryDetails_number_production_lines'),
                "annual_output_value" => $request->get('factoryDetails_annual_output_value'),
                "measure_id" => $request->get('factoryDetails_measure_id'),
            ];

            DB::beginTransaction();

            if ($request->hasFile('suppliers_image')) {
                $fileService->removeFile($supplier->logo);
                $path = $fileService->storeSupplierLogo($request->file('suppliers_image'), Auth::id());
                $supplierInputs['logo'] = $path;
                $supplierInputs['image_extension'] = $request->file('suppliers_image')->getClientOriginalExtension();
            }

            $supplierService->update($supplier->id, $supplierInputs);

            if (isset($newCertificationInputs['issued_by'])) {
                if ($request->hasFile('new_certification_image')) {
                    $path = $fileService->storeSupplierLogo($request->file('new_certification_image'), Auth::id());
                    $newCertificationInputs['image'] = $path;
                    $newCertificationInputs['image_extension'] = $request->file('new_certification_image')->getClientOriginalExtension();
                }

                $certificationService->createIfAllInputsFill($supplier, $newCertificationInputs);
            }

            if (isset($factoryInputs['factory_address'])) {
                $factorydetailService->update($supplier->factoryDetail->id, $factoryInputs);
            }

            if ($request->exists('update_certification')) {
                foreach ($request->get('update_certification') as $key => $certification) {
                    if ($request->hasFile('update_certification.' . $key) && isset($request->file('update_certification.' . $key)['image'])) {
                        $oldCertification = $certificationService->find($key);
                        $fileService->removeFile($oldCertification->image);
                        $path = $fileService->storeSupplierLogo($request->file('update_certification.' . $key)['image'],
                            Auth::id());

                        $certification['image'] = $path;
                        $certification['image_extension'] = $request->file('update_certification.' . $key)['image']->getClientOriginalExtension();
                    }
                    $certificationService->update($key, $certification);
                }
            }

            DB::commit();
            Aviso::actualizado(trans('validation.custom.messages.record_updated'));

        } catch (\Illuminate\Auth\Access\AuthorizationException $e) {
            $user = auth()->user();
            $company = $user->supplier;
            $owner = $company->users->where('pivot.is_principal', true)->first();
            Aviso::error(trans('subaccounts.validation_edit',
                ['owner' => $owner->full_name]));
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            Aviso::error(trans('validation.custom.messages.record_updated_error'));
        }

        if ($request->has('tab_name')) {
            if ($request->get('tab_name', 'details') != 'details') {
                return redirect()->action('User\SuppliersController@edit',
                    ['section' => $request->get('tab_name', 'details')]);
            }

        }

        return redirect()->action('User\SuppliersController@edit');
    }

}
