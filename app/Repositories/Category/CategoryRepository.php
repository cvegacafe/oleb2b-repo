<?php namespace App\Repositories\Category;

use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new Category();
        parent::__construct();
    }

    public function all()
    {
        return $this->model->with('subcategories')->get();
    }

    public function getCategoriesBySubCategoriesIds(array $subcategories)
    {
        return $this->model->whereHas('subcategories',function($query)use($subcategories){
            $query->whereIn('id',$subcategories);
        })->get();
    }

    public function getCategoriesBySearch($lang_prefix,$q,$subcategoryId,$countryId,$categoryId = null)
    {
        return  $this->model->whereHas('products',function($query) use ($q, $subcategoryId, $countryId, $lang_prefix){

           if ($q != null) {
                $query->where('products.'.$lang_prefix . '_name', 'like', '%' . $q . '%');
            }

            if ($subcategoryId != null) {
                $query->where('subcategory_id', $subcategoryId);
            }

            if ($countryId != null) {
                $query->where('country_id', $countryId);
            }

            $query->where('is_approved', true);

        })->where(function($query)use($categoryId){
            if($categoryId!=null){
                $query->where('id',$categoryId);
            }
        })->get();

    }
}