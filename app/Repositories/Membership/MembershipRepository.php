<?php namespace App\Repositories\Membership;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:10 PM
 */


use App\Repositories\BaseRepository;
use App\Repositories\User\User;
use Illuminate\Support\Facades\DB;

class MembershipRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Membership();
        parent::__construct();
    }

    public function all()
    {
        return $this->model->with('users')->get();
    }

    public function findByLang($id,$lang)
    {
        return DB::table('memberships')
            ->select('id',$lang.'_name as name','monthly_price','annual_price','monthly_price')
            ->where('id',$id)
            ->first();
    }

    public function findLastUserMembershipPivotRow(User $user)
    {
        return DB::table('membership_user')->where('user_id', $user->id)->first();
    }

    public function updateUserMembershipPivot($pivotId,$data)
    {
        return DB::table('membership_user')->where('id', $pivotId)->update($data);
    }
}