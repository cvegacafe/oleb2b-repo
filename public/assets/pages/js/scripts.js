function showRegisterForm() {
  var top = $('header').height();
  var bg = $('.auth-background');
  $('body').css('overflow', 'hidden');
  $('.mobile-side-menu').removeClass('active');
  $('.toggle-auth.icono .auth').hide();
  $('.toggle-auth.icono .fa').show();

  $('.content-auth .header ul li').removeClass('active');
  $('.content-auth .header ul li a[href="#registrarse"]').parent().addClass('active');

  $('#ingresar').removeClass('in active');
  $('#registrarse').addClass('in active');
  bg.fadeIn();
  if ($(window).width() < 992) {
    bg.css('top', top);
  }
}

function showAuth(redirect) {
  redirect = redirect || null;
  var top = $('header').height();
  var bg = $('.auth-background');
  $('.mobile-side-menu').removeClass('active');
  $('body').css('overflow', 'hidden');
  $('.toggle-auth.icono .auth').hide();
  $('.toggle-auth.icono .fa').show();

  $('.content-auth .header ul li').removeClass('active');
  $('.content-auth .header ul li a[href="#ingresar"]').parent().addClass('active');

  $('#registrarse').removeClass('in active');
  $('#ingresar').addClass('in active');

  if (redirect) {
    $('#redirect_back').val(redirect);
  }

  bg.fadeIn();
  if ($(window).width() < 992) {
    bg.css('top', top);
  }
}

function hideAuth() {
  $('body').css('overflow', 'auto');
  $('.auth-background').fadeOut();
  $('.toggle-auth.icono .auth').show();
  $('.toggle-auth.icono .fa').hide();
}


$(function () {

  let description = $('.descripcion');
  let description_lenght = $('.descripcion p').text().length;
  console.log('description: ', description_lenght);
  if(description_lenght > 250){
    description.addClass('showMore');
  }
  description.find('.toggle').click(function () {
   $(this).parent().toggleClass('active');
  });

  $('.toggle-edit').on('click', function (e) {
    e.preventDefault();

    var form = $($(this).data('form-edit'));
    var show_form = form.find('.show-form');
    var edit_form = form.find('.edit-form');

    show_form.toggleClass('active');
    edit_form.toggleClass('active');
  });

  $('.toggle-menu').on('click', function () {
    $('.mobile-side-menu').addClass('active');
    $('body').css('overflow', 'hidden');
  });

  $('.mobile-side-menu .close').on('click', function () {
    $('body').css('overflow', 'auto');
    $('.mobile-side-menu').removeClass('active');
  });
  /*GLOBALS*/
  $('#async-notifications .dropdown-toggle').on('click', function () {
    console.log('hola');
    $('#async-notifications .dropdown-menu').slideDown('fast');
  });
  $('#async-notifications .cerrar').on('click', function () {
    $('#async-notifications .dropdown-menu').slideUp('fast');
  });

  $('input').focus(function () {
    if ($(this).parent().hasClass('sub')) {
      $('.sub').removeClass('active');
      //console.log($(this));
      $(this).parent().addClass('active')
    }
  });
  $('.toggle-auth, .logueo > a').on('click', function (e) {

    $('.dropdown-auth').addClass('active');
  });
  $('.dropdown-auth').mouseleave(function () {
    $(this).removeClass('active');
  });

  $('.toggle-auth .fa, .content-auth .close').on('click', function () {
    hideAuth();
  });

  $('.showRegister').on('click', function () {
    showRegisterForm();
  });
  $('.showAuth').on('click', function () {
    showAuth();
  });

  /*ENDGLOBALS*/
  /*HOME*/
  $(".slide-noticias-home").slick({
    mobileFirst: true,
    slidesToShow: 1,
    prevArrow: '<button type="button" class="arrws slick-prev"> <i class="fa fa-chevron-left"></i> </button>',
    nextArrow: '<button type="button" class="arrws slick-next"><i  class="fa fa-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });
  $('.camaras, .ferias').slick({
    mobileFirst: true,
    slidesToShow: 1,
    prevArrow: '<button type="button" class="arrws slick-prev"> <i class="fa fa-chevron-left"></i> </button>',
    nextArrow: '<button type="button" class="arrws slick-next"><i  class="fa fa-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 639,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });

  $('.lista-categorias > ul > li').hover(function () {
    $('.mega-subcategory').hide();
    var mega_id = $(this).attr('target');
    console.log(mega_id);
    showMegaSubcategoryMenu(mega_id);
  });
  $('.mega-subcategory').mouseleave(function () {
    hideMegaSubcategoryMenu($(this).attr('id'));
  });
  $(window).scroll(function () {
    hideMegaSubcategoryMenu();
  });

  function showMegaSubcategoryMenu(id) {
    $('#' + id).show();
  }

  function hideMegaSubcategoryMenu(id) {
    if (id) {
      $('#' + id).hide();
    } else {
      $('.mega-subcategory').hide();
    }
  }

  /*FINISH HOME*/

  /*SINGLE PRODUCT*/
  $('.images .gallery').slick({
    slidesToShow: 1,
    mobileFirst: true,
    dots: true,
    arrows: false,
    asNavFor: '.images .nav',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          dots: false,
        }
      }
    ]
  });
  $('.images .nav').slick({
    slidesToShow: 3,
    dots: false,
    arrows: false,
    vertical: true,
    verticalSwiping: true,
    focusOnSelect: true,
    centerMode: true,
    infinite: true,
    asNavFor: '.images .gallery'
  });
  $('.empresa .toggle').on('click', function () {
    $('.empresa .datos').slideToggle();
  });

  $('.carrousel-related-product').slick({
    slidesToShow: 1,
    mobileFirst: true,
    dots: false,
    arrows: true,
    prevArrow: '<i class="fa fa-angle-left prev"></i>',
    nextArrow: '<i class="fa fa-angle-right next"></i>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3
        }
      }
    ]

  });

  /*FINISH SIGLE PRODUCT*/

  /*CATEGORIAS*/

  console.log(window.location.hash.substring(1));
  if (window.location.hash.substring(1)) {
    scrollToSubCategory('category' + window.location.hash.substring(1));
  }

  $('.category-button').on('click', function () {
    var target = $(this).attr('target');
    scrollToSubCategory(target);
  });

  function scrollToSubCategory(id) {
    var offset = $('#' + id).offset().top;
    $('html, body').animate({
      scrollTop: offset
    }, 1000);
  }

  $('.desplegar-mas').on('click', function () {
    $(this).parent().toggleClass('show');
  });


  $('.back-top').on('click', function () {
    $('body, html').animate({
      scrollTop: 0
    })
  });

  /*FINISH CATEGORIES*/


  /*PARA USAR ESTA FUNCTION SE DEBE COLOCAR LA CLASE AL FIGURE*/
  $('.showInLightbox').on('click', function () {
    var img = $(this).find('img').attr('src');
    console.log(img);
    showInLightbox(img);
  });

  function showInLightbox(img) {
    var lightbox = $('.img-lightbox');
    lightbox.find('img').attr('src', img);
    lightbox.show();
    lightbox.click(function () {
      $(this).hide()
    })
  }


  $('.toggleFavoriteText').on('click', function (e) {
    e.preventDefault();
    var product_id = $(this).data('product-id');
    if (IS_AUTH) {
      if ($(this).hasClass('active')) {
        removeFavoriteProduct(product_id);
        $(this).removeClass('active');
        $(this).text($(this).data('save-for-later'));
      } else {
        setFavoriteProduct(product_id);
        $(this).addClass('active');
        $(this).text($(this).data('saved'));
      }

    } else {
      showAuth();
    }
  });

  $('.toggleFavorite').on('click', function (e) {
    e.preventDefault();
    let product_id = $(this).data('product-id');
    if (IS_AUTH) {
      if ($(this).hasClass('active')) {
        removeFavoriteProduct(product_id);
          $(this).removeClass('active');
      } else {
        setFavoriteProduct(product_id);
          $(this).addClass('active');
      }

    } else {
      showAuth();
    }
  });

  /*GLOBALS*/
  function setFavoriteProduct(product_id) {
    let request = $.ajax({
      url: setFavoriteURL,
      method: "POST",
      data: {product_id: product_id, _token: _TOKEN},
      dataType: "json"
    });
    request.done(function (response) {
      return !!(response.response == 'ok' || response.response == 'exists');
    });
    request.fail(function (jqXHR, textStatus) {
      return false;
    });
  }

  function removeFavoriteProduct(product_id) {
    let request = $.ajax({
      url: removeFavoriteURL,
      method: "POST",
      data: {product_id: product_id, _token: _TOKEN},
      dataType: "json"
    });
    request.done(function (response) {
      return !!(response.response == 'ok' || response.response == 'exists');
    });
    request.fail(function () {
      return false;
    });
  }
});