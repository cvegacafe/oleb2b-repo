<?php namespace App\Http\Middleware;

use App\Library\Aviso;
use Closure;
use Illuminate\Support\Facades\Auth;

class CanSupplierPostingProducts
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->isSupplier() || Auth::user()->suppliers->isEmpty()){
            Aviso::warning(trans('alerts.middlewares.supplier_data_required'));
            return redirect()->route('company.profile.show');
        }

        return $next($request);
    }
}
