<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendContactFormEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $name, $email, $subject, $message;

    /**
     * Create a new job instance.
     *
     */
    public function __construct($name, $email, $subject, $message)
    {

        $this->name = $name;

        $this->email = $email;

        $this->subject = $subject;

        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $data['name'] = $this->name;
        $data['email'] = $this->email;
        $data['subject'] = $this->subject;
        $data['message'] = $this->message;

        $mailer->send('mails.contact-form', ['data' => $data],
            function ($m) {
                $m->from($this->email, $this->name);
                $m->to('emmanuelbarturen@gmail.com', 'Emmanuel')->subject($this->subject);
                $m->to('v.cristianalfredo@gmail.com', 'Cristian')->subject($this->subject);
                $m->to('olesya@olebaba.com', 'Olesya')->subject($this->subject);
            });
    }
}
