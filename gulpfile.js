var gulp       = require('gulp');
var elixir     = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('./resources/assets/pages/sass/styles.scss','public/assets/pages/css/styles.css');
    mix.sass('./resources/assets/sass/app.scss','public/assets/user/css/app.css');
    mix.sass('./resources/assets/landings/latam-exporta/sass/styles.scss','public/assets/landing/latam-exporta/css/styles.css');

    mix.browserSync({
        proxy: 'oleb2b.test',
        notify 			: false,
        open: false,
        files: [
            "*.php",
            "assets/scripts/*.js",
            "resources/views/*/*.*",
            "resources/views/*/*/*.*",
            "./public/assets/pages/css/*.css",
            "./public/assets/user/css/*.css",
            "./public/assets/landing/latam-exporta/css/*.css"
        ] });
});
