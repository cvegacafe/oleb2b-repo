<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\NotificationService;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    private $notificationService = null;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function all()
    {
        $collection = $this->notificationService->getLastNotifications(Auth::id());

        return response()->json($collection->toArray());
    }
}
