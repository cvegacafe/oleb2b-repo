<?php namespace App\Services;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */

use App\Repositories\Supercategory\SupercategoryRepository;

class SupercategoryService extends BaseService
{
    public function __construct(SupercategoryRepository $supercategoryRepository)
    {
        $this->mainRepo = $supercategoryRepository;
        parent::__construct();
    }
}