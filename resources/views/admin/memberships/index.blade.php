@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Membresias :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre Espanol</th>
                            <th>Nombre Ingles</th>
                            <th>Nombre Ruso</th>
                            <th>Banners Principales </th>
                            <th>Banners Interiores</th>
                            <th>Productos Posteados</th>
                            <th>Precio Mensual</th>
                            <th>Precio Anual</th>
                            <th>Respues a cotizaciones</th>
                            <th>SubCuentas</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($memberships as $membership)
                        <tr>
                            <td><b>{{$membership->id}}</b></td>
                            <td>{{$membership->es_name}}</td>
                            <td>{{$membership->en_name}}</td>
                            <td>{{$membership->ru_name}}</td>
                            <td>Rotacion {{$membership->main_rotation_banners}} mins. por mes en promedio</td>
                            <td>Rotacion {{$membership->inner_rotation_banners}} mins. por mes en promedio</td>
                            <td>{{$membership->product_posting}}</td>
                            <td>${{$membership->monthly_price}} USD </td>
                            <td>${{$membership->annual_price}} USD </td>
                            <td>{{$membership->buying_request_responses?'SI':'NO'}}</td>
                            <td>{{$membership->subaccounts}}</td>
                            <td>
                                <a href="{{action('Admin\MembershipsController@edit',$membership)}}"><button class="btn btn-primary btn-xs" ><span class="fa fa-pencil"></span></button></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection