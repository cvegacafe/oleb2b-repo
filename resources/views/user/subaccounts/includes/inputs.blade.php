<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
            <div class="">
                <label for="name" class="control-label">Names</label>
                <input id="name" type="text" class="form-control" name="names" value="{{old('names') }}" required autofocus>
                @if ($errors->has('names'))
                    <span class="help-block">
                        <strong>{{ $errors->first('names') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('last_names') ? ' has-error' : '' }}">
            <div class="">
                <label for="last_names" class="control-label">Last Names</label>
                <input id="last_names" type="text" class="form-control" name="last_names" value="{{ old('last_names') }}" required autofocus>
                @if ($errors->has('last_names'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_names') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="">
                <label for="email" class="control-label">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="">
                <label for="password" class="control-label">Password</label>
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <div class="">
                <label for="password-confirm" class="control-label">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
            <div class="">
                <label for="office_phone" class="control-label">Office Phone</label>
                <input id="office_phone" type="text" class="form-control" name="office_phone" value="{{ old('office_phone') }}" required>
                @if ($errors->has('office_phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('office_phone') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
            <div class="">
                <label for="mobile_phone" class="control-label">Mobile Phone</label>
                <input id="mobile_phone" type="text" class="form-control" name="mobile_phone" value="{{ old('mobile_phone') }}" required>

                @if ($errors->has('mobile_phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('mobile_phone') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
            <div class="">
                <label for="postal_code" class="control-label">Languages</label>
                <div class="checkbox-list">
                    @foreach($languages as $language)
                        <label class="checkbox-inline">
                            @if(isset($user) && $user->languages->contains($language->id))
                                <input id="inlineCheckbox{{$language->id}}" checked type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->name}}
                            @else
                                <input id="inlineCheckbox{{$language->id}}" {{in_array($language->id,old('languages',[]))?'checked':''}} type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->name}}
                            @endif
                        </label>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="form-group{{ $errors->has('other_languages') ? ' has-error' : '' }}">
            <div class="">
                <label for="other_languages" class="control-label">Others Languages</label>
                <input id="other_languages" type="text" class="form-control" name="other_languages" value="{{ old('other_languages') }}" >
                @if ($errors->has('other_languages'))
                    <span class="help-block">
                        <strong>{{ $errors->first('other_languages') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
            <div class="">
                <label for="postal_code" class="control-label">Markets</label>
                <div class="checkbox-list">
                    @foreach($markets as $market)
                        <label class="checkbox-inline">
                            @if(isset($user) && $user->markets->contains($market->id))
                                <input id="inlineCheckbox{{$market->id}}" checked type="checkbox" name="markets[]" value="{{$market->id}}"/>&nbsp;{{$market->name}}
                            @else
                                <input id="inlineCheckbox{{$market->id}}" {{in_array($market->id,old('markets',[]))?'checked':''}} type="checkbox" name="markets[]" value="{{$market->id}}"/>&nbsp;{{$market->name}}
                            @endif
                        </label>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>