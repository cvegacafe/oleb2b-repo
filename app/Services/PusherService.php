<?php namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Pusher;

class PusherService
{
    private $pusher;

    public function __construct()
    {
        $this->pusher = new Pusher(
            env('PUSHER_KEY'),
            env('PUSHER_SECRET'),
            env('PUSHER_APP_ID'),
            array('encrypted' => true)
        );
    }

    public function sendToUser($userId, $title, $created_at, $icon, $url)
    {
        $data['title'] = $title;
        $data['created_at'] = $created_at;
        $data['icon'] = $icon;
        $data['url'] = $url;
        $this->pusher->trigger('notifications-' . $userId, 'notification', $data);
    }

    public function sendMessage($channel, $message)
    {
        $this->pusher->trigger($channel, 'new-message', $message);
    }

    public function auth($channelName, $socketId, $userId, $username)
    {
        \Debugbar::disable();

        return $this->pusher->presence_auth($channelName, $socketId, $userId, ['username' => $username]);
    }

    public function test()
    {
        $data['title'] = 'hello world';
        $data['description'] = 'hello world';
        $data['created_at'] = 'hello world';
        $data['icon'] = 'fa fa-user';
        $data['url'] = route('user.messages.products.index',['product'=>1,'thread'=>2]);
        $this->pusher->trigger('notifications-' . Auth::id(), 'notification', $data);
    }

}
