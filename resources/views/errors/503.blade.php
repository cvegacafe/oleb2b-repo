@extends('layouts.pages')
@section('title')
    Home
@endsection
@section('content')
    <div class="buscador">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h1 class="titulo">{{trans('portal.main_slogan')}}</h1>
                    <div class="input-container">
                        <form action="" method="GET">
                            <select name="tipoBusqueda" id="tipoBusqueda">
                                <option value="Buscar: ">{{trans('portal.search')}}:</option>
                                <option>{{trans('portal.product')}}</option>
                                <option value="proveedor">{{trans('portal.supplier')}}</option>
                            </select>
                            <input type="text" name="q" placeholder="{{trans('portal.search_products')}}" >
                            <button>{{trans('portal.search')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="buying-requests" id="buying-requests">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="empty"><strong>503 {{trans('portal.error_503')}}:</strong> {{trans('portal.error_503_text')}}</h3>
                </div>
            </div>

        </div>
    </section>
    <div class="registrate">
        <div class="centro">
            <h3>{{trans('portal.sale_online_is_easy')}}</h3>
            <button>{{trans('portal.join_free')}}</button>
        </div>
    </div>
@endsection