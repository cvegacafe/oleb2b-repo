<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CertificationService;
use Illuminate\Http\Request;

class CertificatesController extends Controller
{
    /**
     * @param Request $request
     * @param CertificationService $certificationService
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, CertificationService $certificationService)
    {
        $destroyed = $certificationService->destroy($request->get('cert_id'));
        $url = route('company.profile.show', ['section' => 'certifications']);
        return response()->json(['destroyed' => $destroyed, 'url' => $url]);
    }
}
