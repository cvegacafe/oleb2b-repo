@if(Session::get('aviso-msg'))
    <script>
        swal({
            title: "{{Session::get('aviso-msg')}}",
            @if(Session::get('aviso-content'))
            text: "{{Session::get('aviso-content')}}",
            @endif
            type: "{{Session::get('aviso-tipo')}}",
            confirmButtonText: "OK",
            confirmButtonColor: '#FF6A31'
        });
    </script>
@endif

@if(Session::get('status'))
    <script>
        swal({
            title: "{{trans('passwords.send_password_reset_link')}}",
            @if(Session::get('status'))
            text: "{!! Session::get('status') !!}",
            @endif
            confirmButtonText: "OK",
            confirmButtonColor: '#FF6A31'
        });
    </script>
@endif

