<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('es_name',150);
            $table->string('en_name',150);
            $table->string('ru_name',150);

            $table->text('es_description');
            $table->text('en_description');
            $table->text('ru_description');

            $table->string('es_color',50)->nullable();
            $table->string('en_color',50)->nullable();
            $table->string('ru_color',50)->nullable();

            $table->string('es_type',50)->nullable();
            $table->string('en_type',50)->nullable();
            $table->string('ru_type',50)->nullable();

            $table->string('es_slug',1000)->unique();
            $table->string('en_slug',1000)->unique();
            $table->string('ru_slug',1000)->unique();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('subcategory_id')->unsigned();
            $table->foreign('subcategory_id')->references('id')->on('subcategories');
            $table->json('pictures')->nullable();
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->text('supply_ability')->nullable();
            $table->integer('supply_measure_id')->unsigned();
            $table->foreign('supply_measure_id')->references('id')->on('measurements');
            $table->string('supply_ability_frecuency')->nullable();
            $table->text('moq')->nullable();
            $table->integer('moq_measure_id')->unsigned();
            $table->foreign('moq_measure_id')->references('id')->on('measurements');
            $table->smallInteger('delivery_time')->nullable();
            $table->decimal('price');
            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->integer('sale_unit_measure_id')->unsigned();
            $table->foreign('sale_unit_measure_id')->references('id')->on('measurements');
            $table->json('ports')->nullable();
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
