<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'App Name') }}</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/pace.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colorbox/colorbox.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/endless.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/endless-skin.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/pages/img/favicon.png')}}">
@yield('styles')
<!-- Scripts -->
</head>
<body class="overflow-hidden">
<!-- Overlay Div -->
<div id="overlay" class="transparent"></div>
<div id="wrapper" class="preload">
    <div id="top-nav" class="fixed skin-6">
        <a href="/" class="brand">
            {{ config('app.name', 'App Name') }}
        </a><!-- /brand -->
        <button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <ul class="nav-notification clearfix">
            <li class="profile dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <strong> Admin</strong>
                    <span><i class="fa fa-chevron-down"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="clearfix" href="#">
                            <img src="{{ asset('assets/img/default-user.png') }}" alt="User Avatar">
                            <div class="detail">
                                <strong>Administrador</strong>
                                <p class="grey"> adasdsaddsadas</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                           class="main-link logoutConfirm_open"
                           href="#"><i class="fa fa-lock fa-lg"></i> Log out</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div><!-- /top-nav-->
    <aside class="fixed skin-6">
        <div class="sidebar-inner scrollable-sidebar">
            <div class="size-toggle">
                <a class="btn btn-sm" id="sizeToggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </div><!-- /size-toggle -->
            <div class="user-block clearfix">
                <img src="{{ asset('assets/img/default-user.png') }}" alt="User Avatar">
                <div class="detail">
                    <strong>Administrador</strong>
                    {{--<ul class="list-inline">
                        <li><a href="#">Perfil</a></li>
                    </ul>--}}
                </div>
            </div><!-- /user-block -->
            <div class="main-menu">
                <ul>
                    {{--<li class="{{ request()->url() == '' ? 'active': ''}}">
                        <a href="">
                        <span class="menu-icon">
                            <i class="fa fa-tachometer fa-lg"></i>
                        </span>
                            <span class="text">
                            Dashboard
                        </span>
                            <span class="menu-hover"></span>
                        </a>
                    </li>--}}
                    <li class="openable open">
                        <a href="#">
                            <span class="menu-icon">
                                <i class="fa fa-shopping-cart fa-lg"></i>
                            </span>
                            <span class="text">
                                Órdenes
                            </span>
                            <span class="menu-hover"></span>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="#"><span
                                            class="submenu-label">Abiertas  </span></a>
                            </li>
                            <li>
                                <a href="#"><span
                                            class="submenu-label">Abiertas  </span></a>
                            </li>
                            <li>
                                <a href="#"><span
                                            class="submenu-label">Abiertas  </span></a>
                            </li>
                            <li>
                                <a href="#"><span
                                            class="submenu-label">Abiertas  </span></a>
                            </li>
                            <li>
                                <a href="#"><span
                                            class="submenu-label">Abiertas  </span></a>
                            </li>

                        </ul>
                    </li>
                    <li class="openable open">
                        <a href="#">
                            <span class="menu-icon">
                                <i class="fa fa-users fa-lg"></i>
                            </span>
                            <span class="text">
                                Usuarios
                            </span>
                            <span class="menu-hover"></span>
                        </a>
                        <ul class="submenu">
                            <li><a href="#">
                                    <span class="submenu-label">Clientes </span></a>
                            </li>
                            <li><a href="#">
                                    <span class="submenu-label">Clientes </span></a>
                            </li>
                            <li><a href="#">
                                    <span class="submenu-label">Clientes </span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /main-menu -->
        </div><!-- /sidebar-inner -->
    </aside>
    <div id="main-container">

        @yield('content')
    </div><!-- /main-container -->
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-sm-6">
                 <span class="footer-brand">
                     <strong class="text-danger">Oleb2b</strong> Panel
                 </span>
                <p class="no-margin">
                    &copy; 2018 <strong>Oleb2b</strong>. ALL Rights Reserved.
                </p>
            </div><!-- /.col -->
        </div><!-- /.row-->
    </footer>
</div><!-- /wrapper -->
<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
<!-- Placed at the end of the document so the pages load faster -->
<!-- Jquery -->
<script src="{{asset('assets/js/jquery-1.10.2.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Sparkline -->
<script src='{{asset('assets/js/jquery.sparkline.min.js')}}'></script>
<!-- Pace -->
<script src='{{asset('assets/js/uncompressed/pace.js')}}'></script>
<!-- Slimscroll -->
<script src='{{asset('assets/js/jquery.slimscroll.min.js')}}'></script>
<!-- Modernizr -->
<script src='{{asset('assets/js/modernizr.min.js')}}'></script>
<!-- Cookie -->
<script src='{{asset('assets/js/jquery.cookie.min.js')}}'></script>
<!-- Endless -->
<script src="{{asset('assets/js/endless/endless.js')}}"></script>
<script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.validate.js')}}"></script>
<script>
    $('.validate').validate();
</script>
<script src="{{asset('assets/js/global.js')}}"></script>
@yield('scripts')
</body>
</html>
