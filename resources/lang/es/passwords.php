<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'button_change_password' => 'Cambiar Contraseña',
    'old_password_input' => 'Ingresa tu antigua contraseña',
    'new_password_input' => 'Ingresa tu nueva contraseña',
    'new_password_input_confirmation' => 'Vuelve a Ingresar tu nueva contraseña',
    'password' => 'Las contraseñas deben tener al menos seis caracteres y coincidir con la confirmación.',
    'reset' => '¡Tu contraseña ha sido restablecida!',
    'sent' => '¡Hemos enviado por correo electrónico el enlace para restablecer contraseña!',
    'token' => 'Este token de restablecimiento de contraseña no es válida.',
    'user' => "No podemos encontrar un usuario con ese e-mail.",
    'send_password_reset_link' => " Enviar correo para reestablecer contraseña",
    'back_to_login' => "Volver al login",
    'message_mail_change_password'=>'Haga click en el siguiente boton para reestabler su contraseña',
    'message_paste_browser'=>'Si tiene problemas al hacer clic en el botón " :name ", copie y pegue la URL que aparece a continuación en su navegador web:'
];
