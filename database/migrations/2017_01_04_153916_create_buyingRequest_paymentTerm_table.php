<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyingRequestPaymentTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyingRequest_paymentTerm', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('buyingRequest_id')->unsigned();
            $table->foreign('buyingRequest_id')->references('id')->on('buyingRequests')->onDelete('cascade');
            $table->integer('paymentTerm_id')->unsigned();
            $table->foreign('paymentTerm_id')->references('id')->on('paymentTerms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyingRequest_paymentTerm');
    }
}
