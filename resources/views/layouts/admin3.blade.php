<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/pace.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colorbox/colorbox.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/endless.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/endless-skin.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/pages/img/favicon.png')}}">
    @yield('styles')
</head>
<body class="overflow-hidden">
<!-- Overlay Div -->
<div id="overlay" class="transparent"></div>
<div id="wrapper" class="preload">
    <div id="top-nav" class="fixed skin-6">
        <a href="#" class="brand">
            {{ config('app.name', 'Laravel') }}
        </a><!-- /brand -->
        @if (!auth()->guard('admin')->check())
            <ul class="nav-notification clearfix">
                <li><a href="{{ route('admin.login') }}">Login</a></li>
            </ul>
        @else
            <button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav-notification clearfix">
                <li class="profile dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <strong> {{auth()->guard('admin')->user()->full_name }}</strong>
                        <span><i class="fa fa-chevron-down"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="clearfix" href="#">
                                <img src="{{ asset('assets/img/default-user.png') }}" alt="User Avatar">
                                <div class="detail">
                                    <strong>{{auth()->guard('admin')->user()->full_name }}</strong>
                                    <p class="grey"> administrator </p>
                                </div>
                            </a>
                        </li>
                        {{--<li><a tabindex="-1" href="profile.html" class="main-link"><i class="fa fa-edit fa-lg"></i> Editar
                                perfil</a></li>
                        <li class="divider"></li>--}}
                        <li>
                            <a onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                               class="main-link logoutConfirm_open"
                               href="#"><i class="fa fa-lock fa-lg"></i> Salir</a>
                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        @endif
    </div><!-- /top-nav-->
    @if(auth()->guard('admin')->check())
        <aside class="fixed skin-6">
            <div class="sidebar-inner scrollable-sidebar">
                <div class="size-toggle">
                    <a class="btn btn-sm" id="sizeToggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </div><!-- /size-toggle -->
                <div class="user-block clearfix">
                    <img src="{{ asset('assets/img/default-user.png') }}" alt="User Avatar">
                    <div class="detail ellipsis">
                        <ul class="list">
                            <li><strong title="Name">{{auth()->guard('admin')->user()->full_name}}</strong></li>
                            <li>Administrator</li>
                        </ul>
                    </div>
                </div><!-- /user-block -->
                <div class="main-menu">
                    <ul>
                        <li class="{{\Request::url()==action('Admin\UsersController@index')?'active':''}}">
                            <a href="{{action('Admin\UsersController@index')}}">
                                <span class="menu-icon"><i class="fa fa-user fa-lg"></i></span>
                                <span class="text">Usuarios</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                        <li class="{{\Request::url()==action('Admin\CountriesController@index')?'active':''}}">
                            <a href="{{action('Admin\CountriesController@index')}}">
                                <span class="menu-icon"><i class="fa fa-flag fa-lg"></i></span>
                                <span class="text">Paises</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>

                        <li class="openable open">
                            <a href="#"><span class="menu-icon"><i class="fa fa-sitemap fa-lg"></i></span>
                                <span class="text">Categorias</span>
                                <span class="menu-hover"></span>
                            </a>
                            <ul class="submenu">
                                <li class="{{\Request::url()==action('Admin\SupercategoriesController@index')?'active':''}}">
                                    <a href="{{action('Admin\SupercategoriesController@index')}}">
                                        <span class="submenu-label">SuperCategorias</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==action('Admin\CategoriesController@index')?'active':''}}">
                                    <a href="{{action('Admin\CategoriesController@index')}}">
                                        <span class="submenu-label">Categorias</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==action('Admin\SubcategoriesController@index')?'active':''}}">
                                    <a href="{{action('Admin\SubcategoriesController@index')}}">
                                        <span class="submenu-label">SubCategorias</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="openable open">
                            <a href="#"><span class="menu-icon"><i class="fa fa-cogs fa-lg"></i></span>
                                <span class="text">Config</span>
                                <span class="menu-hover"></span>
                            </a>
                            <ul class="submenu">
                                <li class="{{\Request::url()==action('Admin\CurrenciesController@index')?'active':''}}">
                                    <a href="{{action('Admin\CurrenciesController@index')}}">
                                        <span class="submenu-label">Monedas</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==action('Admin\MeasurementsController@index')?'active':''}}">
                                    <a href="{{action('Admin\MeasurementsController@index')}}">
                                        <span class="submenu-label">Medidas</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==action('Admin\DeliveryTermsController@index')?'active':''}}">
                                    <a href="{{action('Admin\DeliveryTermsController@index')}}">
                                        <span class="submenu-label">Terminos</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==action('Admin\TaxTypesController@index')?'active':''}}">
                                    <a href="{{action('Admin\TaxTypesController@index')}}">
                                        <span class="submenu-label">Identificación tributaria</span>
                                    </a>
                                </li>

                                <li class="{{\Request::url()==route('admin.show.news')?'active':''}}">
                                    <a href="{{route('admin.show.news')}}">
                                        <span class="submenu-label">News</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==route('admin.sliders.index')?'active':''}}">
                                    <a href="{{route('admin.sliders.index')}}">
                                        <span class="submenu-label">Sliders</span>
                                    </a>
                                </li>
                                <li class="{{\Request::url()==route('admin.oleb2b-images.index')?'active':''}}">
                                    <a href="{{route('admin.oleb2b-images.index')}}">
                                        <span class="submenu-label">Oleb2b Images</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="{{\Request::url()==action('Admin\MembershipsController@index')?'active':''}}">
                            <a href="{{action('Admin\MembershipsController@index')}}">
                                <span class="menu-icon"><i class="fa fa-flag fa-lg"></i></span>
                                <span class="text">Membresías</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                        <li class="{{\Request::url()==action('Admin\ProductsController@index')?'active':''}}">
                            <a href="{{action('Admin\ProductsController@index')}}">
                                <span class="menu-icon"><i class="fa fa-barcode fa-lg"></i></span>
                                <span class="text">Productos</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                        <li class="{{\Request::url()==action('Admin\SupplierController@index')?'active':''}}">
                            <a href="{{action('Admin\SupplierController@index')}}">
                                <span class="menu-icon"><i class="fa fa-truck fa-lg"></i></span>
                                <span class="text">Proveedores</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                        <li class="{{\Request::url()==action('Admin\BuyingRequestController@index')?'active':''}}">
                            <a href="{{action('Admin\BuyingRequestController@index')}}">
                                <span class="menu-icon"><i class="fa fa-search fa-lg"></i></span>
                                <span class="text">Buying Request</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>
                    </ul>
                </div><!-- /main-menu -->
            </div><!-- /sidebar-inner -->
        </aside>
    @endif
    <div id="main-container" style="padding: 50px">
        @yield('content')
    </div><!-- /main-container -->
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-sm-6">
                        <span class="footer-brand">
                            <strong class="text-danger">Oleb2b</strong>
                        </span>
                <p class="no-margin">
                    &copy; 2017 <strong>oleb2b.com</strong>. ALL Rights Reserved.
                </p>
            </div><!-- /.col -->
        </div><!-- /.row-->
    </footer>
</div><!-- /wrapper -->
<a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
<!-- Le javascript -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Jquery -->
<script src="{{asset('assets/js/jquery-1.10.2.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Sparkline -->
<script src='{{asset('assets/js/jquery.sparkline.min.js')}}'></script>
<!-- Pace -->
<script src='{{asset('assets/js/uncompressed/pace.js')}}'></script>
<!-- Slimscroll -->
<script src='{{asset('assets/js/jquery.slimscroll.min.js')}}'></script>
<!-- Modernizr -->
<script src='{{asset('assets/js/modernizr.min.js')}}'></script>
<!-- Cookie -->
<script src='{{asset('assets/js/jquery.cookie.min.js')}}'></script>
<!-- Endless -->
<script src="{{asset('assets/js/endless/endless.js')}}"></script>
<script src="{{asset('assets/js/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.validate.js')}}"></script>
<script>
    $('.validate').validate();
</script>
<script src="{{asset('assets/js/global.js')}}"></script>
@yield('scripts')
</body>
</html>
