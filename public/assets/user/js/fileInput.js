var position = 0;

$('#seleccionar-fotos').on('click',function(){
    $file = $('.file'+position);
    if($file.val()){
        $file.click();
    }else{
        position++;
        appendInputFile(position)
        $('.file'+position).click();
    }
});

$('.preview-files').on('mouseenter','div span i',function(){
    $(this).parent().parent().find('figure').fadeIn();
}).on('mouseleave','div',function(){
    $(this).parent().parent().find('figure').fadeOut();
});
function ResizeImage() {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        var filesToUploads = document.getElementById('imageFile').files;
        var file = filesToUploads[0];
        if (file) {
            console.log(file);
            var reader = new FileReader();
            // Set the image once loaded into file reader
            reader.onload = function(e) {

                var img = document.createElement("img");
                img.src = e.target.result;

                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);

                var MAX_WIDTH = 100;
                var MAX_HEIGHT = 100;
                var width = img.width;
                var height = img.height;

                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }

                canvas.width = width;
                canvas.height = height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0, width, height);
               // console.log(img);
                dataurl = canvas.toDataURL(file.type);
                console.log(dataurl);
                document.getElementById('output').src = dataurl;
            };
            reader.readAsDataURL(file);
        }
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}
function previewFile(element){
    var $this = $(element);
    //console.log(position);

    if(validateFile($this)){
        var fileName = getFileName($this.val());
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.preview-files').append('<div class="file'+position+'"> <span><i class="fa fa-picture-o"></i><small>'+fileName+'</small></span> <figure><img src="'+e.target.result+'"></figure> <div class="close" onclick="removeFile('+position+')"><i class="fa fa-close"></i></div> </div>');
        };

        reader.readAsDataURL(element.files[0]);
        if(!position === 1){
            position++;
            appendInputFile(position)
        }
    }
}

function appendInputFile(position){
    $('#filesContainer').append($('<input/>').attr('type', 'file').attr('name', 'images[]').addClass('input-file file'+position).attr('onchange',"previewFile(this, "+position+")"));
}

function validateFile(input){

    var extencionesAceptadas = [
        'jpeg',
        'jpg',
        'png',
        'gif',
        'bmp'
    ];

    var maxCount = 5;

    if($('#filesContainer input[type="file"]').length > maxCount){
        //TODO: maximo de imagenes permitido
        alert('maximo de imagenes permitido. Max: 5');
        return false;
    }

    var fileExtension = input.val().split('.').pop();

    if(!extencionesAceptadas.includes(fileExtension)){
        input.val('');
        alert('tipo de archivo no permitido, solo permitimos .jpg, .png, .gif, .bmp ');
        return false;
    }

    return true;
}
function removeOldFile(file){
    var $this = $(file);
    var position = $this.data('position');
    $('#filesContainer').append('<input type="hidden" name="filesToRemove[]" value="'+position+'">');
    $this.parent().remove();
}

function removeFile(position){
    $('.file'+position).remove();
}

function getFileName(fullPath){
    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
    var filename = fullPath.substring(startIndex);
    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
    }
    return filename;
}