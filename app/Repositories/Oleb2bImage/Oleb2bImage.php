<?php namespace App\Repositories\Oleb2bImage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Oleb2bImage
 * @package App\Repositories\Oleb2bImage
 */
class Oleb2bImage extends Model
{
    /**
     * @var string
     */
    protected $table = 'oleb2b_images';

    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['title', 'path', 'section', 'url'];

    const SECTIONS = [
        self::SECTION_CCL,
        self::SECTION_EVENTS,
        self::SECTION_PARTNERS,
        self::SECTION_BACKGROUND
    ];

    const SECTION_BACKGROUND = 'background';
    const SECTION_CCL = 'ccl';
    const SECTION_EVENTS = 'events';
    const SECTION_PARTNERS = 'partners';

}
