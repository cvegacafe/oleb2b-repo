<?php namespace App\Http\Controllers\Api;

use App\Services\SavedBuyingRequestService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SavedBuyingRequestsController extends Controller
{
    private $savedBuyingRequestService = null;

    public function __construct(SavedBuyingRequestService $savedBuyingRequestService)
    {
        $this->savedBuyingRequestService = $savedBuyingRequestService;
    }


    public function savedBuyingRequestCount()
    {
        $savedBuyingRequest = $this->savedBuyingRequestService->getSavedBuyingRequestByUser(Auth::id());

        return response()->json(['status'=>'success','count'=>$savedBuyingRequest->count()]);
    }

    public function create(Request $request)
    {
        $request['user_id'] = Auth::id();

        if ($this->savedBuyingRequestService->findSavedBuyingRequest($request['user_id'], $request->get('buyingRequest_id'))) {
            return response()->json(['response' => 'exists', 'message' => 'ya existe']);
        }

        $result = $this->savedBuyingRequestService->create($request->all());

        if ($result) {
            return response()->json(['response' => 'ok', 'message' => 'Agregado']);
        }

        return response()->json(['response' => 'error', 'message' => 'No se pudo agregar']);
    }

    public function remove(Request $request)
    {
        $user_id = Auth::id();
        $savedBuyingRequest = $this->savedBuyingRequestService->findSavedBuyingRequest($request->buyingRequest_id, $user_id);

        if(!$savedBuyingRequest){
            return response()->json(['response' => 'error', 'message' => 'No se encontro elemento']);
        }

        $result = $this->savedBuyingRequestService->destroy($savedBuyingRequest->id);

        if ($result) {
            return response()->json(['response' => 'ok', 'message' => 'Removido']);
        }

        return response()->json(['response' => 'error', 'message' => 'No se pudo remover de tus favoritos']);
    }
}
