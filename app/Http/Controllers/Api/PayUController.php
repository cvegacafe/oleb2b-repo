<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PayuSignatureRequest;
use App\Library\Constants;
use App\Services\MembershipService;
use App\Services\OrderService;
use App\Services\PayUService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PayUController extends Controller
{
    public function getSignature(
        PayuSignatureRequest $request,
        PayUService $payUService,
        MembershipService $membershipService,
        OrderService $orderService
    ) {
        try {
            $membership = $membershipService->findByLang($request->get('membership_id'),$request->get('lang'));
            $currency = 'USD';
            $frequency = $request->get('frequency');

            if ($frequency == 'm') {
                $amount = $membership->monthly_price;
                $referenceCode = uniqid('#'.Auth::id().'M-');
                $description = 'Buy Membership '.$membership->name;
            } elseif ($frequency == 'a') {
                $amount = $membership->annual_price;
                $referenceCode = uniqid('#'.Auth::id().'Y-');
                $description = 'Buy Membership '.$membership->name;
            } else {
                return response()->json(['response' => 'error', 'msg' => 'Unknown Frequency'], 500);
            }

            $signature = $payUService->getSignaturePayU($referenceCode, $amount,$currency);

            $newOrder = [
                'signature'=>$signature,
                'reference_code'=>$referenceCode,
                'user_id'=>Auth::id(),
                'membership_id'=>$membership->id,
                'frequency'=>$frequency,
                'amount'=>$amount,
                'description'=>$description,
                'status'=>Constants::ORDER_STATUS_PENDING
            ];

            $order = $orderService->create($newOrder);

            if($order){
                return response()->json([
                    'signature' => $signature,
                    'referenceCode'=>$referenceCode,
                    'amount' => $amount,
                    'currency'=>$currency,
                    'description'=>$description
                ]);
            }

            return response()->json(['response' => 'error', 'msg' => 'Error Saving Order'], 500);

        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['response' => 'error', 'msg' => 'internal error'], 500);
        }
    }
}
