<?php namespace App\Repositories\Market;

use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Market extends Model
{
    public $table = 'markets';
    public $fillable = ['en_name', 'es_name', 'ru_name'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'market_user', 'user_id', 'market_id')->withTimestamps();
    }

    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }
}
