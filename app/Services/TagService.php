<?php namespace App\Services;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:20 PM
 */
use App\Repositories\Tag\TagRepository;

/**
 * Class TagService
 * @package App\Services
 */
class TagService extends BaseService
{

    /**
     * TagService constructor.
     */
    public function __construct()
    {
        $this->mainRepo = new TagRepository();
        parent::__construct();
    }

    /**
     * @param $tagName
     * @return \App\Repositories\Tag\Tag
     */
    public function findOrCreate($tagName)
    {
        return $this->mainRepo->findOrCreate($tagName);
    }
}