<?php namespace App\Http\Controllers;

use App\Repositories\Product\Product;
use App\Services\ProductService;
use Watson\Sitemap\Facades\Sitemap;


/**
 * @property ProductService productService
 */
class SitemapController extends Controller
{
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        Sitemap::addSitemap(route('sitemap.products'));
        Sitemap::addSitemap(route('sitemap.statics'));

        return Sitemap::index();
    }

    /**
     * @return mixed
     */
    public function products()
    {
        $products = $this->productService->all()->where('is_approved', '=', 1);
        foreach ($products as $product) {
            Sitemap::addTag(route('portal.singleProduct', $product->es_slug), $product->updated_at, 'daily', '0.8');
        }
        return Sitemap::render();
    }

    /**
     * @return mixed
     */
    public function statics()
    {
        Sitemap::addTag(route('portal.categories'));
        Sitemap::addTag(route('portal.buyingRequests'));
        Sitemap::addTag(route('portal.contact'));
        Sitemap::addTag(route('portal.memberships'));
        Sitemap::addTag(route('web.about'));

        return Sitemap::render();
    }
}
