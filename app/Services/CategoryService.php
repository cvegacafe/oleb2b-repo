<?php namespace App\Services;

use App\Repositories\Category\CategoryRepository;
use Illuminate\Support\Facades\Lang;

/**
 * Class CategoryService
 * @package App\Services
 */
class CategoryService extends BaseService
{

    /**
     * CategoryService constructor.
     * @param CategoryRepository $categoryRepository
     * @throws \Exception
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->mainRepo = $categoryRepository;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $categories = $this->mainRepo->all();
    }

    /**
     * @param $supercategoryId
     * @return mixed
     */
    public function getBySupercategoryId($supercategoryId)
    {
        return $this->mainRepo->getByColumn('supercategory_id', $supercategoryId);
    }

    /**
     * @deprecated
     * @param $q
     * @param $subcategoryId
     * @param $countryId
     * @param $categoryId
     * @return mixed
     */
    public function getCategoriesBySearch($q,$subcategoryId,$countryId,$categoryId)
    {
        return $this->mainRepo->getCategoriesBySearch(Lang::locale(),$q,$subcategoryId,$countryId,$categoryId);
    }

    /**
     * @deprecated
     * @param array $subcategoriesIds
     * @return mixed
     */
    public function getCategoriesBySubCategoryIds(array $subcategoriesIds)
    {
        return $this->mainRepo->getCategoriesBySubCategoriesIds($subcategoriesIds);
    }
}