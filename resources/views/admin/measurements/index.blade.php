@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Measurements :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Esp</th>
                            <th>Ing</th>
                            <th>Rus</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($measurements as $measurement)
                        <tr>
                            <td><b>{{$measurement->id}}</b></td>
                            <td>{{$measurement->es_name}}</td>
                            <td>{{$measurement->en_name}}</td>
                            <td>{{$measurement->ru_name}}</td>
                            <td>
                                <a href="{{action('Admin\MeasurementsController@edit',$measurement)}}"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="fa fa-pencil"></span></button></a>
                                <form action="{{action('Admin\MeasurementsController@destroy',$measurement)}}" method="POST" onsubmit="return confirm('Esta seguro q desea eliminar {{$measurement->es_name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs" ><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <form action="{{action('Admin\MeasurementsController@store')}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="text" name="es_name" class="form-control" placeholder="Medida en espanol"></td>
                            <td><input type="text" name="en_name" class="form-control" placeholder="Medida en ingles"></td>
                            <td><input type="text" name="ru_name" class="form-control" placeholder="Medida en ruso"></td>
                            <td colspan="2"><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
                        </tr>
                        </tfoot>
                    </form>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection