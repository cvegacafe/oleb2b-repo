<?php namespace App\Repositories\Payu;

use App\Repositories\BaseRepository;

class PayuRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Payu();
        parent::__construct();
    }
}