<?php

use App\Repositories\PaymentTerm\PaymentTerm;
use Illuminate\Database\Seeder;

class PaymentTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentTerm::create(['id'=>1,'name'=>'TT']);
        PaymentTerm::create(['id'=>2,'name'=>'LC']);
        PaymentTerm::create(['id'=>3,'name'=>'Western Union']);
        PaymentTerm::create(['id'=>4,'name'=>'CAD']);
        PaymentTerm::create(['id'=>999,'name'=>'Other']);
    }
}
