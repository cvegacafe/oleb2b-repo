<?php namespace App\Repositories\Attachment;

use App\Repositories\Message\Message;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    public $fillable = ['message_id', 'path', 'type', 'name'];

    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}
