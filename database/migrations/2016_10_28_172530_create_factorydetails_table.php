<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactorydetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factorydetails', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('supplier_id')->unsigned()->unique();
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->string('factory_address');
            $table->string('factory_size');
            $table->string('number_qc_staff');
            $table->string('number_rd_staff');
            $table->string('number_production_lines');
            $table->string('annual_output_value');
            $table->integer('measure_id')->unsigned();
            $table->foreign('measure_id')->references('id')->on('measurements');
            $table->string('annual_turnover')->nullable();
            $table->boolean('design_service_offered')->default(false);
            $table->boolean('oem_service_offered')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factorydetails');
    }
}
