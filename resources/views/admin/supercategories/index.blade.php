@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">SuperCategorias :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Espanol</th>
                        <th>Ingles</th>
                        <th>Ruso</th>
                        <th>Subcategorias</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($supercategories as $supercategory)
                        <tr>
                            <td><b>{{$supercategory->id}}</b></td>
                            <td>{{$supercategory->es_name}}
                                <form action="{{action('Admin\SupercategoriesController@update',[$supercategory])}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$supercategory->es_name}}','{{$supercategory->id}}_es_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="es_name" id="{{$supercategory->id}}_es_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$supercategory->en_name}}
                                <form action="{{action('Admin\SupercategoriesController@update',[$supercategory])}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$supercategory->en_name}}','{{$supercategory->id}}_en_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="en_name" id="{{$supercategory->id}}_en_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$supercategory->ru_name}}
                                <form action="{{action('Admin\SupercategoriesController@update',[$supercategory])}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$supercategory->ru_name}}','{{$supercategory->id}}_ru_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="ru_name" id="{{$supercategory->id}}_ru_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$supercategory->categories->count()}}</td>
                            <td>
                                <a href="{{action('Admin\SupercategoriesController@show',$supercategory)}}">
                                    <button class="btn btn-primary btn-xs" title="Ver"><span class="fa fa-list"></span>
                                    </button>
                                </a>
                                <form action="{{action('Admin\SupercategoriesController@destroy',$supercategory)}}"
                                      method="POST"
                                      onsubmit="return confirm('Esta seguro q desea eliminar {{$supercategory->es_name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs"><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <form action="{{action('Admin\SupercategoriesController@store')}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="text" name="es_name" class="form-control"
                                       placeholder="Categoria en espanol"></td>
                            <td><input type="text" name="en_name" class="form-control"
                                       placeholder="Categoria en ingles"></td>
                            <td><input type="text" name="ru_name" class="form-control" placeholder="Categoria en ruso">
                            </td>
                            <td colspan="2">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                            </td>
                        </tr>
                        </tfoot>
                    </form>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function cambiarValor(value, element) {
            var newvalue = prompt("Ingrese Nombre", value);
            if (newvalue != null) {
                console.log(element);
                $('#'+element).val(newvalue);
                return true;
            } else {
                return false;
            }
        }
    </script>
@endsection