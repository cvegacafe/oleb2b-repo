<?php namespace App\Services;

use App\Repositories\Slider\SliderRepository;
use Illuminate\Support\Facades\Lang;

/**
 * Class SliderService
 * @package App\Services
 */
class SliderService extends BaseService
{

    /**
     * SliderService constructor.
     */
    public function __construct()
    {
        $this->mainRepo = new SliderRepository();
        parent::__construct();
    }

    /**
     * @param int $max
     * @return mixed
     */
    public function getByLang(int $max)
    {
        return $this->mainRepo->getSliders($max, Lang::locale());
    }

}