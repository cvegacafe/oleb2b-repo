<?php namespace App\Repositories\FactoryDetail;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:10 PM
 */


use App\Repositories\BaseRepository;

class FactoryDetailRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new FactoryDetail();
        parent::__construct();
    }
}