<?php
return [
    'basic' => 'BASIC',
    'advanced' => 'ADVANCED',
    'premium' => 'PREMIUM',


    'cost' => 'COST',
    'product_posting' => 'PRODUCT POSTING',
    'banner_main_page' => "BANNER ON THE MAIN PAGE*",
    'banner_inner_pages' => "BANNER ON THE INNER PAGE**",
    'featured_products' => "FEATURED PRODUCTS",
    'ability_quote_buying_request' => "ABILITY TO QUOTE BUYING REQUESTS",
    'subaccounts' => 'SUB ACCOUNTS***',

    'disclaimer1'=>'Disclaimer Promotions: This promotion is valid for annual subscription only. The total amount will be charged at the beginning of your membership. The duration of membership is 12 months + 3 FREE months. No refund available for promotions.',

    'choose_plan' => 'Choose Plan',
    'active' => 'Active',
    'unlimited' => 'Unlimited',

    'average_rotation' => 'Average rotation time :time min/month',
    'cond_1' => '*THE BANNERS WILL BE DESIGNED BY OLEBABA.COM S.A.C. CREATIVE AGENCY, 4 BANNERS PER YEAR: ROTATION EVERY 3 MONTHS. FIRST BANNER WILL BE ONLINE WITHIN 72 HOURS AFTER THE MEMBERSHIP IS CONFIRMED ',
    'cond_2' => '**THE BANNERS WILL BE DESIGNED BY  OLEBABA.COM S.A.C CREATIVE AGENCY, 4 BANNERS PER YEAR: ROTATION EVERY 3 MONTHS. FIRST BANNER WILL BE ONLINE WITHIN 72 HOURS AFTER THE MEMBERSHIP IS CONFIRMED ',
    'cond_3' => '***THE FUNCTION ALLOWS YOU TO PROVIDE SEVERAL CONTACTS DEPENDING ON THE PRODUCT AND COUNTRY 	 '
];