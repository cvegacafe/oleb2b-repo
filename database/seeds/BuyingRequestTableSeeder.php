<?php

use App\Repositories\BuyingRequest\BuyingRequest;
use Illuminate\Database\Seeder;

class BuyingRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Repositories\User\User::where('email','supplier@oleb2b.com')->first();
        $subcategory = \App\Repositories\Subcategory\Subcategory::all();
        $countries = \App\Repositories\Country\Country::all();
        $measurements = \App\Repositories\Measure\Measure::all();
        $paymentTerms = \App\Repositories\PaymentTerm\PaymentTerm::all();
        $languages = \App\Repositories\Language\Language::all();
        factory(BuyingRequest::class,10)->create([
            'user_id'=>$user->id,
            'subcategory_id'=>$subcategory->random()->id,
            'country_id'=>$countries->random()->id,
            'measure_id'=>$measurements->random()->id,
            'is_approved'=>true

        ])->each(function($buyingRequest)use($languages,$paymentTerms){
            $buyingRequest->paymentTerms()->saveMany($paymentTerms->random(2));
            $buyingRequest->languages()->saveMany($languages->random(2));
        });
    }
}
