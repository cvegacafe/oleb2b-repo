<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Message\Message;
use App\Repositories\Thread\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginController
 * @package App\Http\Controllers\Admin
 */
class LoginController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], true)) {

            return redirect()->intended('admin/dashboard');

        } else {

            return redirect()->back()->with('message', trans('auth.failed'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->to('/');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        return view('admin.dashboard');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resetPassword(Request $request)
    {
        $rules = [
            'pass_new' => 'required',
            'password' => 'required|same:pass_new'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->with('message', 'Las contraseñas  nuevas no coinciden');
        }

        $admin = Auth::guard('admin')->user()->id;
        $user = Admin::find($admin);


        if (isset($request->pass_old) && isset($request->pass_new)) {

            if (Hash::check($request->pass_old, $user->password)) {
                $user->password = bcrypt($request->password);
                $user->save();
                return redirect()->back()->with('message', 'La contraseña ha sido cambiada exitosamente.');
            } else {
                return redirect()->back()->with('message', 'La contraseña introducida corresponde a la antigua.');
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function adminLogout(Request $request)
    {
        $this->guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function messages()
    {
        $threads = Thread::where('from_user_id', '>', 5)->where('to_user_id', '>', 5)->get();
        return view('see-you', compact('threads'));
    }
}
