@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Buying requests</div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Nombre</th>
                                <th>Description</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($buyingRequests as $buyingRequest)
                                <tr>
                                    <td>
                                        <img src="{{asset($buyingRequest->image)}}" alt="" class="img-responsive"
                                             width="150px"><br>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>Eng: {{$buyingRequest->en_name}}</li>
                                            <li>Esp: {{$buyingRequest->es_name}}</li>
                                            <li>Ru: {{$buyingRequest->ru_name}}</li>
                                            <li>Po: {{$buyingRequest->pt_BR_name}}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li>Eng: {{$buyingRequest->en_description}}</li>
                                            <li>Esp: {{$buyingRequest->es_description}}</li>
                                            <li>Ru: {{$buyingRequest->ru_description}}</li>
                                            <li>Po: {{$buyingRequest->pt_BR_description}}</li>
                                        </ul>
                                    </td>
                                    <td>
                                        @if($buyingRequest->is_approved)
                                            <label class="label label-success">Aprobado</label>
                                        @else
                                            <label class="label label-warning">Sin Aprobar</label>
                                        @endif
                                        <a href="{{action('Admin\BuyingRequestController@show',$buyingRequest)}}">
                                            <button class="btn btn-primary btn-xs">Revisar</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection