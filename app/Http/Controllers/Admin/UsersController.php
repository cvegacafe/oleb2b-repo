<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\User\User;
use App\Services\CountryService;
use App\Services\FileService;
use App\Services\LanguageService;
use App\Services\MembershipService;
use App\Services\UserService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;


class UsersController extends Controller
{
  private $userService;

  public function __construct(UserService $userService)
  {
    $this->userService = $userService;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = $this->userService->paginate(10, 'DESC', ['country', 'memberships']);
    return view('admin.users.index', compact('users'));
  }

  public function allUsers()
  {
    return Datatables::of(User::with(['country', 'memberships'])->get())
//      ->order(function ($query) {
////        dd($query);
//        $query->orderBy('id', 'asc');
//      })
      ->addColumn('action', function ($user) {
        return '<a href="' . action('Admin\UsersController@show', $user) . '" class="btn btn-xs btn-primary"> Ver</a>';
      })->make(true);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.users.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param User $user
   * @param MembershipService $membershipService
   * @return \Illuminate\Http\Response
   */
  public function show(User $user, MembershipService $membershipService)
  {
    $lastMembership = $user->activeMembership();
    $memberships = $membershipService->all();
    return view('admin.users.show', compact('user', 'lastMembership', 'memberships'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param User $user
   * @param CountryService $countryService
   * @param LanguageService $languageService
   * @param MembershipService $membershipService
   * @return \Illuminate\Http\Response
   */
  public function edit(
    User $user,
    CountryService $countryService,
    LanguageService $languageService,
    MembershipService $membershipService
  )
  {
    $countries = $countryService->all();
    $languages = $languageService->all();
    $lastMembership = $user->activeMembership();
    $memberships = $membershipService->all();

    return view('admin.users.edit', compact('user', 'lastMembership', 'countries', 'languages', 'memberships'));
  }

  /**
   * @param Request $request
   * @param User $user
   * @param FileService $fileService
   * @param MembershipService $membershipService
   * @return $this|\Illuminate\Http\RedirectResponse
   */
  public function update(Request $request, User $user, FileService $fileService, MembershipService $membershipService)
  {
    $rules = [
      "names" => "required|max:80",
      "last_names" => "required|max:80",
      "country_id" => "required|exists:countries,id",
      "office_phone" => "required|max:25",
      "mobile_phone" => "required|max:25",
      "address" => "required|max:200",
      "city" => "required|max:60",
      "postal_code" => "required|max:20",
      "languages" => "required|existsArray:languages,id",
      "other_languages" => "max:100",
      "company" => "max:250",
      'membership_id' => "required|exists:memberships,id",
      'membership_product_posting' => 'required|max:100000',
      'membership_amount' => 'required|max:10000000',
      'membership_frequency' => 'required|in:A,M',
      'membership_created_at' => 'required',
      'membership_finish_at' => 'required',
    ];

    if ($request->exists('change_email')) {
      $rules = array_merge($rules, ["email" => 'email|unique:users,email']);
    }

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)
        ->withInput();
    }

    if ($request->exists('remove_image')) {
      $fileService->removeFile($user->image);
    }

    $user->fill($request->all());
    $user->save();

    $pivotMembership = [
      'membership_id' => $request->get('membership_id'),
      'product_posting' => $request->get('membership_product_posting'),
      'amount' => $request->get('membership_amount'),
      'frequency' => $request->get('membership_frequency') == 'A' ? 'A' : 'M',
      'created_at' => $request->get('membership_created_at'),
      'finish_at' => $request->get('membership_finish_at'),
      'is_active' => true
    ];

    $membershipService->updateLastUserMembershipPivotRow($pivotMembership, $user);
    Aviso::actualizado();

    return redirect()->action('Admin\UsersController@show', $user);
  }

  /**
   * add membership to the specified user.
   *
   * @param  \Illuminate\Http\Request $request
   * @param User $user
   * @param MembershipService $membershipService
   * @return \Illuminate\Http\Response
   */
  public function addMembershipToUser(Request $request, User $user, MembershipService $membershipService)
  {
    try {
      $selectedMembership = $membershipService->find($request->get('membership_id'));
      $membershipService->updateLastUserMembershipPivotRow([
        'finish_at' => Carbon::now()->subHour(),
        'is_active' => false
      ], $user);

      $user->memberships()->attach($selectedMembership->id, [
        'main_rotation_banners' => $selectedMembership->main_rotation_banners,
        'inner_rotation_banners' => $selectedMembership->inner_rotation_banners,
        'product_posting' => $request->get('product_posting'),
        'amount' => $request->get('amount'),
        'frequency' => $request->get('frequency') == 'A' ? 'A' : 'M',
        'created_at' => Carbon::parse($request->get('created_at'))->toDateTimeString(),
        'finish_at' => Carbon::parse($request->get('finish_at'))->toDateTimeString(),
        'is_active' => true
      ]);
      Aviso::actualizado();
    } catch (Exception $e) {
      Log::error($e);
      Aviso::noActualizado();

    }

    return redirect()->action('Admin\UsersController@show', $user);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
