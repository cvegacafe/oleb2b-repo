<?php

return [
    'my_products' => 'ТОВАРЫ',
    'complete_in' => 'Заполнить на русском',
    'complete' => 'Заполнено',
    'incomplete' => 'Не заполнено',
    'form' => [
        'first_name' => 'Имя',
        'last_names' => 'Фамилия',
        'password' => 'Пароль',
        'confirm_password' => 'Подтвердить Пароль',
        'email_address' => 'Имейл',
        'country' => 'Страна',
        'office_phone' => 'Номер Тел.  (+7 123 4567)',
        'mobile_phone' => 'Мобильный (+7 123 4567)',
        'address' => 'Адрес',
        'city' => 'Город',
        'postal_code' => 'Почт.индекс',
        'languages' => 'Языки',
        'others_languages' => 'Другие языки',
        'company' => 'Название Компании'
    ],
    'can_sell_buy' => 'Для поставщиков/покупателей',
    'can_buy' => 'Для покупателей',
    'update_image' => 'Добавить фото/аватар ',
    'button-add-photos' => 'Добавить логотип/фото',
    'exporter_manual' => 'Pуководство экспортера'
];
