<?php namespace App\Repositories\Country;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class CountryRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Country();
        parent::__construct();
    }

    public function all()
    {
        return $this->model->with('language')->get();
    }

    public function getCountriesFluent($lang)
    {
        return DB::table('countries')
            ->select('id',$lang.'_name as name','enabled_for_supplier','abbreviation')
            ->get();
    }

    public function getCountriesBySearch(
        $lang_prefix,
        $q = null,
        $subcategoryId = null,
        $countryId = null
    ) {
        return $this->model->whereHas('products',function($query)use ($q, $subcategoryId, $countryId, $lang_prefix){

            if ($q != null) {
                $query->where($lang_prefix . '_name', 'like', '%' . $q . '%');
            }

            if ($subcategoryId != null) {
                $query->where('subcategory_id', $subcategoryId);
            }

            if ($countryId != null) {
                $query->where('country_id', $countryId);
            }
            $query->where('is_approved', true);
        })->get();
    }
}