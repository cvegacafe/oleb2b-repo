<div class="row single-request {{ !$product->is_approved ? 'inhabilitado':''}}">
    @if(!$product->is_approved)
    <div class="col-xs-12">
        <div class="inhabilitado-msg">
            <strong>NO PUBLICADO</strong>
        </div>
    </div>
    @endif
    <div class="col-xs-12 col-sm-4 col-lg-2">
        <a href="{{route('portal.singleProduct',$product->slug)}}" target="_blank">
            <figure>
                <img src="{{asset($product->main_photo)}}" alt="">
            </figure>
        </a>
    </div>
    <div class="col-xs-12 col-sm-4 product">
        <ul>
            <li class="name"><a href="{{route('portal.singleProduct',$product->slug)}}"  target="_blank">{{$product->name}}</a></li>
            <li>{{$product->description}}</li>
            <li><strong>{{trans('products.place_origin')}}: </strong> {{$product->country->name}}</li>
            <li><strong>{{trans('products.moq')}}:</strong> {{$product->moq}} {{$product->moqMeasure->name}}</li>
            <li><strong>{{trans('products.price')}}:</strong>
                @if($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_FIXED)
                    {{$product->price}}
                    {{$product->currency->currency_code}} / {{$product->saleUnitMeasure->name}}
                @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_ASK)
                    {{trans('priceTypes.ask_seller')}}
                @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_RANGE)
                   {{$product->price_range}}
                    {{$product->currency->currency_code}} / {{$product->saleUnitMeasure->name}}
                @endif
                <br>
                </li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-4 col-lg-3 supplier">
        <button data-toggle="collapse" data-target="#datos-empresa">{{trans('products.buyer_info')}} <i class="fa fa-caret-down"></i></button>
        <ul id="datos-empresa" class="collapse">
            <li class="name">{{$product->user->supplier->company_name}}</li>
            <li> {{$product->user->supplier->country->name}}</li>
            @if($product->user->activeMembership())
            <li><strong>{{trans('products.membership')}}: {{$product->user->activeMembership()->name}} </strong></li>
            @endif
            @if($product->is_organic)
                <li><img src="{{asset('assets/pages/img/organic_vf.png')}}" alt="" width="100px"></li>
                @endif
        </ul>
    </div>
    @if(isset($button1['text']) && $button2['text'])
    <div class="col-xs-12 col-lg-3 buttons" >
        <button class="btn-block {{$button1['class']}}"  onclick="{{isset($button1['onClick'])?$button1['onClick']:''}}" >{{$button1['text']}}</button>
        <button class="btn-block {{$button2['class']}}"  onclick="{{isset($button2['onClick'])?$button2['onClick']:''}}">{{$button2['text']}}</button>
    </div>
    @endif
    @if(isset($link1['text']) && $link2['text'])
        <div class="col-xs-12 col-lg-3 buttons" >
            <a href="{{$link1['url']}}" class="btn-block text-center contact">{{$link1['text']}}</a>
            <form action="{{$deleteForm['action']}}" method="POST" class="sweet-confirm"
                  data-title="{{trans('alerts.products.title_delete')}}"
                  data-message="{{trans('alerts.products.message_confirm',['name'=>$deleteForm['product_name']])}}"
                  data-ok-button="{{trans('common.forms.buttons.delete')}}"
                  data-cancel-button="{{trans('common.forms.buttons.cancel')}}">
                <input type="hidden" name="_method" value="DELETE">
                {{csrf_field()}}
                <button class="btn-block text-center save" >{{$deleteForm['button']}}</button>
            </form>
        </div>
    @endif
</div>