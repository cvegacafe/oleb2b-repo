@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                {{trans('menus.user.manage_account')}}
                            </div>
                            <div class="col-xs-12 col-md-6 text-right">
                                <a href="#" class="boton-azul-no-shadow toggle-edit"
                                   data-form-edit="#administrar-cuenta-form">{{trans('common.forms.buttons.edit')}} <i
                                            class="fa fa-edit"></i></a>
                                <a href="#" class="boton-azul-no-shadow toggle-edit"
                                   data-form-edit="#change-password-form">{{trans('passwords.button_change_password')}} <i
                                            class="fa fa-eye"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content" id="change-password-form">
                        <div class="tab-pane show-form active">
                            <div class="tab-content" id="administrar-cuenta-form">
                                <div class="tab-pane active panel-body show-form">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <dl class="dl-horizontal">
                                                <dt>{{trans('user.form.first_name')}}:</dt>
                                                <dd>{{$user->names}}</dd>

                                                <dt>{{trans('user.form.last_names')}}:</dt>
                                                <dd>{{$user->last_names}}</dd>

                                                <dt>{{trans('user.form.email_address')}}:</dt>
                                                <dd>{{$user->email}}</dd>

                                                <dt>{{trans('user.form.country')}}:</dt>
                                                <dd>{{$user->country->name}}</dd>

                                                <dt>{{trans('user.form.company')}}:</dt>
                                                <dd>{{$user->company}}</dd>

                                                <dt>{{trans('user.form.office_phone')}}:</dt>
                                                <dd>{{$user->office_phone}}</dd>

                                                <dt>{{trans('user.form.mobile_phone')}}:</dt>
                                                <dd>{{$user->mobile_phone}}</dd>

                                                <dt>{{trans('user.form.address')}}:</dt>
                                                <dd>{{$user->address}}</dd>

                                                <dt>{{trans('user.form.city')}}:</dt>
                                                <dd>{{$user->city}}</dd>

                                                <dt>{{trans('user.form.postal_code')}}:</dt>
                                                <dd>{{$user->postal_code}}</dd>

                                                <dt>{{trans('user.form.languages')}}:</dt>
                                                <dd>{{$user->languages->implode('name',', ')}}</dd>

                                                <dt>{{trans('user.form.others_languages')}}:</dt>
                                                <dd>{{$user->other_languages}}</dd>

                                                {{-- <dt>{{trans('subaccounts.markets')}}: </dt>
                                                 <dd>{{$user->markets->implode('name',', ')}}</dd>--}}
                                            </dl>
                                        </div>
                                        <div class="col-xs-12 col-md-6 text-center">
                                            <img src="{{asset($user->image)}}" width="200px">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane panel-body edit-form">
                                    <form class="" role="form" method="POST"
                                          action="{{ route('account.manage.update',$user) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                                                    <label for="name"
                                                           class=" control-label">{{trans('user.form.first_name')}}</label>
                                                    <input id="name" type="text" class="form-control" name="names"
                                                           value="{{$user->names}}" required autofocus>
                                                    @if ($errors->has('names'))
                                                        <span class="help-block">
                                                <strong>{{ $errors->first('names') }}</strong>
                                            </span>
                                                    @endif
                                                </div>
                                                <div class="form-group{{ $errors->has('last_names') ? ' has-error' : '' }}">
                                                    <label for="last_names"
                                                           class="control-label">{{trans('user.form.last_names')}}</label>
                                                    <div class="">
                                                        <input id="last_names" type="text" class="form-control"
                                                               name="last_names" value="{{$user->last_names}}" required
                                                               autofocus>
                                                        @if ($errors->has('last_names'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('last_names') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="email"
                                                           class="control-label">{{trans('user.form.email_address')}}</label>
                                                    <div class="">
                                                        <input id="email" type="email" class="form-control" name="email"
                                                               value="{{$user->email}}" required>
                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                                    <label for="country_id"
                                                           class="control-label">{{trans('user.form.country')}}</label>
                                                    <div class="">
                                                        <select name="country_id" id="country_id"
                                                                class="form-control required">
                                                            @foreach($countries as $country)
                                                                @if(isset($user) && $user->country_id==$country->id)
                                                                    <option value="{{$country->id}}"
                                                                            selected>{{$country->name}}</option>
                                                                @else
                                                                    <option value="{{$country->id}}" {{$country->id==old('country_id')?'selected':''}}>{{$country->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('country_id'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('country_id') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                                    <label for="company"
                                                           class="control-label">{{trans('user.form.company')}}</label>
                                                    <div class="">
                                                        <input id="company" type="text" class="form-control" name="company"
                                                               value="{{$user->company}}" required>
                                                        @if ($errors->has('company'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('company') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                                    <label for="office_phone"
                                                           class="control-label">{{trans('user.form.office_phone')}}</label>
                                                    <div class="">
                                                        <input id="office_phone" type="text" class="form-control"
                                                               name="office_phone" value="{{$user->office_phone}}" required>
                                                        @if ($errors->has('office_phone'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('office_phone') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                                                    <label for="mobile_phone"
                                                           class="control-label">{{trans('user.form.mobile_phone')}}</label>
                                                    <div class="">
                                                        <input id="mobile_phone" type="text" class="form-control"
                                                               name="mobile_phone" value="{{$user->mobile_phone}}" required>

                                                        @if ($errors->has('mobile_phone'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('mobile_phone') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                {{--  <div class="form-group{{ $errors->has('markets') ? ' has-error' : '' }}">
                                                      <div class="">
                                                          <label for="postal_code" class="control-label">{{trans('subaccounts.markets')}}</label>
                                                          <div class="checkbox-list">
                                                              @foreach($markets as $market)
                                                                  <label class="checkbox-inline">
                                                                      @if(isset($user) && $user->markets->contains($market->id))
                                                                          <input id="inlineCheckbox{{$market->id}}" checked type="checkbox" name="markets[]" value="{{$market->id}}"/>&nbsp;{{$market->name}}
                                                                      @else
                                                                          <input id="inlineCheckbox{{$market->id}}" {{in_array($market->id,old('markets',[]))?'checked':''}} type="checkbox" name="markets[]" value="{{$market->id}}"/>&nbsp;{{$market->name}}
                                                                      @endif
                                                                  </label>
                                                              @endforeach
                                                          </div>
                                                      </div>
                                                  </div>--}}
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                    <label for="address"
                                                           class="control-label">{{trans('user.form.address')}}</label>
                                                    <div class="">
                                                        <input id="address" type="text" class="form-control" name="address"
                                                               value="{{$user->address}}" required>
                                                        @if ($errors->has('address'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                                    <label for="city"
                                                           class="control-label">{{trans('user.form.city')}}</label>
                                                    <div class="">
                                                        <input id="city" type="text" class="form-control" name="city"
                                                               value="{{$user->city}}" required>
                                                        @if ($errors->has('city'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                                    <label for="postal_code"
                                                           class="control-label">{{trans('user.form.postal_code')}}</label>
                                                    <div class="">
                                                        <input id="postal_code" type="text" class="form-control"
                                                               name="postal_code" value="{{$user->postal_code}}" required>
                                                        @if ($errors->has('postal_code'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('postal_code') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                                    <label for="postal_code"
                                                           class="control-label">{{trans('user.form.languages')}}</label>
                                                    <div class="">
                                                        <div class="checkbox-list">
                                                            @foreach($languages as $language)
                                                                <label class="checkbox-inline">
                                                                    @if(isset($user) && $user->languages->contains($language->id))
                                                                        <input id="inlineCheckbox{{$language->id}}" checked
                                                                               type="checkbox" name="languages[]"
                                                                               value="{{$language->id}}"/>
                                                                        &nbsp;{{$language->name}}
                                                                    @else
                                                                        <input id="inlineCheckbox{{$language->id}}"
                                                                               {{in_array($language->id,old('languages',[]))?'checked':''}} type="checkbox"
                                                                               name="languages[]" value="{{$language->id}}"/>
                                                                        &nbsp;{{$language->name}}
                                                                    @endif
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('other_languages') ? ' has-error' : '' }}">
                                                    <label for="other_languages"
                                                           class="control-label">{{trans('user.form.others_languages')}}</label>
                                                    <div class="">
                                                        <input id="other_languages" type="text" class="form-control"
                                                               name="other_languages" value="{{$user->other_languages}}">
                                                        @if ($errors->has('other_languages'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('other_languages') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                                                    {{--<label for="image" class="control-label">Actualizar Imagen/Foto/Avatar</label>--}}
                                                    {{--<img src="{{asset($user->image)}}" alt="" class="img-responsive" width="200px">--}}
                                                    {{--<div class="">--}}
                                                    {{--<input id="image" type="file" class="form-control" name="avatar" >--}}
                                                    {{--@if ($errors->has('avatar'))--}}
                                                    {{--<span class="help-block">--}}
                                                    {{--<strong>{{ $errors->first('avatar') }}</strong>--}}
                                                    {{--</span>--}}
                                                    {{--@endif--}}
                                                    {{--</div>--}}
                                                    <div id="filesContainer">
                                                        <button id="seleccionar-fotos" type="button"
                                                                class="boton-azul-no-shadow"><i
                                                                    class="fa fa-plus"></i> {{ trans('user.button-add-photos') }}
                                                        </button>
                                                        <div class="preview-files">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group text-right">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{trans('common.forms.buttons.save')}}
                                                        <i class="fa fa-angle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane edit-form">
                            <div class="tab-pane active panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6 text-center">
                                        <form class="form-horizontal" role="form" method="POST"
                                              action="{{ route('account.manage.change-password') }}">
                                            {{ csrf_field() }}
                                            <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                                <div class="col-xs-12">
                                                    <div class="sub active">
                                                        <input id="old_password" type="password" name="old_password"
                                                               placeholder="{{trans('passwords.old_password_input')}}"
                                                               required autofocus class="form-control">
                                                        @if ($errors->has('old_password'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('old_password') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                                <div class="col-xs-12">
                                                    <div class="sub">
                                                        <input id="new_password" type="password"
                                                               placeholder="{{trans('passwords.new_password_input')}}"
                                                               name="new_password" required class="form-control">
                                                        @if ($errors->has('new_password'))
                                                            <span class="help-block">
                                                        <strong>{{ $errors->first('new_password') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                                                <div class="col-xs-12">
                                                    <div class="sub">
                                                        <input id="new_password_confirmation" type="password"
                                                               placeholder="{{trans('passwords.new_password_input_confirmation')}}"
                                                               name="new_password_confirmation" class="form-control"
                                                               required>
                                                        @if ($errors->has('new_password_confirmation'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{trans('passwords.button_change_password')}}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane panel-body edit-form">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var position = 0;

        $('#seleccionar-fotos').on('click', function () {
            $file = $('.file' + position);
            if ($file.val()) {
                $file.click();
            } else {
                position++;
                appendInputFile(position)
                $('.file' + position).click();
            }
        });

        $('.preview-files').on('mouseenter', 'div span i', function () {
            $(this).parent().parent().find('figure').fadeIn();
        }).on('mouseleave', 'div', function () {
            $(this).parent().parent().find('figure').fadeOut();
        });

        function previewFile(element) {
            var $this = $(element);
            //console.log(position);

            if (validateFile($this)) {
                var fileName = getFileName($this.val());

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.preview-files').append('<div class="file' + position + '"> <span><i class="fa fa-picture-o"></i><small>' + fileName + '</small></span> <figure><img src="' + e.target.result + '"></figure> <div class="close" onclick="removeFile(' + position + ')"><i class="fa fa-close"></i></div> </div>');
                };

                reader.readAsDataURL(element.files[0]);
                if (!position === 1) {
                    position++;
                    appendInputFile(position)
                }
            }
        }

        function appendInputFile(position) {
            $('#filesContainer').append($('<input/>').attr('type', 'file').attr('name', 'avatar').addClass('input-file file' + position).attr('onchange', "previewFile(this, " + position + ")"));
        }

        function validateFile(input) {

            var extencionesAceptadas = [
                'jpeg',
                'jpg',
                'png',
                'gif',
                'bmp'
            ];

            var maxSize = 2000;

            var maxCount = 1;

            //console.log((1024 * maxSize),input[0].files[0].size);

            if (input[0].files[0].size > (1024 * maxSize)) {
                //TODO: alerta de imagen muy grande
                alert('imagen muy grande');
                return false;
            }

            if ($('#filesContainer input').length > maxCount) {
                //TODO: maximo de imagenes permitido
                alert('maximo de imagenes permitido');
                return false;
            }

            var fileExtension = input.val().split('.').pop();

            if (!extencionesAceptadas.includes(fileExtension)) {
                input.val('');
                alert('tipo de archivo no permitido');
                return false;
            }

            return true;
        }

        function removeOldFile(file) {
            var $this = $(file);
            var position = $this.data('position');
            $('#filesContainer').append('<input type="hidden" name="filesToRemove[]" value="' + position + '">');
            $this.parent().remove();
        }

        function removeFile(position) {
            $('.file' + position).remove();
        }

        function getFileName(fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            return filename;
        }
    </script>
@endsection