<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\Supercategory\Supercategory;
use App\Services\LanguageService;
use App\Services\SupercategoryService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class SupercategoriesController
 * @package App\Http\Controllers\Admin
 */
class SupercategoriesController extends Controller
{
    /**
     * @var SupercategoryService
     */
    /**
     * @var LanguageService|SupercategoryService
     */
    private $supercategoryService, $languageService;

    /**
     * SupercategoriesController constructor.
     * @param SupercategoryService $supercategoryService
     * @param LanguageService $languageService
     */
    public function __construct(SupercategoryService $supercategoryService, LanguageService $languageService)
    {
        $this->supercategoryService = $supercategoryService;
        $this->languageService = $languageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supercategories = $this->supercategoryService->all();
        return view('admin.supercategories.index', compact('supercategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->supercategoryService->create($request->all());
            Aviso::guardado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }
        return redirect()->action('Admin\SupercategoriesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param Supercategory $supercategory
     * @return \Illuminate\Http\Response
     */
    public function show(Supercategory $supercategory)
    {
        return view('admin.supercategories.show', compact('supercategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Supercategory $category
     * @return \Illuminate\Http\Response
     */
    /*public function edit(Supercategory $supercategory)
    {
        $languages  = $this->languageService->all();
        return view('admin.supercategories.edit',compact('supercategory','languages'));
    }*/


    /**
     * @param Request $request
     * @param Supercategory $supercategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Supercategory $supercategory, SupercategoryService $supercategoryService)
    {
        $supercategoryService->update($supercategory->id, $request->except(['_token', '_method']));
        Aviso::actualizado();
        /*try {
            if ($this->supercategoryService->update($supercategory->id, ['name' => $request->get('name')])) {
                foreach ($request->get('languages', []) as $key => $item) {
                    if ($supercategory->languages->contains($key))
                        $supercategory->languages()->updateExistingPivot($key, ['translation' => $item]);
                    else
                        $supercategory->languages()->attach($key, ['translation' => $item]);
                }
            }
            Aviso::actualizado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noActualizado();
        }*/
        return redirect()->action('Admin\SupercategoriesController@index');
    }


    /**
     * @param Supercategory $supercategory
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Supercategory $supercategory)
    {
        try {
            $supercategory->categories()->delete();
            $supercategory->delete();
            Aviso::eliminado();

        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado();
        }

        return redirect()->action('Admin\SupercategoriesController@index');
    }
}
