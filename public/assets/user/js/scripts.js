$(function () {
    var app_url = window.location.pathname;

    /*DASHBOARD*/

    $('.toggle-aside').on('click',function(){
        $('.sidebar-container').toggleClass('aside-active');
    });

    $('.func-preview-input-file').on('change',function(){
        var file = $(this);
        file.parent().find('.func-preview-img').attr('src',file[0].files[0].name);//TODO: buscar funcion para reemplazar imagen
        console.log(file[0].files[0]);

    });


    $('.sweet-confirm').on('submit', function (e) {
        e.preventDefault();

        var title = $(this).data('title');

        var message = $(this).data('message');

        var confirmButtonText = $(this).data('ok-button');

        var cancelButtonText = $(this).data('cancel-button');

        var form = this;

        swal({
            title: title,
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: confirmButtonText,
            cancelButtonText: cancelButtonText,
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                form.submit();
            }
        });
    });
    $('.date_picker').datetimepicker({
        format:'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down",
            next: 'fa fa-caret-right',
            previous: 'fa fa-caret-left',
        }
    });

    $('.toggle-edit').on('click',function(e) {
        e.preventDefault();
        var form = $($(this).data('form-edit'));

        var show_form = form.find('.show-form');
        var edit_form = form.find('.edit-form');

        show_form.toggleClass('active in');
        edit_form.toggleClass('active in');
    });
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}



function showPreventMessage(target, message, title, buttonText) {
    swal({
        title: title,
        text: message,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: false
    },
    function (isConfirm) {
        if (isConfirm) {
            swal.close();
            window.location.href = target;
        }
    });
}

