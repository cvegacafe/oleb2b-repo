<?php namespace App\Repositories\Attachment;

use App\Repositories\BaseRepository;

class AttachmentRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Attachment();
        parent::__construct();
    }

}