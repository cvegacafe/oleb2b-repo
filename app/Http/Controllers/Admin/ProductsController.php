<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductUpdateRequest;
use App\Library\Aviso;
use App\Repositories\Product\Product;
use App\Services\CountryService;
use App\Services\CurrencyService;
use App\Services\DeliveryTermService;
use App\Services\FileService;
use App\Services\MeasureService;
use App\Services\PaymentTermService;
use App\Services\ProductService;
use App\Services\SupercategoryService;
use Exception;
use Illuminate\Support\Facades\Log;

class ProductsController extends Controller
{

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productService->paginate(5);
        return view('admin.products.index', compact('products'));
    }


    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        $product = is_numeric($product) ? $this->productService->find($product) : $this->productService->findBySlug($product);
        return view('admin.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @param SupercategoryService $supercategoryService
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param DeliveryTermService $deliveryTermService
     * @param PaymentTermService $paymentTermService
     * @param CurrencyService $currencyService
     * @return \Illuminate\Http\Response
     */
    public function edit(
        Product $product,
        SupercategoryService $supercategoryService,
        CountryService $countryService,
        MeasureService $measureService,
        DeliveryTermService $deliveryTermService,
        PaymentTermService $paymentTermService,
        CurrencyService $currencyService
    ) {

        $supercategories = $supercategoryService->all();
        $countries = $countryService->all();
        $measurements = $measureService->all();
        $deliveryTerms = $deliveryTermService->all();
        $paymentTerms = $paymentTermService->all();
        $currencies = $currencyService->all();
        return view('admin.products.edit',
            compact('product', 'supercategories', 'countries', 'measurements', 'deliveryTerms', 'paymentTerms',
                'currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param $slug
     * @param ProductService $productService
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProductUpdateRequest $request,
        $slug,
        ProductService $productService
    ) {
        $product = $productService->findBySlug($slug);
        if ($request->has('delivery_terms')) {
            $product->deliveryTerms()->sync($request->get('delivery_terms'));
        } else {
            $product->deliveryTerms()->detach();
        }

        if ($request->has('payment_terms')) {
            $product->paymentTerms()->sync($request->get('payment_terms'));
        } else {
            $product->paymentTerms()->detach();
        }

        $productService->updateWithOutTranslation($product->id, $request->all());
        return redirect()->route('users.show', $product->user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, FileService $fileService)
    {
        try {
            $fileService->removeFile($product->pictures);
            $product->delete();
            Aviso::eliminado(trans('alerts.products.deleted'));
        } catch (Exception $e) {
            Aviso::noEliminado(trans('alerts.products.deleted_error'));
            Log::error($e);
        }
        return redirect()->action('Admin\ProductsController@index');
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function enable(Product $product)
    {
        $product->is_approved = true;
        $product->save();
        return redirect()->action('Admin\ProductsController@show', $product);
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function disable(Product $product)
    {
        $product->is_approved = false;
        $product->save();
        return redirect()->action('Admin\ProductsController@show', $product);
    }
}
