<?php
return [
    'submit_buying_request' => 'Crear Solicitud de Cotización',
    'edit_buying_request' => 'Editar Solicitud de Cotización',
    'quantity' => 'Cantidad',
    'order_quantity' => 'Cantidad solicitada',
    'buyer_from' => 'Origen del Comprador',
    'start_date' => 'Fecha de inicio',
    'end_date' => 'Fecha de cierre',
    'contact_buyer' => 'Contactar Comprador',
    'country_of_delivery' => 'País de entrega',
    'save_later' => 'Guardar',
    'buyer_info' => 'Información del comprador',
    'product_description' => 'Descripción Del Producto',
    'buyer' => 'Comprador',
    'this_buyer_speak' => 'Este comprador habla los siguientes idiomas',
    'greeting_buying_request' => 'Hola, Estoy interesado en este producto',
    'default_name_no_supplier' => 'Privado'
];