<!DOCTYPE html>
<html>
<head>
	<title>Mail</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style>
*{
	box-sizing: border-box;
}
	#cabecera{
		font-family: "Arial", sans-serif;
		font-size: 40px;
		font-weight: bold;
	}
	#parrafo{
		font-family: arial;
		font-size: 16px;
		margin-bottom: 40px;
	}
	#logo{
		margin-top:-90px;
		opacity: 0.85;
		width: 220px;
	}
	#header{
		opacity: 0.85;
		margin-right: auto;
		margin-left: auto;
	}
	.header{
		padding-bottom: 100px;
		margin-top: 25px;
		
	}
	#boton{
		border-radius: 30px;
		background-color: #FF430A;
		opacity: 0.75e;
		height: 50px;
		width: 200px;
		margin-bottom: 80px;
	}
	#informe2{
		font-family: "Arial", sans-serif;
		font-size: 18px;

	}
	#url{
		font-family: "Arial",sans-serif;
		font-weight: bold;
		font-size: 20px;
		margin-left: 65px;
		font-color:black !important;
	}
</style>
<body>
	<div class="container">
		<div class="header">
			<img class="img-responsive center-block" id="header" src="{{asset('assets/emails/header.png')}}">
			<img class="img-responsive center-block" id="logo" src="{{asset('assets/pages/img/logo-blanco.png')}}">
		</div>
			<h1 id="cabecera" class="text-center">Hola</h1>
		
		<p id="parrafo" class="text-center">Haga clic en el siguiente boton para restablecer su contraseña</p>
			<div class="text-center">
				<button href="" class="btn btn-warning" id="boton">Cambiar Contraseña</button>
			</div>
		<p class="text-center" id="informe2">Si tiene problemas al hacer clic en el boton "cambiar de contraseña", copie y pegue la URL que aparece en su navegador web: </p>
		<p class="text-left" id="url"><a href="http://oleb2b.com/pass-word/ret/5a900cf67e34ab9f73ab1a60e8f9d01026d037b0d265119b413de165643fac7d">http://oleb2b.com/pass-<br>word/ret/5a900cf67e34ab9f73ab1a60e8f9d01026d037b0d265119b413de165<br>643fac7d</a></p>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>

