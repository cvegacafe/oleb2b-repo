<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiAllCountriesRequest;
use App\Http\Requests\CountryEnableRequest;
use App\Services\CountryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class CountriesController extends Controller
{

    private $countryService = null;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    public function enable(CountryEnableRequest $countryEnableRequest)
    {
        $val = $countryEnableRequest->get('checked') === 'true' ? true : false;
        $response = $this->countryService->update(
            $countryEnableRequest->get('id'),
            ['enabled_for_supplier' => $val]
        );
        return response()->json(['response' => $response, 'value' => $val]);
    }

    public function getAllCountriesByGroup(ApiAllCountriesRequest $request)
    {
        $currentLanguage = $request->get('lang',Lang::locale());
        $groupedCountries = $this->countryService->getGroupedCountries($currentLanguage);
        return response()->json(['sell_and_buy' => $groupedCountries[1],'only_buy'=>$groupedCountries[0]]);
    }
}
