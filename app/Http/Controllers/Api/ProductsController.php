<?php namespace App\Http\Controllers\Api;

use App\Repositories\Product\Product;
use App\Services\FileService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    private $productService;

    public function __construct(ProductService $productService, FileService $fileService)
    {
        $this->productService = $productService;
        $this->fileService = $fileService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function productsCount()
    {
        $products = $this->productService->getByUserId(Auth::id());

        return response()->json(['status'=>'success','count'=>$products->count()]);
    }

    /**
     * @param $product_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage($product_id, Request $request){

        $product = Product::find($product_id);
        if(count($product->pictures) <= 1){
            return response()->json([
                'No puedes publicar productos sin imagenes'
            ], 405);

        }

        $removed = $this->fileService->removeFile($product->pictures[$request->key]);

        if($removed){
            $list = [];
            foreach($product->pictures as $key => $value){
                if(!$key == $request->key){
                    $list[] = $value;
                }
            }

            $product->pictures = $list;
            $product->save();
        }

        return response()->json([
            'Imagen eliminada papuuu'
        ]);
    }
}
