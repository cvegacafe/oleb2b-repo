<?php
return [
    'submit_buying_request' => 'Разместить Запрос Кп',
    'edit_buying_request' => 'Редактировать Запрос КП',
    'quantity' => 'количество',
    'order_quantity' => 'Размер Заказа',
    'buyer_from' => 'Покупатель Из:',
    'start_date' => 'Начало',
    'end_date' => 'Окончание',
    'contact_buyer' => 'Связаться с покупателем',
    'country_of_delivery' => 'Страна Доставки',
    'save_later' => 'Сохранить',
    'default_name_no_supplier' => 'Частный',
    'product_description' => 'Описание Товара',
    'buyer' => 'Покупателя',
    'this_buyer_speak' => '',
    'buyer_info' => 'О покупателе',
];