@extends('layouts.pages')
@section('title')
    Plataforma de venta B2B para exportadores de America Latina y Rusia
@endsection
@section('content')
    <div class="buscador">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <form action="{{route('portal.search.build')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-container">
                            <select id="tipoBusqueda">
                                <option value="Buscar: ">{{trans('portal.search')}}:</option>
                                <option>{{trans('portal.product')}}</option>
                                {{-- <option value="proveedor">{{trans('portal.supplier')}}</option>--}}
                            </select>
                            <input type="text" name="q" placeholder="{{trans('portal.search_products')}}">
                            <button>{{trans('portal.search')}}</button>
                        </div>
                    </form>
                    <h1 class="titulo">{{trans('portal.main_slogan')}}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-top-home container-fluid">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-md-push-4 col-lg-6 col-lg-push-3">
                <div class="banners">
                    @foreach($sliders as $slider)
                        <figure>
                            @if($slider->url)
                                <a href="{{$slider->url}}" target="_blank">
                                    <img src="{{asset($slider->path)}}" class="img-responsive" alt="{{$slider->title}}">
                                </a>
                            @else
                                <img src="{{asset($slider->path)}}" class="img-responsive" alt="{{$slider->title}}">
                            @endif
                        </figure>
                    @endforeach
                </div>
                @if(Auth::check())
                    <div class="sub-banner">
                        <a href="{{route('portal.memberships')}}">
                            <img src="{{asset('assets/pages/img/membership_promo_'.Lang::locale().'.png')}}" alt=""
                                 class="img-responsive">
                        </a>
                    </div>
                @else
                    <div class="sub-banner">
                        <a href="#">
                            <img src="{{asset('assets/pages/img/membership_promo_new_'.Lang::locale().'.png')}}" alt=""
                                 class="img-responsive" onclick="showRegisterForm()">
                        </a>
                    </div>
                @endif
            </div>
            <div class="col-xs-12 col-md-4 col-md-pull-8 col-lg-3 col-lg-pull-6  col-categorias">
                <div class="categorias-home">
                    <a href="{{route('portal.categories')}}">
                        <i class="icon-menu"></i>
                        <span>{{trans('portal.categories')}}</span>
                    </a>
                </div>
                <div class="lista-categorias">
                    <ul>
                        @foreach($superCategories->take(8) as $superCategory)
                            <li target="mega{{$superCategory->id}}">
                                <a href="{{route('portal.categories')}}#{{$superCategory->id}}">
                                    <i class="icon-icon-{{$superCategory->id}}"></i>
                                    {{$superCategory->name}}
                                    <i class="fa fa-angle-right right"></i>
                                </a>
                            </li>
                            <div class="mega-subcategory" id="mega{{ $superCategory->id }}">
                                <div class="icon-bg">
                                    <i class="icon-icon-{{$superCategory->id}}"></i>
                                </div>
                                <div class="subgategories">
                                    @foreach($superCategory->categories as $category)
                                        <div class="single-sub">
                                            <h4 class="title">
                                                {{$category->name}}
                                            </h4>
                                            <ul>
                                                @foreach($category->subcategories->take(5) as $subCategory)
                                                    <li>
                                                        <a href="{{route('portal.search',['q'=>'productos','sub'=>$subCategory->id,])}}">{{$subCategory->name}}</a>
                                                    </li>
                                                @endforeach
                                                <li class="category-color{{$superCategory->id}}"><a
                                                            class="category-color{{$superCategory->id}}"
                                                            href="{{route('portal.categories')}}">{{trans('common.more')}}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="footer category{{$superCategory->id}}">

                                </div>
                            </div>
                        @endforeach
                        <li class="vermas"><a href="{{route('portal.categories')}}">{{trans('common.more')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-lg-3">
                <div class="busqueda-pais">
                    <span>{{trans('portal.search_by_country')}}</span>
                </div>
                <div class="paises">
                    @foreach($countries as $country)
                        <a href="{{route('portal.search',['country'=>$country->name,'q'=>trans('dashboard.products')])}}">
                            <div class="flag">
                                <img src="{{asset('assets/banderas/'.$country->abbreviation.'.jpg')}}" alt=""
                                     width="30px" height="20px">
                                <span>{{$country->name}}</span>
                            </div>
                        </a>
                    @endforeach
                </div>
                <a href="{{route('portal.buyingRequests')}}">
                    <div class="solicitudes-home text-center">
                        {{trans('portal.buying_request')}}
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="productos-destacados container-fluid">
        <h2 class="titulo-1 text-center">{{trans('portal.featured_products')}}</h2>
        <div class="row">
            @forelse($featuredProducts as $product)
                <div class="col-xs-12 col-xsm-6 col-md-4 col-lg-15">
                    <div class="producto">
                        <div class="header">
                            <span class="cont-organic">
                                @if($product->is_organic)
                                    <img src="{{asset('assets/pages/img/organic_vf.png')}}" alt="" width="90px"
                                         class="img-responsive pull-left organic">
                                @endif
                            </span>
                            <div>
                                @if($authUser)
                                    @if($myProducts->contains('id',$product->id))
                                        <a href="{{route('user.products.edit',$product)}}">
                                            <button class="edit">
                                                {{trans('products.edit_product')}}
                                            </button>
                                        </a>
                                    @elseif($threads->contains('product_id',$product->id))
                                        <a href="{{route('user.messages.products.index', ['product' => $product->id, 'thread' => $threads->where('product_id',$product->id)->first()->id])}}">
                                            <button class="edit">
                                                {{trans('menus.user.messages')}}
                                            </button>
                                        </a>
                                    @else
                                        <button onclick="contactWithSupplier('{{$product->id}}','{{$product->name}}','{{asset($product->main_photo)}}','{{$product->user->languages->implode(\Lang::locale().'_name',' ,')}}')">
                                            {{trans('portal.contact_supplier')}}
                                        </button>
                                    @endif

                                    @if($favoritesProducts->contains('product_id',$product->id))
                                        <button data-product-id="{{$product->id}}" class="toggleFavorite active"><i
                                                    class="fa fa-heart"></i></button>
                                    @else
                                        <button data-product-id="{{$product->id}}" class="toggleFavorite"><i
                                                    class="fa fa-heart"></i></button>
                                    @endif
                                @else
                                    <button onclick="showAuth('{{route('portal.singleProduct',$product->slug). '#contact'}}')">{{trans('portal.contact_supplier')}}</button>
                                    <button class="toggleFavorite" data-product-id="{{$product->id}}"><i
                                                class="fa fa-heart"></i></button>
                                @endif
                            </div>
                        </div>
                        <div class="body">
                            <a href="{{route('portal.singleProduct',$product->slug)}}">
                                <div class="nombre titulo-3" title="{{$product->name}}">
                                    {{ str_limit($product->name, 40) }}
                                </div>
                                <figure class="thumb">
                                    <img src="{{asset($product->main_photo)}}" alt="">
                                </figure>
                            </a>
                            <div class="minimo titulo-5">
                                {{trans('products.moq')}}: {{$product->moq}} {{$product->moqMeasure->name}}
                            </div>
                            <div class="precio">
                                @if($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_FIXED)
                                    <span class=" text-roboto">{{$product->price}}</span>
                                    {{$product->currency->currency_code}} / {{$product->supplyMeasure->name}}
                                @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_ASK)
                                    {{trans('priceTypes.ask_seller')}}
                                @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_RANGE)
                                    <span class=" text-roboto">{{$product->price_range}}</span>
                                    {{$product->currency->currency_code}} / {{$product->supplyMeasure->name}}
                                @endif
                              {{--  <span class="text-roboto-b">{{$product->price ?? ''}}</span> {{$product->currency->currency_code}}
                                / {{$product->saleUnitMeasure->name}}--}}
                            </div>
                            <div class="origin">
                                <small>
                                    <strong>
                                        {{trans('products.place_origin')}}: {{$product->country->name}}
                                    </strong>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-xs-12">{{trans('portal.not_featured_products')}}.</div>
            @endforelse
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button class="ver-mas"> {{trans('portal.see_more')}}</button>
            </div>
        </div>
    </div>
    <div class="como-funciona-home">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="titulo-1">
                        {{trans('portal.how_it_works')}}
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                            <div class="front">
                                <div class="item compradores">
                                    <div class="icon">
                                        <i class="icon-compradores"></i>
                                    </div>
                                    <div class="desc">
                                        {{trans('portal.for')}} <span>{{trans('portal.buyers')}}</span>
                                    </div>
                                    <button>{{trans('portal.see_more')}}</button>
                                </div>
                            </div>
                            <div class="back">
                                <h3>{{trans('portal.buyers_card.for_buyers')}}</h3>
                                <ul>
                                    <li><strong>1.</strong> {{trans('portal.buyers_card.find_new_products')}}</li>
                                    <li><strong>2.</strong> {{trans('portal.buyers_card.posting_buying_request')}}</li>
                                    <li><strong>3.</strong> {{trans('portal.buyers_card.improve_sourcing')}}</li>
                                </ul>
                                <i class="icon-compradores"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                            <div class="front">
                                <div class="item usuarios">
                                    <div class="icon">
                                        <i class="icon-nuevo-usuraio"></i>
                                    </div>
                                    <div class="desc">
                                        {{trans('portal.new')}} <span>{{trans('portal.users')}} </span>
                                    </div>
                                    <button>{{trans('portal.see_more')}}</button>
                                </div>
                            </div>
                            <div class="back">
                                <h3>{{trans('portal.new_users_card.new_users')}}</h3>
                                <ul>
                                    <li><strong>1.</strong> {{trans('portal.new_users_card.register')}}</li>
                                    <li><strong>2.</strong> {{trans('portal.new_users_card.benefits')}}</li>
                                    <li><strong>3.</strong> {{trans('portal.new_users_card.new_tool')}}</li>
                                </ul>
                                <i class="icon-nuevo-usuraio"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                        <div class="flipper">
                            <div class="front">
                                <div class="item vendedores">
                                    <div class="icon">
                                        <i class="icon-vendedores"></i>
                                    </div>
                                    <div class="desc">
                                        {{trans('portal.for')}} <span>{{trans('portal.suppliers')}}</span>
                                    </div>
                                    <button>{{trans('portal.see_more')}}</button>
                                </div>
                            </div>
                            <div class="back">
                                <h3>{{trans('portal.suppliers_card.for_suppliers')}}</h3>
                                <ul>
                                    <li><strong>1.</strong> {{trans('portal.suppliers_card.register_company')}}</li>
                                    <li><strong>2.</strong> {{trans('portal.suppliers_card.upload_products')}}</li>
                                    <li><strong>3.</strong> {{trans('portal.suppliers_card.membership_plan')}}</li>
                                    <li><strong>4.</strong> {{trans('portal.suppliers_card.view_buying_request')}}</li>
                                    <li><strong>5.</strong> {{trans('portal.suppliers_card.improve_your_sales')}}</li>
                                </ul>
                                <i class="icon-vendedores"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="noticias-home">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="titulo-1">{{trans('portal.latest_news')}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="slide-noticias-home">
                        @forelse($news->take(6) as $new)
                            <div class="noticia">
                                <figure class="img-destacada">
                                    <img src="{{$new->image}}" alt="" class="img-responsive">
                                </figure>
                                <div class="descripcion">
                                    <h3 class="titulo-4">{{$new->title}}</h3>
                                    <p>{{$new->short_description}}</p>
                                </div>
                                <div class="fecha">
                                    <span> {{$new->created_at->format('d')}} </span>
                                    {{$new->created_at->format('M')}}<br>{{$new->created_at->format('Y')}}
                                </div>
                                <a href="{{$new->link}}" target="_blank">
                                    <button class="ver-mas">{{trans('common.more')}}</button>
                                </a>
                            </div>
                        @empty
                            {{--<div class="noticia">
                                <figure class="img-destacada">
                                    <img src="http://lorempixel.com/480/300" alt="" class="img-responsive">
                                </figure>
                                <div class="descripcion">
                                    <h3 class="titulo-4">Lorem ipsum dolor sit amet</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolore error,
                                        ex fugit.</p>
                                </div>
                                <div class="fecha">
                                    <span> 11 </span>
                                    <strong>
                                        Dic<br>2016
                                    </strong>
                                </div>
                                <button class="ver-mas">{{trans('common.more')}}</button>
                            </div>
                            <div class="noticia">
                                <figure class="img-destacada">
                                    <img src="http://lorempixel.com/480/300" alt="" class="img-responsive">
                                </figure>
                                <div class="descripcion">
                                    <h3 class="titulo-4">Lorem ipsum dolor sit amet</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus dolore error,
                                        ex fugit.</p>
                                </div>
                                <div class="fecha">
                                    <span> 11 </span>
                                    <strong>
                                        Dic<br>2016
                                    </strong>
                                </div>
                                <button class="ver-mas">{{trans('common.more')}}</button>
                            </div>--}}
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="camaras-comercio container">
        <div class="row">
            <div class="col-xs-12 col-lg-3">
                <h2 class="titulo-3">{{trans('portal.trade_organizations')}}</h2>
                <button>{{trans('common.more')}}</button>
            </div>
            <div class="col-xs-12 col-lg-9">
                <div class="camaras">
                    @forelse($cclImages  as $image)
                        @if($image->url)
                            <figure class="camara">
                                <a href="{{ $image->url }}" target="_blank">
                                    <img src="{{asset($image->path)}}" alt="{{$image->title}}" class="img-responsive">
                                </a>
                            </figure>
                        @else
                            <figure class="camara">
                                <img src="{{asset($image->path)}}" alt="{{$image->title}}" class="img-responsive">
                            </figure>
                        @endif
                    @empty
                        <figure class="camara">
                            <a href="http://www.promperu.gob.pe/" target="_blank">
                                <img src="{{asset('assets/pages/img/prom_peru.jpg')}}" alt="Prom Perú"
                                     class="img-responsive">
                            </a>
                        </figure>
                        <figure class="camara">
                            <a href="http://www.promexico.mx/" target="_blank">
                                <img src="{{asset('assets/pages/img/pro_mexico.jpg')}}" alt="Pro méxico"
                                     class="img-responsive">
                            </a>
                        </figure>
                        <figure class="camara">
                            <a href="http://www.aligalat.org/" target="_blank">
                                <img src="{{asset('assets/pages/img/aliga_logo.png')}}" alt="Aliga"
                                     class="img-responsive">
                            </a>
                        </figure>
                        <figure class="camara">
                            <a href="https://www.fogapi.com.pe/" target="_blank">
                                <img src="{{asset('assets/pages/img/fogapi_logo-.jpg')}}" alt="Fogapi"
                                     class="img-responsive">
                            </a>
                        </figure>
                        <figure class="camara">
                            <a href="https://www.ceplan.gob.pe/" target="_blank">
                                <img src="{{asset('assets/pages/img/logo-cepla.png')}}" alt="Cepla"
                                     class="img-responsive">
                            </a>
                        </figure>
                        <figure class="camara">
                            <a href="http://www.proesa.gob.sv/" target="_blank">
                                <img src="{{asset('assets/pages/img/proesa.png')}}" alt="Proesa" class="img-responsive">
                            </a>
                        </figure>
                    @endforelse

                </div>
            </div>
        </div>
    </div>
    <div class="ferias-eventos container">
        <div class="row">
            <div class="col-xs-12 col-lg-3">
                <h2 class="titulo-3">{{trans('portal.trade_shows_and_events')}}</h2>
                <button>{{trans('portal.see_more')}}</button>
            </div>
            <div class="col-xs-12 col-lg-9">
                <div class="ferias">
                    @forelse($eventsImages  as $image)
                        @if($image->url)
                            <figure class="camara">
                                <a href="{{ $image->url }}" target="_blank">
                                    <img src="{{asset($image->path)}}" alt="{{$image->title}}" class="img-responsive">
                                </a>
                            </figure>
                        @else
                            <figure class="camara">
                                <img src="{{asset($image->path)}}" alt="{{$image->title}}" class="img-responsive">
                            </figure>
                        @endif
                    @empty
                        <figure class="camara">
                            <a href="http://www.prod-expo.ru/en" target="_blank">
                                <img src="{{asset('assets/pages/img/prod_18_240x120_eng.gif')}}" class="img-responsive">
                            </a>
                        </figure>
                        <figure class="camara">
                            <img src="{{asset('assets/pages/img/alimentic.png')}}" alt="" class="img-responsive">
                        </figure>
                    @endforelse

                </div>
            </div>
        </div>
    </div>
    <div class="ferias-eventos container">
        <div class="row">
            <div class="col-xs-12 col-lg-3">
                <h2 class="titulo-3">{{trans('portal.partners')}}</h2>
                <button>{{trans('portal.see_more')}}</button>
            </div>
            <div class="col-xs-12 col-lg-9">
                <div class="ferias">
                    @forelse($partnersImages  as $image)
                        @if($image->url)
                            <figure class="camara">
                                <a href="{{ $image->url }}" target="_blank">
                                    <img src="{{asset($image->path)}}" alt="{{$image->title}}" class="img-responsive">
                                </a>
                            </figure>
                        @else
                            <figure class="camara">
                                <img src="{{asset($image->path)}}" alt="{{$image->title}}" class="img-responsive">
                            </figure>
                        @endif
                    @empty
                        <figure class="camara">
                            <a href="http://www.lcvesta.com/" target="_blank">
                                <img src="{{asset('assets/pages/img/vesta-logistic-company.png')}}"
                                     alt="Vesta logistic company" class="img-responsive">
                            </a>
                        </figure>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <div class="registrate">
        <div class="centro">
            <h3>{{trans('portal.sale_online_is_easy')}}</h3>
            @if(Auth::check())
                <a href="{{route('user.products.create')}}">
                    <button>{{trans('portal.upload_products')}}</button>
                </a>
            @else
                <button onclick="showRegisterForm()">{{trans('portal.join_free')}}</button>
            @endif
        </div>
    </div>
    @include('pages.includes.supplier-contact-popup',[
        'countries' => $allCountries,
        'measurements'=>$measurements,
        'paymentTerms' => $paymentTerms,
        'languages' => $languages
        ])
@endsection
@section('styles')
    <style>
        .top-top-nav.home:after {
            background: url(/assets/img/adex/alpaca.png) no-repeat center center;
        }
    </style>
@endsection
@section('scripts')
    <script>
      $('.slider-bg').slick({
        slideToShow: 1,
        arrows: false,
        dots: false,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        speed: 1200,
        draggable: false
      });
    </script>
    <script>
      function contactWithSupplier(product_id, product_name, product_photo, languages) {
        var modal = $('#SupplierModal');
        modal.find('#product_id').val(product_id);
        modal.find('#product_name').text(product_name);
        modal.find('#product_photo').attr('src', product_photo);
        modal.find('#input_product_photo').val(product_photo);
        modal.find('#languages').text(languages);
        modal.find('#SupplierFormHtml')[0].reset();
        modal.modal();
      }

      $(function () {
        var max_chars = 1000;
        $('#description').keyup(function () {
          var chars = $(this).val().length;
          var diff = max_chars - chars;
          $('#contador').html(diff);
        });
        $('.banners').slick({
          slidesToShow: 1,
          arrows: false,
          dots: false,
          autoplay: true,
          autoplaySpeed: 2600
        });
        $('.slider-bg').fadeIn('slow');
      })
    </script>

    <script>
      var position = 0;

      $('#seleccionar-fotos').on('click', function () {
        $file = $('.file' + position);
        if ($file.val()) {
          $file.click();
        } else {
          position++;
          appendInputFile(position);
          $('.file' + position).click();
        }
      });

      $('.preview-files').on('mouseenter', 'div span i', function () {
        $(this).parent().parent().find('figure').fadeIn();
      }).on('mouseleave', 'div', function () {
        $(this).parent().parent().find('figure').fadeOut();
      });

      function previewFile(element) {
        var $this = $(element);
        //console.log(position);

        if (validateFile($this)) {
          var fileName = getFileName($this.val());

          var reader = new FileReader();

          reader.onload = function (e) {
            $('.preview-files').append('<div class="file' + position + '"> <span><i class="fa fa-picture-o"></i><small>' + fileName + '</small></span> <figure><img src="' + e.target.result + '"></figure> <div class="close" onclick="removeFile(' + position + ')"><i class="fa fa-close"></i></div> </div>');
          };

          reader.readAsDataURL(element.files[0]);
          if (!position === 1) {
            position++;
            appendInputFile(position)
          }
        }
      }

      function appendInputFile(position) {
        $('#filesContainer').append($('<input/>').attr('type', 'file').attr('name', 'file').addClass('input-file file' + position).attr('onchange', "previewFile(this, " + position + ")"));
      }

      function validateFile(input) {

        var extencionesAceptadas = [
          'jpeg',
          'jpg',
          'png',
          'gif',
          'bmp',
          'pdf',
          'doc',
          'docx',
          'xls',
          'ppt',
          'pptx'
        ];

        var maxSize = 2000;

        var maxCount = 1;

        //console.log((1024 * maxSize),input[0].files[0].size);

        if (input[0].files[0].size > (1024 * maxSize)) {
          //TODO: alerta de imagen muy grande
          alert('imagen muy grande');
          return false;
        }

        if ($('#filesContainer input').length > maxCount) {
          //TODO: maximo de imagenes permitido
          alert('maximo de imagenes permitido');
          return false;
        }

        var fileExtension = input.val().split('.').pop();

        if (!extencionesAceptadas.includes(fileExtension)) {
          input.val('');
          alert('tipo de archivo no permitido');
          return false;
        }

        return true;
      }

      function removeOldFile(file) {
        var $this = $(file);
        var position = $this.data('position');
        $('#filesContainer').append('<input type="hidden" name="filesToRemove[]" value="' + position + '">');
        $this.parent().remove();
      }

      function removeFile(position) {
        $('.file' + position).remove();
      }

      function getFileName(fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
          filename = filename.substring(1);
        }
        return filename;
      }
    </script>
@endsection
