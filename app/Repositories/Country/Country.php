<?php namespace App\Repositories\Country;

use App\Repositories\Language\Language;
use App\Repositories\Product\Product;
use App\Repositories\Taxtypes\Taxtypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Country extends Model
{
    public $table = 'countries';

    public $fillable = ['en_name', 'es_name', 'ru_name', 'abbreviation', 'enabled_for_supplier', 'language_id'];

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function taxtypes()
    {
        return $this->belongsToMany(Taxtypes::class, 'country_taxtype', 'country_id', 'taxtype_id')->withPivot('digits');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }

    public function getTaxTypeAttribute()
    {
        return $this->taxtypes()->first();
    }
}
