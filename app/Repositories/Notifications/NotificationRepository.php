<?php namespace App\Repositories\Notifications;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class NotificationRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Notification();
        parent::__construct();
    }

    public function getLastUnreadNotifications($userId,$lang)
    {
        return DB::table('notifications')
            ->where('user_id',$userId)
            ->where('read_at',null)
            ->select('id','url','created_at',$lang.'_title as title')
            ->get();
    }
}