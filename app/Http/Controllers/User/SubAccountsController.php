<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSubAccountRequest;
use App\Library\Aviso;
use App\Library\Constants;
use App\Repositories\User\User;
use App\Services\LanguageService;
use App\Services\MarketService;
use App\Services\ProductService;
use App\Services\UserService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SubAccountsController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('premiumUser');
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Guard $guard
     * @return \Illuminate\Http\Response
     */
    public function index(Guard $guard)
    {
        $users = $this->userService->indexSubAccounts($guard->user()->supplier, $guard->id());

        return view('user.subaccounts.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param LanguageService $languageService
     * @param MarketService $marketService
     * @return \Illuminate\Http\Response
     */
    public function create(LanguageService $languageService, MarketService $marketService)
    {
        $languages = $languageService->all();
        $markets = $marketService->all();

        return view('user.subaccounts.create', compact('languages', 'markets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateSubAccountRequest|Request $request
     * @param Guard $guard
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSubAccountRequest $request, Guard $guard)
    {
        try {
            DB::beginTransaction();
            $this->userService->createSubAccount($request->all(), $guard->user()->supplier, $guard->user());
            Aviso::success(trans('validation.custom.messages.record_updated'));
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            Aviso::success(trans('validation.custom.messages.record_updated_error'));
        }

        return redirect()->action('User\SubAccountsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @param LanguageService $languageService
     * @param MarketService $marketService
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, LanguageService $languageService, MarketService $marketService)
    {
        $languages = $languageService->all();
        $markets = $marketService->all();

        return view('user.subaccounts.edit', compact('user', 'languages', 'markets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @param UserService $userService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, UserService $userService)
    {
        try {

            DB::beginTransaction();
            $userService->update($user->id, $request->all());

            if ($request->has('markets')) {
                $user->languages()->sync($request->get('languages'));
            }

            if ($request->has('markets')) {
                $user->markets()->sync($request->get('markets'));
            }

            Aviso::success(trans('validation.custom.messages.record_updated'));
            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            Aviso::success(trans('validation.custom.messages.record_updated_error'));
        }

        return redirect()->route('user.sub-accounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @param ProductService $productService
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, ProductService $productService)
    {
        try {
            DB::beginTransaction();
            $this->authorize('deleteSubAccount', $user);
            $user->markets()->detach();
            $user->languages()->detach();
            $user->suppliers()->detach();
            $productsId = $user->products->pluck('id')->toArray();
            if (!$productService->transferProductsToUser($productsId, Auth::user())) {
                Throw new Exception('No se pudo transferir los productos');
            }
            $user->memberships()->detach();
            $user->status = Constants::USER_STATUS_BLOCKED;
            $user->save();
            $user->delete();
            DB::commit();
            Aviso::eliminado(trans('validation.custom.messages.record_deleted'));

        } catch (AuthorizationException $ea) {
            DB::rollback();
            Aviso::error(trans('validation.custom.messages.unauthorized_action'));
        } catch (Exception $e) {
            Log::error($e);
            DB::rollback();
            Aviso::error(trans('validation.custom.messages.record_deleted_error'));
        }

        return redirect()->route('user.sub-accounts.index');
    }
}
