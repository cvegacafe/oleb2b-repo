<?php namespace App\Repositories\News;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'news';

    protected $fillable = [
        'title',
        'short_description',
        'image',
        'link'
    ];
}
