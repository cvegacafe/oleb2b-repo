<?php namespace App\Repositories\Oleb2bImage;

use App\Repositories\BaseRepository;

/**
 * Class Oleb2bImageRepository
 * @package App\Repositories\Oleb2bImage
 */
class Oleb2bImageRepository extends BaseRepository
{
    /**
     * Oleb2bImageRepository constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->model = new Oleb2bImage();
        parent::__construct();
    }
}