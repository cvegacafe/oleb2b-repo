<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('company_type')->unsigned();
            $table->foreign('company_type')->references('id')->on('companyTypes');
            $table->string('company_name');
            $table->text('es_company_description')->nullable();
            $table->text('en_company_description')->nullable();
            $table->text('ru_company_description')->nullable();
            $table->string('member_trade_organization')->nullable();
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('taxtype_id')->unsigned();
            $table->foreign('taxtype_id')->references('id')->on('taxtypes');
            $table->string('tax_number',30);
            $table->string('company_legal_address');
            $table->string('postal_code',10);
            $table->string('website');
            $table->string('year_company_registered',4);
            $table->integer('number_employees');
            $table->integer('supercategory_id')->unsigned();
            $table->foreign('supercategory_id')->references('id')->on('supercategories');
            $table->string('logo')->nullable();
            $table->json('images')->nullable();
            $table->boolean('is_approved')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
