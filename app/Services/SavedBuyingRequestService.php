<?php namespace App\Services;

use App\Repositories\SavedBuyingRequest\SavedBuyingRequestRepository;

class SavedBuyingRequestService extends BaseService
{

    public function __construct(SavedBuyingRequestRepository $savedBuyingRequestRepository)
    {
        $this->mainRepo = $savedBuyingRequestRepository;
        parent::__construct();
    }

    public function getSavedBuyingRequestByUser($userId)
    {
        return $this->mainRepo->getByColumn('user_id',$userId);
    }

    public function findSavedBuyingRequest($buyingRequestId,$userId)
    {
        return $this->mainRepo->findByBuyingRequestIdUserId($buyingRequestId,$userId);
    }
}