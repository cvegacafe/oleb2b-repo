<?php

return [
    // Home
    'main_slogan' => 'B2B Platform for Latin American and Russian exporters',
    'search' => 'Search',
    'product' => 'Product',
    'supplier' => 'Supplier',
    'search_products' => 'Search products in English',
    'categories' => 'Categories',
    'search_by_country' => 'Search by country',
    'buying_request' => 'Buying Requests',
    'featured_products' => 'Featured Products',
    'contact_supplier' => 'Contact Supplier',
    'contact_us' => 'Contact Us',
    'contact_us_subject' => 'Subject*',
    'contact_us_name' => 'Name*',
    'contact_us_email' => 'Email*',
    'contact_us_message ' => 'Message*',
    'contact_us_send' => 'Send',
    'not_featured_products' => 'No related products',
    'how_it_works' => 'How does it work?',
    'see_more' => 'see more',
    'for' => 'For',
    'new' => 'New',
    'free' => ' ',
    'buyers' => 'Buyers',
    'users' => 'Users',
    'suppliers' => 'Suppliers',
    'latest_news' => 'Latest News',
    'trade_organizations' => 'Trade Organizations and Agencies',
    'trade_shows_and_events' => 'Trade Shows and Events',
    'partners' => 'Partners',
    'sale_online_is_easy' => 'Selling Online is Easy',
    'join_free' => 'Join Free',
    'customer_support' => 'Customer support',
    'free_to_start_online_trading' => 'Free to start Online Trading',
    'already_member' => 'Already a ',
    'member' => 'member',
    'sign_in' => 'Sign In',
    'greeting' => 'Hello',
    'enter_at_your_account' => 'log in to your account',
    'forgot_password' => 'Did you forget your password?',
    'remember_me' => 'Remember me',
    'upload_products'=>'Upload your products',
    //Show Single Product

    'error_403' => 'Forbidden',
    'error_404' => 'Not Found',
    'error_503' => 'Internal Error',

    'error_403_text' => 'Access not allowed.',
    'error_404_text' => 'We haven\'t found what you are looking for',
    'error_503_text' => 'Something went Wrong.',

    'company_details' => 'Company Details',
    'photos_and_logos' => 'Photos and Logos',
    'factory_details' => 'Factory Details',
    'certifications_and_trademarks' => 'Certificates and Trademarks',
    'save_for_later' => 'Save for Later',
    'saved' => 'Saved',
    'request_quote' => 'Buying Request',
    'related_products' => 'Related Products',
    'more_products_from_seller' => 'More Products from this supplier',

    'search_buying_request' => 'Search Buying Requests',
    'terms_of_use' => 'Terms of use',
    'i_accept' => 'I Accept',
    'supplier_speak_languages' => 'This supplier speaks the following languages',
    'your_message' => 'Tu mensaje',

    /*seccion como funciona*/
    'suppliers_card' => [
        'for_suppliers' => 'For Suppliers',
        'register_company' => 'Register your company',
        'upload_products' => 'Upload your products',
        'membership_plan' => 'Choose membership plan',
        'view_buying_request' => 'View Buying Requests',
        'improve_your_sales' => 'Improve your sales with OleB2B.com'
    ],

    'new_users_card' => [
        'new_users' => 'New Users',
        'register' => 'Register',
        'benefits' => 'Benefit from using OleB2B.com platform',
        'new_tool' => 'Add a new tool to your Business'
    ],

    'buyers_card' => [
        'for_buyers'=>'For Buyers',
        'find_new_products' => 'Find new products and suppliers',
        'posting_buying_request' => 'Post your Buying Requests and receive quotations',
        'improve_sourcing' => 'Improve your sourcing with OleB2B.com'
    ]

];
