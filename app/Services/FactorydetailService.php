<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:24 PM
 */

use App\Repositories\FactoryDetail\FactoryDetailRepository;

class FactorydetailService extends BaseService
{

    public function __construct(FactoryDetailRepository $factoryDetailRepository)
    {
        $this->mainRepo = $factoryDetailRepository;
        parent::__construct();
    }

    public function createFactoryDetails(array $inputs,$supplierId)
    {
        $inputs['supplier_id'] = $supplierId;
        return $this->mainRepo->create($inputs);
    }
}