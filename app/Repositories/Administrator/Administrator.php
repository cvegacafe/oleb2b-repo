<?php namespace App\Repositories\Administrator;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $full_name
 * @property mixed last_names
 * @property mixed names
 */
class Administrator extends Authenticatable
{
    public $table = 'administrators';

    public $fillable = ['email', 'password', 'names', 'last_names', 'status', 'is_superadmin'];

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->names . ' ' . $this->last_names;
    }
}
