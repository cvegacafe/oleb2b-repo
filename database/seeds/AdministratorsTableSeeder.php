<?php

use App\Library\Constants;
use App\Repositories\Administrator\Administrator;
use Illuminate\Database\Seeder;

class AdministratorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Administrator::create([
            'email'=>'admin@oleb2b.com',
            'password'=>bcrypt('@krutoi2017$!'),
            'names'=>'Admin',
            'last_names'=>'.',
            'status'=>Constants::USER_STATUS_ACTIVE,
            'is_superadmin'=>true
        ]);
    }
}
