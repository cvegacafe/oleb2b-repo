<?php namespace App\Http\Middleware;

use App\Library\Aviso;
use Closure;

class isCompleteProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            $user = auth()->user();
            if (!$user->address || !$user->office_phone || !$user->mobile_phone || $user->languages->count() == 0) {
                Aviso::error('Perfil incompleto');
                return redirect()->route('account.manage.show');
            }
        }

        return $next($request);
    }
}
