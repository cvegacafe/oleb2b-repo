<?php
return [
    'buttons' => [
        'ok' => 'Сохранить'
    ],

    'messages' => [
        'message_send' => 'Сообщение отправлено',
        'message_not_send' => 'Не удалось отправить сообщение',
        'attachment_not_found' => 'Не найден прикрепленный файл',
        'no_registered_error' => 'Регистрация пользователя не осуществлена',
        'membership_pay_success' => 'Оплата прошла успешно. Категория поставщика: :name'
    ],

    'middlewares' => [
        'has_not_product_available' => 'Добавлено максимальное количество товаров',
        'supplier_data_required' => 'Чтобы добавить товары, заполните, пожалуйста, раздел "информация о компании"'
    ],

    'password' => [
        'password_updated' => 'Пароль изменен',
        'password_updated_error' => 'Не получилось изменить пароль',
        'password_current_invalid' => 'Пароль неверный',
    ],

    'subaccounts' => [
        'title_delete' => 'Удалить Запрос этот КП',
        'message_confirm' => 'Удалить этот Запрос КП :name ?'
    ],

    'buyingRequest' => [
        'title_delete' => 'Удалить Запрос этот КП',
        'message_confirm' => 'Удалить этот Запрос КП :name ?',
        'deleted' => 'Запрос КП удален',
        'deleted_error' => 'Не получилось удалить Запрос КП'
    ],

    'products' => [
        'title_delete' => 'Удалить товар',
        'message_confirm' => 'Удалить товар: :name ?',
        'deleted' => 'Товар удален',
        'deleted_error' => 'Не получилось удалить товар'
    ],

    'favoriteProducts' => [
        'title_delete' => 'Удалить сохраненный товар',
        'message_confirm' => 'Удалить сохраненный товар :name ?'
    ],
    'validateEmail' => [
        'message' => 'Мы отправили имейл для подтверждения Вашего имейл адреса',
        'message_confirm' => 'Имейл подтвержден',
    ],
    'delete_certificate' => [
        'message' => ' Удалить этот сертификат?',
        'message_confirm' => ' ',
    ],
];