<?php namespace App\Services;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Oleb2bImage\Oleb2bImageRepository;
use Illuminate\Support\Facades\Lang;

/**
 * Class Oleb2bImageService
 * @package App\Services
 */
class Oleb2bImageService extends BaseService
{
    /**
     * Oleb2bImageService constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->mainRepo = new Oleb2bImageRepository;
        parent::__construct();
    }

    /**
     * @param $sectionName
     * @return mixed
     */
    public function getBySection($sectionName)
    {
        return $this->mainRepo->getByColumn('section', $sectionName);
    }
}