<?php namespace App\Policies;

use App\Repositories\Thread\Thread;
use App\Repositories\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThreadPolicy
{
    use HandlesAuthorization;

    public function view(User $user,Thread $thread)
    {
        return $user->id == $thread->from_user_id || $user->id == $thread->to_user_id;
    }
}
