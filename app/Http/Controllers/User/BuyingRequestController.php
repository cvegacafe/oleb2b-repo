<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserBuyingRequest;
use App\Jobs\SendNewBuyingRequestMail;
use App\Library\Aviso;
use App\Repositories\BuyingRequest\BuyingRequest;
use App\Services\BuyingRequestService;
use App\Services\CategoryService;
use App\Services\CountryService;
use App\Services\CurrencyService;
use App\Services\EmailService;
use App\Services\FileService;
use App\Services\LanguageService;
use App\Services\MeasureService;
use App\Services\PaymentTermService;
use App\Services\SubcategoryService;
use App\Services\SupercategoryService;
use App\Services\TranslateService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BuyingRequestController extends Controller
{
    private $buyingRequestService;

    public function __construct(BuyingRequestService $buyingRequestService)
    {
        $this->buyingRequestService = $buyingRequestService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buyingRequests = $this->buyingRequestService->getUserBuyingRequest(Auth::id());

        return view('user.buying-request.index', compact('buyingRequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SupercategoryService $superCategoryService
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param PaymentTermService $paymentTermService
     * @param CurrencyService $currencyService
     * @param LanguageService $languageService
     * @param CategoryService $categoryService
     * @param SubcategoryService $subcategoryService
     * @return \Illuminate\Http\Response
     */
    public function create(
        SupercategoryService $superCategoryService,
        CountryService $countryService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        CurrencyService $currencyService,
        LanguageService $languageService,
        CategoryService $categoryService,
        SubcategoryService $subcategoryService
    ) {
        $superCategories = $superCategoryService->all();
        $countries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $currencies = $currencyService->all();
        $languages = $languageService->all();
        $categories = $categoryService->all();
        $subcategories = $subcategoryService->all();

        return view('user.buying-request.create', compact('superCategories', 'countries', 'measurements',
            'deliveryTerms', 'paymentTerms', 'currencies', 'languages', 'categories', 'subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserBuyingRequest|Request $request
     * @param TranslateService $translateService
     * @param FileService $fileService
     * @param EmailService $emailService
     * @return \Illuminate\Http\Response
     */
    public function store(
        UserBuyingRequest $request,
        TranslateService $translateService,
        FileService $fileService,
        EmailService $emailService
    ) {
        try {
            DB::beginTransaction();

            if ($request->hasFile('photo')) {
                $pathsImage = $fileService->storeBuyingRequest($request->file('photo'), Auth::id());
                $request['image'] = $pathsImage;
            }

            $request['user_id'] = Auth::id();
            $request['es_name'] = $translateService->transToSpanish($request->get('name'));
            $request['en_name'] = $translateService->transToEnglish($request->get('name'));
            $request['ru_name'] = $translateService->transToRussian($request->get('name'));
            $request['es_description'] = $translateService->transToSpanish($request->get('description'));
            $request['en_description'] = $translateService->transToEnglish($request->get('description'));
            $request['ru_description'] = $translateService->transToRussian($request->get('description'));
            $buyingRequest = $this->buyingRequestService->create($request->all());
            $buyingRequest->paymentTerms()->attach($request->get('payment_terms', []));
            $buyingRequest->languages()->attach($request->get('languages', []));
            $buyingRequest->is_approved = true;
            $buyingRequest->save();
            $emailService->sendMailToSupplierInSuperCategory($buyingRequest->subcategory->category->supercategory,
                $buyingRequest);

            DB::commit();
            Aviso::guardado(trans('validation.custom.messages.record_saved'));

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            Aviso::noGuardado(trans('validation.custom.messages.record_saved_error'));

            return redirect()->back();
        }

        return redirect()->action('User\BuyingRequestController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BuyingRequest $buyingRequest
     * @param SupercategoryService $superCategoryService
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param PaymentTermService $paymentTermService
     * @param CurrencyService $currencyService
     * @param LanguageService $languageService
     * @return \Illuminate\Http\Response
     */
    public function edit(
        BuyingRequest $buyingRequest,
        SupercategoryService $superCategoryService,
        CountryService $countryService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        CurrencyService $currencyService,
        LanguageService $languageService
    ) {
        $superCategories = $superCategoryService->all();
        $countries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $currencies = $currencyService->all();
        $languages = $languageService->all();

        return view('user.buying-request.edit', compact('buyingRequest', 'superCategories', 'countries',
            'measurements', 'deliveryTerms', 'paymentTerms', 'currencies', 'languages'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param BuyingRequest $buyingRequest
     * @param TranslateService $translateService
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     */
    public function update(
        Request $request,
        BuyingRequest $buyingRequest,
        TranslateService $translateService,
        FileService $fileService
    ) {

        try {
            DB::beginTransaction();

            if ($request->hasFile('photo')) {
                $fileService->removeFile($buyingRequest->image);
                $pathsImage = $fileService->storeBuyingRequest($request->file('photo'), Auth::id());
                $request['image'] = $pathsImage;
            }

            $request['es_name'] = $translateService->transToSpanish($request->get('name'));
            $request['en_name'] = $translateService->transToEnglish($request->get('name'));
            $request['ru_name'] = $translateService->transToRussian($request->get('name'));
            $request['es_description'] = $translateService->transToSpanish($request->get('description'));
            $request['en_description'] = $translateService->transToEnglish($request->get('description'));
            $request['ru_description'] = $translateService->transToRussian($request->get('description'));
            $rs = $this->buyingRequestService->update($buyingRequest->id, $request->all());

            if ($rs) {
                $buyingRequest->paymentTerms()->sync($request->get('payment_terms', []));
                $buyingRequest->languages()->sync($request->get('languages', []));
                Aviso::actualizado(trans('validation.custom.messages.record_updated'));
                DB::commit();
            } else {
                Aviso::noActualizado(trans('validation.custom.messages.record_updated_error'));
            }

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            Aviso::noActualizado(trans('validation.custom.messages.record_updated_error'));
        }

        return redirect()->route('user.buying-request.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BuyingRequest $buyingRequest
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     */
    public function destroy(BuyingRequest $buyingRequest, FileService $fileService)
    {
        try {
            $fileService->removeFile($buyingRequest->image);
            $buyingRequest->paymentTerms()->detach();
            $buyingRequest->languages()->detach();
            $buyingRequest->delete();
            Aviso::eliminado(trans('validation.custom.messages.record_deleted'));
        } catch (Exception $e) {
            Log::error($e);
            Aviso::eliminado(trans('validation.custom.messages.record_deleted_error'));
        }

        return redirect()->route('user.buying-request.index');
    }
}
