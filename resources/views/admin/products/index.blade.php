@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Productos :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Nombre</th>
                        <th>Description</th>
                        <th>Color</th>
                        <th>Tipo</th>
                        <th>Supplier</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products->sortByDesc('status') as $product)
                        <tr>
                            <td>
                                <img src="{{asset($product->main_photo)}}" alt="" class="img-responsive"
                                     width="150px"><br>
                            </td>
                            <td>
                                <ul>
                                    <li>Eng: {{$product->en_name}}</li>
                                    <li>Esp: {{$product->es_name}}</li>
                                    <li>Ru: {{$product->ru_name}}</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>Eng: {{$product->en_description}}</li>
                                    <li>Esp: {{$product->es_description}}</li>
                                    <li>Ru: {{$product->ru_description}}</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>Eng: {{$product->en_color}}</li>
                                    <li>Esp: {{$product->es_color}}</li>
                                    <li>Ru: {{$product->ru_color}}</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>Eng: {{$product->en_type}}</li>
                                    <li>Esp: {{$product->es_type}}</li>
                                    <li>Ru: {{$product->ru_type}}</li>
                                </ul>
                            </td>
                            <td>
                                <a href="{{action('Admin\SupplierController@edit',$product->user->supplier->id)}}">{{$product->user->supplier->company_name}}</a>
                            </td>
                            <td>
                                <label class="label label-{{$product->status_color}}">{{$product->status_name}}</label>
                                <a href="{{action('Admin\ProductsController@show',$product)}}">
                                    <button class="btn btn-primary btn-xs">Revisar</button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection