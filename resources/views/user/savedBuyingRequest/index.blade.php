@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="titulo-1 titulo-dashboard">{{trans('menus.user.saved_buying_request')}}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @include('global-partials.avisos')
                <section class="buying-requests">
                    @foreach($buyingRequests as $buyingRequest)
                        <div class="row single-request">
                           @include('global-partials.buying-request-row')

                            <div class="col-xs-12 col-lg-3 buttons" >
                                <button class="contact" onclick="contactWithBuyer('{{$buyingRequest->id}}','{{$buyingRequest->name}}','{{asset('images/uploads/products/'.$buyingRequest->image)}}','{{$buyingRequest->user->languages->implode(\Lang::locale().'_name',' ,')}}','{{$buyingRequest->description}}')">{{trans('buyingRequest.contact_buyer')}}</button>
                                <form action="{{route('user.saved-buying-request.destroy',$buyingRequest)}}" method="POST" class="sweet-confirm"
                                      data-title="{{trans('alerts.buyingRequest.title_delete')}}"
                                      data-message="{{trans('alerts.buyingRequest.message_confirm',['name'=>$buyingRequest->name])}}"
                                      data-ok-button="{{trans('alerts.buttons.ok')}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{csrf_field()}}
                                    <button class="btn-block save"  >{{trans('common.forms.buttons.delete')}}</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </section>
            </div>
        </div>
    </div>
    @include('pages.includes.buyer-contact-popup',[
        'countries' => $allCountries,
        'measurements'=>$measurements,
        'paymentTerms' => $paymentTerms,
        'languages' => $languages
        ])
@endsection
@section('js')
    <script>
        function contactWithBuyer(br_id,product_name,product_photo,languages,description){
            var modal = $('#BuyerModal');
            modal.find('#buyingRequest_id').val(br_id);
            modal.find('#product_name').text(product_name);
            modal.find('#product_photo').attr('src',product_photo);
            modal.find('#languages').text(languages);
            modal.find('#description').text(description);
            modal.find('#message').val(' ');
            modal.modal();
        }
    </script>
@endsection