<?php namespace App\Repositories\Message;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class MessageRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Message();
        parent::__construct();
    }

    /**
     * @param $threadId
     * @return mixed
     */
    public function getLastMessage($threadId)
    {
        return DB::table('threads as t')
            ->join('messages as m', 'm.thread_id', '=', 't.id')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->leftJoin('supplier_user as su', 'su.user_id', '=', 'u.id')
            ->leftJoin('suppliers as s', 'su.supplier_id', '=', 's.id')
            ->leftJoin('attachments as at', 'm.id', '=', 'at.message_id')
            ->where('t.id', $threadId)
            ->select('m.id as id', 'm.user_id', 'u.names as username', 'm.body as content', 'm.created_at',
                'u.image as image', 'at.id as attachment_id', 'at.name as attachment_name')
            ->orderBy('m.id', 'ASC')
            ->get();
    }

    /**
     * @param $threadId
     * @return mixed
     */
    public function getUnreadMessages($threadId)
    {
        return DB::table('threads as t')
            ->join('messages as m', 'm.thread_id', '=', 't.id')
            ->join('users as u', 'm.user_id', '=', 'u.id')
            ->leftJoin('supplier_user as su', 'su.user_id', '=', 'u.id')
            ->leftJoin('suppliers as s', 'su.supplier_id', '=', 's.id')
            ->leftJoin('attachments as at', 'm.id', '=', 'at.message_id')
            ->where('t.id', $threadId)
            ->where('m.read_at', null)
            ->select('m.id as id', 'm.user_id', 'u.names as username', 'm.body as content', 'm.created_at',
                'u.image as image', 'at.id as attachment_id', 'at.name as attachment_name')
            ->orderBy('m.id', 'ASC')
            ->get();
    }

    /**
     * @param array $messages
     * @param array $inputs
     * @param $userId
     * @return mixed
     */
    public function updateMultipleMessages(array $messages, array $inputs, $userId)
    {
        return $this->model->whereIn('id', $messages)->where('user_id', '<>', $userId)->update($inputs);
    }

    /**
     * @param $type
     * @param $userId
     * @param $viewed
     * @return mixed
     */
    public function getMessagesByUser($type, $userId, $viewed)
    {
        return DB::table('messages as m')
            ->join('threads as t', 'm.thread_id', '=', 't.id')
            ->where('t.type', $type)
            ->where('t.to_user_id', $userId)
            ->where('m.read_at', $viewed)
            ->get();
    }

    /**
     * @param $type
     * @param $userId
     * @param $viewed
     * @return mixed
     */
    public function getAllMessagesByUser($type, $userId, $viewed)
    {
        return DB::table('messages as m')
            ->join('threads as t', 'm.thread_id', '=', 't.id')
            ->where('t.type', $type)
            ->where('t.to_user_id', $userId)
            ->orWhere('t.from_user_id', $userId)
            ->where('m.read_at', $viewed)
            ->get();
    }

    public function getUnseenMessagesByUser($type, $userId)
    {
        return DB::select(DB::raw("SELECT * from messages as m inner join threads as t on t.id=m.thread_id
        where t.type = $type and (t.to_user_id = $userId or t.from_user_id = $userId) and m.read_at is null and m.user_id <> $userId"));
        /*return DB::table('messages as m')
            ->join('threads as t', 'm.thread_id', '=', 't.id')
            ->where('t.type', $type)
            ->where('t.from_user_id', $userId)
            ->orWhere('t.to_user_id', $userId)
            ->where('m.read_at', NULL)
            ->where('m.user_id','<>',$userId)
            ->get();*/
    }
}