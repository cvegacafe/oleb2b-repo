@extends('layouts.app')
@section('css')
    <style>
        .red-color {
            color: red !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">{{trans('menus.user.create_company_profile')}}</div>
                    <div class="panel-body">
                        @if(auth()->user()->type !== \App\Repositories\User\User::TYPE_BUYER)
                            <ul class="perfil-empresa-tabs">
                                <li class="active"><a data-toggle="tab"
                                                      href="#details">{{trans('common.forms.suppliers.company_details')}}</a>
                                </li>
                                <li><a data-toggle="tab"
                                       href="#certifications">{{trans('common.forms.suppliers.certifications_and_trademarks')}}</a>
                                </li>
                                <li><a data-toggle="tab"
                                       href="#factory">{{trans('common.forms.suppliers.factory_details')}}</a></li>
                                <li><a data-toggle="tab"
                                       href="#supplier-membership">{{trans('menus.user.memberships')}}</a>
                                </li>
                            </ul>
                        @endif
                        <form role="form" method="POST" action="{{ route('company.profile.store') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="tab-content">
                                <div id="details" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group{{ $errors->has('suppliers_company_name') ? ' has-error' : '' }}">
                                                <label for="company_name"
                                                       class="control-label">{{trans('common.forms.suppliers.company_name')}} <span class="red-color">*</span></label>
                                                <input id="company_name" type="text" class="form-control"
                                                       name="suppliers_company_name"
                                                       value="{{old('suppliers_company_name')}}" required autofocus>
                                                @if ($errors->has('suppliers_company_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('suppliers_company_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_company_description') ? ' has-error' : '' }}">
                                                <label for="company_description"
                                                       class="control-label">{{trans('common.forms.suppliers.company_description')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <textarea name="suppliers_company_description" class="form-control"
                                                              id="suppliers_company_description" cols="10"
                                                              style="height: 114px" rows="4"
                                                              placeholder="{{trans('user.complete_in')}}">{{old('suppliers_company_description')}}</textarea>
                                                    @if ($errors->has('suppliers_company_description'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_company_description') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_member_trade_organization') ? ' has-error' : '' }}">
                                                <label for="suppliers_member_trade_organization"
                                                       class="control-label">{{trans('common.forms.suppliers.member_trade_organization')}}</label>
                                                <div class="">
                                                    <input id="suppliers_member_trade_organization" type="text"
                                                           class="form-control"
                                                           name="suppliers_member_trade_organization"
                                                           value="{{old('suppliers_member_trade_organization')}}">
                                                    @if ($errors->has('suppliers_member_trade_organization'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_member_trade_organization') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_country_id') ? ' has-error' : '' }}">
                                                <label for="suppliers[country_id]" class="control-label">{{trans('common.forms.suppliers.location')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <select name="suppliers_country_id" id="suppliers_country_id"
                                                            class="form-control required">
                                                        @foreach($countries as $country)
                                                            <option value="{{$country->id}}"
                                                                    data-tax-id="{{$country->taxtypes->first()['id']}}"
                                                                    data-tax-name="{{$country->taxtypes->first()['name']}}"
                                                                    data-tax-digits="{{$country->taxtypes->first()['pivot']['digits']}}"
                                                                    {{$country->id==old('suppliers_country_id')?'selected':''}}
                                                            >{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('suppliers_country_id'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_country_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_tax_number') ? ' has-error' : '' }}">
                                                <label for="suppliers_taxtype_id"
                                                       class="control-label">{{trans('common.forms.suppliers.tax_number')}}<span class="red-color">*</span></label>
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        <input type="hidden" id="supplier-taxtype-id"
                                                               name="suppliers_taxtype_id">
                                                        <div id="supplier-taxtype-name" class="text-center"
                                                             style="line-height: 36px;font-weight: 700"></div>
                                                    </div>
                                                    <div class="col-xs-8">
                                                        <input id="suppliers_tax_number" type="text"
                                                               class="form-control" name="suppliers_tax_number"
                                                               value="{{old('suppliers_tax_number')}}">
                                                    </div>
                                                    @if ($errors->has('suppliers_tax_number'))
                                                        <span class="help-block">
                                                                        <strong>{{ str_replace('suppliers tax number',trans('common.forms.suppliers.tax_number'),$errors->first('suppliers_tax_number')) }}</strong>
                                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_company_legal_address') ? ' has-error' : '' }}">
                                                <label for="suppliers_company_legal_address"
                                                       class="control-label">{{trans('common.forms.suppliers.company_legal_address')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <input id="suppliers_company_legal_address" type="text"
                                                           class="form-control" name="suppliers_company_legal_address"
                                                           value="{{old('suppliers_company_legal_address')}}" required>
                                                    @if ($errors->has('suppliers_company_legal_address'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_company_legal_address') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                                <label for="postal_code"
                                                       class="control-label">{{trans('common.forms.suppliers.postal_code')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <input id="postal_code" type="text" class="form-control"
                                                           name="postal_code"
                                                           value="{{old('postal_code')}}"  maxlength="10">
                                                    @if ($errors->has('postal_code'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('postal_code') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_images') ? ' has-error' : '' }}">
                                                <label for="description"
                                                       class="control-label">{{trans('common.forms.suppliers.images')}}</label>
                                                <div class="">
                                                    <input type="file" name="suppliers_image" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                                <label for="office_phone"
                                                       class="control-label">{{trans('user.form.office_phone')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <input id="office_phone" type="text" class="form-control"
                                                           name="office_phone"
                                                           value="{{old('office_phone')}}" required>
                                                    @if ($errors->has('office_phone'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('office_phone') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                                                <label for="mobile_phone"
                                                       class="control-label">{{trans('user.form.mobile_phone')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <input id="mobile_phone" type="text" class="form-control"
                                                           name="mobile_phone"
                                                           value="{{old('mobile_phone')}}" required>
                                                    @if ($errors->has('mobile_phone'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('mobile_phone') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_website') ? ' has-error' : '' }}">
                                                <label for="suppliers_website"
                                                       class="control-label">{{trans('common.forms.suppliers.website')}}</label>
                                                <div class="">
                                                    <input id="suppliers_website" type="url" onblur="checkURL(this)"
                                                           class="form-control" name="suppliers_website"
                                                           value="{{old('suppliers_website')}}">
                                                    @if ($errors->has('suppliers_website'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_website') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_year_company_registered') ? ' has-error' : '' }}">
                                                <label for="suppliers_year_company_registered"
                                                       class="control-label">{{trans('common.forms.suppliers.year_company_registered')}}</label>
                                                <div class="">
                                                    <input id="suppliers_year_company_registered" type="number"
                                                           class="form-control" name="suppliers_year_company_registered"
                                                           value="{{old('suppliers_year_company_registered')}}">
                                                    @if ($errors->has('suppliers_year_company_registered'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_year_company_registered') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_company_type') ? ' has-error' : '' }}">
                                                <label for="suppliers_company_type"
                                                       class="control-label">{{trans('common.forms.suppliers.company_type')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <select name="suppliers_company_type" id="suppliers_company_type"
                                                            class="form-control">
                                                        @foreach($typesCompanies as $typesCompany)
                                                            <option value="{{$typesCompany->id}}" {{$typesCompany->id == old('suppliers_company_type') ? 'selected' : ''}}>{{$typesCompany->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('suppliers_number_employees') ? ' has-error' : '' }}">
                                                <label for="suppliers_number_employees"
                                                       class="control-label">{{trans('common.forms.suppliers.number_employees')}}<span class="red-color">*</span></label>
                                                <div class="">
                                                    <input id="suppliers_number_employees" type="number"
                                                           class="form-control" name="suppliers_number_employees"
                                                           value="{{ old('suppliers_number_employees')}}" required>
                                                    @if ($errors->has('suppliers_number_employees'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_number_employees') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="suppliers_supercategory_id"
                                                       class="control-label">{{trans('common.forms.suppliers.main_business')}}</label>
                                                <div class="">
                                                    <select name="suppliers_supercategory_id"
                                                            id="suppliers_supercategory_id"
                                                            class="form-control required">
                                                        @foreach($supercategories as $supercategory)
                                                            <option value="{{$supercategory->id}}" {{$supercategory->id == old('suppliers_supercategory_id') ? 'selected' : '' }}>{{$supercategory->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                                <label for="languages">{{trans('user.form.languages')}}</label>
                                                <div class="">
                                                    <div class="checkbox-list">
                                                        @foreach($languages as $language)
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" name="languages[]"
                                                                       value="{{$language->id}}"/>&nbsp;
                                                                {{$language->name}}
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                    @if ($errors->has('languages'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('languages') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('other_languages') ? ' has-error' : '' }}"
                                                 style="margin-top: 27px">
                                                <label for="other_languages"
                                                       class="control-label">{{trans('user.form.others_languages')}}</label>
                                                <div class="">
                                                    <input id="other_languages" type="text"
                                                           class="form-control" name="other_languages"
                                                           value="{{ old('other_languages')}}" >
                                                    @if ($errors->has('other_languages'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('other_languages') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-6">
                                            <button type="submit" class="btn btn-primary pull-right">
                                                {{trans('common.forms.buttons.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="certifications" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group{{ $errors->has('new_certification_type_certification') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="new_certification_type_certification"
                                                           class="control-label">{{trans('common.forms.suppliers.type_certification')}}</label>
                                                    <input id="new_certification_type_certification" type="text"
                                                           class="form-control"
                                                           name="new_certification_type_certification"
                                                           value="{{old('new_certification_type_certification')}}">
                                                    @if ($errors->has('new_certification_type_certification'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('new_certification_type_certification') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('new_certification_issued_by') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="new_certification_issued_by"
                                                           class="control-label">{{trans('common.forms.suppliers.issued_by')}}</label>
                                                    <input id="new_certification_issued_by" type="text"
                                                           class="form-control" name="new_certification_issued_by"
                                                           value="{{old('new_certification_issued_by')}}">
                                                    @if ($errors->has('new_certification_issued_by'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('new_certification_issued_by') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('new_certification_start_date') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="new_certification_start_date"
                                                           class="control-label">{{trans('common.forms.suppliers.start_date')}}</label>
                                                    <div class='input-group date date_picker'>
                                                        <input type='text' class="form-control"
                                                               name="new_certification_start_date"
                                                               value="{{old('new_certification_start_date')}}"/>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                    @if ($errors->has('new_certification_start_date'))
                                                        <span class="help-block"><strong>{{ $errors->first('new_certification_start_date') }}</strong></span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('new_certification_expiration_date') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="new_certification_expiration_date"
                                                           class="control-label">{{trans('common.forms.suppliers.expiration_date')}}</label>
                                                    <div class='input-group date date_picker'>
                                                        <input type='text' class="form-control"
                                                               name="new_certification_expiration_date"
                                                               value="{{old('new_certification_expiration_date')}}"/>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                    @if ($errors->has('new_certification_expiration_date'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('new_certification_expiration_date') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('new_certification_image') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="new_certification_image"
                                                           class="control-label">{{trans('common.forms.suppliers.certification_image')}}</label>
                                                    <input id="new_certification_image" type="file" class="form-control"
                                                           name="new_certification_image"
                                                           value="{{old('new_certification_image')}}">
                                                    @if ($errors->has('new_certification_image'))
                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('new_certification_image') }}</strong>
                                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-6">
                                            <button type="submit" class="btn btn-primary pull-right">
                                                {{trans('common.forms.buttons.add')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="factory" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group{{ $errors->has('factoryDetails_factory_address') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="factoryDetails_factory_address"
                                                           class="control-label">{{trans('common.forms.suppliers.factory_address')}}</label>
                                                    <input id="factoryDetails_factory_address" type="text"
                                                           class="form-control" name="factoryDetails_factory_address"
                                                           value="{{old('factoryDetails_factory_address')}}">
                                                    @if ($errors->has('factoryDetails_factory_address'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_factory_address') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('factoryDetails_factory_size') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="factoryDetails_factory_size"
                                                           class="control-label">{{trans('common.forms.suppliers.factory_size')}}</label>
                                                    <input id="factoryDetails_factory_size" type="number"
                                                           class="form-control" name="factoryDetails_factory_size"
                                                           value="{{old('factoryDetails_factory_size')}}">
                                                    @if ($errors->has('factoryDetails_factory_size'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_factory_size') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('factoryDetails_oem_service_offered') || $errors->has('factoryDetails_design_service_offered') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label
                                                            class="control-label">{{trans('common.forms.suppliers.contract_manufacturing')}}
                                                        :</label>
                                                    <br>
                                                    <label>
                                                        <input id="factoryDetails_oem_service_offered" type="checkbox"
                                                               name="factoryDetails_oem_service_offered" {{old('factoryDetails_oem_service_offered')?'checked':''}}>
                                                        {{trans('common.forms.suppliers.oem_service_offered')}}
                                                    </label>
                                                    <label>
                                                        <input id="factoryDetails_design_service_offered"
                                                               type="checkbox"
                                                               name="factoryDetails_design_service_offered" {{old('factoryDetails_design_service_offered')?'checked':''}}>
                                                        {{trans('common.forms.suppliers.design_service_offered')}}
                                                    </label>
                                                    @if ($errors->has('factoryDetails_oem_service_offered'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_oem_service_offered') }}</strong>
                                                        </span>
                                                    @endif
                                                    @if ($errors->has('factoryDetails_design_service_offered'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_design_service_offered') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('factoryDetails_number_qc_staff') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="factoryDetails_number_qc_staff"
                                                           class="control-label">{{trans('common.forms.suppliers.qty_employees_QC')}}</label>
                                                    <input id="factoryDetails_number_qc_staff" type="number"
                                                           class="form-control" name="factoryDetails_number_qc_staff"
                                                           value="{{old('factoryDetails_number_qc_staff')}}">
                                                    @if ($errors->has('factoryDetails_number_qc_staff'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_number_qc_staff') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group{{ $errors->has('factoryDetails_number_rd_staff') ? ' has-error' : '' }}">
                                                <label for="factoryDetails_number_rd_staff"
                                                       class="control-label">{{trans('common.forms.suppliers.qty_employees_product_design')}}</label>
                                                <input id="factoryDetails_number_rd_staff" type="number"
                                                       class="form-control" name="factoryDetails_number_rd_staff"
                                                       value="{{old('factoryDetails_number_rd_staff')}}">
                                                @if ($errors->has('factoryDetails_number_rd_staff'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('factoryDetails_number_rd_staff') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('factoryDetails_number_production_lines') ? ' has-error' : '' }}">
                                                <label for="factoryDetails_number_production_lines"
                                                       class="control-label">{{trans('common.forms.suppliers.qty_employees_production_lines')}}</label>
                                                <input id="factoryDetails_number_production_lines" type="number"
                                                       class="form-control"
                                                       name="factoryDetails_number_production_lines"
                                                       value="{{old('factoryDetails_number_production_lines')}}">
                                                @if ($errors->has('factoryDetails_number_production_lines'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('factoryDetails_number_production_lines') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('factoryDetails_annual_output_value') ? ' has-error' : '' }}">
                                                <div class="">
                                                    <label for="factoryDetails_annual_output_value"
                                                           class="control-label">{{trans('common.forms.suppliers.qty_annual_output_value')}}
                                                        (USD)</label>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <input id="factoryDetails_annual_output_value" type="number"
                                                                   class="form-control"
                                                                   name="factoryDetails_annual_output_value"
                                                                   value="{{old('factoryDetails_annual_output_value')}}">
                                                        </div>
                                                        {{-- <div class="col-xs-6">
                                                             <select name="factoryDetails_measure_id" id="factoryDetails_measure_id" class="form-control">
                                                                 @foreach($measurements as $measurement)
                                                                     <option value="{{$measurement->id}}">{{$measurement->name}}</option>
                                                                 @endforeach
                                                             </select>
                                                         </div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-6">
                                            <button type="submit" class="btn btn-primary pull-right">
                                                {{trans('common.forms.buttons.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="supplier-membership" class="tab-pane fade">
                                    <div class="membresias">
                                        <div class="row logos-top">
                                            <div class="col-xs-3">

                                            </div>
                                            <div class="col-xs-3 free">
                                                <div class="top">
                                                    {{trans('memberships.basic')}}
                                                </div>
                                                <div class="icon">
                                                    <figure>
                                                        <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Basic.png')}}"
                                                             class="img-responsive">
                                                    </figure>
                                                </div>
                                            </div>
                                            <div class="col-xs-3 advanced">
                                                <div class="top">
                                                    {{trans('memberships.advanced')}}
                                                </div>
                                                <div class="icon">
                                                    <figure>
                                                        <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Advanced.png')}}"
                                                             class="img-responsive">
                                                    </figure>
                                                </div>
                                            </div>
                                            <div class="col-xs-3 premium">
                                                <div class="top">
                                                    {{trans('memberships.premium')}}
                                                </div>
                                                <div class="icon">
                                                    <figure>
                                                        <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Premium.png')}}"
                                                             class="img-responsive">
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cuerpo">
                                            <div class="row">
                                                <div class="col-xs-3 description">
                                <span>
                                  {{--  <i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.product_posting')}}
                                </span>
                                                </div>
                                                <div class="col-xs-3 free">
                                <span>
                                     {{App\Repositories\Membership\Membership::find(1)->product_posting}}
                                </span>
                                                </div>
                                                <div class="col-xs-3 premium">
                                <span>
                                    {{trans('memberships.unlimited')}}
                                </span>
                                                </div>
                                                <div class="col-xs-3 advanced">
                                <span>
                                    {{trans('memberships.unlimited')}}
                                </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3 description">
                                <span>

                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.banner_main_page')}}

                                </span>
                                                </div>
                                                <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 premium">
                                <span>

                               {{trans('memberships.average_rotation',['time'=>$advancedMembership->main_rotation_banners]) }}

                                </span>
                                                </div>
                                                <div class="col-xs-3 advanced">
                                <span>

                                {{trans('memberships.average_rotation',['time'=>$premiumMembership->main_rotation_banners]) }}

                                </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3 description">
                                <span>

                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.banner_inner_pages')}}

                                </span>
                                                </div>
                                                <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 premium">
                                <span>

                               {{trans('memberships.average_rotation',['time'=>$advancedMembership->inner_rotation_banners]) }}

                                </span>
                                                </div>
                                                <div class="col-xs-3 advanced">
                                <span>

                               {{trans('memberships.average_rotation',['time'=>$premiumMembership->inner_rotation_banners]) }}

                                </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3 description">
                                <span>

                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.featured_products')}}


                                </span>
                                                </div>
                                                <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 advanced">
                                <span>

                                 <i class="fa fa-check"></i>

                                </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3 description">
                                <span>
                                   {{-- <i class="fa fa-check-circle-o"></i>--}}
                                    <span class="rows2">
                                       {{trans('memberships.ability_quote_buying_request')}}
                                    </span>
                                </span>
                                                </div>
                                                <div class="col-xs-3 free">
                                <span>

                                 <i class="fa fa-check"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 advanced">
                                <span>

                                 <i class="fa fa-check"></i>

                                </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-3 description">
                                <span>
                                    {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.subaccounts')}}
                                </span>
                                                </div>
                                                <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                </div>
                                                <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="choose">
                                            <div class="row">
                                                <div class="col-xs-3">

                                                </div>
                                                <div class="col-xs-3 free">
                                                    @if(Auth::user()->activeMembership()->id == \App\Library\Constants::MEMBERSHIP_BASIC)
                                                        <div class="inner">
                                                            <button class="disabled">{{trans('memberships.active')}}</button>
                                                        </div>
                                                    @else
                                                        <div class="inner">
                                                            <button class="free disabled">{{trans('memberships.choose_plan')}}</button>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-xs-3">
                                                    @if(Auth::user()->activeMembership()->id == $advancedMembership->id)
                                                        <div class="inner">
                                                            <button class="disabled">{{trans('memberships.active')}}</button>
                                                        </div>
                                                    @else
                                                        <div class="inner">
                                                            <button class="advanced" data-toggle="modal"
                                                                    data-target="#modal-advanced">{{trans('memberships.choose_plan')}}</button>
                                                        </div>
                                                    @endif

                                                </div>
                                                <div class="col-xs-3">
                                                    @if(Auth::user()->activeMembership()->id == \App\Library\Constants::MEMBERSHIP_PREMIUM)
                                                        <div class="inner">
                                                            <button class="disabled">{{trans('memberships.active')}}</button>
                                                        </div>
                                                    @else
                                                        <div class="inner">
                                                            <button class="premium" data-toggle="modal"
                                                                    data-target="#modal-premium">{{trans('memberships.choose_plan')}}</button>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-premium">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{$premiumMembership->name}} : {{trans('memberships.choose_plan')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name="method" id="frecuency_premium" class="form-control" required>
                            <option selected disabled>-- {{trans('memberships.choose_plan')}}</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_MONTHLY}}">
                                USD {{$premiumMembership->monthly_price}} / {{trans('common.month')}}</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_ANNUAL}}">
                                USD {{$premiumMembership->annual_price}} / {{trans('common.year')}} </option>
                        </select>
                        <span class="help-block">
                            <strong id="error-msg"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <small>{{trans('memberships.disclaimer1')}}</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{trans('common.forms.buttons.close')}}</button>
                    <button type="button" class="btn btn-primary"
                            onClick="sendPlanSelection('{{$premiumMembership->id}}',this)">{{trans('common.forms.buttons.confirm')}}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="modal-advanced">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{$advancedMembership->name}} : {{trans('memberships.choose_plan')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name="method" id="frecuency_advanced" class="form-control" required>
                            <option selected disabled>-- {{trans('memberships.choose_plan')}}</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_MONTHLY}}">
                                USD {{$advancedMembership->monthly_price}} / {{trans('common.month')}}</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_ANNUAL}}">
                                USD {{$advancedMembership->annual_price}} / {{trans('common.year')}} </option>
                        </select>
                        <span class="help-block">
                            <strong id="error-msg"></strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <small>{{trans('memberships.disclaimer1')}}</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            onClick="sendPlanSelection('{{$advancedMembership->id}}',this)">{{trans('common.forms.buttons.confirm')}}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @include('user.memberships.payu-form')
@endsection
@section('js')
    <script>
        function checkURL(abc) {
            var string = abc.value;
            if (!~string.indexOf("http")) {
                string = "http://" + string;
            }
            abc.value = string;
            return abc
        }

        $(function () {

            var sectionErrorParameter = getUrlParameter('section');

            if (sectionErrorParameter) {
                var tab = $('#' + sectionErrorParameter);
                var tabTitle = $('a[href="#' + sectionErrorParameter + '"]');
                $('.tab-pane').removeClass('in active');
                tab.addClass('in active');
                tab.find('.edit-form').addClass('in active');
            }

            loadTaxType();

            @if(old('suppliers_country_id'))
            loadTaxType();
            @else
            $('#suppliers_country_id').on('change', function () {
                loadTaxType();
            });
            @endif



        });

        function loadTaxType() {
            var supplier_country_selected = $('#suppliers_country_id option:selected');
            var taxId = supplier_country_selected.data('tax-id');
            var taxName = supplier_country_selected.data('tax-name');
            var taxDigits = supplier_country_selected.data('tax-digits');
            //console.log(taxId,taxName);
            $('#supplier-taxtype-id').val(taxId);
            $('#supplier-taxtype-name').text(taxName);
        }

        function sendPlanSelection(membershipId, el) {

            var content = $(el).parent().parent().find('.modal-body');
            var plan = content.find('select option:selected');

            if (plan.attr('disabled') == 'disabled') {
                var form_group = content.find('.form-group');
                form_group.addClass('has-error');
                var errorMsg = content.find('#error-msg');
                errorMsg.text('Debe seleccionar un plan');
                return;
            }

            var request = $.ajax({
                url: '{{route('api.payu.signature')}}',
                method: "POST",
                data: {
                    membership_id: membershipId,
                    frequency: plan.val(),
                    _token: window.Laravel.csrfToken,
                    lang: window.Laravel.lang
                },

                dataType: "json",
                async: false
            });

            request.done(function (data) {
                var form = $('#payu-form');
                form.find('#signature').val(data.signature);
                form.find('#description').val(data.description);
                form.find('#referenceCode').val(data.referenceCode);
                form.find('#amount').val(data.amount);
                form.find('#currency').val(data.currency);
                form.submit();
                return true;
            });

            request.fail(function (jqXHR, textStatus) {
                console.log(jqXHR);
                console.log("Request failed: " + textStatus);
            });
        }
    </script>
@endsection