<?php namespace App\Http\Middleware;

use App\Library\Aviso;
use App\Library\Constants;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class PremiumUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUser = Auth::user();
        $currentMembership = $currentUser->activeMembership();

        if ($currentMembership && $currentMembership->id == Constants::MEMBERSHIP_PREMIUM && Carbon::parse($currentMembership->pivot->finish_at)->gte(Carbon::now())) {
            return $next($request);
        }

        Aviso::error(trans('common.messages.be_premium_user'));
        return redirect()->route('user.memberships.index');
    }
}
