<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\MarketService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class MarketsController extends Controller
{
    private $marketService = null;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function getMarkets(Request $request)
    {
        $currentLanguage = $request->get('lang', Lang::locale());
        $languages = $this->marketService->getMarketsByLang($currentLanguage);

        return response()->json(['markets' => $languages]);
    }
}
