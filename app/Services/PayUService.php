<?php namespace App\Services;

use App\Library\Constants;
use App\Notifications\NewPurchasedMembership;
use App\Repositories\Payu\PayuRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class PayUService extends BaseService
{

    public $mainRepo = null;

    public function __construct(PayuRepository $payuRepository)
    {
        $this->mainRepo = $payuRepository;
        parent::__construct();
    }

    public function getParameters()
    {
        if (true) {
            return (object)[
                'merchantId' => '508029',
                'ApiKey' => '4Vj8eK4rloUd272L48hsrarnUA',
                'test' => true,
                'accountId' => '512323',
                'referenceCode' => uniqid('TestPayU_'),
                'url' => 'https://sandbox.gateway.payulatam.com/ppp-web-gateway'
            ];
        }

        return (object)[
            'merchantId' => '576778',
            'ApiKey' => '8G1VNgC4zhnAvIjX71395CFi0C',
            'accountId' => '639795',
            'test' => false,
            'referenceCode' => uniqid('Oleb2b-'),
            'url' => 'https://gateway.payulatam.com/ppp-web-gateway/'
        ];
    }

    public function getSignaturePayU($referenceCode, $amount, $currency)
    {
        $payu = $this->getParameters();
        $signature = md5($payu->ApiKey . '~' . $payu->merchantId . '~' . $referenceCode . '~' . $amount . '~' . $currency);
        return $signature;
    }

    public function pageResponse($request)
    {
        $data = [];

        switch ($request->get('transactionState')) {
            case Constants::PAYU_TRANSACTION_STATE_APPROVED:
                $data['title'] = 'Gracias!';
                $data['subtitle'] = 'Transacci&oacute;n aprobada';
                $data['msg'] = 'Su compra ha sido realizada exitosamente.';
                break;
            case Constants::PAYU_TRANSACTION_STATE_DECLINED:
                $data['title'] = 'Oopps!';
                $data['subtitle'] = 'La operaci&oacute;n ha sido rechazada por Payu.';
                $data['msg'] = 'Payu ha rechazado su solicitud de pago. Por favor verifique sus datos e intentelo nuevamente';
                break;
            case Constants::PAYU_TRANSACTION_STATE_ERROR:
                $data['title'] = 'ERROR';
                $data['subtitle'] = 'Transacci&oacute;n fallida :(';
                $data['msg'] = 'Ha ocurrido un error en el proceso de pago de Payu. Por favor verifique los datos ingresados e int&eacute;ntelo nuevamente';
                break;
            case Constants::PAYU_TRANSACTION_STATE_PENDING:
                $data['title'] = 'Pendiente';
                $data['subtitle'] = 'Transacci&oacute; pendiente';
                $data['msg'] = 'Payu ha marcado esta transacci&oacute;n como pendiente. ';
                break;
            default:
                $data['title'] = 'Mensaje';
                $data['subtitle'] = 'Mensaje de Payu';
                $data['msg'] = $request->get('mensaje');
        }

        return $data;
    }


    public function confirmation(
        $request,
        OrderService $orderService,
        MessengerService $messengerService
    ) {
        if (!$this->mainRepo->firstByColumn('reference_sale', $request->get('reference_sale'))) {
            $newPayu = [
                'reference_pol' => $request->get('reference_pol'),
                'currency' => $request->get('currency'),
                'is_test' => $request->get('test') == '1' ? true : false,
                'transaction_date' => $request->get('transaction_date'),
                'cc_holder' => $request->get('cc_holder'),
                'description' => $request->get('description'),
                'value' => $request->get('value'),
                'email_buyer' => $request->get('email_buyer'),
                'response_message_pol' => $request->get('response_message_pol'),
                'transaction_id' => $request->get('transaction_id'),
                'sign' => $request->get('sign'),
                'billing_address' => $request->get('billing_address'),
                'payment_method_name' => $request->get('payment_method_name'),
                'shipping_country' => $request->get('shipping_country'),
                'commision_pol_currency' => $request->get('commision_pol_currency'),
                'commision_pol' => $request->get('commision_pol'),
                'ip' => $request->get('ip'),
                'billing_city' => $request->get('billing_city'),
                'reference_sale' => $request->get('reference_sale')
            ];

            $payu = $this->mainRepo->create($newPayu);
            $order = $orderService->findByReferenceCode($payu->reference_sale);

            if ($order) {
                $user = $order->user;
                $membership = $order->membership;
                $now = Carbon::now();
                $user->memberships()->save($membership, [
                    'main_rotation_banners' => $membership->main_rotation_banners,
                    'inner_rotation_banners' => $membership->inner_rotation_banners,
                    'product_posting' => $membership->product_posting,
                    'amount' => $order->amount,
                    'frequency' => $order->frequency,
                    'finish_at' => $order->frequency == Constants::MEMBERSHIP_FREQUENCY_ANNUAL ? $now->addYear()->addMonths(3) : $now->addMonth()//TODO: quitar los 3 meses en anual cuando acabe promocion
                ]);

                $order->status = Constants::ORDER_STATUS_ACCEPTED;
                $order->save();
                $messengerService->sendMessageSuccessPayment($user->id, $membership->name);
                $user->notify(new NewPurchasedMembership($user, $membership));

            } else {
                Log::error('No se pudo encontrar una orden para el pago ingresado');
            }
        }
    }
}
