<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Services\CountryService;
use App\Services\TaxTypeService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TaxTypesController extends Controller
{

    private $taxTypeService, $countryService;

    public function __construct(TaxTypeService $taxTypeService, CountryService $countryService)
    {
        $this->taxTypeService = $taxTypeService;
        $this->countryService = $countryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxtypes = $this->taxTypeService->all();
        $countries = $this->countryService->all();

        return view('admin.taxtypes.index', compact('taxtypes', 'countries'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $taxType = $this->taxTypeService->create($request->all());

            foreach ($request->get('countries', []) as $key => $item) {
                $taxType->countries()->attach($item);
            }

            Aviso::guardado();

        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }
        return redirect()->action('Admin\TaxTypesController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $taxtype = $this->taxTypeService->find($id);
        $countries = $this->countryService->all();

        return view('admin.taxtypes.edit', compact('taxtype', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $taxType = $this->taxTypeService->find($id);
            if ($this->taxTypeService->update($taxType->id, ['name' => $request->get('name')])) {
                $taxType->countries()->detach();
                foreach ($request->get('countries', []) as $key => $item) {
                    $taxType->countries()->attach($item);
                }

            }

            Aviso::actualizado();

        } catch (Exception $e) {
            Log::error($e);
            Aviso::noActualizado();
        }
        return redirect()->action('Admin\TaxTypesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $taxType = $this->taxTypeService->find($id);
        $taxType->countries()->detach();
        $taxType->delete();
        Aviso::eliminado();
        return redirect()->action('Admin\TaxTypesController@index');
    }

}
