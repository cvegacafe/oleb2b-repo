<?php namespace App\Repositories\Product;

use App\DTOs\SearchableDTO;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;


/**
 * Class ProductRepository
 * @package App\Repositories\Product
 */
class ProductRepository extends BaseRepository
{
    /**
     * ProductRepository constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->model = new Product();
        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->model->with('subcategory', 'country', 'deliveryTerms', 'currency', 'paymentTerms')->get();
    }

    /**
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRandomProducts($limit)
    {
        return $this->model->with('moqMeasure', 'currency', 'saleUnitMeasure', 'country')->where('is_approved',
            true)->inRandomOrder()->limit($limit)->get();
    }

    /**
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function findBySlugs($slug)
    {
        return $this->model->with('subcategory', 'subcategory.category.supercategory')
            ->where('es_slug', $slug)->orWhere('en_slug', $slug)->orWhere('ru_slug', $slug)->first();
    }

    /**
     * @param $userId
     * @param $relationShipName
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getProductsHas($userId, $relationShipName)
    {
        return $this->model->with($relationShipName)->has($relationShipName)->where('user_id', $userId)->get();
    }

    /**
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getProductsByUserThreads($userId)
    {
        return $this->model->with('threads', 'threads.toUser', 'threads.fromUser')->whereHas('threads',
            function ($query) use ($userId) {
                $query->where('from_user_id', $userId);
                $query->orWhere('to_user_id', $userId);
            })->get();
    }

    /**
     * @param $lang_prefix
     * @param $ElementsByPage
     * @param null $q
     * @param array $countriesNames
     * @param array $categoryIds
     * @return SearchableDTO
     */
    public function searchApprovedPaginate(
        $lang_prefix,
        $ElementsByPage,
        $q = null,
        $countriesNames = [],
        $categoryIds = []
    ): SearchableDTO {

        $query = $this->model->with(['country', 'moqMeasure', 'currency', 'saleUnitMeasure', 'user'])
            ->where('is_approved', true)
            ->where(function ($query) use ($q, $lang_prefix) {
                if ($q != null && $q != trans('dashboard.products')) {
                    $words = explode(' ', $q);
                    foreach ($words as $word) {
                        $query->where('products.' . $lang_prefix . '_name', 'like', '%' . $word . '%');
                        $query->orWhere('products.' . $lang_prefix . '_description', 'like', '%' . $q . '%');
                    }
                }
            })
            ->join('users as u', 'u.id', '=', 'products.user_id')
            ->join('membership_user as mu', 'mu.user_id', '=', 'u.id')
            ->where('mu.is_active', 1)->where(function ($query) {
                if (request()->has('sc')) {
                    $query->where('subcategory_id', request()->get('sc'));
                }
            });

        # Get countries in search
        $countriesQuery = clone $query;

        $countries = $countriesQuery->select([
            'countries.' . $lang_prefix . '_name as full_name',
            'products.country_id as country_id',
            DB::raw('count(*) as total')
        ])->whereHas('subcategory.category', function ($query) use ($categoryIds) {
            if (!empty($categoryIds)) {
                $query->whereIn('id', $categoryIds);
            }
        })->join('countries', 'countries.id', '=', 'products.country_id')->groupBy('products.country_id')->get();

        # Get categories in search
        $categoriesQuery = clone $query;
        $categoriesProducts = $categoriesQuery->select([
            'categories.' . $lang_prefix . '_name as full_name',
            'categories.id as id',
            DB::raw('count(*) as total')
        ])->whereHas('country', function ($query) use ($countriesNames, $lang_prefix) {
            if (!empty($countriesNames)) {
                $query->whereIn($lang_prefix . '_name', $countriesNames);
            }
        })->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->join('categories', 'subcategories.category_id', '=',
                'categories.id')->groupBy('categories.id')->get();

        # Get products in search
        if ($countriesNames && $countriesNames != trans('common.all')) {
            $query->whereHas('country', function ($query) use ($countriesNames, $lang_prefix) {
                $query->whereIn($lang_prefix . '_name', $countriesNames);
            });
        }

        $productsPagination = $query->whereHas('subcategory.category', function ($query) use ($categoryIds) {
            if (!empty($categoryIds)) {
                $query->whereIn('id', $categoryIds);
            }
        })->orderBy('mu.membership_id', 'desc')->inRandomOrder()->paginate($ElementsByPage);

        return new SearchableDTO($productsPagination, $countries, $categoriesProducts);
    }
}