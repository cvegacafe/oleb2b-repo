<?php namespace App\Jobs;

use App\Repositories\BuyingRequest\BuyingRequest;
use App\Repositories\Supercategory\Supercategory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNewBuyingRequestMail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $supercategory = null;

    private $buyingRequest = null;
    /**
     * MailToSuppliers constructor.
     * @param Supercategory $supercategory
     * @param BuyingRequest $buyingRequest
     */
    public function __construct(Supercategory $supercategory,BuyingRequest $buyingRequest)
    {
        $this->supercategory = $supercategory;

        $this->buyingRequest = $buyingRequest;
    }

    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        foreach ($this->supercategory->suppliers as $supplier) {

            foreach ($supplier->users as $user) {

                $data['buying_request_id'] = $this->buyingRequest->id;

                $mailer->send('mails.new-buying-request', ['data' => $data], function ($m)use($user) {
                    $m->from('no-reply@oleb2b.com', 'Oleb2b.com');
                    $m->to($user->email, $user->names)->subject(trans('mails.new_buying_request',[],null,$user->languages->first()->abbreviation));
                });

            }

        }
    }
}
