@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Monedas :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Moneda</th>
                            <th>Simbolo</th>
                            <th>Habilitado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($currencies as $currency)
                        <tr>
                            <td><b>{{$currency->id}}</b></td>
                            <td>{{$currency->currency}}</td>
                            <td>{{$currency->currency_code}}</td>
                            <td><input type="checkbox" {{$currency->enabled?'checked':''}} id="{{$currency->id}}" class="chk2"/> <label for="{{$currency->id}}" style="cursor:hand">{{$currency->enabled?'habilitado':'deshabilitado'}}</label> </td>
                            <td>
                                <a href="{{action('Admin\CurrenciesController@edit',$currency)}}"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="fa fa-pencil"></span></button></a>
                                <form action="{{action('Admin\CurrenciesController@destroy',$currency)}}" method="POST" onsubmit="return confirm('Esta seguro q desea eliminar {{$currency->currency}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs" ><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <form action="{{action('Admin\CurrenciesController@store')}}" method="POST">
                        {{csrf_field()}}
                        <tfoot>
                            <tr>
                                <td></td>
                                <td><input type="text" name="currency" class="form-control"></td>
                                <td><input type="text" name="currency_code" class="form-control"></td>
                                <td><input type="checkbox" name="enabled" class="form-control"></td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
                            </tr>
                        </tfoot>
                    </form>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        var path = '{{route('api.currency.enable')}}';

        $(function(){
            $('.chk2').on('click',function(e){
                e.preventDefault();
                var checked = false;
                var self = $(this);
                var id = $(this).attr('id');
                if ($(this).is(':checked')){
                    checked = true;
                }
                var request = $.ajax({
                    url: path,
                    method: "POST",
                    data: {_token: window.Laravel.csrfToken,checked:checked,id:id,_method:'put'},
                    dataType: "json"
                });
                request.done(function (data) {
                    if(data.response==true){
                        self.prop('checked',data.value);
                        if(data.value){
                            self.parent().find('label').text('habilitado');
                        }else{
                            self.parent().find('label').text('inahabilitado');
                        }

                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });
        });
    </script>
@endsection
