<?php namespace App\Repositories\PaymentTerm;

use App\Repositories\BaseRepository;

class PaymentTermRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new PaymentTerm();
        parent::__construct();
    }
}