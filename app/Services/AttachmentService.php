<?php namespace App\Services;

use App\Repositories\Attachment\AttachmentRepository;

class AttachmentService extends BaseService
{

    public function __construct(AttachmentRepository $attachmentRepository)
    {
        $this->mainRepo = $attachmentRepository;
        parent::__construct();
    }

}