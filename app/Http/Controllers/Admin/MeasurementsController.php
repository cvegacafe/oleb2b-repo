<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Library\Aviso;
use App\Repositories\Measure\Measure;
use App\Services\MeasureService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MeasurementsController extends Controller
{
    private $measureService;

    public function __construct(MeasureService $measureService)
    {
        $this->measureService = $measureService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measurements = $this->measureService->all();

        return view('admin.measurements.index', compact('measurements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.measurements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->measureService->create($request->all());
            Aviso::guardado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }

        return redirect()->action('Admin\MeasurementsController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Measure $measurement
     * @return \Illuminate\Http\Response
     */
    public function edit(Measure $measurement)
    {
        return view('admin.measurements.edit', compact('measurement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Measure $measurement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Measure $measurement)
    {
        try {
            $this->measureService->update($measurement->id, $request->all());
            Aviso::actualizado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noActualizado();
        }

        return redirect()->action('Admin\MeasurementsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Measure $measurement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Measure $measurement)
    {
        try {
            $measurement->delete();
            Aviso::eliminado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado();
        }

        return redirect()->action('Admin\MeasurementsController@index');
    }
}
