<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SendNewMessage;
use App\Library\Constants;
use App\Services\AttachmentService;
use App\Services\FileService;
use App\Services\MessageService;
use App\Services\PusherService;
use App\Services\ThreadService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MessagesController extends Controller
{
  private $messageService = null;

  private $pusherService = null;

  public function __construct(MessageService $messageService, PusherService $pusherService)
  {
    $this->messageService = $messageService;
    $this->pusherService = $pusherService;
  }

  /**
   * @param Request $request
   * @param ThreadService $threadService
   * @return \Illuminate\Http\JsonResponse
   */
  public function getLastMessages(Request $request, ThreadService $threadService)
  {
    $thread = $threadService->find($request->get('thread_id'));
    $this->authorize('view', $thread);
    $messages['messages'] = $this->messageService->getLastMessages($thread);
    foreach ($messages['messages'] as $message) {
      if ($message->attachment_id != null) {
        $message->attachment_url = route('web.file.download', $message->attachment_id);
      }
      $message->created_at = Carbon::parse($message->created_at)->format('d/m/Y g:i:s a');
      $message->image = $message->image ? asset($message->image) : asset(Constants::USER_DEFAULT_IMAGE);
    }
    $messages['isSupplier'] = $thread->product->user_id === auth()->id();
    $messages['buyer'] = $thread->buyer;
    $messages['supplier'] = $thread->supplier;
    return response()->json($messages);
  }

  /**
   * @param Request $request
   * @param FileService $fileService
   * @param AttachmentService $attachmentService
   */
  public function postSendMessage(Request $request, FileService $fileService, AttachmentService $attachmentService)
  {
    try {

      $threadId = explode('-', $request->get('channel'));
      $sender_user_id = Auth::id();
      $content = e($request->input('chat_text'));

      $message = $this->messageService->create([
        'thread_id' => $threadId[2],
        'user_id' => $sender_user_id,
        'body' => $content
      ]);

      $pusherMessage = [
        'content' => $content,
        'username' => Auth::user()->names,
        'image' => $request->get('own_image'),
        'timestamp' => (time() * 1000),
        'user_id' => Auth::id(),
        'id' => $message->id
      ];

      if ($request->hasFile('file')) {
        $path = $fileService->storeAttachmentMessage($request->file('file'), Auth::id());
        $newAttachment = [
          'message_id' => $message->id,
          'path' => $path,
          'type' => $request->file('file')->getClientOriginalExtension(),
          'name' => $request->file('file')->getClientOriginalName(),
        ];

        $attachment = $attachmentService->create($newAttachment);
        $attachmentInfo = [
          'attachment_url' => route('web.file.download', $attachment->id),
          'attachment_name' => $attachment->name
        ];

        $pusherMessage = array_merge($pusherMessage, $attachmentInfo);
      }

      $this->pusherService->sendMessage($request->get('channel'), $pusherMessage);
      dispatch(new SendNewMessage($threadId[2], Auth::id()));

    } catch (Exception $e) {
      Log::error($e);
    }
  }

  /**
   * @param MessageService $messageService
   * @return \Illuminate\Http\JsonResponse
   */
  public function countProductMessages(MessageService $messageService)
  {
    $messages = $messageService->getMessagesProductsFromUser(Auth::id());
    // dd($messages);
    return response()->json(['response' => 'ok', 'count' => count($messages)]);
  }

  /**
   * @param MessageService $messageService
   * @return \Illuminate\Http\JsonResponse
   */
  public function countBuyingRequestMessages(MessageService $messageService)
  {
    $messages = $messageService->getMessagesBuyingRequestFromUser(Auth::id());

    return response()->json(['response' => 'ok', 'count' => count($messages)]);
  }

  /**
   * @param Request $request
   */
  public function postAuth(Request $request)
  {
    \Debugbar::disable();

    echo $this->pusherService->auth($request->get('channel_name'), $request->get('socket_id'), Auth::id(),
      Auth::user()->names);
  }
}
