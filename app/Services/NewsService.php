<?php namespace App\Services;

use App\Repositories\News\NewsRepository;

class NewsService extends BaseService
{

    public function __construct(NewsRepository $newsRepository)
    {
        $this->mainRepo = $newsRepository;
        parent::__construct();
    }
}