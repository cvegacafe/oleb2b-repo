@extends('layouts.pages')
@section('title')
    Home
@endsection
@section('content')
    <div class="container-fluid categorias-top">
        <div class="col-xs-12">
            <h1>{{trans('portal.categories')}}</h1>
        </div>
    </div>
    <div class="container categories-buttons">
        <div class="row">
            @foreach($superCategories as $row)
            <div class="col-xs-12 col-xmd-6 col-md-4 col-lg-15">
                <div class="category-button category{{$row->id}}" target="category{{$row->id}}">
                    <i class="icon-icon-{{$row->id}}"></i>
                    <span>{{$row->name}}</span>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @foreach($superCategories as $row)
        <div class="category-lists category{{$row->id}}" id="category{{$row->id}}">
            <i class="icon-icon-{{$row->id}}"></i>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="title">
                            <h3>{{$row->name}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="subcategories">
                <div class="row">
                    @foreach($row->categories as $category)
                        <div class="col-xs-12 col-sm-2 col-lg-4">
                            <h4 class="category-color{{$row->id}}">{{$category->name}}</h4>
                            <ul>
                                @foreach($category->subcategories as $subcategory)
                                    <li><a href="{{route('portal.search',['q'=>trans('dashboard.products'),'countries'=>trans('common.all'),'sc'=>$subcategory->id])}}">{{$subcategory->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
                <div class="desplegar-mas">
                    <span>
                        <span>{{trans('common.more')}}</span>
                        <i class="fa fa-arrow-down"></i>
                    </span>
                </div>
            </div>
        </div>
    @endforeach
    <div class="back-top">
        <i class="fa fa-arrow-up"></i>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){

            showBackTop();
            $(document).on('scroll',function(){
                showBackTop();
            });
            function showBackTop(){
                var scrollTop = $(document).scrollTop();
                if(scrollTop > 900){
                    $('.back-top').addClass('scrolled');
                }else{
                    $('.back-top').removeClass('scrolled');
                }
            }
        })
    </script>
@endsection