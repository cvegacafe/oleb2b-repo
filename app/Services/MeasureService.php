<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:22 PM
 */

use App\Repositories\Measure\MeasureRepository;

class MeasureService extends BaseService
{

    public function __construct(MeasureRepository $measureRepository)
    {
        $this->mainRepo = $measureRepository;
        parent::__construct();
    }

}