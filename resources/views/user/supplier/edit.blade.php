@extends('layouts.app')
@section('css')
    <style>
        .img-preview {
            width: 150px;
            margin-top: 20px;
        }
        .red-color {
            color: red !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">{{trans('menus.user.manage_company_profile')}}</div>
                    <div class="panel-body">
                        @if(auth()->user()->type !== \App\Repositories\User\User::TYPE_BUYER)
                            <ul class="perfil-empresa-tabs">
                                <li class="active"><a data-toggle="tab"
                                                      href="#details">{{trans('common.forms.suppliers.company_details')}}</a>
                                </li>
                                <li><a data-toggle="tab"
                                       href="#certifications">{{trans('common.forms.suppliers.certifications_and_trademarks')}}</a>
                                </li>
                                <li><a data-toggle="tab"
                                       href="#factory">{{trans('common.forms.suppliers.factory_details')}}</a></li>
                                <li><a data-toggle="tab"
                                       href="#supplier-membership">{{trans('menus.user.memberships')}}</a>
                                </li>
                            </ul>
                        @endif
                        <form role="form" method="POST" action="{{ route('company.profile.update',$supplier) }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="tab_name" id="tab_name" value="">
                            <div class="tab-content">
                                <div id="details" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <a href="#" class="boton-azul-no-shadow toggle-edit"
                                               data-form-edit="#edit-company-detail">{{trans('common.forms.buttons.edit')}}
                                                <i class="fa fa-edit"></i></a>
                                        </div>
                                    </div>
                                    <div class="tab-content" id="edit-company-detail">
                                        <div class="tab-pane fade in active show-form">
                                            <hr>
                                            <div class="col-xs-12 col-md-7">
                                                <dl class="dl-horizontal">
                                                    <dt>{{trans('common.forms.suppliers.company_name')}}</dt>
                                                    <dd>{{$supplier->company_name}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.company_description')}}</dt>
                                                    <dd>{{$supplier->CompanyDescription}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.member_trade_organization')}}</dt>
                                                    <dd>{{$supplier->member_trade_organization}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.location')}}</dt>
                                                    <dd>{{$supplier->country->name}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.tax_number')}}</dt>
                                                    <dd>{{$supplier->taxtype->name}}: {{$supplier->tax_number}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.company_legal_address')}}</dt>
                                                    <dd>{{$supplier->company_legal_address}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.postal_code')}}</dt>
                                                    <dd>{{$supplier->postal_code}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.website')}}</dt>
                                                    <dd><a href="{{$supplier->website}}">{{$supplier->website}}</a>
                                                    </dd>

                                                    <dt>{{trans('common.forms.suppliers.year_company_registered')}}</dt>
                                                    <dd>{{$supplier->year_company_registered}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.company_type')}}</dt>
                                                    <dd>{{$supplier->companyType->name}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.number_employees')}}</dt>
                                                    <dd>{{$supplier->number_employees}}</dd>

                                                    <dt>{{trans('common.forms.suppliers.main_business')}}</dt>
                                                    <dd>{{$supplier->superCategory->name}}</dd>
                                                </dl>
                                            </div>
                                            <div class="col-xs-12 col-md-5 text-center">
                                                <img src="{{asset($supplier->logo)}}"
                                                     style="max-width: 200px; max-height: 300px;">
                                            </div>
                                        </div>
                                        <div class="tab-pane fade edit-form">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group{{ $errors->has('suppliers_company_name') ? ' has-error' : '' }}">
                                                        <label for="company_name"
                                                               class="control-label">{{trans('common.forms.suppliers.company_name')}} <span class="red-color">*</span></label>
                                                        <input id="company_name" type="text" class="form-control"
                                                               name="suppliers_company_name"
                                                               value="{{$supplier->company_name}}" required
                                                               autofocus>
                                                        @if ($errors->has('suppliers_company_name'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('suppliers_company_name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group{{ $errors->has('suppliers_company_description') ? ' has-error' : '' }}">
                                                        <label for="company_description"
                                                               class="control-label">{{trans('common.forms.suppliers.company_description')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <textarea name="suppliers_company_description"
                                                                      class="form-control" id="es_company_description"
                                                                      maxlength="1000" cols="50" rows="5"
                                                                      placeholder="{{trans('user.complete_in')}}">{{$supplier->company_description}}</textarea>
                                                            <span id="contador"></span>
                                                            @if ($errors->has('suppliers_company_description'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('suppliers_company_description') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_member_trade_organization') ? ' has-error' : '' }}">
                                                        <label for="suppliers_member_trade_organization"
                                                               class="control-label">{{trans('common.forms.suppliers.member_trade_organization')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <input id="suppliers_member_trade_organization"
                                                                   type="text"
                                                                   class="form-control"
                                                                   name="suppliers_member_trade_organization"
                                                                   value="{{$supplier->member_trade_organization}}">
                                                            @if ($errors->has('suppliers_member_trade_organization'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_member_trade_organization') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_country_id') ? ' has-error' : '' }}">
                                                        <label for="suppliers_country_id"
                                                               class="control-label">{{trans('common.forms.suppliers.location')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <select name="suppliers_country_id"
                                                                    id="suppliers_country_id"
                                                                    class="form-control required">
                                                                @foreach($countries as $country)
                                                                    @if(isset($supplier) && $supplier->country_id==$country->id)
                                                                        <option value="{{$country->id}}" selected
                                                                                data-tax-id="{{$country->taxtypes->first()['id']}}"
                                                                                data-tax-name="{{$country->taxtypes->first()['name']}}"
                                                                                data-tax-digits="{{$country->taxtypes->first()['pivot']['digits']}}">{{$country->name}}</option>
                                                                    @else
                                                                        <option value="{{$country->id}}"
                                                                                data-tax-id="{{$country->taxtypes->first()['id']}}"
                                                                                data-tax-name="{{$country->taxtypes->first()['name']}}"
                                                                                data-tax-digits="{{$country->taxtypes->first()['pivot']['digits']}}"
                                                                                {{$country->id==old('suppliers_country_id')?'selected':''}}
                                                                        >{{$country->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('suppliers_country_id'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_country_id') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_tax_number') ? ' has-error' : '' }}">
                                                        <label for="suppliers_taxtype_id"
                                                               class="control-label">{{trans('common.forms.suppliers.tax_number')}} <span class="red-color">*</span></label>
                                                        <div class="row">
                                                            <div class="col-xs-2">
                                                                <input type="hidden" id="supplier-taxtype-id"
                                                                       name="suppliers_taxtype_id">
                                                                <div id="supplier-taxtype-name" class="text-center"
                                                                     style="line-height: 36px;font-weight: 700"></div>
                                                            </div>
                                                            <div class="col-xs-10">
                                                                <input id="suppliers_tax_number" type="text"
                                                                       class="form-control"
                                                                       name="suppliers_tax_number"
                                                                       value="{{$supplier->tax_number}}">
                                                                @if ($errors->has('suppliers_tax_number'))
                                                                    <span class="help-block">
                                                                        <strong>{{ str_replace('suppliers tax number',trans('common.forms.suppliers.tax_number'),$errors->first('suppliers_tax_number')) }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_images') ? ' has-error' : '' }}">
                                                        <label for="description"
                                                               class="control-label">{{trans('common.forms.suppliers.images')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <input type="file" name="suppliers_image"
                                                                   class="form-control">
                                                        </div>
                                                        <img src="{{asset($supplier->logo)}}" alt=""
                                                             class="img-preview">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group{{ $errors->has('suppliers_company_legal_address') ? ' has-error' : '' }}">
                                                        <label for="suppliers_company_legal_address"
                                                               class="control-label">{{trans('common.forms.suppliers.company_legal_address')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <input id="suppliers_company_legal_address" type="text"
                                                                   class="form-control"
                                                                   name="suppliers_company_legal_address"
                                                                   value="{{$supplier->company_legal_address}}"
                                                                   required>
                                                            @if ($errors->has('suppliers_company_legal_address'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_company_legal_address') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_postal_code') ? ' has-error' : '' }}">
                                                        <label for="suppliers_postal_code"
                                                               class="control-label">{{trans('common.forms.suppliers.postal_code')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <input id="suppliers_postal_code" type="text"
                                                                   class="form-control" name="suppliers_postal_code"
                                                                   value="{{$supplier->postal_code}}" required>
                                                            @if ($errors->has('suppliers_postal_code'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_postal_code') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_website') ? ' has-error' : '' }}">
                                                        <label for="suppliers_website"
                                                               class="control-label">{{trans('common.forms.suppliers.website')}} </label>
                                                        <div class="">
                                                            <input id="suppliers_website" onblur="checkURL(this)"
                                                                   class="form-control" name="suppliers_website"
                                                                   value="{{$supplier->website}}">
                                                            @if ($errors->has('suppliers_website'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_website') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('suppliers_year_company_registered') ? ' has-error' : '' }}">
                                                        <label for="suppliers_year_company_registered"
                                                               class="control-label">{{trans('common.forms.suppliers.year_company_registered')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <input id="suppliers_year_company_registered"
                                                                   type="text"
                                                                   class="form-control"
                                                                   name="suppliers_year_company_registered"
                                                                   value="{{$supplier->year_company_registered}}">
                                                            @if ($errors->has('suppliers_year_company_registered'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_year_company_registered') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_company_type') ? ' has-error' : '' }}">
                                                        <label for="suppliers_company_type"
                                                               class="control-label">{{trans('common.forms.suppliers.company_type')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <select name="suppliers_company_type"
                                                                    id="suppliers_company_type"
                                                                    class="form-control">
                                                                @foreach($typesCompanies as $typesCompany)
                                                                    <option value="{{$typesCompany->id}}" {{ $supplier->company_type == $typesCompany->id ? 'selected' : '' }}>{{$typesCompany->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('suppliers_number_employees') ? ' has-error' : '' }}">
                                                        <label for="suppliers_number_employees"
                                                               class="control-label">{{trans('common.forms.suppliers.number_employees')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <input id="suppliers_number_employees" type="number"
                                                                   class="form-control"
                                                                   name="suppliers_number_employees"
                                                                   value="{{$supplier->number_employees}}" required>
                                                            @if ($errors->has('suppliers_number_employees'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('suppliers_number_employees') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="suppliers_supercategory_id"
                                                               class="control-label">{{trans('common.forms.suppliers.main_business')}} <span class="red-color">*</span></label>
                                                        <div class="">
                                                            <select name="suppliers_supercategory_id"
                                                                    id="suppliers_supercategory_id"
                                                                    class="form-control required">
                                                                @foreach($supercategories as $supercategory)
                                                                    <option value="{{$supercategory->id}}" {{$supplier->supercategory_id == $supercategory->id ? 'selected': ''}}>{{$supercategory->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group text-right">
                                                        <button type="submit" class="btn btn-primary">
                                                            {{trans('common.forms.buttons.save')}}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="certifications" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <a href="#" class="toggle-edit boton-azul-no-shadow"
                                               style="font-size: 13px; float:right; padding-top:10px;"
                                               data-form-edit="#new-certification">{{trans('common.forms.suppliers.new_certification')}}
                                                <i class="fa fa-new"></i></a>
                                        </div>
                                    </div>
                                    <div class="tab-content" id="new-certification">
                                        <div class="tab-pane fade in active show-form">
                                            <div class="row">
                                                @foreach($supplier->certifications as $certification)
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading ">
                                                                {{trans('common.forms.suppliers.certification_image')}} {{$loop->iteration}}
                                                                <a href="#" class="toggle-edit"
                                                                   style="font-size: 13px; float:right; padding-top:10px;"
                                                                   data-form-edit="#certification-{{$loop->iteration}}">{{trans('common.forms.buttons.edit')}}
                                                                    <i class="fa fa-edit"></i></a>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="tab-content"
                                                                     id="certification-{{$loop->iteration}}">
                                                                    <div class="tab-pane active show-form">
                                                                        <dl class="dl-horizontal">
                                                                            <dt>{{trans('common.forms.suppliers.type_certification')}}</dt>
                                                                            <dd>{{$certification->type_certification}}</dd>

                                                                            <dt>{{trans('common.forms.suppliers.issued_by')}}</dt>
                                                                            <dd>{{$certification->issued_by}}</dd>

                                                                            <dt>{{trans('common.forms.suppliers.start_date')}}</dt>
                                                                            <dd>{{$certification->start_date}}</dd>

                                                                            <dt>{{trans('common.forms.suppliers.expiration_date')}}</dt>
                                                                            <dd>{{$certification->expiration_date}}</dd>

                                                                            <dt>{{trans('common.forms.suppliers.certification_image')}}</dt>
                                                                            <dd class="certification-image">
                                                                                @if(strtolower($certification->image_extension) == 'jpg' || strtolower($certification->image_extension) == 'png' || strtolower($certification->image_extension) == 'jpeg' || strtolower($certification->image_extension) == 'bmp')
                                                                                    <a href="{{route('web.certificate.download',$certification->id)}}"><img
                                                                                                src="{{asset($certification->image)}}"
                                                                                                class="img-responsive"></a>
                                                                                @else
                                                                                    <a href="{{route('web.certificate.download',$certification->id)}}"><img
                                                                                                src="{{asset('assets/pages/img/'.$certification->image_extension.'.png')}}"
                                                                                                alt=""
                                                                                                class="img-responsive"></a>
                                                                            @endif
                                                                        </dl>
                                                                    </div>
                                                                    <div class="tab-pane edit-form">
                                                                        <div class="form-group{{ $errors->has('certifications[type_certification]') ? ' has-error' : '' }}">
                                                                            <div class="">
                                                                                <label for="update_certification[{{$certification->id}}][type_certification]"
                                                                                       class="control-label">{{trans('common.forms.suppliers.type_certification')}}</label>
                                                                                <input id="update_certification[{{$certification->id}}][type_certification]"
                                                                                       type="text"
                                                                                       class="form-control"
                                                                                       name="update_certification[{{$certification->id}}][type_certification]"
                                                                                       value="{{$certification->type_certification}}">
                                                                                @if ($errors->has('update_certification['.$certification->id.'][type_certification]'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('update_certification['.$certification->id,'][type_certification]') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group{{ $errors->has('certifications[issued_by]') ? ' has-error' : '' }}">
                                                                            <div class="">
                                                                                <label for="update_certification[{{$certification->id}}][issued_by]"
                                                                                       class="control-label">{{trans('common.forms.suppliers.issued_by')}}</label>
                                                                                <input id="update_certification[{{$certification->id}}][issued_by]"
                                                                                       type="text"
                                                                                       class="form-control"
                                                                                       name="update_certification[{{$certification->id}}][issued_by]"
                                                                                       value="{{$certification->issued_by}}">
                                                                                @if ($errors->has('certifications[issued_by]'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('certifications[issued_by]') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group{{ $errors->has('certifications[start_date]') ? ' has-error' : '' }}">
                                                                            <div class="">
                                                                                <label for="update_certification[{{$certification->id}}][start_date]"
                                                                                       class="control-label">{{trans('common.forms.suppliers.start_date')}}</label>
                                                                                <div class='input-group date date_picker'>
                                                                                    <input type='text'
                                                                                           class="form-control"
                                                                                           name="update_certification[{{$certification->id}}][start_date]"
                                                                                           value="{{$certification->start_date}}"/>
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar"></span>
                                                                                    </span>
                                                                                </div>
                                                                                @if ($errors->has('certifications[start_date]'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('certifications[start_date]') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group{{ $errors->has('certifications[expiration_date]') ? ' has-error' : '' }}">
                                                                            <div class="">
                                                                                <label for="update_certification[{{$certification->id}}][expiration_date]"
                                                                                       class="control-label">{{trans('common.forms.suppliers.expiration_date')}}</label>
                                                                                <div class='input-group date date_picker'>
                                                                                    <input type='text'
                                                                                           class="form-control"
                                                                                           name=update_certification[{{$certification->id}}][expiration_date]
                                                                                           value="{{$certification->expiration_date}}"/>
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar"></span>
                                                                                    </span>
                                                                                </div>
                                                                                @if ($errors->has('certifications[expiration_date]'))
                                                                                    <span class="help-block">
                                                                                        <strong>{{ $errors->first('certifications[expiration_date]') }}</strong>
                                                                                    </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group{{ $errors->has('certifications[image]') ? ' has-error' : '' }}">
                                                                            <div class="">
                                                                                <label for="update_certification[{{$certification->id}}][image]"
                                                                                       class="control-label">{{trans('common.forms.suppliers.certification_image')}}</label>
                                                                                <input id="update_certification[{{$certification->id}}][image]"
                                                                                       type="file"
                                                                                       class="form-control"
                                                                                       name="update_certification[{{$certification->id}}][image]"
                                                                                       value="{{$certification->image}}">
                                                                                @if ($errors->has('certifications[image]'))
                                                                                    <span class="help-block">
                                                                                <strong>{{ $errors->first('certifications[image][]') }}</strong>
                                                                            </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="form-group text-right">
                                                                                <button type="button"
                                                                                        class="btn boton-azul-no-shadow"
                                                                                        onclick="deleteCertificate({{$certification->id}})">
                                                                                    {{trans('common.forms.buttons.delete')}}
                                                                                </button>
                                                                                <button type="submit"
                                                                                        class="btn btn-primary">
                                                                                    {{trans('common.forms.buttons.save')}}
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="tab-pane fade edit-form">
                                            <div class="col-xs-12">
                                                <div class="form-group{{ $errors->has('new_certification_type_certification') ? ' has-error' : '' }}">
                                                    <div class="">
                                                        <label for="new_certification_type_certification"
                                                               class="control-label">{{trans('common.forms.suppliers.type_certification')}}</label>
                                                        <input id="new_certification_type_certification" type="text"
                                                               class="form-control"
                                                               name="new_certification_type_certification"
                                                               maxlength="250"
                                                               value="{{old('new_certification_type_certification')}}">
                                                        @if ($errors->has('new_certification_type_certification'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('new_certification_type_certification') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('new_certification_issued_by') ? ' has-error' : '' }}">
                                                    <div class="">
                                                        <label for="new_certification_issued_by"
                                                               class="control-label">{{trans('common.forms.suppliers.issued_by')}}</label>
                                                        <input id="new_certification_issued_by" type="text"
                                                               maxlength="250" class="form-control"
                                                               name="new_certification_issued_by"
                                                               value="{{old('new_certification_issued_by')}}">
                                                        @if ($errors->has('new_certification_issued_by'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('new_certification_issued_by') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('new_certification_start_date') ? ' has-error' : '' }}">
                                                    <div class="">
                                                        <label for="new_certification_start_date"
                                                               class="control-label">{{trans('common.forms.suppliers.start_date')}}</label>
                                                        <div class='input-group date date_picker'>
                                                            <input type='text' class="form-control"
                                                                   name="new_certification_start_date"
                                                                   value="{{old('new_certification_start_date')}}"/>
                                                            <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        </div>
                                                        @if ($errors->has('new_certification_start_date'))
                                                            <span class="help-block"><strong>{{ $errors->first('new_certification_start_date') }}</strong></span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('new_certification_expiration_date') ? ' has-error' : '' }}">
                                                    <div class="">
                                                        <label for="new_certification_expiration_date"
                                                               class="control-label">{{trans('common.forms.suppliers.expiration_date')}}</label>
                                                        <div class='input-group date date_picker'>
                                                            <input type='text' class="form-control"
                                                                   name="new_certification_expiration_date"
                                                                   value="{{old('new_certification_expiration_date')}}"/>
                                                            <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        </div>
                                                        @if ($errors->has('new_certification_expiration_date'))
                                                            <span class="help-block">
                                                            <strong>{{ $errors->first('new_certification_expiration_date') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group{{ $errors->has('new_certification_image') ? ' has-error' : '' }}">
                                                    <div class="">
                                                        <label for="new_certification_image"
                                                               class="control-label">{{trans('common.forms.suppliers.certification_image')}}</label>
                                                        <input id="new_certification_image" type="file"
                                                               class="form-control" name="new_certification_image"
                                                               value="{{old('new_certification_image')}}">
                                                        @if ($errors->has('new_certification_image'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('new_certification_image') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group text-right">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{trans('common.forms.buttons.add')}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div id="factory" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <a href="#" class="boton-azul-no-shadow toggle-edit"
                                               data-form-edit="#edit-company-factory">{{trans('common.forms.buttons.edit')}}
                                                <i class="fa fa-edit"></i></a>
                                        </div>
                                    </div>
                                    <div class="tab-content" id="edit-company-factory">
                                        <div class="tab-pane fade in active show-form">
                                            <dl class="dl-horizontal dl-lines lg-dt">
                                                <dt>{{trans('common.forms.suppliers.factory_address')}}</dt>
                                                <dd>{{$supplier->factoryDetail->factory_address}}</dd>

                                                <dt>{{trans('common.forms.suppliers.factory_size')}}</dt>
                                                <dd>{{$supplier->factoryDetail->factory_size}}</dd>

                                                <dt>{{trans('common.forms.suppliers.contract_manufacturing')}}</dt>
                                                <dd>
                                                    {{trans('common.forms.suppliers.oem_service_offered')}}
                                                    : {{$supplier->factoryDetail->oem_service_offered?'Sí':'No'}}
                                                    <br>
                                                    {{trans('common.forms.suppliers.design_service_offered')}}
                                                    : {{$supplier->factoryDetail->design_service_offered?'Sí':'No'}}
                                                </dd>
                                                <dt>{{trans('common.forms.suppliers.qty_employees_QC')}}</dt>
                                                <dd>{{$supplier->factoryDetail->number_qc_staff}}</dd>

                                                <dt>{{trans('common.forms.suppliers.qty_employees_product_design')}}</dt>
                                                <dd>{{$supplier->factoryDetail->number_rd_staff}}</dd>

                                                <dt>{{trans('common.forms.suppliers.qty_employees_production_lines')}}</dt>
                                                <dd>{{$supplier->factoryDetail->number_production_lines}}</dd>

                                                <dt>{{trans('common.forms.suppliers.qty_annual_output_value')}}</dt>
                                                <dd>{{$supplier->factoryDetail->annual_output_value ? $supplier->factoryDetail->annual_output_value : 0}}
                                                    USD
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="tab-pane fade edit-form">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group{{ $errors->has('factoryDetails_factory_address') ? ' has-error' : '' }}">
                                                        <div class="">
                                                            <label for="factoryDetails_factory_address"
                                                                   class="control-label">{{trans('common.forms.suppliers.factory_address')}}</label>
                                                            <input id="factoryDetails_factory_address" type="text"
                                                                   class="form-control"
                                                                   name="factoryDetails_factory_address"
                                                                   value="{{$supplier->factoryDetail->factory_address}}">
                                                            @if ($errors->has('factoryDetails_factory_address'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_factory_address') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('factoryDetails_factory_size') ? ' has-error' : '' }}">
                                                        <div class="">
                                                            <label for="factoryDetails_factory_size"
                                                                   class="control-label">{{trans('common.forms.suppliers.factory_size')}}</label>
                                                            <input id="factoryDetails_factory_size" type="number"
                                                                   class="form-control"
                                                                   name="factoryDetails_factory_size"
                                                                   value="{{$supplier->factoryDetail->factory_size}}">
                                                            @if ($errors->has('factoryDetails_factory_size'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_factory_size') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('factoryDetails_oem_service_offered') || $errors->has('factoryDetails_design_service_offered') ? ' has-error' : '' }}">
                                                        <div class="">
                                                            <label
                                                                    class="control-label">{{trans('common.forms.suppliers.contract_manufacturing')}}
                                                                :</label>
                                                            <br>
                                                            <label>
                                                                <input id="factoryDetails_oem_service_offered"
                                                                       type="checkbox"
                                                                       name="factoryDetails_oem_service_offered" {{$supplier->factoryDetail->oem_service_offered?'checked':''}}>
                                                                {{trans('common.forms.suppliers.oem_service_offered')}}
                                                            </label>
                                                            <label>
                                                                <input id="factoryDetails_design_service_offered"
                                                                       type="checkbox"
                                                                       name="factoryDetails_design_service_offered" {{$supplier->factoryDetail->design_service_offered?'checked':''}}>
                                                                {{trans('common.forms.suppliers.design_service_offered')}}
                                                            </label>
                                                            @if ($errors->has('factoryDetails_oem_service_offered'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_oem_service_offered') }}</strong>
                                                        </span>
                                                            @endif
                                                            @if ($errors->has('factoryDetails_design_service_offered'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_design_service_offered') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group{{ $errors->has('factoryDetails_number_qc_staff') ? ' has-error' : '' }}">
                                                        <div class="">
                                                            <label for="factoryDetails_number_qc_staff"
                                                                   class="control-label">{{trans('common.forms.suppliers.qty_employees_QC')}}</label>
                                                            <input id="factoryDetails_number_qc_staff" type="number"
                                                                   class="form-control"
                                                                   name="factoryDetails_number_qc_staff"
                                                                   value="{{$supplier->factoryDetail->number_qc_staff}}">
                                                            @if ($errors->has('factoryDetails_number_qc_staff'))
                                                                <span class="help-block">
                                                            <strong>{{ $errors->first('factoryDetails_number_qc_staff') }}</strong>
                                                        </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="form-group{{ $errors->has('factoryDetails_number_rd_staff') ? ' has-error' : '' }}">
                                                        <label for="factoryDetails_number_rd_staff"
                                                               class="control-label">{{trans('common.forms.suppliers.qty_employees_product_design')}}</label>
                                                        <input id="factoryDetails_number_rd_staff" type="number"
                                                               class="form-control"
                                                               name="factoryDetails_number_rd_staff"
                                                               value="{{$supplier->factoryDetail->number_rd_staff}}">
                                                        @if ($errors->has('factoryDetails_number_rd_staff'))
                                                            <span class="help-block">
                                                        <strong>{{ $errors->first('factoryDetails_number_rd_staff') }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group{{ $errors->has('factoryDetails_number_production_lines') ? ' has-error' : '' }}">
                                                        <label for="factoryDetails_number_production_lines"
                                                               class="control-label">{{trans('common.forms.suppliers.qty_employees_production_lines')}}</label>
                                                        <input id="factoryDetails_number_production_lines"
                                                               type="number"
                                                               class="form-control"
                                                               name="factoryDetails_number_production_lines"
                                                               value="{{$supplier->factoryDetail->number_production_lines}}">
                                                        @if ($errors->has('factoryDetails_number_production_lines'))
                                                            <span class="help-block">
                                                        <strong>{{ $errors->first('factoryDetails_number_production_lines') }}</strong>
                                                    </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group{{ $errors->has('factoryDetails_annual_output_value') ? ' has-error' : '' }}">
                                                        <div class="">
                                                            <label for="factoryDetails_annual_output_value"
                                                                   class="control-label">{{trans('common.forms.suppliers.qty_annual_output_value')}}
                                                                (USD)</label>
                                                            <div class="row">
                                                                <div class="col-xs-6">
                                                                    <input id="factoryDetails_annual_output_value"
                                                                           type="text" class="form-control"
                                                                           name="factoryDetails_annual_output_value"
                                                                           value="{{$supplier->factoryDetail->annual_output_value}}">
                                                                </div>
                                                                {{--  <div class="col-xs-6">
                                                                      <select name="factoryDetails_measure_id" id="factoryDetails_measure_id" class="form-control">
                                                                          @foreach($measurements as $measurement)
                                                                              <option value="{{$measurement->id}}">{{$measurement->name}}</option>
                                                                          @endforeach
                                                                      </select>
                                                                  </div>--}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group text-right">
                                                        <button type="submit" class="btn btn-primary">
                                                            {{trans('common.forms.buttons.save')}}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="supplier-membership" class="tab-pane fade">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="membresias">
                                                    <div class="row logos-top">
                                                        <div class="col-xs-3">

                                                        </div>
                                                        <div class="col-xs-3 free">
                                                            <div class="top">
                                                                {{trans('memberships.basic')}}
                                                            </div>
                                                            <div class="icon">
                                                                <figure>
                                                                    <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Basic.png')}}"
                                                                         class="img-responsive">
                                                                </figure>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 advanced">
                                                            <div class="top">
                                                                {{trans('memberships.advanced')}}
                                                            </div>
                                                            <div class="icon">
                                                                <figure>
                                                                    <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Advanced.png')}}"
                                                                         class="img-responsive">
                                                                </figure>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 premium">
                                                            <div class="top">
                                                                {{trans('memberships.premium')}}
                                                            </div>
                                                            <div class="icon">
                                                                <figure>
                                                                    <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Premium.png')}}"
                                                                         class="img-responsive">
                                                                </figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="cuerpo">
                                                        <div class="row">
                                                            <div class="col-xs-3 description">
                                <span>
                                  {{--  <i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.product_posting')}}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 free">
                                <span>
                                    {{App\Repositories\Membership\Membership::find(1)->product_posting}}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 premium">
                                <span>
                                    {{trans('memberships.unlimited')}}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 advanced">
                                <span>
                                     {{trans('memberships.unlimited')}}
                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 description">
                                <span>
                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.banner_main_page')}}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 free">
                                <span>
                                <i class="fa fa-close"></i>
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 premium">
                                <span>
                                    {{trans('memberships.average_rotation',['time'=>$advancedMembership->main_rotation_banners]) }}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 advanced">
                                <span>
                               {{trans('memberships.average_rotation',['time'=>$premiumMembership->main_rotation_banners]) }}
                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 description">
                                <span>
                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.banner_inner_pages')}}

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 free">
                                <span>
                                <i class="fa fa-close"></i>
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 premium">
                                <span>
                                 {{trans('memberships.average_rotation',['time'=>$advancedMembership->inner_rotation_banners]) }}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 advanced">
                                <span>

                               {{trans('memberships.average_rotation',['time'=>$premiumMembership->inner_rotation_banners]) }}

                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 description">
                                <span>

                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.featured_products')}}

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 description">
                                <span>
                                   {{-- <i class="fa fa-check-circle-o"></i>--}}
                                    <span class="rows2">
                                       {{trans('memberships.ability_quote_buying_request')}}
                                    </span>
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3 description">
                                <span>
                                    {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.subaccounts')}}
                                </span>
                                                            </div>
                                                            <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 premium">
                                <span>

                                 <i class="fa fa-close"></i>

                                </span>
                                                            </div>
                                                            <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="choose">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                            </div>
                                                            <div class="col-xs-3 free">
                                                                @if(Auth::user()->activeMembership()->id == \App\Library\Constants::MEMBERSHIP_BASIC)
                                                                    <div class="inner">
                                                                        <button class="disabled">{{trans('memberships.active')}}</button>
                                                                    </div>
                                                                @else
                                                                    <div class="inner">
                                                                        <button class="free disabled">{{trans('memberships.choose_plan')}}</button>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="col-xs-3">
                                                                @if(Auth::user()->activeMembership()->id == $advancedMembership->id)
                                                                    <div class="inner">
                                                                        <button class="disabled">{{trans('memberships.active')}}</button>
                                                                    </div>
                                                                @else
                                                                    <div class="inner">
                                                                        <button class="advanced" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#modal-advanced">{{trans('memberships.choose_plan')}}</button>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="col-xs-3">
                                                                @if(Auth::user()->activeMembership()->id == \App\Library\Constants::MEMBERSHIP_PREMIUM)
                                                                    <div class="inner">
                                                                        <button class="disabled">{{trans('memberships.active')}}</button>
                                                                    </div>
                                                                @else
                                                                    <div class="inner">
                                                                        <button class="premium" type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#modal-premium">{{trans('memberships.choose_plan')}}</button>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br><br>
                                    <small class="text-roboto">{{trans('memberships.cond_1')}}</small>
                                    <br><br>
                                    <small class="text-roboto">{{trans('memberships.cond_2')}}</small>
                                    <br><br>
                                    <small class="text-roboto">{{trans('memberships.cond_3')}}</small>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-premium">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{$premiumMembership->name}} : Choose Plan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name="method" id="frecuency_premium" class="form-control" required>
                            <option selected disabled>-- Select plan</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_MONTHLY}}">
                                USD {{$premiumMembership->monthly_price}}/ {{trans('common.month')}}</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_ANNUAL}}">
                                USD {{$premiumMembership->annual_price}} / {{trans('common.year')}} </option>
                        </select>
                        <span class="help-block">
                            <strong id="error-msg"></strong>
                        </span>
                    </div>
                    @include('global-partials.purchase-promotion')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            onClick="sendPlanSelection('{{$premiumMembership->id}}',this)">Save changes
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="modal-advanced">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{$advancedMembership->name}} : Choose Plan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name="method" id="frecuency_advanced" class="form-control" required>
                            <option selected disabled>-- Select plan</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_MONTHLY}}">
                                USD {{$advancedMembership->monthly_price}} / {{trans('common.month')}}</option>
                            <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_ANNUAL}}">
                                USD {{$advancedMembership->annual_price}} / {{trans('common.year')}} </option>
                        </select>
                        <span class="help-block">
                            <strong id="error-msg"></strong>
                        </span>
                    </div>
                    @include('global-partials.purchase-promotion')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"
                            onClick="sendPlanSelection('{{$advancedMembership->id}}',this)">Save changes
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    @include('user.memberships.payu-form')
@endsection
@section('js')
    <script>
        function checkURL(abc) {
            var string = abc.value;
            if (string && !string.match(/^http([s]?):\/\/.*/)) {
                string = "http://" + string;
            }
            abc.value = string;
            return abc
        }

        $(function () {

            $('#es_company_description').keyup(function () {
                var chars = $(this).val().length;
                var max_chars = $(this).attr('maxlength');
                var diff = max_chars - chars;
                $('#contador').html(diff);
                if (diff < 0) {
                    $('#contador').style('color', 'red');
                }
            });

            var sectionErrorParameter = getUrlParameter('section');
            var actionParameter = getUrlParameter('action');
            if (sectionErrorParameter) {
                $('.perfil-empresa-tabs li').removeClass('in active');
                $('.perfil-empresa-tabs li').removeClass('in active');
                var tab = $('#' + sectionErrorParameter);
                var links = $('a[href="#' + sectionErrorParameter + '"]').parent();
                links.removeClass('active');
                $('form > .tab-content > div').removeClass('active');

                tab.addClass('in active');
                links.addClass('in active');
                console.log(sectionErrorParameter);
                if (sectionErrorParameter !== 'certifications') {
                    tab.find('.show-form').removeClass('in active');
                    tab.find('.edit-form').addClass('in active');
                }

                if (actionParameter === 'create') {
                    $('#new-certification .tab-pane').removeClass('in active');
                    $('#new-certification .edit-form').addClass('in active');
                }
            }

            loadTaxType();

            @if(old('suppliers_country_id'))
            loadTaxType();
            @else
            $('#suppliers_country_id').on('change', function () {
                loadTaxType();
            });
                    @endif


            var activeTab = $('.perfil-empresa-tabs li.active a').attr('href');
            $('#tab_name').val(activeTab.replace('#', ''));
            $('.perfil-empresa-tabs li').on('click', function () {
                $('#tab_name').val($(this).find('a').attr('href').replace('#', ''));
            });

        });

        function loadTaxType() {
            var supplier_country_selected = $('#suppliers_country_id option:selected');
            var taxId = supplier_country_selected.data('tax-id');
            var taxName = supplier_country_selected.data('tax-name');
            var taxDigits = supplier_country_selected.data('tax-digits');
            //console.log(taxId,taxName);
            $('#supplier-taxtype-id').val(taxId);
            $('#supplier-taxtype-name').text(taxName);
        }


        function sendPlanSelection(membershipId, el) {
            var content = $(el).parent().parent().find('.modal-body');
            var plan = content.find('select option:selected');
            if (plan.attr('disabled') === 'disabled') {
                var form_group = content.find('.form-group');
                form_group.addClass('has-error');
                var errorMsg = content.find('#error-msg');
                errorMsg.text('Debe seleccionar un plan');
                return;
            }

            var request = $.ajax({
                url: '{{route('api.payu.signature')}}',
                method: "POST",
                data: {
                    membership_id: membershipId,
                    frequency: plan.val(),
                    _token: window.Laravel.csrfToken,
                    lang: window.Laravel.lang
                },
                dataType: "json",
                async: false
            });

            request.done(function (data) {
                var form = $('#payu-form');
                form.find('#signature').val(data.signature);
                form.find('#description').val(data.description);
                form.find('#referenceCode').val(data.referenceCode);
                form.find('#amount').val(data.amount);
                form.find('#currency').val(data.currency);
                form.submit();
                return true;
            });

            request.fail(function (jqXHR, textStatus) {
                console.log(jqXHR);
                console.log("Request failed: " + textStatus);
            });
        }

        function deleteCertificate(certId) {
            var title = $(this).data('title') || "{{trans('alerts.delete_certificate.message')}}";
            var text = $(this).data('text') || "{{trans('alerts.delete_certificate.message_confirm')}}";
            var type = $(this).data('type') || "warning";
            var btnCancel = $(this).data('btn-cancel') || "{{trans('common.forms.buttons.cancel')}}";
            var btnOk = $(this).data('btn-ok') || "{{trans('common.forms.buttons.delete')}}";
            swal({
                    title: title,
                    text: text,
                    type: type,
                    showCancelButton: true,
                    cancelButtonText: btnCancel,
                    cancelButtonColor: "#FF6A31",
                    confirmButtonColor: "#FF6A31",
                    confirmButtonText: btnOk,
                    closeOnConfirm: true
                },
                function () {
                    $.ajax({
                        url: "{{ route('api.certificates.delete') }}",
                        method: "DELETE",
                        data: {cert_id: certId, _token: '{{csrf_token()}}'}
                    }).done(function (response) {
                        if (response) {
                            window.location.href = response.url;
                        }
                        alert("Data Saved: " + msg);

                    }).fail(function (jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);
                    });
                });
        }
    </script>
@endsection