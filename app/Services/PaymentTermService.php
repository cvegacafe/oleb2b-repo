<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:25 PM
 */
use App\Repositories\PaymentTerm\PaymentTermRepository;

class PaymentTermService extends BaseService
{

    public function __construct(PaymentTermRepository $paymentTermRepository)
    {
        $this->mainRepo = $paymentTermRepository;
        parent::__construct();
    }

    public function getManyById(array $ids)
    {
        return $this->mainRepo->getColumnWhereIn('id',$ids);
    }
}