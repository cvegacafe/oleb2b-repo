<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnMeasureIdFactoryDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('factorydetails', 'measure_id')){
            Schema::table('factorydetails', function (Blueprint $table) {
                $table->dropIndex('factorydetails_measure_id_foreign');
                $table->dropColumn('measure_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('factorydetails', function($table) {
            $table->integer('measure_id')->unsigned()->default(1);
            $table->foreign('measure_id')->references('id')->on('measurements');
        });
    }
}
