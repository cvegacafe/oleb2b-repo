<?php namespace App\Repositories\Subcategory;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:11 PM
 */

use App\Repositories\BaseRepository;

class SubcategoryRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Subcategory();
        parent::__construct();
    }

    public function all()
    {
        return $this->model->with('category')->get();
    }
}