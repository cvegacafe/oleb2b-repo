<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" href="{{asset('assets/pages/img/favicon.png')}}">
    @yield('css')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                    </button>
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li class="{{\Request::url()==action('Admin\UsersController@index')?'active':''}}"><a href="{{action('Admin\UsersController@index')}}">Usuarios</a></li>
                        <li class="{{\Request::url()==action('Admin\CountriesController@index')?'active':''}}"><a href="{{action('Admin\CountriesController@index')}}">Paises</a></li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="{{\Request::url()==action('Admin\SupercategoriesController@index')?'active':''}}"><a href="{{action('Admin\SupercategoriesController@index')}}">SuperCategorias</a></li>
                                <li class="{{\Request::url()==action('Admin\CategoriesController@index')?'active':''}}"><a href="{{action('Admin\CategoriesController@index')}}">Categorias</a></li>
                                <li class="{{\Request::url()==action('Admin\SubcategoriesController@index')?'active':''}}"><a href="{{action('Admin\SubcategoriesController@index')}}">SubCategorias</a></li>
                            </ul>
                        </li>
                        <li class="{{\Request::url()==action('Admin\CurrenciesController@index')?'active':''}}"><a href="{{action('Admin\CurrenciesController@index')}}">Monedas</a></li>
                        <li class="{{\Request::url()==action('Admin\MeasurementsController@index')?'active':''}}"><a href="{{action('Admin\MeasurementsController@index')}}">Medidas</a></li>
                        <li class="{{\Request::url()==action('Admin\DeliveryTermsController@index')?'active':''}}"><a href="{{action('Admin\DeliveryTermsController@index')}}">Terminos</a></li>


                        <li class="{{\Request::url()==action('Admin\MembershipsController@index')?'active':''}}"><a href="{{action('Admin\MembershipsController@index')}}">Membresias</a></li>
                        <li class="{{\Request::url()==action('Admin\ProductsController@index')?'active':''}}"><a href="{{action('Admin\ProductsController@index')}}">Productos</a></li>
                        <li class="{{\Request::url()==action('Admin\SupplierController@index')?'active':''}}"><a href="{{
                           action('Admin\SupplierController@index')}}">Proveedores</a></li> 
                        <li class="{{\Request::url()==action('Admin\BuyingRequestController@index')?'active':''}}"><a href="{{action('Admin\BuyingRequestController@index')}}">Buying Request</a></li>
                        <li class="{{\Request::url()==action('Admin\TaxTypesController@index')?'active':''}}"><a href="{{action('Admin\TaxTypesController@index')}}">Identificacion Tributaria</a></li>
                        <li class="{{\Request::url()==route('admin.show.news')?'active':''}}"><a href="{{route('admin.show.news')}}">News</a></li>
                        <li class="{{\Request::url()==route('admin.sliders.index')?'active':''}}"><a href="{{route('admin.sliders.index')}}">Sliders</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (!Auth::guard('admin')->check())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::guard('admin')->user()->names }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <div class="col-md-10 col-md-offset-1 content">
            @include('global-partials.avisos')
            @yield('content')
        </div>
    </div>
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="{{asset('assets/packages/sweetalert/dist/sweetalert.min.js')}}"></script>
    @yield('js')
</body>
</html>
