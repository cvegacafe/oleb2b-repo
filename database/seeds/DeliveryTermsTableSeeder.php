<?php

use App\Repositories\DeliveryTerm\DeliveryTerm;
use Illuminate\Database\Seeder;

class DeliveryTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeliveryTerm::create(['id'=>1,'name'=>'EXW','en_description'=>'EX Works','es_description'=>'En Fábrica','ru_description'=>'Франко завод (указанное место): товар со склада продавца']);
        DeliveryTerm::create(['id'=>2,'name'=>'FCA','en_description'=>'Free Carrier','es_description'=>'Franco transportista','ru_description'=>'Франко перевозчик (указанное место): товар доставляется перевозчику заказчика']);
        DeliveryTerm::create(['id'=>3,'name'=>'FAS','en_description'=>'Free Alongside Ship','es_description'=>'Al costado del buque','ru_description'=>'Свободно вдоль борта судна (указан порт погрузки): товар доставляется к кораблю заказчика']);
        DeliveryTerm::create(['id'=>4,'name'=>'FOB','en_description'=>'Free on Board','es_description'=>'A bordo del buque','ru_description'=>'Свободно на борту (указан порт погрузки): товар погружается на корабль заказчика']);
        DeliveryTerm::create(['id'=>5,'name'=>'CFR','en_description'=>'Cost & Freight','es_description'=>'Coste y Flete','ru_description'=>'Стоимость и фрахт (указан порт назначения): товар доставляется до порта заказчика (безвыгрузки)']);
        DeliveryTerm::create(['id'=>6,'name'=>'CIF','en_description'=>'Cost Insurance & Freight','es_description'=>'Coste  Flete y Seguro Pagados hasta','ru_description'=>'Стоимость, страхование и фрахт (указан порт назначения): товар страхуется и доставляется допорта заказчика (без выгрузки)']);
        DeliveryTerm::create(['id'=>7,'name'=>'CPT','en_description'=>'Carriage Paid To','es_description'=>'Transporte Pagado Hasta','ru_description'=>'Перевозка оплачена до (указано место назначения): товар доставляется перевозчику заказчика вуказанном месте назначения']);
        DeliveryTerm::create(['id'=>8,'name'=>'DAT','en_description'=>'Delivered at Terminal','es_description'=>'Entregado en Terminal','ru_description'=>'Поставка на терминале']);
        DeliveryTerm::create(['id'=>9,'name'=>'DAP','en_description'=>'Delivered at Place','es_description'=>'Entregado en un Lugar','ru_description'=>'Поставка в месте назначения']);
        DeliveryTerm::create(['id'=>10,'name'=>'DDP','en_description'=>'Delivered Duty Paid','es_description'=>'Entregado Con Pago de Derechos','ru_description'=>'Поставка с оплатой пошлин (указано место назначения): товар доставляется заказчику,очищенный от пошлин и рисков']);
    }
}
