<?php namespace App\Providers;

use App\Policies\AttachmentPolicy;
use App\Policies\ProductPolicy;
use App\Policies\SupplierPolicy;
use App\Policies\ThreadPolicy;
use App\Policies\UserPolicy;
use App\Repositories\Attachment\Attachment;
use App\Repositories\Product\Product;
use App\Repositories\Supplier\Supplier;
use App\Repositories\Thread\Thread;
use App\Repositories\User\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Supplier::class => SupplierPolicy::class,
        Thread::class => ThreadPolicy::class,
        Attachment::class => AttachmentPolicy::class,
        Product::class => ProductPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
