<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdministratorsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(MembershipsSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(PaymentTermsTableSeeder::class);
        $this->call(MeasurementsTableSeeder::class);
        $this->call(SupercategoriesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SubcategoriesTableSeeder::class);
        $this->call(TaxTypesTableSeeder::class);
        $this->call(DeliveryTermsTableSeeder::class);
        $this->call(CompanyTypesTableSeeder::class);
        $this->call(MarketsTableSeeder::class);
        //Comentar a partir de aqui para entrar a produccion
        $this->call(SuppliersTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsSeederTable::class);
        $this->call(BuyingRequestTableSeeder::class);
        $this->call(UpdatePaymentTermsSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
