<table class="table table-bordered table-condensed table-hover table-striped table-vertical-center">
    <thead>
    <tr>
        <th></th>
        <th>Nombre</th>
        <th>Sección</th>
        <th>Enlace</th>
        <th>Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($images as $image)
        <tr>
            <td>
                <img src="{{asset($image->path)}}" alt="" style="width: 200px"
                     class="img-responsive"><br>
            </td>
            <td>
                <p>{{$image->title}}</p>
            </td>
            <td>
                <p>{{trans('statuses.sections.'.$image->section)}}</p>
            </td>
            <td>
                <p>
                    <a href="{{$image->url}}" target="_blank">Visitar Sitio</a>
                </p>
            </td>

            <td>
                <form action="{{ route('admin.oleb2b-images.destroy',$image) }}"
                      method="POST"
                      onsubmit="return confirm('Seguro que desea eliminar la imagen?')">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>