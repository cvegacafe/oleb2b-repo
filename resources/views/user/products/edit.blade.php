@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">{{trans('products.edit_product')}} <h5
                                class="pull-right">{{trans('products.required_input')}}</h5></div>
                    <div class="panel-body">
                        <form class="" role="form" method="POST" action="{{route('user.products.update',$product)}}"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="category"
                                                                                                class="control-label">{{trans('products.product_supercategory')}}</label>
                                    <select name="category" id="supercategory" class="form-control required">
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($supercategories as $supercategory)
                                            <option value="{{$supercategory->id}}" {{$supercategory->id==$product->subcategory->category->supercategory->id?'Selected':''}}>{{$supercategory->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">

                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="subcategory_id"
                                                                                                class="control-label">{{trans('products.product_category')}}</label>
                                    <select name="category_id" id="category" class="form-control required">
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($product->subcategory->category->supercategory->categories as $category)
                                            <option value="{{$category->id}}" {{$category->id==$product->subcategory->category->id?'selected':''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">

                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="subcategory_id"
                                                                                                class="control-label">{{trans('products.product_subcategory')}}</label>
                                    <select name="subcategory_id" id="subcategory_id" class="form-control required">
                                        <option value="{{$product->subcategory->id}}"
                                                selected>{{$product->subcategory->name}}</option>
                                    </select>
                                    @if ($errors->has('subcategory_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('subcategory_id') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="name"
                                                                                                class="control-label">{{trans('products.name')}} </label>
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{$product->name}}" required
                                           placeholder="{{trans('user.complete_in')}}">
                                    @if ($errors->has('es_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <div class="">
                                        <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="description"
                                                                                                    class="control-label">{{trans('products.description')}}</label>
                                        <textarea name="description" id="description" cols="40" rows="5"
                                                  class="form-control"
                                                  placeholder="{{trans('user.complete_in')}}">{{$product->description}}</textarea>
                                        <span id="contador"></span>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                                    <div class="">
                                        <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="description"
                                                                                                    class="control-label">{{trans('products.search_words')}}</label>
                                        <input name="tags" value="{{$product->tags->implode('name',',')}}," id="tags"
                                               class="form-control tags" data-role="tagsinput"
                                               placeholder="Términos para la búsqueda">
                                        <span id="contador"></span>
                                        @if ($errors->has('tags'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="type"
                                                                                                class="control-label">{{trans('products.type')}}</label>
                                    <input id="type" type="text" class="form-control" name="type"
                                           value="{{$product->type}}">
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="is_organic"
                                           class="control-label">{{trans('products.is_organic')}}</label>
                                    <input id="is_organic" type="checkbox" name="is_organic"
                                           placeholder="{{trans('user.complete_in')}}" {{$product->is_organic ?'checked':''}}>
                                </div>
                                <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">

                                    <label for="color" class="control-label">{{trans('products.color')}}</label>
                                    <input id="color" type="text" class="form-control" name="color"
                                           value="{{$product->color}}">
                                    @if ($errors->has('color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group{{ $errors->has('photos') ? ' has-error' : '' }}">
                                    <div class="" id="filesContainer">
                                        <label for="description"
                                               class="control-label">{{trans('products.product_photos')}}</label>

                                        <div class="preview-files">
                                            @if($product->pictures)
                                                @foreach($product->pictures as $key => $picture)
                                                    <div class="oldFile{{$key}}"><span><i class="fa fa-picture-o"></i><small>Foto {{$key + 1 }}</small></span>
                                                        <figure><img src="{{asset($picture)}}"></figure>
                                                        <div class="close" data-position="{{$key}}"
                                                             onclick="removeOldFile(this)"><i class="fa fa-close"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>

                                        <button id="seleccionar-fotos" type="button" class="boton-azul-no-shadow"><i
                                                    class="fa fa-plus"></i> {{trans('user.update_image')}}</button>
                                        @if($errors->has('photos'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('photos') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i>
                                    <label for="country_id"
                                           class="control-label">{{trans('products.place_origin')}}</label>
                                    <select name="country_id" id="country_id" class="form-control required" required>
                                        <option value="0" selected
                                                disabled>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{$country->id==$product->country_id?'selected':''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('supply_ability') ? ' has-error' : '' }}">

                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="supply_ability"
                                                                                                class="control-label">{{trans('products.supply_ability')}}</label>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <input id="supply_ability" type="text" class="form-control"
                                                   name="supply_ability" value="{{$product->supply_ability}}" required>
                                        </div>
                                        <div class="col-xs-5">
                                            <select name="supply_measure_id" id="supply_measure_id"
                                                    class="form-control required">
                                                @foreach($measurements as $measure)
                                                    <option value="{{$measure->id}}" {{$measure->id==$product->supply_measure_id?'selected':''}}>{{$measure->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-4">
                                            <select name="supply_ability_frecuency" id="supply_ability_frecuency"
                                                    class="form-control required">
                                                <option value="YEAR" {{"YEAR"==$product->supply_ability_frecuency?'selected':''}}>{{trans('common.year')}}</option>
                                                <option value="MONTH" {{"MONTH"==$product->supply_ability_frecuency?'selected':''}}>{{trans('common.month')}}</option>
                                                <option value="DAY" {{"DAY"==$product->supply_ability_frecuency?'selected':''}}>{{trans('common.day')}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    @if ($errors->has('supply_ability_frecuency'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('supply_ability_frecuency') }}</strong>
                                            </span>
                                    @endif

                                </div>
                                <div class="form-group{{ $errors->has('moq') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i>
                                    <label for="moq" class="control-label">{{trans('products.moq')}} </label>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input id="moq" type="text" class="form-control" name="moq"
                                                   value="{{$product->moq}}" required>
                                        </div>
                                        <div class="col-xs-6">
                                            <select name="moq_measure_id" id="supply_measure_id"
                                                    class="form-control required">
                                                @foreach($measurements as $measure)
                                                    <option value="{{$measure->id}}" {{$measure->id==$product->moq_measure_id?'selected':''}}>{{$measure->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->has('moq'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('moq') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('delivery_time') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i>
                                    <label for="delivery_time" class="control-label">
                                        {{trans('products.delivery_time')}}
                                        ({{trans('common.days')}})
                                    </label>
                                    <input id="delivery_time" type="number" class="form-control" name="delivery_time"
                                           value="{{$product->delivery_time}}" required>
                                    @if ($errors->has('delivery_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('delivery_time') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i>
                                    <label for="price" class="control-label">{{trans('products.price')}}</label>
                                    <div class="row">
                                        <div class="col-xs-12" id="div_prices_1">
                                            <select name="price_type" id="price_type" class="form-control required"
                                                    required>
                                                @foreach(\App\Repositories\Product\Product::TYPE_PRICES as $type)
                                                    <option value="{{$type}}" {{old('price_type',$product->price_type) == $type ? 'selected' : '' }}>{{trans('priceTypes.'.$type)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <div class="row" id="prices_div">
                                        @if($product->price_type === \App\Repositories\Product\Product::TYPE_PRICE_FIXED)
                                            @php
                                                $price1 = $product->price;
                                                $price2 = 0;
                                            @endphp
                                        @elseif($product->price_type === \App\Repositories\Product\Product::TYPE_PRICE_RANGE)
                                            @php
                                                $priceRange = explode(' - ', $product->price_range);
                                                $price1 = $priceRange[0];
                                                $price2 = $priceRange[1];
                                            @endphp
                                        @else
                                            @php
                                                $price1 = 0;
                                                $price2 = 0;
                                            @endphp
                                        @endif
                                        <div class="col-xs-6" id="div_prices_2">
                                            <input id="price" placeholder="0.00" step="10" type="number" min="0"
                                                   class="form-control" name="price"
                                                   value="{{old('price',$price1)}}">
                                        </div>

                                        <div class="col-xs-6 col-sm-3" id="div_prices_3" style="display: none">
                                            <input id="price2" placeholder="0.00" step="10" type="number" min="0"
                                                   class="form-control" name="price2" value="{{old('price2',$price2)}}">
                                        </div>


                                        <div class="col-xs-6 col-sm-2">
                                            <select name="currency_id" id="currency_id"
                                                    class="form-control required pull-left">
                                                @foreach($currencies as $currency)
                                                    <option value="{{$currency->id}}" {{old('currency_id',0) == $currency->id ? 'selected' : '' }}>{{$currency->currency_code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-1 text-center">
                                            <label>{{ trans('products.per') }}</label>
                                        </div>
                                        <div class="col-xs-3">
                                            <select name="sale_unit_measure_id" id="sale_unit_measure_id"
                                                    class="form-control required">
                                                @foreach($measurements as $measure)
                                                    <option value="{{$measure->id}}" {{old('sale_unit_measure_id',0) == $measure->id ? 'selected' : '' }}>{{$measure->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <style>
                                        #prices_div {
                                            display: flex;
                                            margin: 0 -2px;
                                        }

                                        #prices_div > div {
                                            padding: 0 2px;
                                        }
                                    </style>
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('price_measure_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i><label for="delivery_terms"
                                                                                               class="control-label">{{trans('products.delivery_terms')}}</label>
                                    <div class="checkbox-list">
                                        @foreach($deliveryTerms as $deliveryTerm)
                                            <label class="checkbox-inline" data-toggle="tooltip"
                                                   title="{{$deliveryTerm->description}}">
                                                <input id="{{$deliveryTerm->id}}" type="checkbox"
                                                       name="delivery_terms[]"
                                                       value="{{$deliveryTerm->id}}" {{$product->deliveryTerms->contains('id',$deliveryTerm->id)?'checked':''}}/>&nbsp;{{$deliveryTerm->name}}
                                            </label>
                                        @endforeach
                                        @if ($errors->has('office_phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('office_phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('payment_terms') ? ' has-error' : '' }}">
                                    <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="payment_terms"
                                                                                                class="control-label">{{trans('products.payment_terms')}}</label>
                                    <div class="">
                                        @foreach($paymentTerms as $paymentTerm)
                                            <label class="checkbox-inline" data-toggle="tooltip"
                                                   title="{{$paymentTerm->description}}">
                                                <input id="{{$paymentTerm->id}}" type="checkbox" name="payment_terms[]"
                                                       value="{{$paymentTerm->id}}" {{$product->paymentTerms->contains('id',$paymentTerm->id)?'checked':''}}/>&nbsp;{{$paymentTerm->name}}
                                            </label>
                                        @endforeach
                                        @if ($errors->has('payment_terms'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('payment_terms') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group{{ $errors->has('ports') ? ' has-error' : '' }}">
                                    <label for="ports" class="control-label">{{trans('products.ports')}}</label>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input id="ports" type="text" class="form-control" name="ports[]"
                                                   value="{{isset($product->ports[0]) ? $product->ports[0] : ''}}">
                                        </div>
                                        <div class="col-xs-4">
                                            <input id="ports" type="text" class="form-control" name="ports[]"
                                                   value="{{isset($product->ports[1]) ? $product->ports[1] : ''}}">
                                        </div>
                                        <div class="col-xs-4">
                                            <input id="ports" type="text" class="form-control" name="ports[]"
                                                   value="{{isset($product->ports[2]) ? $product->ports[2] : ''}}">
                                        </div>
                                    </div>
                                    @if ($errors->has('ports'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ports') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                @if($subaccounts != null)
                                    <div class="form-group">
                                        <label class="control-label">{{trans('products.sale_manager_name')}}</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="label"
                                                       style="color: #636b6f"> {{trans('menus.user.subaccounts')}} </label>
                                                <select class="form-control required" name="user_id">
                                                    @foreach($subaccounts as $subaccount)
                                                        @if($subaccount->id == $product->user_id)
                                                            <option value="{{$subaccount->id}}"
                                                                    selected>{{$subaccount->names.' '.$subaccount->last_names}}</option>
                                                        @else
                                                            <option value="{{$subaccount->id}}">{{$subaccount->names.' '.$subaccount->last_names}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-6">
                                    <button type="submit" class="btn btn-primary pull-right">
                                        {{trans('common.forms.buttons.save')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}">
    <style>
        .file-preview .close {
            display: block;
            float: none;
            padding-right: 14px;
            text-align: right;
        }

        .krajee-default .file-preview-image {
            width: 240px !important;
            height: auto !important;
        }

        .bootstrap-tagsinput {
            width: 100%;
        }

        #prices_div {
            display: flex;
            margin: 0 -2px;
        }

        #prices_div > div {
            padding: 0 2px;
        }
    </style>

@endsection
@section('js')

    <script src="{{asset('assets/user/js/fileInput.js')}}?v=2"></script>
    <script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>

    <script>

        let PATH_SUPERCATEGORY = '{{route('api.categories.getBySupercategory')}}';

        let PATH_CATEGORY = '{{route('api.subcategories.getByCategory')}}';

        let d1 = $('#div_prices_1');
        let d2 = $('#div_prices_2');
        let d3 = $('#div_prices_3');
        let d4 = $('#prices_div');

        function setPriceTypes(_val) {
            if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_ASK}}') {
                d2.hide();
                d3.hide();
                d2.find('input').prop('required', false);
                d3.find('input').prop('required', false);
                d4.hide();
            }
            if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_FIXED}}') {
                d2.show();
                d3.hide();
                d2.addClass('col-xs-6');
                d2.find('input').prop('required', true);
                d3.find('input').prop('required', false);
                d4.show();
            }
            if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_RANGE}}') {
                d2.removeClass();
                d2.addClass('col-xs-3');
                d2.show();
                d3.removeClass();
                d3.addClass('col-xs-3');
                d3.show();
                d2.find('input').prop('required', true);
                d3.find('input').prop('required', true);
                d4.show();
            }
        }

        $(function () {
            d3.hide();
            setPriceTypes('{{$product->price_type}}');
            $('#price_type').change(function () {
                var _val = $(this).val();
                setPriceTypes(_val);
            });

            $("#tags").tagsinput({
                splitOn: ','
            });

            var max_chars = 10000;
            $('[data-toggle="tooltip"]').tooltip();
            $('#description').keyup(function () {
                var chars = $(this).val().length;
                var diff = max_chars - chars;
                $('#contador').html(diff);
            });

            $('#supercategory').on('change', function (e) {

                $('#category').append("<option value='0'>{{trans('common.loading')}}</option>");

                e.preventDefault();

                var supercategory_id = $(this).find(':selected').val();

                var request = $.ajax({
                    url: PATH_SUPERCATEGORY,
                    method: "GET",
                    data: {supercategory_id: supercategory_id, language: '{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#category').find('option').remove();
                    $('#subcategory_id').find('option').remove();
                    if (data.length > 0) {
                        $.each(data, function (index, element) {
                            $('#category').append("<option value='" + element.id + "'>" + element.trans_name + "</option>");
                        });

                        $('#category').trigger('change');
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            $('#category').on('change', function (e) {
                e.preventDefault();

                var category_id = $(this).find(':selected').val();

                $('#subcategory_id').append("<option value='0'>{{trans('common.loading')}}</option>");

                var request = $.ajax({
                    url: PATH_CATEGORY,
                    method: "GET",
                    data: {category_id: category_id, language: '{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#subcategory_id').find('option').remove();
                    if (data.length > 0) {
                        $.each(data, function (index, element) {
                            $('#subcategory_id').append("<option value='" + element.id + "'>" + element.trans_name + "</option>");
                        });
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });

            });
        });
    </script>
@endsection