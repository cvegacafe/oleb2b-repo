<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\SubcategoryService;
use Illuminate\Http\Request;

class SubcategoriesController extends Controller
{
    private $subcategories;

    public function __construct(SubcategoryService $subcategoryService)
    {
        $this->subcategories = $subcategoryService;
    }

    public function getSubcategoriesByCategoryId(Request $request)
    {
        $categoryId = $request->get('category_id');
        $subcategories = $this->subcategories->getByCategoryId($categoryId);
        foreach ($subcategories as $subcategory) {
            switch ($request->get('language')) {
                case 'es':
                    $subcategory['trans_name'] = $subcategory->es_name;;
                    break;
                case 'en':
                    $subcategory['trans_name'] = $subcategory->en_name;;
                    break;
                case 'ru':
                    $subcategory['trans_name'] = $subcategory->ru_name;;
                    break;
                default:
                    $subcategory['trans_name'] = trans('common.messages.without_translation', [], [], $request->get('language'));
            }
        }
        return response()->json($subcategories);
    }
}