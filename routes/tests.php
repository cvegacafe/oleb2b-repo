<?php

use App\Notifications\SendEmailForNewMessage;
use App\Repositories\User\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Auth;
use Stichoza\GoogleTranslate\TranslateClient;

Route::get('info', function () {
    return phpinfo();
});

Route::get('the-main', function () {
    $user = auth()->user();
    $company = $user->supplier;
    dd($company->users->where('pivot.is_principal',true)->first()->full_name);
});
Route::get('/image', function () {
    return view('image-resize');
});
Route::get('mail-welcome', function () {
    App::setLocale('es');
    $userName = 'name';
    $confirmationUrl = 'www.google.com';
    $password = '12321';
    return view('mails.welcome', compact('userName', 'confirmationUrl', 'password'));
});

Route::get('mail-new-message', function () {
    App::setLocale('ru');
    $userName = 'nombre apellido';
    $actionUrl = 'url';
    return view('mails.new-message', compact('userName', 'actionUrl'));

});

Route::get('mail-reset-password', function () {
    App::setLocale('es');
    $name = 'Nombre';
    $actionUrl = 'url';
    return view('mails.reset-password', compact('actionUrl', 'name'));
});

Route::get('mail-new-purchase', function () {
    App::setLocale('ru');
    $userName = 'Nombre';
    $membershipName = 'Membership Name';
    $actionUrl = 'url';
    $emailResponse = 'banner@oleb2b.com';
    return view('mails.new-purchased-membership', compact('userName', 'membershipName', 'actionUrl', 'emailResponse'));

});

Route::get('mail-new-buying-request', function () {
    App::setLocale('ru');
    $actionUrl = 'url';
    $supercategoryName = 'Category';
    $userName = 'Nombre';
    return view('mails.new-buying-request', compact('actionUrl', 'supercategoryName', 'userName'));
});

Route::get('mail-new-subaccount', function () {
    App::setLocale('ru');
    $actionUrl = 'url';
    $supercategoryName = 'Category';
    $userName = 'Nombre';
    return view('mails.new-buying-request', compact('actionUrl', 'supercategoryName', 'userName'));
});

Route::get('/test/mail/{lang}', function ($lang = 'es') {
    app()->setLocale($lang);
    $user = User::find(1);
    if (!$user) {
        return 'no existe el correo';
    } else {
        $user->notify(new \App\Notifications\WelcomeToOleb2b($user));
        $user->notify(new \App\Notifications\SendEmailForNewMessage($user, 'http://www.google.com'));
        $user->notify(new \App\Notifications\ResetPasswordNotification(csrf_token()));
        $membership = \App\Repositories\Membership\Membership::find(1);
        $user->notify(new \App\Notifications\NewPurchasedMembership($user, $membership));
        $buyerrequest = \App\Repositories\BuyingRequest\BuyingRequest::find(1);
        if (!$buyerrequest) {
            $buyerrequest = factory(\App\Repositories\BuyingRequest\BuyingRequest::class)->create();
        }
        $super = \App\Repositories\Supercategory\Supercategory::first();
        $user->notify(new \App\Notifications\NewBuyingRequestForSuppliers($super, $buyerrequest, $lang, $user));
        echo 'enviado';
    }

});


Route::get('update-memberships', function () {
    $lastMemberships = \DB::table('membership_user')->select('user_id')->groupBy('user_id')->get();
    dd($lastMemberships);
});

Route::get('/reenviar-activacion', function (\App\Services\MessengerService $messengerService) {
    Auth::user()->notify(new SendEmailForNewMessage(Auth::user(), 'http://google.com'));
    return 'ok';
});

Route::get('/test-br', function () {
    return env('PAYU_TEST');
});

Route::get('/carbon', function () {
    return \Carbon\Carbon::now()->addYear()->addMonths(3);
});

Route::get('/job', function () {

    try {
        dispatch(new \App\Jobs\TestJob());
    } catch (Exception $e) {
        dd($e);
    }

    return 'sent job testing';
});
Route::get('/user-threads', function () {
    $threads = Auth::user()->load('threads', 'threads.products')->threads;
    //dd($threads);
    dd($threads->where('products.id', 4)->all());
});


Route::get('/mail-confirm', function () {
    $userName = 'name';
    $confirmationUrl = 'http://www.facebook.com ';
    $password = 'xddas';
    return view('mails.welcome', compact('userName', 'confirmationUrl', 'password'));
});

Route::get('/mail-purchased-membership/', function () {

    $userName = 'name';
    $membershipName = 'http://www.facebook.com ';
    $actionUrl = '';
    $emailResponse = 'response@madas.com';
    return view('mails.new-purchased-membership', compact('userName', 'membershipName', 'actionUrl', 'emailResponse'));
});


Route::get('/mail-message', function () {
    $userName = 'name';
    $actionUrl = 'http://www.facebook.com ';
    return view('mails.new-message', compact('userName', 'actionUrl'));
});

Route::get('/new-buyin-request/{lang}', function ($lang) {
    $userName = 'name';
    $actionUrl = 'http://www.facebook.com ';
    return view('mails.new-message.' . $lang, compact('userName', 'actionUrl'));
});

Route::get('/mail-test', function (Mailer $mailer) {

    $data['name'] = 'chichilo';
    $data['email'] = 'chicholo@chicho.com';
    $data['subject'] = 'sujecto';
    $data['message'] = 'estees elmensaje';

    $mailer->send('mails.contact-form', ['data' => $data], function ($m) {
        $m->from(env('MAIL_USERNAME'), 'Mensaje desde portal Oleb2b.com');
        $m->to('emmanuelbarturen@gmail.com', 'Oleb2b.com Team')->subject('test testing');
    });

    return 'correo enviado';

});

Route::get('/remove-avatar', function (\App\Services\FileService $fileService) {
    $rs = $fileService->removeFile("images/uploads/users/1/1_1487177522_58a48732372b7.jpg");
    dd($rs);
});

Route::post('/upload', function (\App\Services\FileService $fileService, \Illuminate\Http\Request $request) {

    if ($request->hasFile('image')) {
        $file = $request->file('image');
        $path = $fileService->storeUserAvatar($file, 1);
        dd($path);
    }
});

Route::get('/membership', function () {
    $freq = \App\Library\Constants::MEMBERSHIP_FREQUENCY;
    dd($freq['M']);
});

Route::get('/user', function () {

    dd(Auth::user()->mainImage(asset('')));
});

Route::get('/messages-count', function (\App\Services\MessageService $messageService) {

    $messages = $messageService->getMessagesProductsFromUser(Auth::id());
    dd($messages->count());
});

Route::get('/threads', function (\App\Services\ThreadService $threadService) {

    dd($threadService->getThreadsWithProductsByBuyerUser(Auth::id()));
});

Route::get('/notifications', function (\App\Services\NotificationService $notificationService) {

    dd($notificationService->getLastNotifications(Auth::id()));
});

Route::get('/products', function (\App\Services\ProductService $productService) {

    dd($productService->getProductsHasThreads(Auth::id()));
});

Route::get('/const', function () {
    $now = \App\Library\Constants::AVAILABLE_LANG;
    dd(implode(',', $now));
});

Route::get('/testDB', function () {
    $now = \DB::table('paymentTerms')->whereIn('id', [1, 2, 22])->get()->count();
    dd($now);
});

Route::get('/check', function (\App\Services\CountryService $countryService) {
    $tr = new TranslateClient();
    $tr->setSource('en'); // Translate from English
    foreach ($countryService->all() as $country) {
        $tr->setTarget('es');
        $country->es_name = $tr->translate($country->en_name);
        $tr->setTarget('ru');
        $country->ru_name = $tr->translate($country->en_name);
        $country->save();
    }
});

Route::get('/test-notification', function (\App\Services\PusherService $pusherService) {

    $pusherService->test();
});

Route::get('/countries', function () {
    $cli = new GuzzleHttp\Client();
    $res = $cli->get('https://restcountries.eu/rest/v1/all');
    $enableds = collect([
        ['name' => 'Argentina', 'language_id' => 2],
        ['name' => 'Bolivia', 'language_id' => 2],
        ['name' => 'Chile', 'language_id' => 2],
        ['name' => 'Colombia', 'language_id' => 2],
        ['name' => 'Costa Rica', 'language_id' => 2],
        ['name' => 'Cuba', 'language_id' => 2],
        ['name' => 'Ecuador', 'language_id' => 2],
        ['name' => 'El Salvador', 'language_id' => 2],
        ['name' => 'Guatemala', 'language_id' => 2],
        ['name' => 'Honduras', 'language_id' => 2],
        ['name' => 'Mexico', 'language_id' => 2],
        ['name' => 'Nicaragua', 'language_id' => 2],
        ['name' => 'Panama', 'language_id' => 2],
        ['name' => 'Paraguay', 'language_id' => 2],
        ['name' => 'Peru', 'language_id' => 2],
        ['name' => 'Puerto Rico', 'language_id' => 2],
        ['name' => 'Dominican Republic', 'language_id' => 2],
        ['name' => 'Uruguay', 'language_id' => 2],
        ['name' => 'Venezuela', 'language_id' => 2],
        ['name' => 'Russia', 'language_id' => 3],
        ['name' => 'Brazil', 'language_id' => 4]

    ]);

    foreach (\GuzzleHttp\json_decode($res->getBody()) as $item) {
        if ($enableds->contains('name', $item->name)) {
            if ($item->name == 'Russia') {
                $language = 3;
            } elseif ($item->name == 'Brazil') {
                $language = 4;
            } else {
                $language = 2;
            }
            echo "Country::create(['language_id'=>" . $language . ",'name'=>'" . $item->name . "','abbreviation'=>'" . $item->alpha2Code . "','enabled_for_supplier'=>true]);<br>";
            continue;
        }
        echo "Country::create(['language_id'=>1,'name'=>'" . $item->name . "','abbreviation'=>'" . $item->alpha2Code . "','enabled_for_supplier'=>false]);<br>";
    }

});