<?php

use App\Repositories\User\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $suppliers = \App\Repositories\Supplier\Supplier::all();

        $markets = \App\Repositories\Market\Market::all();

        $memberships = \App\Repositories\Membership\Membership::all();

        $userSupplier = factory(User::class)->create([
            'email' => 'supplier@oleb2b.com',
            'names' => 'supplier',
            'country_id' => 174,
            'last_names' => 'Vendedor-Proveedor',
            'status' => \App\Library\Constants::USER_STATUS_ACTIVE
        ]);

        $userSupplier->languages()->saveMany(\App\Repositories\Language\Language::all()->random(2));

        $userSupplier->suppliers()->attach($suppliers->random(1)->id, ['is_principal' => true]);

        $userSupplier->markets()->saveMany($markets->random(2));

        $userSupplier->memberships()->save($memberships->where('id', 3)->first(), [
            'main_rotation_banners' => 120,
            'inner_rotation_banners' => 60,
            'product_posting' => 9999999,
            'amount' => 40,
            'frequency' => 'M',
            'finish_at' => \Carbon\Carbon::now()->addMonth()
        ]);

        $this->command->info('Specific user supplier@oleb2b.com account has been created!...');

        $userBuyer = factory(User::class)->create([
            'email' => 'buyer@oleb2b.com',
            'names' => 'buyer',
            'country_id' => 213,
            'last_names' => 'comprador',
            'status' => \App\Library\Constants::USER_STATUS_ACTIVE
        ]);

        $userBuyer->languages()->saveMany(\App\Repositories\Language\Language::all()->random(2));

        $userBuyer->memberships()->save($memberships->where('id', 1)->first(), [
            'main_rotation_banners' => 120,
            'inner_rotation_banners' => 60,
            'product_posting' => 9999999,
            'amount' => 40,
            'frequency' => 'M',
            'finish_at' => \Carbon\Carbon::now()->addMonth()
        ]);

        $this->command->info('Specific user buyer@oleb2b.com account has been created!...');

        factory(User::class, 40)->create()->each(function ($user) use ($markets, $memberships) {

            $user->languages()->saveMany(\App\Repositories\Language\Language::all()->random(2));

            $user->markets()->saveMany($markets->random(2));

            $membership = $memberships->random();

            $user->memberships()->save($membership, [
                'main_rotation_banners' => $membership->main_rotation_banners,
                'inner_rotation_banners' => $membership->inner_rotation_banners,
                'product_posting' => $membership->product_posting,
                'amount' => $membership->monthly_price,
                'frequency' => 'M',
                'finish_at' => \Carbon\Carbon::now()->addDay(2)
            ]);
        });

        $this->command->info('Random users account has been created!...');
    }
}
