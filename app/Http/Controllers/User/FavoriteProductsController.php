<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\Product\Product;
use App\Services\CountryService;
use App\Services\FavoriteProductService;
use App\Services\LanguageService;
use App\Services\MeasureService;
use App\Services\PaymentTermService;
use App\Services\ProductService;
use App\Services\ThreadService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FavoriteProductsController extends Controller
{
    private $favoriteProductService = null;

    public function __construct(FavoriteProductService $favoriteProductService)
    {
        $this->favoriteProductService = $favoriteProductService;
    }

    /**
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param PaymentTermService $paymentTermService
     * @param LanguageService $languageService
     * @param ProductService $productService
     * @param ThreadService $threadService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(
        CountryService $countryService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        LanguageService $languageService,
        ProductService $productService,
        ThreadService $threadService
    ) {
        $allCountries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $languages = $languageService->all();
        $favoriteProducts = $this->favoriteProductService->getFavoritesByUser(Auth::id());
        $products = collect([]);
        $authUser = Auth::check();

        if ($authUser && !Auth::user()->suppliers->isEmpty()) {
            $myProducts = $productService->getProductsBySupplier(Auth::user()->supplier, 999999);
        } else {
            $myProducts = collect([]);
        }

        $threads = $threadService->getThreadsWithProductsByBuyerUser(Auth::id());

        foreach ($favoriteProducts as $favoriteProduct) {
            $products->push($favoriteProduct->product);
        }

        return view('user.favoriteProducts.index', compact('products', 'allCountries', 'measurements', 'paymentTerms',
            'languages', 'myProducts', 'threads'));
    }

    public function destroy(Product $product)
    {
        try {
            $favoriteProduct = $this->favoriteProductService->getFavoriteProductId($product->id, Auth::id());

            if ($favoriteProduct == null) {
                abort(503);
            }

            $favoriteProduct->delete();
            Aviso::eliminado(trans('validation.custom.messages.record_deleted'));
        } catch (Exception $e) {
            Log::error($e);
            Aviso::eliminado(trans('validation.custom.messages.record_deleted_error'));
        }

        return redirect()->route('user.saved-products.index');
    }
}
