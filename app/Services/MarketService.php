<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */

use App\Repositories\Market\MarketRepository;

class MarketService extends BaseService
{

    public function __construct(MarketRepository $marketRepository)
    {
        $this->mainRepo = $marketRepository;
        parent::__construct();
    }

    public function getMarketsByLang($lang)
    {
        return $this->mainRepo->getMarketsByLang($lang);
    }
}