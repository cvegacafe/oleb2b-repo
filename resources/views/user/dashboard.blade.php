@extends('layouts.app')
@section('content')
    <section class="buying-requests">
        <div class="container-fluid">
            <div class="row cabecera">
                <div class="col-xs-12 col-sm-8">
                    {{--<h3 class="titulo-1 titulo-dashboard">Dashboard</h3>--}}
                </div>
            </div>
            <div class="row direct-access">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{route('company.profile.show')}}">
                        <div class="item red">
                            <div class="body">
                                <div class="icon">
                                    <i class="fa fa-copy"></i>
                                </div>
                                <div class="text">
                                    <strong>{{trans('dashboard.company_info')}}</strong>
                                </div>
                            </div>
                            <div class="foot">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <a href="{{route('company.profile.show').'?section=details'}}">
                                            <button class="active update"><small>{{trans('dashboard.update')}}</small></button>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 actions">
                                        @if(!$completeCompanyInfo)
                                            <button class="active miss"  data-toggle="tooltip" title="{{trans('user.incomplete')}}"><i class="fa fa-warning"></i></button>
                                        @else
                                            <button class="active ok"  data-toggle="tooltip" title="{{trans('user.complete')}}"><i class="fa fa-check"></i></button>
                                        @endif
                                        {{--<button class="active miss"  data-toggle="tooltip" title="Completa los datos"><i class="fa fa-check"></i></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{route('user.products.index')}}">
                        <div class="item red">
                            <div class="body">
                                <div class="icon">
                                    <i class="icon-produ"></i>
                                </div>
                                <div class="text">
                                    <strong>{{trans('dashboard.products')}} - <span class="number">{{$productsCount}}</span> </strong>
                                </div>
                            </div>
                            <div class="foot">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <a href="{{route('user.products.create')}}">
                                            <button class="active update"><small>{{trans('dashboard.add_products')}}</small></button>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 actions">
                                        @if($productsCount > 0 )
                                        <button class="active ok"  data-toggle="tooltip" title="{{trans('user.complete')}}"><i class="fa fa-check"></i></button>
                                        @else
                                        <button class="active miss"  data-toggle="tooltip" title="{{trans('user.incomplete')}}"><i class="fa fa-warning"></i></button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{route('company.profile.show')}}">
                        <div class="item red">
                            <div class="body">
                                <div class="icon">
                                    <i class="icon-img"></i>
                                </div>
                                <div class="text">
                                    <strong>{{trans('dashboard.company_photos')}}</strong>
                                </div>
                            </div>
                            <div class="foot">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <a href="{{route('company.profile.show').'?section=details'}}">
                                            <button class="active update"><small>{{trans('dashboard.add_photos')}}</small></button>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 actions">
                                        @if($completeCompanyPhotos)
                                            <button class="active miss"  data-toggle="tooltip" title="{{trans('user.incomplete')}}"><i class="fa fa-warning"></i></button>
                                        @else
                                            <button class="active ok"  data-toggle="tooltip" title="{{trans('user.complete')}}"><i class="fa fa-check"></i></button>
                                        @endif
                                        {{--<button class="active miss"  data-toggle="tooltip" title="Completa los datos"><i class="fa fa-check"></i></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{route('company.profile.show').'?section=certifications'}}">
                        <div class="item red">
                            <div class="body">
                                <div class="icon">
                                    <i class="icon-certi"></i>
                                </div>
                                <div class="text">
                                    <strong>{{trans('dashboard.certificates')}}</strong>
                                </div>
                            </div>
                            <div class="foot">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <a href="{{route('user.products.create')}}">
                                            <button class="active update"><small>{{trans('dashboard.update')}}</small></button>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 actions">
                                        @if($hasCertifications)
                                            <button class="active miss"  data-toggle="tooltip" title="{{trans('user.incomplete')}}"><i class="fa fa-warning"></i></button>
                                        @else
                                            <button class="active ok"  data-toggle="tooltip" title="{{trans('user.complete')}}"><i class="fa fa-check"></i></button>
                                        @endif
                                        {{--<button class="active miss"  data-toggle="tooltip" title="Completa los datos"><i class="fa fa-check"></i></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{route('user.buying-request.index')}}">
                        <div class="item red">
                            <div class="body">
                                <div class="icon">
                                    <i class="icon-requests"></i>
                                </div>
                                <div class="text">
                                    <strong>{{trans('dashboard.buying_requests')}}</strong>
                                </div>
                            </div>
                            <div class="foot">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <a href="{{route('user.buying-request.create')}}">
                                            <button class="active update"><small>{{trans('dashboard.submit')}}</small></button>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 actions">
                                        {{--<button class="active ok"  data-toggle="tooltip" title="Completo"><i class="fa fa-check"></i></button>--}}
                                        {{--<button class="active miss"  data-toggle="tooltip" title="Completa los datos"><i class="fa fa-check"></i></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{route('user.memberships.index')}}">
                        <div class="item red">
                            <div class="body">
                                <div class="membresia">
                                    <img src="{{asset('assets/pages/img/icon_'.Auth::user()->memberships->last()->en_name.'.svg')}}" alt="">
                                </div>
                                <div class="text">
                                    <strong>{{Auth::user()->memberships->last()->nameByLang(Lang::locale())}}</strong>
                                </div>
                            </div>
                            <div class="foot">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <a href="{{route('user.memberships.index')}}">
                                            <button class="active update"><small>{{trans('dashboard.upgrade')}}</small></button>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 actions">
                                        {{--<button class="active ok"  data-toggle="tooltip" title="Completo"><i class="fa fa-check"></i></button>--}}
                                        {{--<button class="active miss"  data-toggle="tooltip" title="Completa los datos"><i class="fa fa-check"></i></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script>
//        $('.item').hover(function(){
//            $(this).find('button').addClass('active');
//        },function(){
//            $(this).find('button').removeClass('active');
//        });
    </script>
@endsection