@extends('layouts.pages')
@section('title')
    Search
@endsection
@section('styles')
    <style>
        mark {
            background: #ffffa3 !important;
        }

        .searched-tags {
            margin-top: 40px;
        }

        .searched-tags ul {
            margin: 0;
            padding: 0;
        }

        .searched-tags ul li {
            position: relative;
            display: inline-block;
            background-color: #d8d8d8;
            padding: 3px 30px 3px 12px;
            overflow: hidden;
            font-size: 14px;
            margin-bottom: 4px;
            margin-right: 5px;
            border-radius: 70px;
            text-align: -webkit-match-parent;
        }

        .searched-tags ul li h3 {
            float: left;
            display: inline-block;
            vertical-align: middle;
            line-height: 23px;
            margin: 0;
            font-size: 14px;
            font-weight: 400;
            width: 100%;
            max-width: 200px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .searched-tags ul li a {
            position: absolute;
            right: 7px;
            font-size: 18px;
            color: #848484;
            line-height: 24px;
        }

        .section-filters .nav-collapsible ul {
            padding: 0;
        }

        .section-filters .nav-collapsible li.padre .facetas-search ul {
            max-height: 260px;
            overflow: auto;

        }

        .section-filters .nav-collapsible li:not(.facetas-combinables-li) {
            margin: 0;
            list-style: none;
            color: #1ea7dd;
        }

        .section-filters .nav-collapsible li:not(.facetas-combinables-li) p {
            margin: 0;
            list-style: none;
            color: #333333;
        }

        .section-filters .nav-collapsible li:not(.facetas-combinables-li) p span {
            float: right;
            font-size: 13px;
            font-weight: 400;
            color: #333333;
            margin-right: 5px;
        }

        .section-filters .nav-collapsible li .facetas-ul li a {
            display: block;
            padding: 0;
            margin-right: 5px;
            font-size: 14px;
            line-height: 24px;
            color: #1ea7dd;
            overflow: hidden;
            white-space: nowrap;
            word-break: break-all;
            text-overflow: ellipsis;
            letter-spacing: .5px;
        }

        .section-filters .nav-collapsible li .facetas-ul li:hover {
            background: #f4f4f4;
        }

        .section-filters .nav-collapsible li:not(.facetas-combinables-li) .facetas-ul li:not(.facetas-combinables-li) a span {
            float: right;
            font-size: 13px;
            font-weight: 400;
            color: #858585;
        }

        .section-filters .filtros-buscador .panel-filtros .panel-header {
            background: #f4f4f4;
            font-weight: 700;
            text-transform: uppercase;
            padding: 10px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
        }
    </style>
@endsection
@section('content')
    <div class="buscador search">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    {{--<h1 class="titulo">{{trans('portal.main_slogan')}}</h1>--}}
                    <div class="input-container">
                        <form action="{{route('portal.search.build')}}" method="POST">
                            {{ csrf_field() }}
                            <select>
                                <option value="Buscar: ">{{trans('portal.search')}}:</option>
                                <option>{{trans('portal.product')}}</option>
                            </select>
                            <input type="text" name="q" placeholder="{{trans('portal.search_products')}}"
                                   value="{{$q==trans('dashboard.products')?'':$q}}">
                            <button>{{trans('portal.search')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="buying-requests" id="buying-requests">
        <div class="container">
            <div class="row">
                @if($products->count())
                    <div class="col-xs-12 col-md-4 col-lg-3">
                        <div class="searched-tags">
                            <ul>
                                @foreach($selectCountries = array_filter(explode('-',$currentCountries))  as $country)
                                    @if($country!=trans('common.all'))
                                        <li>
                                            <h3>{{$country}}</h3>
                                            @if(count($selectCountries)>1)
                                                <a href="{{route('portal.search',['q'=>$q,'countries'=>implode('-',array_diff($selectCountries, [$country] ) ),'c'=>request()->get('c')])}}"><i
                                                            class="fa fa-times-circle"></i></a>
                                            @else
                                                <a href="{{route('portal.search',['q'=>$q,'countries'=>trans('common.all')])}}"><i
                                                            class="fa fa-times-circle"></i></a>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                                @foreach($selectCategories = array_filter(explode('-',request()->get('c'))) as $categoryId)
                                    <li>
                                        <h3>{{$categoriesInSearch->where('id',$categoryId)->first()->full_name}}</h3>
                                        @if(!empty($selectCategories) && count($selectCategories)>1)
                                            <a href="{{route('portal.search',['q'=>$q,'countries'=>$currentCountries,'c'=>implode('-',array_diff($selectCategories, [$categoryId] ) )])}}"><i
                                                        class="fa fa-times-circle"></i></a>
                                        @else
                                            <a href="{{route('portal.search',['q'=>$q,'countries'=>$currentCountries])}}"><i
                                                        class="fa fa-times-circle"></i></a>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="searched-countries">
                            <div class="section-filters">
                                <div class="section-filters-head">
                                    <h4 title="{{$products->total()}}"><i class="fa fa-filter" aria-hidden="true"></i> Filtros</h4>

                                </div>
                                <div class="filtros-buscador">
                                    <div class="panel-filtros">
                                        <div class="panel-header">
                                            {{trans('products.by_country')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="nav-collapsible">
                                    <ul>
                                        <li class="padre">
                                            <div class="facetas-search">
                                                <ul class="facetas-ul">
                                                    <li class="selector-item ">
                                                        <a href="{{route('portal.search',['q'=>$q,'countries'=>trans('common.all')])}}"
                                                           title="TODO">Todo</a>
                                                    </li>
                                                    @foreach($countriesInSearch as $country)
                                                        <li class="selector-item ">
                                                            @if(!in_array($country->full_name,$selectCountries))
                                                                <a href="{{route('portal.search',['q'=>$q,'countries'=>$country->buildUriCountryParameters($country,$currentCountries),'c'=>request()->get('c')])}}"
                                                                   title="{{$country->full_name}} ({{$country->total}})">{{str_limit($country->full_name,28)}}
                                                                    <span>({{$country->total}})</span></a>
                                                            @else
                                                                <p>{{str_limit($country->full_name,28)}}
                                                                    <span>({{$country->total}})</span></p>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="filtros-buscador">
                                    <div class="panel-filtros">
                                        <div class="panel-header">
                                            {{trans('products.by_category')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="nav-collapsible">
                                    <ul>
                                        <li class="padre">
                                            <div class="facetas-search">
                                                <ul class="facetas-ul">
                                                    @foreach($categoriesInSearch as $category)
                                                        <li class="selector-item ">
                                                            @if(!in_array($category->id,$selectCategories))
                                                                <a href="{{route('portal.search',['q'=>$q,'countries'=>$currentCountries,'c'=>$category->buildUriCategoryParameters($category,request()->get('c'))])}}"
                                                                   title="{{$category->full_name}} ({{$country->total}})">{{str_limit($category->full_name,28)}}
                                                                    <span>({{$category->total}})</span></a>
                                                            @else
                                                                <p>{{str_limit($category->full_name,28)}}
                                                                    <span>({{$category->total}})</span></p>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="filtros-buscador">
                             <div class="panel-filtros">
                                 <div class="panel-header">
                                     {{trans('products.by_category')}}
                                 </div>
                                 <div class="panel-body">
                                     <ul>
                                         @foreach($categoriesInSearch as $category)
                                             <li>
                                                 <a href="{{route('portal.search',['category'=>$category->id,'q'=>Request::get('q','')])}}">{{$category->name}}
                                                     <div class="counter">{{$category->products()->count()}}</div>
                                                 </a>
                                             </li>
                                         @endforeach
                                     </ul>
                                 </div>
                             </div>
                             <div class="panel-filtros">
                                 <div class="panel-header">
                                     {{trans('products.by_country')}}
                                 </div>
                                 <div class="panel-body">
                                     <ul>
                                         @foreach($countriesInSearch as $country)
                                             <li>
                                                 <a href="{{route('portal.search',['country'=>$country->id,'q'=>Request::get('q','')])}}">{{$country->name}}
                                                     <div class="counter">{{$country->products()->count()}}</div>
                                                 </a>
                                             </li>
                                         @endforeach
                                     </ul>
                                 </div>
                             </div>
                         </div>--}}
                    </div>
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        @foreach($products as $product)
                            <div class="row single-request">
                                <div class="col-xs-12 col-sm-4 col-lg-2">
                                    <a href="{{route('portal.singleProduct',$product->slug)}}" target="_blank">
                                        <figure>
                                            <img src="{{asset($product->main_photo)}}" alt="">
                                        </figure>
                                    </a>
                                </div>
                                <div class="col-xs-12 col-sm-4 product">
                                    <ul>
                                        <li class="name"><a
                                                    href="{{route('portal.singleProduct',$product->slug)}}">{{$product->name}} </a>
                                        </li>
                                        <li>{{str_limit($product->description,105)}}</li>
                                        <li><strong>{{trans('products.place_origin')}}
                                                : </strong> {{$product->country->name}}</li>
                                        <li><strong>{{trans('products.moq')}}
                                                :</strong> {{$product->moq}} {{$product->moqMeasure->name}}</li>
                                        <li>
                                            <strong>{{trans('products.price')}}:</strong>
                                            @if($product->price)
                                                <span class="text-roboto">{{$product->price}} {{$product->currency->currency_code}} / {{$product->saleUnitMeasure->name}}</span>
                                            @else
                                                <span>{{trans('priceTypes.ask_seller')}}</span>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-lg-3 supplier">
                                    <button data-toggle="collapse"
                                            data-target="#datos-empresa">{{trans('products.supplier_info')}} <i
                                                class="fa fa-caret-down"></i></button>
                                    <ul id="datos-empresa" class="collapse">
                                        <li class="name">{{$product->user->supplier->company_name}}</li>
                                        <li>{{$product->user->supplier->country->name}}</li>
                                        <li><strong>{{trans('products.membership')}}

                                                :</strong>
                                            @if($product->user->activeMembership())
                                                {{$product->user->activeMembership()->name}}
                                            @endif
                                        </li>
                                        @if($product->is_organic)
                                            <li><img src="{{asset('assets/pages/img/organic_vf.png')}}" alt=""
                                                     width="90px"></li>
                                        @endif
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-lg-3 buttons">
                                    @if($authUser)
                                        @if($myProducts->contains('id',$product->id))
                                            <a href="{{route('user.products.edit',$product)}}">
                                                <button class="contact">
                                                    {{trans('products.edit_product')}}
                                                </button>
                                            </a>
                                        @elseif($threads->contains('product_id',$product->id))
                                            <a href="{{route('user.messages.products.index', ['product' => $product->id, 'thread' => $threads->where('product_id',$product->id)->first()->id])}}">
                                                <button class="contact">
                                                    {{trans('menus.user.messages')}}
                                                </button>
                                            </a>
                                        @else
                                            <button class="contact"
                                                    onclick="contactWithSupplier('{{$product->id}}','{{$product->name}}','{{asset($product->main_photo)}}','{{$product->user->languages->implode(\Lang::locale().'_name',' ,')}}')">
                                                {{trans('portal.contact_supplier')}}
                                            </button>
                                        @endif

                                        @if($favoritesProducts->contains('product_id',$product->id))
                                            <button data-product-id="{{$product->id}}"
                                                    class="toggleFavoriteText active save"
                                                    data-saved="{{trans('portal.saved')}}"
                                                    data-save-for-later="{{trans('portal.save_for_later')}}"> {{trans('portal.saved')}}</button>
                                        @else
                                            <button data-product-id="{{$product->id}}" class="toggleFavoriteText save"
                                                    data-saved="{{trans('portal.saved')}}"
                                                    data-save-for-later="{{trans('portal.save_for_later')}}">{{trans('portal.save_for_later')}}</button>
                                        @endif
                                    @else
                                        <button onclick="showAuth()"
                                                class="contact">{{trans('portal.contact_supplier')}}</button>
                                        <button class="toggleFavorite save" data-product-id="{{$product->id}}"
                                                data-saved="{{trans('portal.saved')}}"
                                                data-save-for-later="{{trans('portal.save_for_later')}}">{{trans('portal.save_for_later')}}</button>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                {{--{{ $products->appends(['q'=>Request::only('q'), 'country'=> \Request::only('country'), 'sub'=>\Request::only('sub'), 'category'=>Request::only('category')])->links() }}--}}
                                {!! $products->appends(['c'=> Request::get('c'),'sc' => Request::get('sc'),'q'=>Request::get('q')])->render() !!}
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xs-12">
                        <h3 class="empty">{{trans('portal.error_404_text')}}</h3>
                    </div>
                    @inject('productService','\App\Services\ProductService')
                @endif
            </div>
        </div>
    </section>
    <div class="registrate">
        <div class="centro">
            <h3>{{trans('portal.sale_online_is_easy')}}</h3>
            <button>{{trans('portal.join_free')}}</button>
        </div>
    </div>
    @include('pages.includes.supplier-contact-popup',[
       'countries' => $allCountries,
       'measurements'=>$measurements,
       'paymentTerms' => $paymentTerms,
       'languages' => $languages
       ])
@endsection


@section('scripts')
    <script src="{{asset('assets/packages/mark.js/dist/jquery.mark.js')}}"></script>
    <script>
        $('.slider-bg').slick({
            slideToShow: 1,
            arrows: false,
            dots: false,
            autoplay: true,
            autoplaySpeed: 2000,
            fade: true,
            speed: 1200,
            draggable: false
        });
    </script>
    <script>
        function contactWithSupplier(product_id, product_name, product_photo, languages) {
            var modal = $('#SupplierModal');
            modal.find('#product_id').val(product_id);
            modal.find('#product_name').text(product_name);
            modal.find('#product_photo').attr('src', product_photo);
            modal.find('#languages').text(languages);
            modal.find('#SupplierFormHtml')[0].reset();
            modal.modal();
        }

        function setFavoriteProduct(product_id, el) {
            var element = $(el);
            var request = $.ajax({
                url: "{{route('api.productFavorite.create')}}",
                method: "POST",
                data: {product_id: product_id, _token: window.Laravel.csrfToken},
                dataType: "json"
            });
            request.done(function (response) {
                if (response.response == 'ok' || response.response == 'exists') {
                    element.addClass('active');
                    element.text('{{trans('portal.saved')}}');
                    element.prop('onclick', null).off('click');
                } else {
                    //TODO: mensaje oportuno
                    alert('no se pudo agregar');
                }
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        }

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            console.log();

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        function mark(text) {

            // Read the keyword
            var keyword = text;

            // Determine selected options
            var options = {};
//            $("input[name='opt[]']").each(function() {
//                options[$(this).val()] = $(this).is(":checked");
//            });
            // Remove previous marked elements and mark
            // the new keyword inside the context
            $("#buying-requests").unmark({
                done: function () {
                    $("#buying-requests").mark(keyword, options);
                }
            })
        }

        $(document).ready(function () {
            var text = window.location.pathname.match(/^(.*?)en-/)[1];
            text = text.replace('/es/search/', '');
            text = text.replace('-', '')
            mark(text);
            //$('.slider-bg').fadeIn('slow');
        });

    </script>
@endsection