<?php namespace App\Repositories\BuyingRequest;

use App\Library\Constants;
use App\Repositories\Country\Country;
use App\Repositories\Language\Language;
use App\Repositories\Measure\Measure;
use App\Repositories\Subcategory\Subcategory;
use App\Repositories\Thread\Thread;
use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class BuyingRequest
 * @package App\Repositories\BuyingRequest
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property mixed $measure
 * @property mixed $user
 * @property mixed $country
 * @property mixed $languages
 * @property mixed $payment_terms
 * @property mixed $subcategory
 * @property mixed $threads
 * @property bool|\Carbon\Carbon|\DateTime|false|float|\Illuminate\Support\Collection|int|mixed|string|static $name
 */
class BuyingRequest extends Model
{
    /**
     * @var string
     */
    public $table = 'buyingRequests';

    /**
     * @var array
     */
    public $fillable = [
        'user_id',
        'subcategory_id',
        'es_name',
        'es_description',
        'en_name',
        'en_description',
        'ru_name',
        'ru_description',
        'order_qty',
        'measure_id',
        'country_id',
        'image',
        'ports',
        'start_date',
        'end_date',
        'is_approved'
    ];

    /**
     * @var array
     */
    public $dates = ['start_date', 'end_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function measure()
    {
        return $this->belongsTo(Measure::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'buyingRequest_language', 'buyingRequest_id', 'language_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function paymentTerms()
    {
        return $this->belongsToMany(Language::class, 'buyingRequest_paymentTerm', 'buyingRequest_id', 'paymentTerm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function threads()
    {
        return $this->belongsToMany(Thread::class, 'buyingRequest_thread', 'buyingRequest_id',
            'thread_id')->withPivot('message')->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }

    /**
     * @return mixed
     */
    public function getDescriptionAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_description');
    }

    /**
     * @return mixed
     */
    public function getTypeAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_type');
    }

    /**
     * @param $value
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value == null ? asset('assets/pages/img/default-product-image.png') : $value;
    }

    /**
     * @return mixed
     */
    public function getColorAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_color');
    }

    /**
     * @return mixed
     */
    public function getSlugAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_slug');
    }

    /**
     * @return mixed
     */
    public function getFormattedStartDateAttribute()
    {
        return $this->start_date->format('d/m/Y');
    }

    /**
     * @return mixed
     */
    public function getFormattedEndDateAttribute()
    {
        return $this->end_date->format('d/m/Y');
    }

    /**
     * @param $lang
     * @return mixed
     */
    public function nameByLang($lang)
    {
        return $this->getAttribute(strtolower($lang) . '_name');
    }

    /**
     * @return mixed|string
     */
    public function getMainPhotoAttribute()
    {
        if (isset($this->image)) {
            return $this->image;
        }

        return asset(Constants::PRODUCT_DEFAULT_IMAGE);
    }

    /**
     * @param $userId
     * @return int
     */
    public function newMessages($userId)
    {
        $messagesCount = 0;

        $threads = $this->threads;

        if($threads->isEmpty()) return 0;

        $threads = $threads->filter(function ($value, $key)use($userId) {
            return $value->from_user_id == $userId ||  $value->to_user_id == $userId;
        });

        foreach($threads as $thread){

            $messages = $thread->newMessages($userId);

            $messagesCount = $messagesCount + $messages;
        }

        return $messagesCount;
    }

}
