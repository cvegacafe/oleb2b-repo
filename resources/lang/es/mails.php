<?php
return [
    'welcome_to_oleb2b' => ' Bienvenido a Oleb2b.com',
    'new_buying_request' => 'Nueva oportunidad de venta!',
    'change_password_title' => 'Haga clic en el siguiente botón para cambiar su contraseña.',
    'change_password_btn' => 'Cambiar Contraseña',
    'change_password_message' => 'Si tiene problemas al hacer clic en el botón " Cambiar Contraseña ", copie y pegue la URL que aparece a continuación en su navegador web:',
    'button_not_working'=>'¿El botón no funciona? Copie este enlace en su navegador',
    'oleb2b_team'=>'Equipo OleB2B',
    'regards'=>'Saludos,',
    'mail_confirm' => [
        'title' => 'Gracias por registrarse en www.OleB2B.com<br>
         <br> Por favor confirme su dirección de correo electrónico haciendo clic en el siguiente enlace. ',
        'action_btn' => ' Confirmar'
    ],
    'mail_purchase' => [
        'subject' => 'Membresía confirmada',
        'title' => 'Su membresía <strong> :membershipName </strong> ha sido confirmada.<br><br> Por favor envíenos sus instrucciones para el diseño de su banner a esta dirección de correo electrónico:',
        'action_btn' => 'Ingresar al Dashboard',
        'banner_message' => 'Los banners estarán en linea dentro de las próximas 72 horas después de recibir sus instrucciones. Gracias!'
    ],
    'mail_message' => [
        'title' => 'Tiene un nuevo mensaje en OleB2B.com.',
        'action_btn' => 'Ver Mensaje'
    ],
    'buying_request' => [
        'subject' => 'Una nueva solicitud de cotización ha sido ingresada a OleB2B.com',
        'title' => 'Una nueva solicitud de cotización ha sido ingresada a OleB2B.com en <b>:supercategoryName</b>. <br><br>Visite OleB2B.com para ver las últimas solicitudes de cotización y empiece a cotizar.',
        'action_btn' => 'Ir a OleB2B.com'
    ]
];