<?php namespace App\Http\Requests;

use App\Library\Constants;
use Illuminate\Foundation\Http\FormRequest;

class ApiAllCountriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $availableLangList = implode(',',Constants::AVAILABLE_LANG);
        return [
            'lang'=>'required|in:'.$availableLangList
        ];
    }
}
