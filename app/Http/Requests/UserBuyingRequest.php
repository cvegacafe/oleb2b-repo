<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserBuyingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subcategory_id' => 'required|exists:subcategories,id',
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'start_date' => 'required',
            'end_date' => 'required',
            'country_id' => 'required|exists:countries,id',
            'order_qty' => 'required|numeric|max:999999999',
            'measure_id' => 'required|exists:measurements,id',
            'payment_terms' => 'required|array',
            'languages' => 'required|array',
            'photo' => 'required'
        ];
    }
}
