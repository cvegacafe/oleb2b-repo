<?php namespace App\Repositories\Category;

use App\Repositories\Product\Product;
use App\Repositories\Subcategory\Subcategory;
use App\Repositories\Supercategory\Supercategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class Category
 * @package App\Repositories\Category
 */
class Category extends Model
{
    /**
     * @var string
     */
    public $table = 'categories';

    /**
     * @var array
     */
    public $fillable = ['supercategory_id', 'es_name', 'en_name', 'ru_name', 'visited'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supercategory()
    {
        return $this->belongsTo(Supercategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function products()
    {
        return $this->hasManyThrough(Product::class, Subcategory::class);
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }

}
