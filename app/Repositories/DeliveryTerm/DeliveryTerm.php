<?php 
namespace App\Repositories\DeliveryTerm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class DeliveryTerm extends Model
{
    public $table = 'deliveryTerms';
    public $fillable = ['name', 'es_description', 'en_description', 'ru_description'];


    public function getDescriptionAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_description');
    }
}
