<?php namespace App\Repositories\CompanyType;

use App\Repositories\BaseRepository;

class CompanyTypeRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new CompanyType();
        parent::__construct();
    }

}