<?php

use Illuminate\Database\Seeder;

/**
 * Class UpdatePaymentTermsSeeder
 */
class UpdatePaymentTermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentTermsService = new \App\Services\PaymentTermService(new \App\Repositories\PaymentTerm\PaymentTermRepository());
        $paymentTermsService->update(1, ['es_description' => 'Transferencia bancaria', 'en_description' => 'Bank transfer', 'ru_description' => 'Банковский перевод']);
        $paymentTermsService->update(2, ['es_description' => 'Carta de crédito', 'en_description' => 'Letter of credit', 'ru_description' => 'Аккредитив']);
        $paymentTermsService->update(3, ['es_description' => 'Western Union', 'en_description' => 'Western Union', 'ru_description' => 'Western Union']);
        $paymentTermsService->update(4, ['es_description' => 'Pago contra documentos', 'en_description' => 'Cash against documents', 'ru_description' => 'Оплата против документов']);
    }

}
