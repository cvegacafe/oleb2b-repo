<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:19 PM
 */

use App\Repositories\Thread\ThreadRepository;
use Illuminate\Support\Facades\Lang;

class ThreadService extends BaseService
{
    public function __construct(ThreadRepository $threadRepository)
    {
        $this->mainRepo = $threadRepository;
        parent::__construct();
    }

    public function getByUser($userId)
    {
        return $this->mainRepo->getByUser($userId)->reverse();
    }

    public function getProductsWithThreads($userId)
    {
        return $this->mainRepo->getProductsWithThreads($userId,Lang::locale());
    }

    public function getThreadsWithProductsByBuyerUser($userId)
    {
        return $this->mainRepo->getThreadsWithProductsByBuyerUser($userId);
    }

    public function getThreadsWithBuyingRequestByBuyerUser($userId)
    {
        return $this->mainRepo->getThreadsWithBuyingRequestByBuyerUser($userId);
    }
}