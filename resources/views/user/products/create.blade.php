@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="panel panel-oleb2b">
                <div class="panel-heading">{{trans('products.add_new_product')}} <h5 class="pull-right">{{trans('products.required_input')}} </h5></div>
                <div class="panel-body">
                    <form role="form" method="POST" action="{{route('user.products.create.post')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="category" class="control-label">{{trans('products.product_supercategory')}}</label>
                                <select name="supercategory" id="supercategory" class="form-control required">
                                    <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                    @foreach($supercategories as $supercategory)
                                        <option value="{{$supercategory->id}}" {{old('supercategory') == $supercategory->id ? 'selected' : ''}}>{{$supercategory->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="subcategory_id" class="control-label">{{trans('products.product_category')}}</label>
                                <select name="category" id="category" class="form-control required">
                                    @if(old('category'))
                                        <option value="{{old('category')}}" selected>{{$categories->where('id',old('category'))->first()->toArray()[Lang::locale().'_name']}}</option>
                                    @else
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="subcategory_id" class="control-label">{{trans('products.product_subcategory')}}</label>
                                <select name="subcategory_id" id="subcategory_id" class="form-control required">
                                    @if(old('subcategory_id'))
                                        <option value="{{old('subcategory_id')}}" selected>{{$subcategories->where('id',old('subcategory_id'))->first()->toArray()[Lang::locale().'_name']}}</option>
                                    @else
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                    @endif
                                </select>
                                @if ($errors->has('subcategory_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subcategory_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="name" class="control-label">{{trans('products.name')}}</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{old('name')}}" required placeholder="{{trans('user.complete_in')}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="description" class="control-label">{{trans('products.description')}}</label>
                                <textarea name="description" id="description" cols="40" rows="5" class="form-control" placeholder="{{trans('user.complete_in')}}">{{old('description')}}</textarea>
                                <span id="contador"></span>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                          {{--  <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                                <div class="">
                                    <label for="description" class="control-label">{{trans('products.search_words')}}</label>
                                    <input name="tags" value="{{ old('tags') }}" id="tags" class="form-control tags" data-role="tagsinput" placeholder="Términos para la búsqueda">
                                    <span id="contador"></span>
                                    @if ($errors->has('tags'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>--}}
                            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                                <label for="type" class="control-label">{{trans('products.type')}}</label>
                                <input id="type" type="text" class="form-control" name="type" value="{{old('type')}}" placeholder="{{trans('user.complete_in')}}">
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="is_organic" class="control-label">{{trans('products.is_organic')}}</label>
                                <input id="is_organic" type="checkbox" name="is_organic" placeholder="{{trans('user.complete_in')}}" {{old('is_organic')=='organic'?'checked':''}}>
                            </div>
                            <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                                <label for="city" class="control-label">{{trans('products.color')}}</label>
                                {{-- TODO: HACER UN COLOR PICKER--}}
                                <input id="color" type="text" class="form-control" name="color" value="{{old('color')}}">
                                @if ($errors->has('color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('images') ? ' has-error' : '' }}">
                                <div class="" id="filesContainer">
                                    <label for="description" class="control-label">{{trans('products.product_photos')}}</label>

                                    <div class="preview-files"></div>
                                    <button id="seleccionar-fotos" type="button" class="boton-azul-no-shadow"><i class="fa fa-plus"></i> {{trans('user.update_image')}}</button>
                                    @if ($errors->has('photos'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('photos') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                @if ($errors->has('images'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group {{ $errors->has('country_id') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="country_id" class="control-label">{{trans('products.place_origin')}}</label>
                                <select name="country_id" id="country_id" class="form-control required" required>
                                    <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" {{old('country_id',0) == $country->id ? 'selected' : '' }}>{{$country->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('supply_ability') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i> <label for="supply_ability" class="control-label">{{trans('products.supply_ability')}}</label>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <input id="supply_ability" type="number" min="0" class="form-control" value="{{old('supply_ability')}}" name="supply_ability" required>
                                    </div>
                                    <div class="col-xs-5">
                                        <select name="supply_measure_id" id="supply_measure_id" class="form-control required">
                                            @foreach($measurements as $measure)
                                                <option value="{{$measure->id}}" {{old('supply_ability',0) == $measure->id ? 'selected' : '' }}>{{$measure->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <select name="supply_ability_frecuency" id="supply_ability_frecuency" class="form-control required">
                                            <option value="YEAR" {{old('supply_ability_frecuency') == 'YEAR' ? 'selected' : '' }}>{{trans('common.year')}}</option>
                                            <option value="MONTH" {{old('supply_ability_frecuency') == 'MONTH' ? 'selected' : '' }} >{{trans('common.month')}}</option>
                                            <option value="DAY" {{old('supply_ability_frecuency') == 'DAY' ? 'selected' : '' }} >{{trans('common.day')}}</option>
                                        </select>
                                    </div>
                                </div>
                                @if ($errors->has('supply_ability_frecuency'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('supply_ability_frecuency') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('moq') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="moq" class="control-label">{{trans('products.moq')}}</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input id="moq" type="number" min="0" class="form-control" name="moq" value="{{old('moq')}}" required>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="moq_measure_id" id="supply_measure_id" class="form-control required">
                                            @foreach($measurements as $measure)
                                                <option value="{{$measure->id}}" {{old('moq_measure_id',0) == $measure->id ? 'selected' : '' }} >{{$measure->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('moq'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('moq') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('delivery_time') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="delivery_time" class="control-label">{{trans('products.delivery_time')}} ({{trans('common.days')}}
                                    )</label>
                                <input id="delivery_time" type="number" min="0" max="999" class="form-control" name="delivery_time" value="{{old('delivery_time')}}" required>
                                @if ($errors->has('delivery_time'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_time') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i>
                                <label for="price" class="control-label">{{trans('products.price')}}</label>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <select name="price_type" id="price_type" class="form-control required" required>
                                            @foreach(\App\Repositories\Product\Product::TYPE_PRICES as $type)
                                                <option value="{{$type}}">{{trans('priceTypes.'.$type)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <div class="row" id="prices_div">
                                    <div class="col-xs-6" id="div_prices_2">
                                        <input id="price" placeholder="0.00" step="10" type="number" min="0" class="form-control" name="price" value="{{old('price')}}">
                                    </div>
                                    <div class="col-xs-6 col-sm-3" id="div_prices_3" style="display: none">
                                        <input id="price2" placeholder="0.00" step="10" type="number" min="0" class="form-control" name="price2" value="{{old('price2')}}">
                                    </div>
                                    <div class="col-xs-6 col-sm-2">
                                        <select name="currency_id" id="currency_id" class="form-control required pull-left">
                                            @foreach($currencies as $currency)
                                                <option value="{{$currency->id}}" {{old('currency_id',0) == $currency->id ? 'selected' : '' }}>{{$currency->currency_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-1 text-center">
                                        <label>{{ trans('products.per') }}</label>
                                    </div>
                                    <div class="col-xs-3">
                                        <select name="sale_unit_measure_id" id="sale_unit_measure_id" class="form-control required">
                                            @foreach($measurements as $measure)
                                                <option value="{{$measure->id}}" {{old('sale_unit_measure_id',0) == $measure->id ? 'selected' : '' }}>{{$measure->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <style>
                                    #prices_div {
                                        display: flex;
                                        margin: 0 -2px;
                                    }
                                    #prices_div > div {
                                        padding: 0 2px;
                                    }
                                </style>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price_measure_id') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="delivery_terms" class="control-label">{{trans('products.delivery_terms')}}</label>
                                <div class="checkbox-list">
                                    @foreach($deliveryTerms as $deliveryTerm)
                                        <label class="checkbox-inline" data-toggle="tooltip" title="{{$deliveryTerm->description}}">
                                            <input id="{{$deliveryTerm->id}}" type="checkbox" name="delivery_terms[]"
                                                   {{in_array($deliveryTerm->id,old('delivery_terms',[]))? 'checked' : ''}} value="{{$deliveryTerm->id}}"/>&nbsp;{{$deliveryTerm->name}}

                                        </label>
                                    @endforeach
                                    @if ($errors->has('office_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('office_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                @if ($errors->has('delivery_terms'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('delivery_terms') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('payment_terms') ? ' has-error' : '' }}">
                                <i class="fa fa-asterisk requiredasterisklabel"></i><label for="mobile_phone" class="control-label">{{trans('products.payment_terms')}}</label>
                                <div class="checkbox-list">
                                    @foreach($paymentTerms as $paymentTerm)
                                        <label class="checkbox-inline" data-toggle="tooltip" title="{{$paymentTerm->description}}">
                                            <input id="{{$paymentTerm->id}}" type="checkbox" name="payment_terms[]"
                                                   {{in_array($paymentTerm->id,old('payment_terms',[]))? 'checked' : ''}}  value="{{$paymentTerm->id}}"/>&nbsp;{{$paymentTerm->name}}
                                        </label>
                                    @endforeach
                                    @if ($errors->has('payment_terms'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('payment_terms') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ports') ? ' has-error' : '' }}">
                                <label for="ports" class="control-label">{{trans('products.ports')}}</label>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <input id="ports" type="text" class="form-control" name="ports[]" value="{{old('ports') && isset(old('ports')[0]) ? old('ports')[0] : '' }}"
                                                   placeholder="Port 1">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <input id="ports" type="text" class="form-control" name="ports[]" value="{{old('ports') && isset(old('ports')[1]) ? old('ports')[1] : '' }}"
                                                   placeholder="Port 2">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <div class="form-group">
                                            <input id="ports" type="text" class="form-control" name="ports[]" value="{{old('ports') && isset(old('ports')[2]) ? old('ports')[2] : '' }}"
                                                   placeholder="Port 3">
                                        </div>
                                    </div>

                                    @if ($errors->has('ports'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ports') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @if($subaccounts!=null && \Auth::user()->isPrincipalAccount())
                                <div class="form-group">
                                    <label class="control-label">{{trans('products.sale_manager_name')}}</label>
                                    <label class="label"> Subaccount </label>
                                    <select class="form-control required" name="user_id">
                                        @foreach($subaccounts as $subaccount)
                                            <option value="{{$subaccount->id}}" {{old('user_id',0) == $subaccount->id ? 'selected' : '' }}>{{$subaccount->names.' '.$subaccount->last_names}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary pull-right btn-lg">
                                        {{trans('common.forms.buttons.save')}}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}">
@endsection
@section('js')
    <script src="{{asset('assets/user/js/fileInput.js')}}?v=2"></script>
    <script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $('input[type="file"]').fileinput({
            theme: 'fa',
            showUpload: false
        });

        $("#tags").tagsinput({
            splitOn: ','
        });

        var PATH_SUPERCATEGORY = '{{route('api.categories.getBySupercategory')}}';
        var PATH_CATEGORY = '{{route('api.subcategories.getByCategory')}}';

        let d1 = $('#div_prices_1');
        let d2 = $('#div_prices_2');
        let d3 = $('#div_prices_3');
        let d4 = $('#prices_div');

        function setPriceTypes(_val) {
          if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_FIXED}}') {
            d2.show();
            d3.hide();
            d2.addClass('col-xs-6');
            d2.find('input').prop('required', true);
            d3.find('input').prop('required', false);
            d4.show();
          }
          else if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_RANGE}}') {
            d2.removeClass();
            d2.addClass('col-xs-3');
            d2.show();
            d3.removeClass();
            d3.addClass('col-xs-3');
            d3.show();
            d2.find('input').prop('required', true);
            d3.find('input').prop('required', true);
            d4.show();
          } else {
            d2.hide();
            d3.hide();
            d2.find('input').prop('required', false);
            d3.find('input').prop('required', false);
            d4.hide();
          }
        }

        $(function () {
            setPriceTypes();
            d3.hide();
            $('#price_type').change(function () {
                const _val = $(this).val();
                console.log('VALUE: ', _val);
                setPriceTypes(_val);
            });

            var max_chars = 10000;

            $('#description').keyup(function () {
                var chars = $(this).val().length;
                var diff = max_chars - chars;
                $('#contador').html(diff);
            });

            $('#supercategory').on('change', function (e) {
                e.preventDefault();
                $('#category').append("<option value='0'>{{trans('common.loading')}}</option>");
                var supercategory_id = $(this).find(':selected').val();
                var request = $.ajax({
                    url: PATH_SUPERCATEGORY,
                    method: "GET",
                    data: {supercategory_id: supercategory_id, language: '{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#category').find('option').remove();
                    $('#subcategory_id').find('option').remove();
                    if (data.length > 0) {
                        $.each(data, function (index, element) {
                            $('#category').append("<option value='" + element.id + "'>" + element.trans_name + "</option>");
                        });
                        $('#category').trigger('change');
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            $('#category').on('change', function (e) {
                e.preventDefault();
                $('#subcategory_id').append("<option value='0'>{{trans('common.loading')}}</option>");
                var category_id = $(this).find(':selected').val();
                var request = $.ajax({
                    url: PATH_CATEGORY,
                    method: "GET",
                    data: {category_id: category_id, language: '{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#subcategory_id').find('option').remove();
                    if (data.length > 0) {
                        $.each(data, function (index, element) {
                            $('#subcategory_id').append("<option value='" + element.id + "'>" + element.trans_name + "</option>");
                        });
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        console.log(e);
                        $('#blah').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#product-images").change(function () {
                readURL(this);
            });

        });
    </script>
@endsection