<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:25 PM
 */
use App\Repositories\Order\OrderRepository;

class OrderService extends BaseService
{

    public function __construct(OrderRepository $orderRepository)
    {
        $this->mainRepo = $orderRepository;
        parent::__construct();
    }

    public function findByReferenceCode($referenceCode)
    {
        return $this->mainRepo->firstByColumn('reference_code',$referenceCode);
    }
}