@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">{{trans('subaccounts.update_subaccount')}}</div>
                    <div class="panel-body">
                        <form class="" role="form" method="POST" action="{{ route('user.sub-accounts.update',$user) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="name" class="control-label">{{trans('user.form.first_name')}}</label>
                                            <input id="name" type="text" class="form-control" name="names" value="{{$user->names}}" required autofocus>
                                            @if ($errors->has('names'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('names') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('last_names') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="last_names" class="control-label">{{trans('user.form.last_names')}}</label>
                                            <input id="last_names" type="text" class="form-control" name="last_names" value="{{$user->last_names}}" required autofocus>
                                            @if ($errors->has('last_names'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('last_names') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="email" class="control-label">{{trans('user.form.email_address')}}</label>
                                            <input id="email" type="email" class="form-control" name="email" value="{{$user->email}}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="office_phone" class="control-label">{{trans('user.form.office_phone')}}</label>
                                            <input id="office_phone" type="text" class="form-control" name="office_phone" value="{{$user->office_phone}}" required>
                                            @if ($errors->has('office_phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('office_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="mobile_phone" class="control-label">{{trans('user.form.mobile_phone')}}</label>
                                            <input id="mobile_phone" type="text" class="form-control" name="mobile_phone" value="{{$user->mobile_phone}}" required>

                                            @if ($errors->has('mobile_phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('mobile_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="postal_code" class="control-label">{{trans('user.form.languages')}}</label>
                                            <div class="checkbox-list">
                                                @foreach($languages as $language)
                                                    <label class="checkbox-inline">
                                                        @if(isset($user) && $user->languages->contains($language->id))
                                                            <input id="inlineCheckbox{{$language->id}}" checked type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->name}}
                                                        @else
                                                            <input id="inlineCheckbox{{$language->id}}" {{in_array($language->id,old('languages',[]))?'checked':''}} type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->name}}
                                                        @endif
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('other_languages') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="other_languages" class="control-label">{{trans('user.form.others_languages')}}</label>
                                            <input id="other_languages" type="text" class="form-control" name="other_languages" value="{{$user->other_languages}}" >
                                            @if ($errors->has('other_languages'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('other_languages') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--<div class="form-group{{ $errors->has('markets') ? ' has-error' : '' }}">
                                        <div class="">
                                            <label for="postal_code" class="control-label">{{trans('subaccounts.markets')}}</label>
                                            <div class="checkbox-list">
                                                @foreach($markets as $market)
                                                    <label class="checkbox-inline">
                                                        @if(isset($user) && $user->markets->contains($market->id))
                                                            <input id="inlineCheckbox{{$market->id}}" checked type="checkbox" name="markets[]" value="{{$market->id}}"/>&nbsp;{{$market->name}}
                                                        @else
                                                            <input id="inlineCheckbox{{$market->id}}" {{in_array($market->id,old('markets',[]))?'checked':''}} type="checkbox" name="markets[]" value="{{$market->id}}"/>&nbsp;{{$market->name}}
                                                        @endif
                                                    </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary pull-right">
                                                {{trans('common.forms.buttons.update')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection