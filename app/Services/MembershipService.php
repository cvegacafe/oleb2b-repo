<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */
use App\Repositories\Membership\MembershipRepository;
use App\Repositories\User\User;

/**
 * Class MembershipService
 * @package App\Services
 */
class MembershipService extends BaseService
{

    /**
     * MembershipService constructor.
     * @param MembershipRepository $membershipRepository
     * @throws \Exception
     */
    public function __construct(MembershipRepository $membershipRepository)
    {
        $this->mainRepo = $membershipRepository;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->mainRepo->all();
    }

    /**
     * @param $id
     * @param $lang
     * @return mixed
     */
    public function findByLang($id,$lang)
    {
        return $this->mainRepo->findByLang($id,$lang);
    }

    /**
     * @param array $data
     * @param User $user
     * @return mixed
     */
    public function updateLastUserMembershipPivotRow(array $data,User $user)
    {
        $pivot = $this->mainRepo->findLastUserMembershipPivotRow($user);
        return $this->mainRepo->updateUserMembershipPivot($pivot->id,$data);
    }
}