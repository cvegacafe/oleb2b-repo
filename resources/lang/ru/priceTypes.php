<?php
return [
  \App\Repositories\Product\Product::TYPE_PRICE_RANGE => 'Порядок цен',
  \App\Repositories\Product\Product::TYPE_PRICE_ASK => 'Цена по запросу',
  \App\Repositories\Product\Product::TYPE_PRICE_FIXED => 'Цена фиксированная',
];