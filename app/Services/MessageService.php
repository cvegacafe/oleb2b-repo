<?php namespace App\Services;

use App\Library\Constants;
use App\Repositories\Message\MessageRepository;
use App\Repositories\Thread\Thread;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MessageService extends BaseService
{

    public function __construct(MessageRepository $messageRepository)
    {
        $this->mainRepo = $messageRepository;
        parent::__construct();
    }

    /**
     * @param Thread $thread
     * @return mixed
     */
    public function getLastMessages(Thread $thread)
    {
        $threads = $this->mainRepo->getLastMessage($thread->id);
        $messagesIds = $thread->messages->where('read_at', null)->pluck('id')->toArray();
        $date = Carbon::now();
        $this->setReadAtMessages($messagesIds, $date, Auth::id());

        return $threads;
    }

    /**
     * @param array $messagesIds
     * @param Carbon $time
     * @param $userId
     * @return mixed
     */
    public function setReadAtMessages(array $messagesIds, Carbon $time, $userId)
    {
        return $this->mainRepo->updateMultipleMessages($messagesIds, ['read_at' => $time], $userId);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getMessagesProductsFromUser($userId)
    {
        return $this->mainRepo->getUnseenMessagesByUser(Constants::THREAD_HAS_PRODUCT, $userId);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getMessagesBuyingRequestFromUser($userId)
    {
        return $this->mainRepo->getUnseenMessagesByUser(Constants::THREAD_HAS_BUYING_REQUEST, $userId);
    }

}