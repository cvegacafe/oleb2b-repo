<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserProductRequest;
use App\Library\Aviso;
use App\Repositories\Product\Product;
use App\Services\CategoryService;
use App\Services\CountryService;
use App\Services\CurrencyService;
use App\Services\DeliveryTermService;
use App\Services\FileService;
use App\Services\MeasureService;
use App\Services\PaymentTermService;
use App\Services\ProductService;
use App\Services\SubcategoryService;
use App\Services\SupercategoryService;
use App\Services\UserService;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class ProductsController
 * @package App\Http\Controllers\User
 */
class ProductsController extends Controller
{

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * ProductsController constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productService->getByUserId(Auth::id());
        return view('user.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SupercategoryService $supercategoryService
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param DeliveryTermService $deliveryTermService
     * @param PaymentTermService $paymentTermService
     * @param CurrencyService $currencyService
     * @param UserService $userService
     * @param Guard $guard
     * @param CategoryService $categoriesService
     * @param SubcategoryService $subcategoryService
     * @return \Illuminate\Http\Response
     */
    public function create(
        SupercategoryService $supercategoryService,
        CountryService $countryService,
        MeasureService $measureService,
        DeliveryTermService $deliveryTermService,
        PaymentTermService $paymentTermService,
        CurrencyService $currencyService,
        UserService $userService,
        Guard $guard,
        CategoryService $categoriesService,
        SubcategoryService $subcategoryService
    ) {
        $supercategories = $supercategoryService->all();
        $categories = $categoriesService->all();
        $subcategories = $subcategoryService->all();
        $countries = $countryService->all();
        $measurements = $measureService->all();
        $deliveryTerms = $deliveryTermService->all();
        $paymentTerms = $paymentTermService->all();
        $currencies = $currencyService->all();
        $subaccounts = null;

        if (Auth::user()->isPremium()) {
            $subaccounts = $userService->indexSubAccounts($guard->user()->supplier, $guard->id());
        }

        return view('user.products.create',
            compact('supercategories', 'countries', 'measurements', 'deliveryTerms', 'paymentTerms', 'currencies',
                'subaccounts', 'categories', 'subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserProductRequest|Request $request
     * @param FileService $fileService
     * @return mixed
     */
    public function store(UserProductRequest $request, FileService $fileService)
    {
        $pathsImages = [];
        try {

            $userId = $request->get('user_id', Auth::id());;

            if ($request->hasFile('images')) {
                $pathsImages = $fileService->storeProducts($request->file('images'), $userId);
            }

            $request->merge(['pictures' => $pathsImages]);
            $request['user_id'] = $userId;
            $validPorts = null;

            foreach ($request->get('ports') as $port) {
                if (!empty($port)) {
                    $validPorts[] = $port;
                }
            }

            $request['ports'] = $validPorts;
            DB::beginTransaction();
            $inputs['status'] = Product::STATUS_NEW;

            if ($request->get('price_type') == Product::TYPE_PRICE_ASK) {
                $request['price'] = null;
            } elseif ($request->get('price_type') == Product::TYPE_PRICE_RANGE) {
                $request['price_range'] = $request->get('price') . ' - ' . $request->get('price2');
                $request['price'] = null;
            }


            $product = $this->productService->create($request->all());
            $product->is_approved = true;
            $product->is_organic = $request->has('is_organic');
            $product->save();
            $product->deliveryTerms()->attach($request->get('delivery_terms'));
            $product->paymentTerms()->attach($request->get('payment_terms'));
            $membership = $product->user->activeMembership();
            if ($membership) {
                $membership->pivot->product_posting = $membership->pivot->product_posting - 1;
                $membership->pivot->save();
            } else {
                //TODO: que pasa si no encuentra la membresía
            }

            Aviso::guardado(trans('validation.custom.messages.record_saved'));
            DB::commit();

        } catch (Exception $e) {
            Log::error($e);
            Aviso::error(trans('validation.custom.messages.record_saved_error'));
            $fileService->removeProducts($pathsImages);
            DB::rollBack();

            return redirect()->back()->withInput($request->all());
        }

        return redirect()->route('user.products.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $slug
     * @param ProductService $productService
     * @param SupercategoryService $supercategoryService
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param DeliveryTermService $deliveryTermService
     * @param PaymentTermService $paymentTermService
     * @param CurrencyService $currencyService
     * @param UserService $userService
     * @param Guard $guard
     * @return \Illuminate\Http\Response
     */
    public
    function edit(
        $slug,
        ProductService $productService,
        SupercategoryService $supercategoryService,
        CountryService $countryService,
        MeasureService $measureService,
        DeliveryTermService $deliveryTermService,
        PaymentTermService $paymentTermService,
        CurrencyService $currencyService,
        UserService $userService,
        Guard $guard
    ) {
        $product = $productService->findBySlug($slug);
        $this->authorize('update', $product);
        if (!$product) {
            Aviso::modelNotFound();
            return redirect()->route('user.products.index');
        }

        $supercategories = $supercategoryService->all();
        $countries = $countryService->all();
        $measurements = $measureService->all();
        $deliveryTerms = $deliveryTermService->all();
        $paymentTerms = $paymentTermService->all();
        $currencies = $currencyService->all();
        $subaccounts = null;

        if (Auth::user()->isPremium()) {
            $subaccounts = $userService->indexSubAccounts($guard->user()->supplier, $guard->id());
        }

        return view('user.products.edit', compact('product', 'supercategories', 'countries', 'measurements',
            'deliveryTerms', 'paymentTerms', 'currencies', 'subaccounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $slug
     * @param ProductService $productService
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     */
    public
    function update(
        Request $request,
        $slug,
        ProductService $productService,
        FileService $fileService
    ) {
        $pathsImages = [];

        try {

            DB::beginTransaction();
            $product = $productService->findBySlug($slug);
            $product->is_organic = $request->has('is_organic');
            $product->save();
            $this->authorize('update', $product);
            $userId = $request->get('user_id', Auth::id());;
            $paths = $product->pictures;
            /*removing images*/
            if ($request->has('filesToRemove')) {
                foreach ($request->filesToRemove as $key2 => $image) {
                    $fileService->removeFile($product->pictures[$key2]);
                    unset($paths[$key2]);
                }
                $paths = array_values($paths);
            }

            if ($request->hasFile('images')) {
                $pathsImages = $fileService->storeProducts($request->file('images'), $userId);
                if ($paths == null) {
                    $paths = $pathsImages;
                } else {
                    $paths = array_merge($paths, $pathsImages);
                }
            }

            $request->merge(['pictures' => $paths]);
            $request['user_id'] = $userId;

            /*$product->deliveryTerms()->sync($request->get('delivery_terms'));
            $product->paymentTerms()->sync($request->get('payment_terms'));*/
            if ($request->has('delivery_terms')) {
                $product->deliveryTerms()->sync($request->get('delivery_terms'));
            } else {
                $product->deliveryTerms()->detach();
            }

            if ($request->has('payment_terms')) {
                $product->paymentTerms()->sync($request->get('payment_terms'));
            } else {
                $product->paymentTerms()->detach();
            }

            if ($request->get('price_type') == Product::TYPE_PRICE_ASK) {
                $request['price'] = null;
            } elseif ($request->get('price_type') == Product::TYPE_PRICE_RANGE) {
                $request['price_range'] = $request->get('price') . ' - ' . $request->get('price2');
                $request['price'] = null;
            }
//            dd($request);
            $productService->update($product->id, $request->all(), false);
            Aviso::guardado(trans('validation.custom.messages.record_updated'));
            DB::commit();

        } catch (Exception $e) {
            Log::error($e);
            Aviso::error(trans('validation.custom.messages.record_updated_error'));
            $fileService->removeProducts($pathsImages);
            DB::rollBack();
        }
        return redirect()->route('user.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $slug
     * @param ProductService $productService
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     */
    public
    function destroy(
        $slug,
        ProductService $productService,
        FileService $fileService
    ) {

        try {
            $product = $productService->findBySlug($slug);
            $this->authorize('destroy', $product);

            if (count($product->pictures)) {
                $fileService->removeProducts($product->pictures);
            }

            $product->deliveryTerms()->detach();
            $product->paymentTerms()->detach();
            $product->threads()->detach();
            $membership = $product->user->activeMembership();
            $membership->pivot->product_posting = $membership->pivot->product_posting + 1;
            $membership->pivot->save();
            $product->delete();
            Aviso::eliminado(trans('validation.custom.messages.record_deleted'));

        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado(trans('validation.custom.messages.record_deleted_error'));
        }

        return redirect()->route('user.products.index');
    }
}
