<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiContactSupplierFormRequest;
use App\Services\BuyingRequestService;
use App\Services\MessengerService;
use App\Services\ProductService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class BuyingRequestController extends Controller
{
    private $buyingRequestService = null;

    public function __construct(BuyingRequestService $buyingRequestService)
    {
        $this->buyingRequestService = $buyingRequestService;
    }


    public function buyingRequestCount()
    {
        $buyingRequest = $this->buyingRequestService->getUserBuyingRequest(Auth::id());

        return response()->json(['response' => 'ok','count'=>$buyingRequest->count()]);
    }

   /* public function contactSupplier(
        Request $request,
        ProductService $productService,
        MessengerService $messengerService
    )
    {
        try {
            $product = $productService->find($request->get('product_id'));
            $request['user_id'] = Auth::id();
            $buyingRequest = $this->buyingRequestService->createProductCopy(($request->all()), $product);
            $newThread['buyingRequest_id'] = $buyingRequest->id;
            $newThread['from_user_id'] = Auth::id();
            $newThread['to_user_id'] = $product->id;
            $messengerService->sendMessageToSupplier($newThread);
            return response()->json(['response' => 'ok'], 200);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['response' => 'error'], 500);
        }
    }*/
}
