<?php namespace App\Repositories\News;

use App\Repositories\BaseRepository;
use App\Repositories\News\News;
use Illuminate\Support\Facades\DB;

class NewsRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new News();
        parent::__construct();
    }

}