<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRangePriceToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement('ALTER TABLE `products` MODIFY `price` DECIMAL(8,2) NULL;');
            $table->string('price_range')->after('price')->nullable();
            $table->string('price_type')->after('price_range')->default(\App\Repositories\Product\Product::TYPE_PRICE_FIXED);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('price_range');
            $table->dropColumn('price_type');
        });
    }
}
