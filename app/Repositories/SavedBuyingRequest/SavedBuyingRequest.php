<?php namespace App\Repositories\SavedBuyingRequest;

use App\Repositories\BuyingRequest\BuyingRequest;
use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;

class SavedBuyingRequest extends Model
{
    public $table = 'savedBuyingRequest';

    protected $fillable = ['buyingRequest_id','user_id'];

    public function buyingRequest()
    {
        return $this->belongsTo(BuyingRequest::class,'buyingRequest_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
