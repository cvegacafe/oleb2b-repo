<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */


use App\Repositories\FavoriteProduct\FavoriteProductRepository;

class FavoriteProductService extends BaseService
{

    public function __construct(FavoriteProductRepository $favoriteProductRepository)
    {
        $this->mainRepo = $favoriteProductRepository;
        parent::__construct();
    }

    public function exists($user_id, $product_id)
    {
        if ($this->mainRepo->findByUserAndProduct($user_id, $product_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function getFavoritesByUser($user_id)
    {
        return $this->mainRepo->getByColumn('user_id', $user_id);
    }

    public function getFavoriteProductId($product_id, $user_id){
        return $this->mainRepo->findByUserAndProduct($user_id, $product_id);
    }
}