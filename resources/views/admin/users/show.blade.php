@extends('layouts.admin3')
@section('content')
    <div class="col-xs-12">
        <a href="{{action('Admin\UsersController@edit',$user)}}" class="btn btn-primary pull-right">Editar</a>
    </div>
    <div class="col-xs-6" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Usuario: </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center">
                        <img alt="User Pic" src="{{asset($user->image)}}" class="img-circle img-responsive"> </div>
                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>Tipo:</td>
                                <td>{{$user->type}}</td>
                            </tr>
                            <tr>
                                <td>Nombres:</td>
                                <td>{{$user->names}}</td>
                            </tr>
                            <tr>
                                <td>Apellidos:</td>
                                <td>{{$user->last_names}}</td>
                            </tr>
                            <tr>
                                <td>Telefono:</td>
                                <td>{{$user->office_phone}}</td>
                            </tr>
                            <tr>
                                <td>Celular:</td>
                                <td>{{$user->mobile_phone}}</td>
                            </tr>
                            <tr>
                            <tr>
                                <td>Empresa:</td>
                                <td>{{$user->company}}</td>
                            </tr>
                            <tr>
                                <td>Dirección:</td>
                                <td>{{$user->address}}</td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td>
                            </tr>
                            <tr>
                                <td>Ubicacion:</td>
                                <td> {{$user->city}} - {{$user->country->name}}  </td>
                            </tr>
                            <tr>
                                <td>Idiomas:</td>
                                <td>{{$user->languages->implode('es_name',', ')}} </td>
                            </tr>
                            <tr>
                                <td>Otros Idiomas:</td>
                                <td>{{$user->other_languages}} </td>
                            </tr>
                           {{-- <tr>
                                <td>Tipos de usuario:</td>
                                <td>
                                    <ul>
                                        <li>buyer: Comprador</li>
                                        <li>supplier: vendedor</li>
                                        <li>both: vendedor y comprador</li>
                                    </ul>
                                </td>
                            </tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Información de última membresia </h3>
            </div>
            @if($lastMembership)
                <div class="panel-body">
                    <div class="row">
                        <div class=" col-md-12 col-lg-12 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>Nombre de membresía:</td>
                                    <td>{{$lastMembership->es_name}}</td>
                                </tr>
                                <tr>
                                    <td>Precio de membresía:</td>
                                    <td>Anual: {{$lastMembership->annual_price}} / Mensual {{$lastMembership->monthly_price}}</td>
                                </tr>
                                <tr>
                                    <td>Precio de Pagado:</td>
                                    <td>{{strtolower($lastMembership->pivot->frequency)=='a'?'Anual':'Mensual'}} {{$lastMembership->pivot->amount}} </td>
                                </tr>
                                <tr>
                                    <td>Publicaciones de Membresia:</td>
                                    <td>{{$lastMembership->product_posting}}</td>
                                </tr>
                                <tr>
                                    <td>Fecha de inicio</td>
                                    <td>
                                        @if($lastMembership->pivot->created_at!= null)
                                            {{Carbon\Carbon::parse($lastMembership->pivot->created_at)->format('d/m/Y')}}
                                        @else
                                            Aun no ha iniciado
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fecha de finalización</td>
                                    <td> @if($lastMembership->pivot->finish_at!=null)
                                            {{Carbon\Carbon::parse($lastMembership->pivot->finish_at)->format('d/m/Y')}}
                                        @else
                                            Aun no ha iniciado
                                        @endif</td>
                                </tr>
                                <tr>
                                    <td><a href="#" data-toggle="tooltip" data-placement="top" title="Publicaciones que ofrece la membresía"><i class="fa fa-info-circle"></i></a> Publicaciones disponibles: </td>
                                    <td> {{$lastMembership->pivot->product_posting}}  publicaciones disponibles</td>
                                </tr>
                                <tr>
                                    <td><a href="#" data-toggle="tooltip" data-placement="top"
                                           title="Productos registrados"><i class="fa fa-info-circle"></i></a> Publicaciones utilizadas:</td>
                                    <td>{{$user->products->count()}} publicaciones utilizadas</td>
                                </tr>
                                <tr>
                                    <td><a href="#" data-toggle="tooltip" data-placement="top"
                                           title="Publicaciones disponibles"><i class="fa fa-info-circle"></i></a> Publicaciones restantes:</td>
                                    <td>{{$lastMembership->pivot->product_posting-$user->products->count()}} publicaciones restantes</td>
                                </tr>
                                <tr>
                                    <td colspan="2">* El número puede variar porque pueden existir productos dados de baja por incumplimiento de los términos y condiciones</td>

                                </tr>

                                <tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <tr>
                    <td>Este usuario no tiene ninguna membresia adquirida</td>
                </tr>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Historial de membresias
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class=" col-md-12 col-lg-12 ">
                        <table class="table table-user-information">
                            <thead>
                            <tr>
                                <td>Tipo de Membresia</td>
                                <td>Publicaciones restantes</td>
                                <td>Precio (USD)</td>
                                <td>Frecuencia</td>
                                <td>Fecha de inicio</td>
                                <td>Fecha de culminación</td>
                                <td>Estado</td>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($user->memberships->reverse() as $membership)
                                <tr>
                                    <td>{{$membership->es_name}}</td>
                                    <td>{{$membership->pivot->product_posting}}</td>
                                    <td>{{$membership->pivot->amount}}</td>
                                    @if(strtoupper($membership->pivot->frequency) == 'M')
                                        <td>Mensual</td>
                                    @else
                                        <td>Anual</td>
                                    @endif
                                    <td>{{$membership->pivot->created_at != null ? Carbon\Carbon::parse($membership->pivot->created_at)->format('d/m/Y'):'Aun no ha iniciado'}}</td>
                                    <td>{{$membership->pivot->finish_at != null ? Carbon\Carbon::parse($membership->pivot->finish_at)->format('d/m/Y'):'Aun no ha iniciado'}}</td>
                                    <td>
                                        @if($membership->pivot->finish_at!=null)
                                            @if($membership->pivot->is_active)
                                                <label class="label label-success">Activo</label>
                                            @else
                                                <label class="label label-default">Vencido</label>
                                            @endif
                                        @else
                                            <label class="label label-warning">Aun no iniciado</label>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>Aun no ha comprado membresias</td>
                                </tr>
                            @endforelse
                            </tbody>
                            <form action="{{route('admin.user.membership.add',$user)}}" method="POST" onsubmit="return confirm('Agregar esta membresia deshabilitara la anterior')">
                                {{csrf_field()}}
                                <tfoot>
                                <tr>
                                    <td>
                                        <select name="membership_id" id="membership_id" class="form-control">
                                            @foreach($memberships as $membership)
                                                <option value="{{$membership->id}}">{{$membership->name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><input type="text" name="product_posting" class="form-control"></td>
                                    <td><input type="text" name="amount" class="form-control"></td>
                                    <td>
                                        <select name="frequency" id="frequency" class="form-control">
                                            <option value="M">Mensual</option>
                                            <option value="A">Anual</option>
                                        </select>
                                    </td>
                                    <td><input type="date" name="created_at" class="form-control" value="{{\Carbon\Carbon::now()}}"></td>
                                    <td><input type="date" name="finish_at" class="form-control" value="{{\Carbon\Carbon::now()->addMonth()}}"></td>
                                    <td><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
                                </tr>
                                </tfoot>
                            </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Lista de productos</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="mytable" class="table table-bordred table-striped">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Nombre</th>
                            <th>Description</th>
                            <th>Color</th>
                            <th>Tipo</th>
                            <th>Supplier</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->products as $product)
                            <tr>
                                <td>
                                    <img src="{{asset($product->main_photo)}}" alt="" class="img-responsive"
                                         width="150px"><br>
                                </td>
                                <td>
                                    {{$product->en_name}}
                                </td>
                                <td>
                                    {{$product->en_description}}
                                </td>
                                <td>
                                    {{$product->en_color}}
                                </td>
                                <td>
                                    {{$product->en_type}}
                                </td>
                                <td><a href="{{action('Admin\SupplierController@edit',$product->user->supplier->id)}}">{{$product->user->supplier->company_name}}</a></td>
                                <td>
                                    @if($product->is_approved)
                                        <label class="label label-success">Aprobado</label>
                                    @else
                                        <label class="label label-warning">Sin Aprobar</label>
                                    @endif
                                    <a href="{{action('Admin\ProductsController@show',$product)}}">
                                        <button class="btn btn-primary btn-xs">Revisar</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
