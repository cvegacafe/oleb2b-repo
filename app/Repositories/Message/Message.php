<?php namespace App\Repositories\Message;

use App\Repositories\Attachment\Attachment;
use App\Repositories\Thread\Thread;
use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $fillable = ['thread_id', 'user_id', 'body', 'read_at'];

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function attachment()
    {
        return $this->hasOne(Attachment::class);
    }
}
