@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('products.add_new_product')}}</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{action('Admin\ProductsController@update',$product)}}"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="category"
                                       class="col-md-4 control-label">{{trans('products.product_supercategory')}}</label>
                                <div class="col-md-6">
                                    <select name="category" id="supercategory" class="form-control required">
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($supercategories as $supercategory)
                                            <option value="{{$supercategory->id}}" {{$supercategory->id==$product->subcategory->category->supercategory->id?'selected':''}}>{{$supercategory->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <label for="subcategory_id"
                                       class="col-md-4 control-label">{{trans('products.product_category')}}</label>
                                <div class="col-md-6">
                                    <select name="category_id" id="category" class="form-control required">
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($product->subcategory->category->supercategory->categories as $category)
                                            <option value="{{$category->id}}" {{$category->id==$product->subcategory->category->id?'selected':''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
                                <label for="subcategory_id"
                                       class="col-md-4 control-label">{{trans('products.product_subcategory')}}</label>
                                <div class="col-md-6">
                                    <select name="subcategory_id" id="subcategory_id" class="form-control required">
                                        <option value="{{$product->subcategory->id}}"
                                                selected>{{$product->subcategory->name}}</option>
                                    </select>
                                    @if ($errors->has('subcategory_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('subcategory_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('es_name') ? ' has-error' : '' }}">
                                <label for="es_name" class="col-md-4 control-label">{{trans('products.name')}}
                                    ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <input id="es_name" type="text" class="form-control" name="es_name"
                                           value="{{$product->es_name}}" required>
                                    @if ($errors->has('es_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label for="en_name" class="col-md-4 control-label">{{trans('products.name')}}
                                    ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <input id="en_name" type="text" class="form-control" name="en_name"
                                           value="{{$product->en_name}}" required>
                                    @if ($errors->has('en_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_name') ? ' has-error' : '' }}">
                                <label for="ru_name" class="col-md-4 control-label">{{trans('products.name')}}
                                    ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <input id="ru_name" type="text" class="form-control" name="ru_name"
                                           value="{{$product->ru_name}}" required>
                                    @if ($errors->has('ru_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('es_description') ? ' has-error' : '' }}">
                                <label for="es_description"
                                       class="col-md-4 control-label">{{trans('products.description')}}
                                    ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <textarea name="es_description" id="es_description" cols="40" rows="5"
                                              class="form-control">{{$product->es_description}}</textarea>
                                    @if ($errors->has('es_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_description') ? ' has-error' : '' }}">
                                <label for="en_description"
                                       class="col-md-4 control-label">{{trans('products.description')}}
                                    ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <textarea name="en_description" id="en_description" cols="40" rows="5"
                                              class="form-control">{{$product->en_description}}</textarea>
                                    @if ($errors->has('en_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_description') ? ' has-error' : '' }}">
                                <label for="ru_description"
                                       class="col-md-4 control-label">{{trans('products.description')}}
                                    ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <textarea name="ru_description" id="ru_description" cols="40" rows="5"
                                              class="form-control">{{$product->ru_description}}</textarea>
                                    @if ($errors->has('ru_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('photos') ? ' has-error' : '' }}">
                                <label for="description"
                                       class="col-md-4 control-label">{{trans('products.product_photos')}}</label>
                                <div class="col-md-6">
                                    <input type="file" name="images[0]" class="form-control">
                                    <input type="file" name="images[1]" class="form-control">
                                    <input type="file" name="images[2]" class="form-control">
                                    @if ($errors->has('photos'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('photos') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="country_id"
                                       class="col-md-4 control-label">{{trans('products.place_origin')}}</label>
                                <div class="col-md-6">
                                    <select name="country_id" id="country_id" class="form-control required">
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{$country->id==$product->country_id?'selected':''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('supply_ability') ? ' has-error' : '' }}">
                                <label for="supply_ability"
                                       class="col-md-4 control-label">{{trans('products.supply_ability')}}</label>
                                <div class="col-md-6">
                                    <div class="col-xs-3">
                                        <input id="supply_ability" type="text" class="form-control"
                                               name="supply_ability" value="{{$product->supply_ability}}" required>
                                    </div>
                                    <div class="col-xs-5">
                                        <select name="supply_measure_id" id="supply_measure_id"
                                                class="form-control required">
                                            @foreach($measurements as $measure)
                                                <option value="{{$measure->id}}" {{$measure->id==$product->supply_measure_id?'selected':''}}>{{$measure->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-4">
                                        <select name="supply_ability_frecuency" id="supply_ability_frecuency"
                                                class="form-control required">
                                            <option value="YEAR" {{"YEAR"==$product->supply_ability_frecuency?'selected':''}}>{{trans('common.year')}}</option>
                                            <option value="MONTH" {{"MONTH"==$product->supply_ability_frecuency?'selected':''}}>{{trans('common.month')}}</option>
                                            <option value="DAY" {{"DAY"==$product->supply_ability_frecuency?'selected':''}}>{{trans('common.day')}}</option>
                                        </select>
                                    </div>

                                    @if ($errors->has('supply_ability_frecuency'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('supply_ability_frecuency') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('moq') ? ' has-error' : '' }}">
                                <label for="moq" class="col-md-4 control-label">{{trans('products.moq')}}</label>
                                <div class="col-md-6">
                                    <div class="col-xs-6">
                                        <input id="moq" type="text" class="form-control" name="moq"
                                               value="{{$product->moq}}" required>
                                    </div>
                                    <div class="col-xs-6">
                                        <select name="moq_measure_id" id="supply_measure_id"
                                                class="form-control required">
                                            @foreach($measurements as $measure)
                                                <option value="{{$measure->id}}" {{$measure->id==$product->moq_measure_id?'selected':''}}>{{$measure->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('moq'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('moq') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('delivery_time') ? ' has-error' : '' }}">
                                <label for="delivery_time"
                                       class="col-md-4 control-label">{{trans('products.delivery_time')}}
                                    ({{trans('common.days')}})</label>
                                <div class="col-md-6">
                                    <input id="delivery_time" type="number" class="form-control" name="delivery_time"
                                           value="{{$product->delivery_time}}" required>
                                    @if ($errors->has('delivery_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('delivery_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                <label for="price" class="col-md-4 control-label">{{trans('products.price')}}</label>
                                <div class="col-md-6">
                                    <div class="col-xs-12" id="div_prices_1">
                                        <select name="price_type" id="price_type" class="form-control required"
                                                required>
                                            @foreach(\App\Repositories\Product\Product::TYPE_PRICES as $type)
                                                <option value="{{$type}}" {{old('price_type',$product->price_type) == $type ? 'selected' : '' }}>{{trans('priceTypes.'.$type)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-12" style="margin-top: 10px" id="prices_div">
                                        @if($product->price_type === \App\Repositories\Product\Product::TYPE_PRICE_FIXED)
                                            @php
                                                $price1 = $product->price;
                                                $price2 = 0;
                                            @endphp
                                        @elseif($product->price_type === \App\Repositories\Product\Product::TYPE_PRICE_RANGE)
                                            @php
                                                $priceRange = explode(' - ', $product->price_range);
                                                $price1 = $priceRange[0];
                                                $price2 = $priceRange[1];
                                            @endphp
                                        @else
                                            @php
                                                $price1 = 0;
                                                $price2 = 0;
                                            @endphp
                                        @endif
                                        <div class="col-xs-6" id="div_prices_2">
                                            <input id="price" placeholder="0.00" step="10" type="number" min="0"
                                                   class="form-control" name="price"
                                                   value="{{old('price',$price1)}}">
                                        </div>

                                        <div class="col-xs-6 col-sm-3" id="div_prices_3" style="display: none">
                                            <input id="price2" placeholder="0.00" step="10" type="number" min="0"
                                                   class="form-control" name="price2" value="{{old('price2',$price2)}}">
                                        </div>


                                        <div class="col-xs-6 col-sm-2">
                                            <select name="currency_id" id="currency_id"
                                                    class="form-control required pull-left">
                                                @foreach($currencies as $currency)
                                                    <option value="{{$currency->id}}" {{old('currency_id',0) == $currency->id ? 'selected' : '' }}>{{$currency->currency_code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-1 text-center">
                                            <label>{{ trans('products.per') }}</label>
                                        </div>
                                        <div class="col-xs-3">
                                            <select name="sale_unit_measure_id" id="sale_unit_measure_id"
                                                    class="form-control required">
                                                @foreach($measurements as $measure)
                                                    <option value="{{$measure->id}}" {{old('sale_unit_measure_id',0) == $measure->id ? 'selected' : '' }}>{{$measure->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price_measure_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                <label for="delivery_terms"
                                       class="col-md-4 control-label">{{trans('products.delivery_terms')}}</label>
                                <div class="col-md-6">
                                    @foreach($deliveryTerms as $deliveryTerm)
                                        <label class="checkbox-inline">
                                            <input id="{{$deliveryTerm->id}}" type="checkbox" name="delivery_terms[]"
                                                   value="{{$deliveryTerm->id}}" {{$product->deliveryTerms->contains('id',$deliveryTerm->id)?'checked':''}}/>&nbsp;{{$deliveryTerm->name}}
                                        </label>
                                    @endforeach
                                    @if ($errors->has('office_phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('office_phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('payment_terms') ? ' has-error' : '' }}">
                                <label for="mobile_phone"
                                       class="col-md-4 control-label">{{trans('products.payment_terms')}}</label>
                                <div class="col-md-6">
                                    @foreach($paymentTerms as $paymentTerm)
                                        <label class="checkbox-inline">
                                            <input id="{{$paymentTerm->id}}" type="checkbox" name="payment_terms[]"
                                                   value="{{$paymentTerm->id}}" {{$product->paymentTerms->contains('id',$paymentTerm->id)?'checked':''}}/>&nbsp;{{$paymentTerm->name}}
                                        </label>
                                    @endforeach
                                    @if ($errors->has('payment_terms'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('payment_terms') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('es_type') ? ' has-error' : '' }}">
                                <label for="es_type" class="col-md-4 control-label">{{trans('products.type')}}
                                    ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <input id="es_type" type="text" class="form-control" name="es_type"
                                           value="{{$product->es_type}}">
                                    @if ($errors->has('es_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_type') ? ' has-error' : '' }}">
                                <label for="en_type" class="col-md-4 control-label">{{trans('products.type')}}
                                    ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <input id="en_type" type="text" class="form-control" name="en_type"
                                           value="{{$product->en_type}}">
                                    @if ($errors->has('en_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_type') ? ' has-error' : '' }}">
                                <label for="ru_type" class="col-md-4 control-label">{{trans('products.type')}}
                                    ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <input id="ru_type" type="text" class="form-control" name="ru_type"
                                           value="{{$product->ru_type}}">
                                    @if ($errors->has('ru_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('es_color') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">{{trans('products.color')}}
                                    ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <input id="es_color" type="text" class="form-control" name="es_color"
                                           value="{{$product->es_color}}">
                                    @if ($errors->has('es_color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_color') ? ' has-error' : '' }}">
                                <label for="en_color" class="col-md-4 control-label">{{trans('products.color')}}
                                    ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <input id="en_color" type="text" class="form-control" name="en_color"
                                           value="{{$product->en_color}}">
                                    @if ($errors->has('en_color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_color') ? ' has-error' : '' }}">
                                <label for="ru_color" class="col-md-4 control-label">{{trans('products.color')}}
                                    ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <input id="ru_color" type="text" class="form-control" name="ru_color"
                                           value="{{$product->ru_color}}">
                                    @if ($errors->has('ru_color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ports') ? ' has-error' : '' }}">
                                <label for="ports" class="col-md-4 control-label">{{trans('products.ports')}}</label>
                                <div class="col-md-6">
                                    <div class="col-xs-4">
                                        <input id="ports" type="text" class="form-control" name="ports[]"
                                               value="{{isset($product->ports[0]) ? $product->ports[0] : ''}}">
                                    </div>
                                    <div class="col-xs-4">
                                        <input id="ports" type="text" class="form-control" name="ports[]"
                                               value="{{isset($product->ports[1]) ? $product->ports[1] : ''}}">
                                    </div>
                                    <div class="col-xs-4">
                                        <input id="ports" type="text" class="form-control" name="ports[]"
                                               value="{{isset($product->ports[2]) ? $product->ports[2] : ''}}">
                                    </div>

                                    @if ($errors->has('ports'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ports') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{trans('common.forms.buttons.save')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .file-preview .close {
            display: block;
            float: none;
            padding-right: 14px;
            text-align: right;
        }

        .krajee-default .file-preview-image {
            width: 240px !important;
            height: auto !important;
        }

        .bootstrap-tagsinput {
            width: 100%;
        }

        #prices_div {
            display: flex;
            margin: 0 -2px;
        }

        #prices_div > div {
            padding: 0 2px;
        }
    </style>
@endsection
@section('scripts')
    <script>
        var PATH_SUPERCATEGORY = '{{route('api.categories.getBySupercategory')}}';
        var PATH_CATEGORY = '{{route('api.subcategories.getByCategory')}}';

        let d1 = $('#div_prices_1');
        let d2 = $('#div_prices_2');
        let d3 = $('#div_prices_3');
        let d4 = $('#prices_div');

        function setPriceTypes(_val) {
            if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_FIXED}}') {
                d2.show();
                d3.hide();
                d2.addClass('col-xs-6');
                d2.find('input').prop('required', true);
                d3.find('input').prop('required', false);
                d4.show();
            }
            else if (_val === '{{\App\Repositories\Product\Product::TYPE_PRICE_RANGE}}') {
                d2.removeClass();
                d2.addClass('col-xs-3');
                d2.show();
                d3.removeClass();
                d3.addClass('col-xs-3');
                d3.show();
                d2.find('input').prop('required', true);
                d3.find('input').prop('required', true);
                d4.show();
            } else {
                d2.hide();
                d3.hide();
                d2.find('input').prop('required', false);
                d3.find('input').prop('required', false);
                d4.hide();
            }
        }
        $(function () {
            setPriceTypes();
            d3.hide();
            $('#price_type').change(function () {
                const _val = $(this).val();
                console.log('VALUE: ', _val);
                setPriceTypes(_val);
            });

            var max_chars = 250;

            $('#description').keyup(function () {
                var chars = $(this).val().length;
                var diff = max_chars - chars;
                $('#contador').html(diff);
            });

            $('#supercategory').on('change', function (e) {
                e.preventDefault();
                $('#category').append("<option value='0'>{{trans('common.loading')}}</option>");
                var supercategory_id = $(this).find(':selected').val();
                var request = $.ajax({
                    url: PATH_SUPERCATEGORY,
                    method: "GET",
                    data: {supercategory_id: supercategory_id, language: '{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#category').find('option').remove();
                    $('#subcategory_id').find('option').remove();
                    if (data.length > 0) {
                        $.each(data, function (index, element) {
                            $('#category').append("<option value='" + element.id + "'>" + element.trans_name + "</option>");
                        });
                        $('#category').trigger('change');
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            $('#category').on('change', function (e) {
                e.preventDefault();
                $('#subcategory_id').append("<option value='0'>{{trans('common.loading')}}</option>");
                var category_id = $(this).find(':selected').val();
                var request = $.ajax({
                    url: PATH_CATEGORY,
                    method: "GET",
                    data: {category_id: category_id, language: '{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#subcategory_id').find('option').remove();
                    if (data.length > 0) {
                        $.each(data, function (index, element) {
                            $('#subcategory_id').append("<option value='" + element.id + "'>" + element.trans_name + "</option>");
                        });
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        console.log(e);
                        $('#blah').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#product-images").change(function () {
                readURL(this);
            });

        });
    </script>
@endsection