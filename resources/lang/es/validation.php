<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used
    | by the validator class. Some of the rules contain multiple versions,
    | such as the size (max, min, between) rules. These versions are used
    | for different input types such as strings and files.
    |
    | These language lines may be easily changed to provide custom error
    | messages in your application. Error messages for custom validation
    | rules may also be added to this file.
    |
    */

    "accepted" => "El campo debe ser aceptado.",
    "active_url" => "El campo no es una URL válida.",
    "after" => "El campo debe ser una fecha después de :date.",
    "alpha" => "El campo sólo puede contener letras.",
    "alpha_dash" => "El campo sólo puede contener letras, números y guiones.",
    "alpha_num" => "El campo sólo puede contener letras y números.",
    "array" => "El campo debe ser un arreglo.",
    "before" => "El campo debe ser una fecha antes :date.",
    "between" => array(
        "numeric" => "El campo debe estar entre :min - :max.",
        "file" => "El campo debe estar entre :min - :max kilobytes.",
        "string" => "El campo debe estar entre :min - :max caracteres.",
        "array" => "El campo debe tener entre :min y :max elementos.",
    ),
    "confirmed" => "El campo confirmación no coincide.",
    "date" => "El campo no es una fecha válida.",
    "date_format" => "El campo no corresponde con el formato :format.",
    "different" => "El campo and :other debe ser diferente.",
    "digits" => "El campo debe ser de :digits dígitos.",
    "digits_between" => "El campo debe terner entre :min y :max dígitos.",
    "email" => "El formato del es invalido.",
    "exists" => "El campo seleccionado es inválido.",
    "image" => "El campo debe ser una imagen.",
    "in" => "El campo seleccionado es inválido.",
    "integer" => "El campo debe ser un entero.",
    "ip" => "El campo Debe ser una dirección IP válida.",
    "match" => "El formato es inválido.",
    "max" => array(
        "numeric" => "El campo debe ser menor que :max.",
        "file" => "El campo debe ser menor que :max kilobytes.",
        "string" => "El campo debe ser menor que :max caracteres.",
        "array" => "El campo debe tener al menos :min elementos.",
    ),

    "mimes" => "El campo debe ser un archivo de tipo :values.",
    "min" => array(
        "numeric" => "El campo debe tener al menos :min.",
        "file" => "El campo debe tener al menos :min kilobytes.",
        "string" => "El campo debe tener al menos :min caracteres.",
    ),
    "not_in" => "El campo seleccionado es invalido.",
    "numeric" => "El campo debe ser un numero.",
    "regex" => "El formato del campo es inválido.",
    "required" => "El campo es requerido",
    "required_if" => "El campo es requerido cuando el campo :other es :value.",
    "required_with" => "El campo es requerido cuando :values está presente.",
    "required_with_all" => "El campo es requerido cuando :values está presente.",
    "required_without" => "El campo es requerido cuando :values no está presente.",
    "required_without_all" => "El campo es requerido cuando ningún :values está presentes.",
    "same" => "El campo y :other debe coincidir.",
    "size" => array(
        "numeric" => "El campo debe ser :size.",
        "file" => "El campo debe terner :size kilobytes.",
        "string" => "El campo debe tener :size caracteres.",
        "array" => "El campo debe contener :size elementos.",
    ),

    "unique" => "El campo ya ha sido tomado.",
    "url" => "El formato de es inválido.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute_rule" to name the lines. This helps keep your
    | custom validation clean and tidy.
    |
    | So, say you want to use a custom validation message when validating that
    | the "email" attribute is unique. Just add "email_unique" to this array
    | with your custom message. The Validator will handle the rest!
    |
    */

    'custom' => array(
        'general' => array(
            'expired' => 'Su sesión ha expirado. Por favor vuelva a ingresar.',
        ),
        'email' => [
            'confirmed' => 'Necesita validar su correo electrónico para acceder.',
            'pending' => 'Su cuenta esta pendiente de verificar el código de agente inmobiliario.',
            'expired' => 'Su membresia ha expirado, debe tener una membresía activa para poder acceder.',
            'disabled' => 'Su cuenta ha sido deshabilitada.',
            'bloqued' => 'Su cuenta ha sido bloqueada.',
        ],
        'messages' => [
            'record_saved' => 'El registro ha sido guardado correctamente',
            'record_saved_error' => 'El registro no ha podido ser guardado.',
            'record_updated' => 'El registro ha sido actualizado correctamente',
            'record_updated_error' => 'El registro no ha podido ser actualizado',
            'record_deleted' => 'El registro ha sido eliminado correctamente',
            'record_deleted_error' => 'El registro no ha podido ser eliminado',
            'unexpected_error' => 'Un error inesperado ha ocurrido',
            'unauthorized_action' => 'Usted no esta autorizado para realizar esta accion'
        ]
    ),

    /*
    |--------------------------------------------------------------------------
    | Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". Your users will thank you.
    |
    | The Validator class will automatically search this array of lines it
    | is attempting to replace the place-holder in messages.
    | It's pretty slick. We think you'll like it.
    |
    */

    'attributes' => array(
        'username' => 'usuario',
        'email' => 'correo electrónico',
        'password' => 'contraseña',
        'rememberme' => 'Recuérdame',
        'forgot_pass' => '¿Has olvidado tu contraseña?',
        'name' => 'nombre',
        'confirm_password' => 'repita la contraseña',
        'register' => 'registrarse',
    ),
);
