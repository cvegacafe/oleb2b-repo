<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class contactBuyerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'buyingRequest_id'=>'required|exists:buyingRequests,id',
            'message'=>'max:1000'
        ];
    }

    public function getRedirectUrl()
    {
        return route('portal.buyingRequests',['show'=>'contact-buyer']);
    }
}
