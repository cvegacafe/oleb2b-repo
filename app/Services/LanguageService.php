<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:23 PM
 */

use App\Repositories\Language\LanguageRepository;

class LanguageService extends BaseService
{

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->mainRepo = $languageRepository;
        parent::__construct();
    }

    public function findByAbbreviation($abbreviation)
    {
        return $this->mainRepo->firstByColumn('abbreviation', $abbreviation);
    }

    public function getLanguagesByLang($lang)
    {
        return $this->mainRepo->getByLangFluent($lang);
    }

    public function getLanguagesOriginalName()
    {
        return $this->mainRepo->getLangByOriginalName();
    }
}