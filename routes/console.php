<?php

use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('oleb2b:countries-slug',
    function (\App\Services\CountryService $countryService) {
        foreach ($countryService->all() as $country) {
            $country->es_slug = str_slug($country->es_name);
            $country->en_slug = str_slug($country->en_name);
            $country->ru_slug = str_slug($country->ru_name);
            $country->save();
        }
    })->describe('slug to countries');
