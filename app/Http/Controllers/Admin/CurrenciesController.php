<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Library\Aviso;
use App\Repositories\Currency\Currency;
use App\Services\CurrencyService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CurrenciesController extends Controller
{
    private $currenciesService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currenciesService = $currencyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = $this->currenciesService->all();

        return view('admin.currencies.index', compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.currencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $this->currenciesService->create([
                'currency' => $request->get('currency'),
                'currency_code' => $request->get('currency_code'),
                'enabled' => $request->has('enabled') ? true : false
            ]);
            Aviso::guardado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }

        return redirect()->action('Admin\CurrenciesController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        return view('admin.currencies.edit', compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currency $currency)
    {
        try {
            $this->currenciesService->update($currency->id, [
                'currency' => $request->get('currency'),
                'currency_code' => $request->get('currency_code'),
                'enabled' => $request->has('enabled') ? true : false,
            ]);
            Aviso::actualizado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noActualizado();
        }

        return redirect()->action('Admin\CurrenciesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        try {
            $currency->delete();
            Aviso::eliminado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado();
        }

        return redirect()->action('Admin\CurrenciesController@index');
    }
}
