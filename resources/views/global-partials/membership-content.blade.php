<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div class="membresias-contenedor">
                <div class="membresias">
                    <div class="row logos-top">
                        <div class="col-xs-3">

                        </div>
                        <div class="col-xs-3 free">
                            <div class="top">
                                {{trans('memberships.basic')}}
                            </div>
                            <div class="icon">
                                <figure>
                                    <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Basic.png')}}"  class="img-responsive">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-3 advanced">
                            <div class="top">
                                {{trans('memberships.advanced')}}
                            </div>
                            <div class="icon">
                                <figure>
                                    <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Advanced.png')}}"  class="img-responsive">
                                </figure>
                            </div>
                        </div>
                        <div class="col-xs-3 premium">
                            <div class="top">
                                {{trans('memberships.premium')}}
                            </div>
                            <div class="icon">
                                <figure>
                                    <img src="{{asset('assets/pages/img/'.Lang::locale().'_icon_Premium.png')}}"  class="img-responsive">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="cuerpo">
                        <div class="row">
                            <div class="col-xs-3 description">
                                <span>
                                  {{--  <i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.product_posting')}}
                                </span>
                            </div>
                            <div class="col-xs-3 free">
                                <span>
                                    {{App\Repositories\Membership\Membership::find(1)->product_posting}}
                                </span>
                            </div>
                            <div class="col-xs-3 premium">
                                <span>
                                    {{trans('memberships.unlimited')}}
                                </span>
                            </div>
                            <div class="col-xs-3 advanced">
                                <span>
                                     {{trans('memberships.unlimited')}}
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 description">
                                <span>
                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.banner_main_page')}}
                                </span>
                            </div>
                            <div class="col-xs-3 free">
                                <span>
                                <i class="fa fa-close"></i>
                                </span>
                            </div>
                            <div class="col-xs-3 premium">
                                <span>
                                    {{trans('memberships.average_rotation',['time'=>$advancedMembership->main_rotation_banners]) }}
                                </span>
                            </div>
                            <div class="col-xs-3 advanced">
                                <span>
                               {{trans('memberships.average_rotation',['time'=>$premiumMembership->main_rotation_banners]) }}
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 description">
                                <span>
                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.banner_inner_pages')}}

                                </span>
                            </div>
                            <div class="col-xs-3 free">
                                <span>
                                <i class="fa fa-close"></i>
                                </span>
                            </div>
                            <div class="col-xs-3 premium">
                                <span>
                                 {{trans('memberships.average_rotation',['time'=>$advancedMembership->inner_rotation_banners]) }}
                                </span>
                            </div>
                            <div class="col-xs-3 advanced">
                                <span>

                               {{trans('memberships.average_rotation',['time'=>$premiumMembership->inner_rotation_banners]) }}

                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 description">
                                <span>

                                {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.featured_products')}}

                                </span>
                            </div>
                            <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                            </div>
                            <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                            </div>
                            <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 description">
                                <span>
                                   {{-- <i class="fa fa-check-circle-o"></i>--}}
                                    <span class="rows2">
                                       {{trans('memberships.ability_quote_buying_request')}}
                                    </span>
                                </span>
                            </div>
                            <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                            </div>
                            <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                            </div>
                            <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 description">
                                <span>
                                    {{--<i class="fa fa-check-circle-o"></i>--}}
                                    {{trans('memberships.subaccounts')}}
                                </span>
                            </div>
                            <div class="col-xs-3 free">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                            </div>
                            <div class="col-xs-3 premium">
                                <span>

                                <i class="fa fa-close"></i>

                                </span>
                            </div>
                            <div class="col-xs-3 advanced">
                                <span>

                                <i class="fa fa-check"></i>

                                </span>
                            </div>
                        </div>
                    </div>
                    @if(Auth::check())
                        <div class="choose">
                            <div class="row">
                                <div class="col-xs-3">

                                </div>
                                <div class="col-xs-3 free" >
                                    @if(Auth::user()->activeMembership()->id == \App\Library\Constants::MEMBERSHIP_BASIC)
                                        <div class="inner">
                                            <button class="disabled">{{trans('memberships.active')}}</button>
                                        </div>
                                    @else
                                        <div class="inner">
                                            <button class="free disabled">{{trans('memberships.choose_plan')}}</button>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-xs-3">
                                    @if(Auth::user()->activeMembership()->id == $advancedMembership->id)
                                        <div class="inner">
                                            <button class="disabled">{{trans('memberships.active')}}</button>
                                        </div>
                                    @else
                                        <div class="inner">
                                            <button class="advanced " data-toggle="modal" data-target="#modal-advanced">{{trans('memberships.choose_plan')}}</button>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-xs-3">
                                    @if(Auth::user()->activeMembership()->id == \App\Library\Constants::MEMBERSHIP_PREMIUM)
                                        <div class="inner">
                                            <button class="disabled">{{trans('memberships.active')}}</button>
                                        </div>
                                    @else
                                        <div class="inner">
                                            <button class="premium" data-toggle="modal" data-target="#modal-premium">{{trans('memberships.choose_plan')}}</button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="choose">
                            <div class="row">
                                <div class="col-xs-3">

                                </div>
                                <div class="col-xs-3 free" >
                                    <div class="inner">
                                        <a href="{{route('user.memberships.index')}}"> <button class="free ">{{trans('memberships.choose_plan')}}</button></a>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="inner">
                                        <a href="{{route('user.memberships.index')}}">
                                            <button class="advanced" data-toggle="modal"
                                                    data-target="#modal-advanced">{{trans('memberships.choose_plan')}}</button>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <div class="inner">
                                        <a href="{{route('user.memberships.index')}}">
                                            <button class="premium" data-toggle="modal"
                                                    data-target="#modal-premium">{{trans('memberships.choose_plan')}}</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <br><br><br>
            <small class="text-roboto">{{trans('memberships.cond_1')}}</small>
            <br><br>
            <small class="text-roboto">{{trans('memberships.cond_2')}}</small>
            <br><br>
            <small class="text-roboto">{{trans('memberships.cond_3')}}</small>
        </div>
    </div>
</div>
@if(Auth::check())
<div class="modal fade" id="modal-premium">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{$premiumMembership->name}} : {{trans('memberships.choose_plan')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select name="method" id="frecuency_premium" class="form-control" required>
                        <option selected disabled>-- {{trans('memberships.choose_plan')}}</option>
                        <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_MONTHLY}}"> USD {{$premiumMembership->monthly_price}}/ {{trans('common.month')}}</option>
                        <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_ANNUAL}}"> USD {{$premiumMembership->annual_price}} / {{trans('common.year')}} </option>
                    </select>
                        <span class="help-block">
                            <strong id="error-msg"></strong>
                        </span>
                </div>
                @include('global-partials.purchase-promotion')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.forms.buttons.close')}}</button>
                <button type="button" class="btn btn-primary" onClick="sendPlanSelection('{{$premiumMembership->id}}',this)">{{trans('common.forms.buttons.confirm')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-advanced">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{$advancedMembership->name}} : {{trans('memberships.choose_plan')}}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select name="method" id="frecuency_advanced" class="form-control" required>
                        <option selected disabled>-- {{trans('memberships.choose_plan')}}</option>
                        <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_MONTHLY}}"> USD {{$advancedMembership->monthly_price}} / {{trans('common.month')}}</option>
                        <option value="{{\App\Library\Constants::MEMBERSHIP_FREQUENCY_ANNUAL}}"> USD {{$advancedMembership->annual_price}} / {{trans('common.year')}} </option>
                    </select>
                        <span class="help-block">
                            <strong id="error-msg"></strong>
                        </span>
                </div>
                @include('global-partials.purchase-promotion')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.forms.buttons.close')}}</button>
                <button type="button" class="btn btn-primary" onClick="sendPlanSelection('{{$advancedMembership->id}}',this)">{{trans('common.forms.buttons.confirm')}}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endif
