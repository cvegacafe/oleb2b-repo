@extends('layouts.admin3')
@section('content')
    <div id="breadcrumb">
        <ul class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="/"> Home</a></li>
            <li class="active"> Oleb2b Imágenes</li>
        </ul>
    </div><!-- /breadcrumb-->
    <ul class="tab-bar grey-tab">
        <li class="{{request('show',str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_BACKGROUND))==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_BACKGROUND) ? 'active in' :''}}">
            <a href="#background" data-toggle="tab">
                <span class="block text-center">
                    <i class="fa fa-file-image-o fa-2x"></i>
                </span>
                Fondo Principal
            </a>
        </li>
        <li class="{{request('show')==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_CCL) ? 'active in' : ''}}">
            <a href="#cc" data-toggle="tab">
                <span class="block text-center">
                    <i class="fa fa-university fa-2x"></i>
                </span>
                Cámara de Comercio
            </a>
        </li>
        <li class="{{request('show')==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_EVENTS) ? 'active in' : ''}}">
            <a href="#events" data-toggle="tab">
                <span class="block text-center">
                    <i class="fa fa-bullhorn fa-2x"></i>
                </span>
                Ferias y Eventos
            </a>
        </li>
        <li class="{{request('show')==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_PARTNERS) ? 'active in' : ''}}">
            <a href="#parners" data-toggle="tab">
                 <span class="block text-center">
                     <i class="fa fa-users fa-2x"></i>
                 </span>
                Socios
            </a>
        </li>
        <li class="pull-right" style="position: relative">
            <p style="position: absolute; top: 10px;right: 5px">
                <a class="btn btn-primary " data-toggle="modal" style="color: #FFF;width: 100px;border-radius: 5px" href="#modal-id" >Nuevo</a>
            </p>
        </li>
    </ul>
    <div class="padding-md">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="tab-pane fade {{request('show',str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_BACKGROUND))==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_BACKGROUND) ? 'active in' : ''}} "
                         id="background">
                        <div class="panel panel-default table-responsive">
                            <div class="panel-heading">
                                Fondo Principal
                            </div>
                            @include('global-partials.admin-oleb2b-images',['images'=>$images->where('section',\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_BACKGROUND)])
                        </div>
                    </div>
                    <div class="tab-pane fade {{request('show')==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_CCL) ? 'active in' : ''}}" id="cc">
                        <div class="panel panel-default table-responsive">
                            <div class="panel-heading">
                                Cámaras de Comercio
                            </div>
                            @include('global-partials.admin-oleb2b-images',['images'=>$images->where('section',\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_CCL)])
                        </div>
                    </div>
                    <div class="tab-pane fade {{request('show')==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_EVENTS) ? 'active in' : ''}}" id="events">
                        <div class="panel panel-default table-responsive">
                            <div class="panel-heading">
                                Ferias y Eventos
                            </div>
                            @include('global-partials.admin-oleb2b-images',['images'=>$images->where('section',\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_EVENTS)])
                        </div>
                    </div>
                    <div class="tab-pane fade {{request('show')==str_slug(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_PARTNERS) ? 'active in' : ''}}"
                         id="parners">
                        <div class="panel panel-default table-responsive">
                            <div class="panel-heading">
                                Socios
                            </div>
                            @include('global-partials.admin-oleb2b-images',['images'=>$images->where('section',\App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_PARTNERS)])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-id">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Subir nueva imágen</h4>
                </div>
                <form action="{{route('admin.oleb2b-images.store')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body ">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group {{ $errors->has('title') ? 'has-error' :'' }}">
                                    <label for="title" class=" control-label">Titulo*</label>
                                    <input type="text" class="form-control input-sm" id="title" name="title"
                                           value="" required>
                                    @if($errors->has('title'))
                                        <small class="text-danger">{{ $errors->first('title') }}</small>
                                    @endif
                                </div><!-- /form-group -->
                                <div class="form-group {{ $errors->has('group') ? 'has-error' :'' }}">
                                    <label for="section" class="control-label">Group</label>
                                    <select name="section" id="section" class="form-control" required>
                                        @foreach(\App\Repositories\Oleb2bImage\Oleb2bImage::SECTIONS as $section)
                                            <option value="{{$section}}">{{trans('statuses.sections.'.$section)}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('group'))
                                        <small class="text-danger">{{ $errors->first('group') }}</small>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('url') ? 'has-error' :'' }}">
                                    <label for="url" class="control-label">Url de redirección (opcional)</label>
                                    <input type="text" class="form-control input-sm" id="url" name="url"
                                           value="">
                                    @if($errors->has('url'))
                                        <small class="text-danger">{{ $errors->first('url') }}</small>
                                    @endif
                                </div><!-- /form-group -->
                                <div class="form-group {{ $errors->has('image') ? 'has-error' :'' }}">
                                    <label for="image" class="control-label">Imagen*</label>
                                    <input type="file" class="form-control input-sm" id="image" name="image"
                                           required>
                                    @if($errors->has('image'))
                                        <small class="text-danger">{{ $errors->first('image') }}</small>
                                    @endif
                                </div><!-- /form-group -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection