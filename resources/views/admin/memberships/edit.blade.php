@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit: {{$membership->name}}</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{action('Admin\MembershipsController@update',$membership)}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nombre espanol</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="es_name" value="{{ $membership->es_name }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nombre ingles</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="en_name" value="{{ $membership->en_name }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nombre ruso</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="ru_name" value="{{ $membership->ru_name }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="main_rotation_banners" class="col-md-4 control-label">Banners Principales</label>
                                <div class="col-md-6">
                                    <input id="main_rotation_banners" type="text" class="form-control" name="main_rotation_banners" value="{{ $membership->main_rotation_banners }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inner_rotation_banners" class="col-md-4 control-label">Banners Interiores</label>
                                <div class="col-md-6">
                                    <input id="inner_rotation_banners" type="text" class="form-control" name="inner_rotation_banners" value="{{ $membership->inner_rotation_banners }}" required >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="product_posting" class="col-md-4 control-label">Productos Posteados</label>
                                <div class="col-md-6">
                                    <input id="product_posting" type="text" class="form-control" name="product_posting" value="{{ $membership->product_posting }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="monthly_price" class="col-md-4 control-label">Precio Mensual (USD)</label>
                                <div class="col-md-6">
                                    <input id="monthly_price" type="text" class="form-control" name="monthly_price" value="{{ $membership->monthly_price }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="annual_price" class="col-md-4 control-label">Precio Anual (USD)</label>
                                <div class="col-md-6">
                                    <input id="annual_price" type="text" class="form-control" name="annual_price" value="{{ $membership->annual_price }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="buying_request_responses" class="col-md-4 control-label">Respuesta a Cotizaciones</label>
                                <div class="col-md-6">
                                    <input id="buying_request_responses" type="checkbox" class="form-control" name="buying_request_responses"  {{ $membership->buying_request_responses?'checked':'' }}>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="subaccounts" class="col-md-4 control-label">SubCuentas</label>
                                <div class="col-md-6">
                                    <input id="subaccounts" type="text" class="form-control" name="subaccounts" value="{{ $membership->subaccounts }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection