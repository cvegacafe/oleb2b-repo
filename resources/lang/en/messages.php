<?php
return [
    'choose_a_conversation_to_show' => 'Choose a conversation to see the messages',
    'all_messages' => 'All Messages',
    'last_messages' => 'last message',
    'attachment' => 'attachment',
    'write_message' => 'Write a message',
    'order_details' => 'Details',
    'measure' => 'Measure',
    'company' => 'Company',
    'no_messages'=>'Sin Mensajes',
    'new_message' => 'You have a new message',
    'we_have_sent_you_an_email' => '<strong>Attention!</strong> We have sent you an email to confirm your email address.',
    'send_again' => 'Send again.',
    'data_missing' => 'Improve your account by adding ',
    'profile_images_missing' => 'some photos',
    'factory_details_images_missing' => ' factory details',
    'certifications_images_missing' => ' certificates',
    'here' => 'Here',
];