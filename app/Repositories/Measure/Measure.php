<?php namespace App\Repositories\Measure;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Measure extends Model
{
    public $table = 'measurements';
    public $fillable = ['es_name', 'en_name', 'ru_name'];

    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }
}
