<?php namespace App\Repositories\User;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:13 PM
 */

use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new User();
        parent::__construct();
    }


    /**
     * @return mixed
     */
    public function premiumUsers()
    {
        return $this->model->whereHas('memberships', function ($q) {
            $q->whereIn('membership_id', [2, 3]);
            $q->where('is_active', 1);
        })->get();
    }

}