<?php namespace App\Repositories\Supplier;

use App\Library\Constants;
use App\Repositories\Certification\Certification;
use App\Repositories\CompanyType\CompanyType;
use App\Repositories\Country\Country;
use App\Repositories\FactoryDetail\FactoryDetail;
use App\Repositories\Supercategory\Supercategory;
use App\Repositories\Taxtypes\Taxtypes;
use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class Supplier
 * @package App\Repositories\Supplier
 */
class Supplier extends Model
{
    /**
     * @var string
     */
    public $table = 'suppliers';

    /**
     * @var array
     */
    public $fillable = [
        'country_id',
        'taxtype_id',
        'company_name',
        'supercategory_id',
        'description',
        'member_trade_organization',
        'tax_number',
        'company_legal_address',
        'postal_code',
        'website',
        'year_company_registered',
        'number_employees',
        'images',
        'es_company_description',
        'company_type',
        'en_company_description',
        'ru_company_description',
        'logo'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'images' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function taxtype()
    {
        return $this->belongsTo(Taxtypes::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'supplier_user', 'supplier_id', 'user_id')->withPivot('is_principal')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certifications()
    {
        return $this->hasMany(Certification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function factoryDetail()
    {
        return $this->hasOne(FactoryDetail::class);
    }

    /**
     * @return mixed
     */
    public function getCompanyDescriptionAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_company_description');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function superCategory()
    {
        return $this->belongsTo(SuperCategory::class, 'supercategory_id', 'id');
    }
    /* public function getCompanyTypeAttribute()
     {
         return $this->getAttribute(strtolower(Lang::locale()) . '_company_type');
     }*/

    /**
     * @param $value
     * @return string
     */
    public function getLogoAttribute($value)
    {
        return $value ? $value : Constants::PRODUCT_DEFAULT_IMAGE;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getOriginalLogoAttribute($value)
    {
        return $this->attributes['logo'];
    }

    public function getFactoryDetailCompleteAttribute()
    {
        $factoryDetails = $this->factoryDetail;
        return $factoryDetails->factory_address && $factoryDetails->factory_size
            && $factoryDetails->number_qc_staff && $factoryDetails->number_rd_staff
            && $factoryDetails->number_production_lines && $factoryDetails->annual_output_value
            && $factoryDetails->annual_turnover && $factoryDetails->design_service_offered;
    }
}
