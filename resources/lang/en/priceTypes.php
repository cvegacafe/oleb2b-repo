<?php
return [
  \App\Repositories\Product\Product::TYPE_PRICE_RANGE => 'Price range',
  \App\Repositories\Product\Product::TYPE_PRICE_ASK => 'Price upon request',
  \App\Repositories\Product\Product::TYPE_PRICE_FIXED => 'Fixed price',
];