<?php namespace App\Services;

use App\DTOs\SearchableDTO;
use App\Repositories\Category\Category;
use App\Repositories\Product\Product;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Subcategory\Subcategory;
use App\Repositories\Supplier\Supplier;
use App\Repositories\User\User;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;

/**
 * Class ProductService
 * @package App\Services
 */
class ProductService extends BaseService
{
    /**
     * @var LanguageService
     */
    private $languageService, $translateService;

    public function __construct(
        ProductRepository $productRepository,
        LanguageService $languageService,
        TranslateService $translateService
    ) {
        $this->mainRepo = $productRepository;
        $this->languageService = $languageService;
        $this->translateService = $translateService;
        parent::__construct();
    }


    /**
     * @param array $inputs
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $inputs)
    {
       //dd($inputs);
        $inputs['es_name'] = $this->translateService->transToSpanish($inputs['name']);
        $inputs['en_name'] = $this->translateService->transToEnglish($inputs['name']);
        $inputs['ru_name'] = $this->translateService->transToRussian($inputs['name']);

        $inputs['es_type'] = $this->translateService->transToSpanish($inputs['type']);
        $inputs['en_type'] = $this->translateService->transToEnglish($inputs['type']);
        $inputs['ru_type'] = $this->translateService->transToRussian($inputs['type']);

        $inputs['es_color'] = $this->translateService->transToSpanish($inputs['color']);
        $inputs['en_color'] = $this->translateService->transToEnglish($inputs['color']);
        $inputs['ru_color'] = $this->translateService->transToRussian($inputs['color']);

        $inputs['es_description'] = $this->translateService->transToSpanish($inputs['description']);
        $inputs['en_description'] = $this->translateService->transToEnglish($inputs['description']);
        $inputs['ru_description'] = $this->translateService->transToRussian($inputs['description']);

        $inputs['es_slug'] = str_slug($inputs['es_name'] . '-' . time() . '-' . uniqid());
        $inputs['en_slug'] = str_slug($inputs['en_name'] . '-' . time() . '-' . uniqid());
        $inputs['ru_slug'] = str_slug($inputs['ru_name'] . '-' . time() . '-' . uniqid());
        if (empty($inputs['tags'])) {
            $tags = [];
        } else {
            $tags = explode(',', $inputs['tags']);
        }

        $product = $this->mainRepo->create($inputs);
        $tagService = new TagService();

        foreach ($tags as $tag) {
            $tags = $tagService->findOrCreate($tag);
            $product->tags()->attach($tags->id);

        }

        return $product;

    }

    public function getAllProducts()
    {
        return $this->mainRepo->all()->where('status', '=', 'active');
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getByUserId($userId)
    {
        return $this->mainRepo->getByColumn('user_id', $userId)->sortByDesc('created_at');
    }

    /**
     * @param $id
     * @param array $inputs
     * @return mixed
     */
    public function updateWithOutTranslation($id, array $inputs)
    {
        $inputs['status'] = Product::STATUS_CHECKED;
        return $this->mainRepo->update($id, $inputs);
    }

    /**
     * @param $id
     * @param array $inputs
     * @param bool $onlyCurrentLang
     * @return mixed
     */
    public function update($id, array $inputs, $onlyCurrentLang = false)
    {
        if ($onlyCurrentLang) {

            $lang = Lang::locale();

            $inputs[$lang . '_name'] = $this->translateService->translateWord($lang, $inputs['name']);
            $inputs[$lang . '_type'] = $this->translateService->translateWord($lang, $inputs['type']);
            $inputs[$lang . '_color'] = $this->translateService->translateWord($lang, $inputs['color']);
            $inputs[$lang . '_description'] = $this->translateService->translateWord($lang, $inputs['description']);

        } else {

            $inputs['es_name'] = $this->translateService->transToSpanish($inputs['name']);
            $inputs['en_name'] = $this->translateService->transToEnglish($inputs['name']);
            $inputs['ru_name'] = $this->translateService->transToRussian($inputs['name']);

            $inputs['es_type'] = $this->translateService->transToSpanish($inputs['type']);
            $inputs['en_type'] = $this->translateService->transToEnglish($inputs['type']);
            $inputs['ru_type'] = $this->translateService->transToRussian($inputs['type']);

            $inputs['es_color'] = $this->translateService->transToSpanish($inputs['color']);
            $inputs['en_color'] = $this->translateService->transToEnglish($inputs['color']);
            $inputs['ru_color'] = $this->translateService->transToRussian($inputs['color']);

            $inputs['es_description'] = $this->translateService->transToSpanish($inputs['description']);
            $inputs['en_description'] = $this->translateService->transToEnglish($inputs['description']);
            $inputs['ru_description'] = $this->translateService->transToRussian($inputs['description']);
        }
        if ($inputs['price_type'] === \App\Repositories\Product\Product::TYPE_PRICE_RANGE) {
//          $inputs['price_range'] =
        }
        $tags = explode(',', $inputs['tags']);
        $inputs['status'] = Product::STATUS_EDITED;
        $updated = $this->mainRepo->update($id, $inputs);
        $tagService = new TagService();

        if ($updated) {
            $product = $this->mainRepo->find($id);
            $product->tags()->detach();
            foreach ($tags as $tag) {
                $tags = $tagService->findOrCreate($tag);
                $product->tags()->attach($tags->id);
            }
        }

        return $updated;
    }

    /**
     * @param array $productsIds
     * @param User $newUser
     * @return mixed
     */
    public function transferProductsToUser(array $productsIds, User $newUser)
    {
        return $this->mainRepo->updateMultipleRowsByColumn('id', $productsIds, ['user_id' => $newUser->id]);
    }

    /**
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getProductsHasThreads($userId)
    {
        return $this->mainRepo->getProductsByUserThreads($userId);
    }

    /**
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function featuredProducts($limit = 15)
    {
        return $this->mainRepo->getRandomProducts($limit);
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->mainRepo->findBySlugs($slug);
    }

    /**
     * @param Supplier $supplier
     * @param int $limit
     * @param int $exceptProductId
     * @return mixed
     */
    public function getProductsBySupplier(Supplier $supplier, $limit = 6, $exceptProductId = 0)
    {
        $products = collect([]);

        foreach ($supplier->users as $user) {
            foreach ($user->products->load('currency', 'moqMeasure', 'saleUnitMeasure')->where('is_approved',
                true) as $product) {

                if ($product->id != $exceptProductId) {
                    $products->push($product);
                }
            }
        }

        return $products->take($limit);
    }

    /**
     * @param Subcategory $subcategory
     * @param int $limit
     * @param int $exceptProductId
     * @return mixed
     */
    public function getRelatedProductsBySubcategoryId(Subcategory $subcategory, $limit = 10, $exceptProductId = 0)
    {
        return $subcategory->products->where('is_approved', true)
            ->load('currency', 'moqMeasure', 'saleUnitMeasure')
            ->take($limit)->filter(function ($value, $key) use ($exceptProductId) {
                return $value->id != $exceptProductId;
            });
    }

    /**
     * @param Subcategory $subcategory
     * @param int $limit
     * @param int $exceptProductId
     * @return mixed
     */
    public function getRelatedProducts(Subcategory $subcategory, $limit = 10, $exceptProductId = 0)
    {
        $relatedProducts = collect();

        $filteredSub = $subcategory->products->where('is_approved', true)
            ->load('currency', 'moqMeasure', 'saleUnitMeasure')
            ->take($limit)->filter(function ($value, $key) use ($exceptProductId) {
                return $value->id != $exceptProductId;
            });

        $relatedProducts = $relatedProducts->merge($filteredSub);


        $filteredCat = $subcategory->category->products->where('is_approved', true)
            ->load('currency', 'moqMeasure', 'saleUnitMeasure')
            ->take($limit)->filter(function ($value, $key) use ($exceptProductId) {
                return $value->id != $exceptProductId;
            });

        $relatedProducts = $relatedProducts->merge($filteredCat);

        return $relatedProducts->take($limit);
//        return $subcategory->category->supercategory->products->where('is_approved', true)
//            ->load('currency', 'moqMeasure', 'saleUnitMeasure')
//            ->take($limit)->filter(function ($value, $key) use ($exceptProductId) {
//                return $value->id != $exceptProductId;
//            });
    }

    /**
     * @param $q
     * @param $countries
     * @param $categoryIds
     * @param int $elementsByPage
     * @return SearchableDTO
     */
    public function searchApprovedPaginate(
        $q,
        $countries,
        $categoryIds,
        $elementsByPage = 10
    ): SearchableDTO {
        $categoryIds = $this->parseCategories($categoryIds);
        $countriesArray = $this->parseCountries($countries);
        return $this->mainRepo->searchApprovedPaginate(Lang::locale(), $elementsByPage, $q, $countriesArray,
            $categoryIds);
    }

    /**
     * @param $countries
     * @return array
     */
    private function parseCountries($countries)
    {
        if ($countries && $countries != trans('common.all')) {
            return array_filter(explode('-', $countries));
        }
        return [];
    }

    /**
     * @param $categoryIds
     * @return array
     */
    private function parseCategories($categoryIds)
    {
        if (!is_null($categoryIds) || !empty($categoryIds)) {
            return array_filter(explode('-', $categoryIds));
        }
        return [];
    }

    /**
     * @param $q
     * @param $subcategoryId
     * @param $countryId
     * @param $categoryId
     * @param $premiumUsers
     * @return \Illuminate\Database\Query\Builder|static
     */
    public function searchApprovedPaginateFeatured(
        $q,
        $subcategoryId,
        $countryId,
        $categoryId,
        $premiumUsers
    ) {
        if ($premiumUsers) {
            $premiumUsers = $premiumUsers->pluck('id');
        } else {
            $premiumUsers = collect()->toArray();
        }
        return $this->mainRepo->searchApprovedPaginateFeatured(Lang::locale(), $q, $subcategoryId, $countryId,
            $categoryId, $premiumUsers);
    }


}