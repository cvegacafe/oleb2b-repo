<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email',255)->unique();
            $table->string('password',128);
            $table->string('names',80)->nullable();
            $table->string('last_names',80)->nullable();
            $table->string('email_confirmacion', 100)->nullable()->default(null);
            $table->tinyInteger('status');
            $table->string('office_phone',25)->nullable();
            $table->string('mobile_phone',25)->nullable();
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
            $table->string('address',200)->nullable();
            $table->string('company')->nullable();
            $table->string('city',60)->nullable();
            $table->string('postal_code',20)->nullable();
            $table->string('other_languages',100)->nullable();
            $table->string('image')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
