<?php

use App\Repositories\Currency\Currency;
use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create(['id'=>1,'currency'=>'Dolar','currency_code'=>'USD','enabled'=>true]);
        Currency::create(['id'=>2,'currency'=>'Euro','currency_code'=>'EUR','enabled'=>true]);
        Currency::create(['id'=>3,'currency'=>'Rublo Ruso','currency_code'=>'RUB','enabled'=>true]);
        Currency::create(['id'=>4,'currency'=>'Yuan Chino','currency_code'=>'CNY','enabled'=>true]);
    }
}
