<?php

use App\Repositories\Membership\Membership;
use Illuminate\Database\Seeder;

class MembershipsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Membership::create([
            'id'=>1,
            'es_name'=>'Básica',
            'en_name'=>'Basic',
            'ru_name'=>'Бесплатный',
            'main_rotation_banners'=>0,
            'inner_rotation_banners'=>0,
            'product_posting'=>3,
            'monthly_price'=>0,
            'annual_price'=>0,
            'buying_request_responses'=>false,
            'subaccounts'=>0
        ]);
        Membership::create([
            'id'=>2,
            'es_name'=>'Avanzada',
            'en_name'=>'Advanced',
            'ru_name'=>'Профессионал',
            'main_rotation_banners'=>60,
            'inner_rotation_banners'=>30,
            'product_posting'=>99999999,
            'monthly_price'=>20,
            'annual_price'=>200,
            'buying_request_responses'=>true,
            'subaccounts'=>0
        ]);
        Membership::create([
            'id'=>3,
            'es_name'=>'Premium',
            'en_name'=>'Premium',
            'ru_name'=>'Премиум',
            'main_rotation_banners'=>120,
            'inner_rotation_banners'=>60,
            'product_posting'=>999999999,
            'monthly_price'=>40,
            'annual_price'=>400,
            'buying_request_responses'=>true,
            'subaccounts'=>5
        ]);
    }
}
