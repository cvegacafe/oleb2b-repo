<?php namespace App\Repositories\SavedBuyingRequest;

use App\Repositories\BaseRepository;

class SavedBuyingRequestRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new SavedBuyingRequest();
        parent::__construct();
    }

    public function findByBuyingRequestIdUserId($buyingRequestId,$userId)
    {
        return $this->model->where('buyingRequest_id',$buyingRequestId)->where('user_id',$userId)->first();
    }
}