<?php

return [
    'company_info' => 'Company info',
    'products' => 'Products',
    'certificates' => 'Certificates',
    'buying_requests' => 'Buying requests',
    'company_photos' => 'Company Photos',
    'advanced' => 'Advanced',
    'update' => 'Update',
    'submit' => 'Submit',
    'add_products' => 'Add Products',
    'add_photos' => 'Add Photos',
    'upgrade' => 'Upgrade',
    'membership' => 'Membership'
];

