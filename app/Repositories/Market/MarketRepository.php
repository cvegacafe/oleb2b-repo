<?php namespace App\Repositories\Market;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class MarketRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Market();
        parent::__construct();
    }

    public function getMarketsByLang($lang)
    {
        return DB::table('markets')
            ->select('id',$lang.'_name as name')
            ->get();
    }
}