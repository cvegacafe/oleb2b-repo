<?php namespace App\Repositories\FactoryDetail;

use App\Repositories\Measure\Measure;
use Illuminate\Database\Eloquent\Model;

class FactoryDetail extends Model
{
    public $table = 'factorydetails';

    public $fillable = [
        'supplier_id',
        'factory_address',
        'factory_size',
        'number_qc_staff',
        'number_rd_staff',
        'number_production_lines',
        'annual_output_value',
        'annual_turnover',
        'design_service_offered',
        'oem_service_offered'
    ];

    public function setOemServiceOfferedAttribute($value)
    {
        return isset($value) && $value == 'on' ? true : false;
    }

    public function setDesignServiceOfferedAttribute($value)
    {
        return isset($value) && $value == 'on' ? true : false;

    }

   /* public function measure()
    {
        return $this->belongsTo(Measure::class);
    }*/
}
