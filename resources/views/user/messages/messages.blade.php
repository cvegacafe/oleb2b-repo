@extends('layouts.app')
@section('css')
    <style>
        .adjunto {
            position: relative;
        }

        .input-mensaje .attachment-info {
            display: none;
            color: blue;
            font-size: 15px;
            position: absolute;
            left: 13%;
            bottom: 5px;
        }

        .input-mensaje .attachment-info .cerrar {
            cursor: pointer;
        }

        .input-mensaje .active {
            background: #FF6A31;
            color: white !important;
        }

        .thread-active {
            border-right: 5px solid #FF6A31;
        }

        .msg_owner {
            margin-left: 5px;
        }

        .new-messages-pin {
            display: inline-block;
            min-width: 4px;
            padding: 4px;
            font-size: 12px;
            font-weight: bold;
            line-height: 1;
            vertical-align: middle;
            white-space: nowrap;
            text-align: center;
            background-color: #FF6A31;
            border-radius: 100%;
        }
    </style>
@endsection
@section('content')
    <section id="mensajes">
        <div class="container-fluid h100">
            <div class="row h100">
                <div class="col-xs-12 col-md-5 col-lg-4 h100">
                    <div class="productos-mensajes">
                        <div class="header">
                            @if($type == 'products')
                                <h3>{{trans('user.my_products')}}</h3>
                            @else
                                <h3>{{trans('portal.buying_request')}}</h3>
                            @endif
                        </div>
                        <div class="body">
                            @forelse($topics as $topic)
                                <div class="inbox" id="product-{{$topic->id}}">
                                    <div class="header">
                                        <h3><i class="fa fa-caret-left"></i> {{trans('messages.all_messages')}}</h3>
                                    </div>
                                    <div class="container-messagges">
                                        @foreach($topic->threads->sortBy('pivot.created_at') as $thread)
                                            @if($thread->from_user_id == \Auth::id() || $thread->to_user_id == \Auth::id() )
                                                @if ($type == 'products')
                                                    <div data-thread-id="{{$thread->id}}"
                                                         @if($thread->product->user_id === auth()->id())
                                                             data-company="{{$thread->buyer->company}}"
                                                             data-clocation="{{$thread->buyer->country->name}}"
                                                            data-principal_business="{{$thread->buyer->supplier ? $thread->buyer->supplier->superCategory->name : '----'}}"
                                                             data-company_type="{{$thread->buyer->supplier ? $thread->buyer->supplier->companyType->name : '----'}}"
                                                             data-forma-de-pago="{{$thread->pivot_payment_terms ? $thread->pivot_payment_terms->implode('name',', '):''}}"
                                                             class="single-message me"
                                                             data-user-interlocutor-type="Comprador"
                                                             data-user-interlocutor-name="{{$thread->fromUser->names.' '.$thread->fromUser->last_names}}"
                                                         @else
                                                             data-company="{{$thread->supplier->supplier->company_name}}"
                                                             class="single-message"
                                                             data-company_type="{{$thread->supplier->supplier->companyType->name}}"
                                                             data-clocation="{{$thread->supplier->country->name}}"
                                                             data-user-interlocutor-type="Vendedor"
                                                             data-user-interlocutor-name="{{$thread->toUser->names.' '.$thread->toUser->last_names}}"
                                                        @endif>
                                                        @else
                                                            <div class="single-message"
                                                                 data-thread-id="{{$thread->id}}"
                                                                 data-location="{{$thread->supplier->counrty->name}}"
                                                                 data-last-message-date=" {{$thread->messages->last()->created_at}}"
                                                                 data-last-viewd="{{$thread->messages->last()->read_at == null ? 0 : 1 }}">
                                                                @endif
                                                                @if(\Auth::id() == $thread->fromUser->id)
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <figure>
                                                                                <img src="{{asset($thread->toUser->image)}}">
                                                                            </figure>
                                                                        </div>
                                                                        <div class="col-xs-8 info">
                                                                            <h3>{{$thread->toUser->names.' '.$thread->toUser->last_names}}</h3>
                                                                            <small>
                                                                                <strong>
                                                                                    @if($thread->toUser->isSupplier())
                                                                                        @if($thread->toUser->supplier)
                                                                                            {{$thread->toUser->supplier->company_name}}
                                                                                        @endif
                                                                                    @else
                                                                                        {{$thread->toUser->company}}
                                                                                    @endif
                                                                                </strong>
                                                                            </small>

                                                                            @if($thread->newMessages(Auth::id()) > 0)
                                                                                <i class="new-messages-pin"></i>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <figure>
                                                                                <img src="{{asset($thread->fromUser->image)}}">
                                                                            </figure>
                                                                        </div>
                                                                        <div class="col-xs-8 info">
                                                                            <h3>{{$thread->fromUser->names.' '.$thread->fromUser->last_names}}</h3>
                                                                            <small>
                                                                                <strong>
                                                                                    @if($thread->fromUser->isSupplier())
                                                                                        @if($thread->fromUser->supplier)
                                                                                            {{$thread->fromUser->supplier->company_name}}
                                                                                        @endif
                                                                                    @else
                                                                                        {{$thread->fromUser->company}}
                                                                                    @endif
                                                                                </strong>
                                                                            </small>
                                                                            @if($thread->newMessages(Auth::id()) > 0)
                                                                                <i class="new-messages-pin"></i>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        @endif
                                                        @endforeach
                                                    </div>
                                    </div>
                                    <div class="producto" data-product-id="{{$topic->id}}">
                                        <div class="row">
                                            <div class="col-xs-5">
                                                <figure>
                                                    <img src="{{asset($topic->mainPhoto)}}">
                                                </figure>
                                            </div>
                                            <div class="col-xs-7">
                                                <h3>{{$topic->name}}</h3>
                                                @if($topic->newMessages(Auth::id()) > 0)
                                                    <i class="new-messages-pin"></i>
                                                @endif
                                                {{--<ul>
                                                    <li><strong>{{trans('messages.last_messages')}}:</strong> {{$topic->threads->last()->created_at->format('d/m/Y H:i A')}}</li>
                                                    <li></li>
                                                </ul>--}}
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                        <div class="text-center" style="padding: 0 10px;">
                                            <h4>{{trans('common.without_messages')}}</h4>
                                        </div>
                                    @endforelse
                                </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-7 col-lg-8 h100 chat-container">
                        <div class="chat">
                            <div class="contact-info">
                                <div class="detalles">
                                    @if($type == 'products')
                                        <ul>
                                            <li><h3>Datos del <span class="interlocutor"></span></h3></li>
                                            <li>
                                                <strong>Nombre:</strong>
                                                <span id="nombre_comprador"></span></li>
                                            <li>
                                                <strong>{{trans('messages.company')}}:</strong>
                                                <span id="company_name"></span>
                                            </li>
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.location')}}:</strong>
                                                <span id="clocation"></span>
                                            </li>
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.company_type')}}:</strong>
                                                <span id="company_type"></span>
                                            </li>
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.main_business')}}:</strong>
                                                <span id="principal_business"></span>
                                            </li>
                                        </ul>
                                    @endif
                                </div>
                                <div class="hideshow"><i class="fa fa-chevron-down"></i></div>
                            </div>
                            <div class="mensajes">
                                <p style="text-align: center;padding: 30px">{{trans('messages.choose_a_conversation_to_show')}}</p>
                            </div>
                            <div class="input-mensaje">
                                <span class="attachment-info"><span
                                            id="filename">{{trans('messages.attachment')}}</span> <i
                                            class="fa fa-times cerrar" title="cerrar"></i></span>
                                <input type="file" name="attachment" id="file-message-attachment" style="display: none"
                                       disabled>
                                <div class="adjunto" title="{{trans('messages.attachment')}}">
                                    <i class="fa fa-paperclip"></i>&nbsp;
                                </div>
                                <div class="box">
                                    <textarea name="" id="input-message" cols="30" rows="10"
                                              placeholder="{{trans('messages.write_message')}} ... "></textarea>
                                </div>
                                <div class="enviar">
                                    <div class="send-message-mobile"><i class="fa fa-arrow-right"></i></div>
                                    <button class="boton-naranja send-message"
                                            id="send-button">{{trans('common.forms.buttons.send')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script id="chat_message_template" type="text/template">
        <div class="mensaje-unico">
            <figure class="logo-empresa">
                <img src="{{asset('assets/pages/img/user_default.png')}}" alt="Logo empresa">
            </figure>
            <div class="texto">
                <small class="msg_owner"></small>
                <a href="#" class="attachment pull-left"><i class="fa fa-paperclip"></i> &nbsp; </a>
                <span id="words">aa Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores
                        dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos,
                        repudiandae? Ea id ipsam itaque rerum soluta.</span>
            </div>
        </div>
    </script>
    <script>
      // Enable pusher logging - don't include this in production
      Pusher.logToConsole = true;

      var _channel = null;

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': '{{csrf_token()}}'
        }
      });

      function init() {
        // send button click handling
        $('.send-message').click(sendMessage);
        $('.send-message-mobile').click(sendMessage);
        $('#input-message').keypress(checkSend);
      }

      // Send on enter/return key
      function checkSend(e) {
        if (e.keyCode === 13) {
          return sendMessage();
        }
      }

      // Handle the send button being clicked
      function sendMessage() {
        var messageText = $('#input-message').val();
        var file = $('#file-message-attachment'); //Files[0] = 1st file
        console.log('text:' + messageText, file.val());
        if (messageText !== '' || file.val()) {

          // Build POST data and make AJAX request
          enableInputArea(false);

          var formData = new FormData();
          formData.append('chat_text', messageText);
          formData.append('file', file[0].files[0]);
          formData.append('channel', _channel);
          formData.append('own_image', '{{\Auth::user()->image}}');

          var request = $.ajax({
            data: formData,
            url: '{{route('api.messages.send')}}',
            method: 'post',
            cache: false,
            contentType: false,
            processData: false
          });

          request.done(function () {
            sendMessageSuccess();
            file.val('');
            $('#filename').hide();
          });
          // Ensure the normal browser event doesn't take place
          return false;
        }
        sendMessageSuccess();
        return false;
      }

      // Handle the success callback
      function sendMessageSuccess() {
        enableInputArea(true);
        console.log('message sent successfully');
      }

      function enableInputArea(enable) {

        var input = $('#input-message');
        var inputFile = $('#file-message-attachment');
        if (enable) {
          input.val('');
          input.attr("disabled", false);
          inputFile.attr('disabled', false);
          $('#send-button').attr('disabled', false);
          input.focus();

        } else {
          input.attr("disabled", "disabled");
          inputFile.attr('disabled', 'disabled');
          $('#send-button').attr('disabled', 'disabled');
        }
      }

      // Build the UI for a new message and add to the DOM

      function addMessage(data) {
        // console.log('tipo' , typeof data)
        if (typeof data === "object") {
          var text;

          text = $('#chat_message_template').text();

          // Create element from template and set values
          var el = $(text);

          el.attr('id', data.user_id);

          if (data.user_id == '{{\Auth::id()}}') {
            el.addClass('supplier');
            el.find('.msg_owner').text('Yo');
          } else {
            el.addClass('buyer');
            el.find('.msg_owner').text(data.username);

          }

          el.find('.texto #words').html(data.content);
          el.find('.author').text(data.username);
          el.attr('title', data.created_at);
          if (data.attachment_url) {
            el.find('.texto .attachment').attr('href', data.attachment_url);
            el.find('.texto .attachment').append(data.attachment_name);
          } else {
            el.find('.texto .attachment').remove();
          }

          if (data.images) {
            var images = JSON.parse(data.images);
            el.find('.logo-empresa img').attr('src', images[0]);
          }
          if (data.image) {
            el.find('.logo-empresa img').attr('src', data.image);
          }
          // Utility to build nicely formatted time
          el.find('.timestamp').text(data.created_at);

          var messages = $('.chat .mensajes');
          messages.append(el);

          // Make sure the incoming message is shown
          messages.scrollTop(messages[0].scrollHeight);
        }
      }

      // Build the UI for members
      function addMember(id, username) {
        //console.log(username);
        // Create element from template and set values
        var el = $('#member').text();
        el = $(el);
        el.find('.member').text(username);
        el.attr('id', 'member' + id);
        var members = $('#members');
        members.append(el)
      }

      function rmMember(id) {
        $('#member' + id).remove();
      }

      $(init);
      /***********************************************/
      var pusher = new Pusher('{{env('PUSHER_KEY')}}', {
        encrypted: true,
        // authEndpoint: '{{route('api.messages.auth')}}',//route('chat.auth')
        auth: {
          headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}'
          }
        }
      });

      function showMessages(thread_id, product_id, product_data) {

        $('#product-' + product_id).addClass('active');

        if (thread_id) {
          $('.thread-active').removeClass('thread-active');

          $("div[data-thread-id='" + thread_id + "']").addClass('thread-active');

          if (product_data) {

            var contact_info = $('.contact-info');
            $('.chat-container').addClass('active');
            $('.chat .contact-info').show(function () {
              contact_info.addClass('show');
            });

            /* console.log(product_data);*/
            contact_info.find('#pais_entrega').text(product_data.pais_entrega);
            contact_info.find('#clocation').text(product_data.clocation);
            console.log('location', product_data);
            contact_info.find('#principal_business').text(product_data.principal_business);
            contact_info.find('#unidad').text(product_data.unidad);
            contact_info.find('#nombre_comprador').text(product_data.nombre_comprador);
            contact_info.find('.interlocutor').text(product_data.interlocutor_type);
            contact_info.find('#company_type').text(product_data.company_type);
            contact_info.find('#company_name').text(product_data.company);

            // setTimeout(function(){
            //     $('.chat .contact-info').removeClass('show');
            // }, 2000);
          }

          enableInputArea(true);
          $('.chat .mensajes p').empty();
          $('.mensajes .mensaje-unico').remove();
          $('#input-message').focus();

          var threadId = thread_id;

          var request = $.ajax({
            url: '{{route('api.messages.last')}}',
            method: 'GET',
            dataType: "json",
            data: {thread_id: threadId}
          });

          var _new_channel = 'channel-thread-' + threadId;

          if (_new_channel != _channel) {

            _channel = _new_channel;

            var channel = pusher.subscribe(_channel);
            console.log('suscrito a ' + _channel);
            channel.bind('new-message', addMessage);

          }

          request.done(function (response) {
            if (response.length === 0) {
              $('.chat .mensajes p').text('{{trans('messages.no_messages')}}');
            } else {

              $.each(response.messages, function (i, item) {
                console.log('message: ', item);
                addMessage(item)
              });
            }
          });

          request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
          });

        }
      }

      function hideMessages() {
        $('.input-mensaje').hide();
        $('.chat .mensajes p').text('{{trans('messages.choose_a_conversation_to_show')}}');
        $('.inbox').removeClass('active');
        $('.chat-container').removeClass('active');
        $('.chat .contact-info').hide().removeClass('show');
        $('.mensajes .mensaje-unico').remove();
        $('#file-message-attachment').attr('disabled', true);
        enableInputArea(false);
      }

      $(function () {

        enableInputArea(false);

        $('.chat .contact-info .hideshow').on('click', function () {
          $(this).parent().toggleClass('show');
        });

        $('.productos-mensajes .header').on('click', function () {
          hideMessages();
        });

        $('.producto').on('click', function () {
          var productId = $(this).data('product-id');
          showMessages(null, productId);
        });

        $('.input-mensaje .attachment-info .cerrar').on('click', function () {
          $("#file-message-attachment").val("");
          $('.attachment-info').hide();
          $('.input-mensaje .adjunto').removeClass('active');
        });

        $('.input-mensaje .adjunto').on('click', function () {

          $('#file-message-attachment').trigger('click');

        });

        $('#file-message-attachment').on('change', function () {
          if ($(this).get(0).files.length != 0) {
            var maxSize = 2000;
            console.log($(this).get(0).files[0].size, (1024 * maxSize));
            if ($(this).get(0).files[0].size > (1024 * maxSize)) {
              $("#file-message-attachment").val("");

              $('.attachment-info').hide();

              $('.input-mensaje .adjunto').removeClass('active');
              alert('Archivo muy grande');
              return;
            }
            var fileName = $(this).val().split('/').pop().split('\\').pop();
            $('#filename').show();
            $('.attachment-info #filename').text(fileName);
            $('.attachment-info').show();
            $('.input-mensaje .adjunto').addClass('active');
          } else {
            $('.attachment-info').hide();
            $('#filename').hide();
            $('.attachment-info #filename').text(' ');
            $('.input-mensaje .adjunto').removeClass('active');
          }
        });

        $('.single-message').on('click', function () {
          const single_message = $(this);
          const product_data = {
            @if($type == 'products')
                'pais_entrega': single_message.data('pais-entrega'),
                'clocation': single_message.data('clocation'),
                'principal_business': single_message.data('principal_business'),
                'company_type': single_message.data('company_type'),
                'forma_pago': single_message.data('forma-de-pago'),
                'nombre_comprador': single_message.data('user-interlocutor-name'),
                'interlocutor_type': single_message.data('user-interlocutor-type'),
                'company': single_message.data('company')
            @endif
          };
          /* console.log(product_data);*/
          showMessages($(this).data('thread-id'), null, product_data);
        });
          @if($type == 'products')
          if (getUrlParameter('product') && getUrlParameter('thread')) {
            var thread_id = getUrlParameter('thread');
            var product_id = getUrlParameter('product');
            showMessages(thread_id, product_id);
          }
          @else
          if (getUrlParameter('buying-request') && getUrlParameter('thread')) {
            var thread_id = getUrlParameter('thread');
            var product_id = getUrlParameter('buying-request');
            showMessages(thread_id, product_id);
          }
          @endif
      });

    </script>
@endsection