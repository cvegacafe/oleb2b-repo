<?php namespace App\Services;

use App\Repositories\Country\CountryRepository;
use Illuminate\Support\Facades\Lang;

/**
 * Class CountryService
 * @package App\Services
 */
class CountryService extends BaseService
{

    /**
     * CountryService constructor.
     * @param CountryRepository $countryRepository
     * @throws \Exception
     */
    public function __construct(CountryRepository $countryRepository)
    {
        $this->mainRepo = $countryRepository;
        parent::__construct();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAllEnableForSuppliers()
    {
        return $this->all()->where('enabled_for_supplier', true);
    }

    /**
     * @param $lang
     * @return mixed
     */
    public function getGroupedCountries($lang)
    {
        $countries = $this->mainRepo->getCountriesFluent($lang);
        return $countries->groupBy('enabled_for_supplier');
    }

    /**
     * @deprecated
     * @param $q
     * @param $subcategoryId
     * @param $countryId
     * @return mixed
     */
    public function getCountriesBySearch($q, $subcategoryId, $countryId)
    {
        return $this->mainRepo->getCountriesBySearch(Lang::locale(), $q, $subcategoryId, $countryId);
    }
}