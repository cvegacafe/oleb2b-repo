<?php namespace App\Services;

use App\Jobs\SendContactFormEmail;
use App\Notifications\NewBuyingRequestForSuppliers;
use App\Repositories\BuyingRequest\BuyingRequest;
use App\Repositories\Supercategory\Supercategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;

class EmailService
{
    private $responseMessage = null;

    /**
     * @return null
     */
    public function getMessage()
    {
        $msg = $this->responseMessage;
        $this->responseMessage = null;
        return $msg;
    }

    /**
     * @param $token
     * @param $usuario
     * @return mixed
     */
    public function validate($token, $usuario)
    {
        try {
            /*Solo se puede validar si el usario esta en estado 'email sin confirmar'. si esta baneado no puede cambiar a activo*/

            if ($usuario->status_id == 4 && $usuario->email_confirmacion == $token) {
                $usuario->status_id = 2;//Pasa a estar activo
                $usuario->save();
                return true;
            }

        } catch (ModelNotFoundException $e) {

            Log::error($e);

            $this->responseMessage = 'No se ha podido encontrar el correo electrónico';

        } catch (\Exception $e) {

            Log::error($e);

            $this->responseMessage = 'Ha ocurrido un error en el proceso, por favor comuniquese con nosotros para solucionar este error.';
        }

        $this->responseMessage = 'Su c&oacute;digo de verificaci&oacute;n no coincide. No se pudo validar su correo.';
        return false;
    }

    /**
     * Genera la validacion por correo, asignando un token al campo email_confirmation y enviandolo al correo del usuario para luego compararlos
     * @param $user
     * @return bool
     */
    public function generateValidation($user)
    {
        try {
            $token = str_replace('/', '.', bcrypt(uniqid('ole-user_' . $user->created_at)));
            $user->email_confirmacion = $token;

            if ($user->save()) {
                return $token;
            }

        } catch (\Exception $e) {
            Log::error($e);
        }

        return null;
    }

    /**
     * @param $name
     * @param $email
     * @param $subject
     * @param $message
     */
    public function sendContactForm($name, $email, $subject, $message)
    {
        dispatch(new SendContactFormEmail($name, $email, $subject, $message));
    }

    /**
     * @param $user
     * @return bool
     */
    public function sendEmailValidation($user)
    {
        try {

            $token = $this->generateValidation($user);
            $data['msg'] = trans('mail.validation.msg');
            $data['btn-title'] = trans('mail.validation.button');
            $data['btn-url'] = route('email-confirm', [$token, $user->email]);
            $data['temp-pwd'] = $user->provisional_password;
            $receiverName = $user->names . ' ' . $user->last_names;

            $this->sendMailFromTeam('emails.validation', $data, $user->email, $receiverName,
                trans('mail.validation.subject'));

        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }

        return true;
    }

    /**
     * @param Supercategory $supercategory
     * @param BuyingRequest $buyingRequest
     */
    public function sendMailToSupplierInSuperCategory(Supercategory $supercategory, BuyingRequest $buyingRequest)
    {
        foreach ($supercategory->suppliers as $supplier) {

            foreach ($supplier->users as $user) {
                $user->notify(new NewBuyingRequestForSuppliers($supercategory, $buyingRequest,
                    $user->languages->first()->abbreviation, $user));
            }
        }
    }

}
