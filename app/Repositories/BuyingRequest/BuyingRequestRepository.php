<?php namespace App\Repositories\BuyingRequest;

use App\Repositories\BaseRepository;

class BuyingRequestRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new BuyingRequest();
        parent::__construct();
    }

    public function getBuyingRequestByUser($userId)
    {
        return $this->model->where('user_id',$userId)->orderBy('created_at','DESC')->get();
    }

    public function all()
    {
        return $this->model->with('measure', 'subcategory', 'country')->get();
    }

    public function allApprovedPaginate($ElementsByPage, $q, $lang_prefix)
    {
        return $this->model->with('measure', 'user.suppliers', 'user.suppliers.country')
            ->where('is_approved', true)
            ->where(function ($query) use ($q, $lang_prefix) {
                $query->where($lang_prefix . '_name', 'like', '%' . $q . '%');
                $query->orWhere($lang_prefix . '_description', 'like', '%' . $q . '%');
            })
            ->orderBy('created_at','DESC')
            ->paginate($ElementsByPage);
    }

    public function getBuyingRequestByUserThreads($userId)
    {
        return $this->model->with('threads','threads.messages')->whereHas('threads', function ($query) use ($userId) {
            $query->where('from_user_id', $userId);
            $query->orWhere('to_user_id', $userId);
        })->get();
    }
}