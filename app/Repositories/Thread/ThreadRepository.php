<?php namespace App\Repositories\Thread;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class ThreadRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Thread();
        parent::__construct();
    }

    public function getByUser($userId)
    {
        return $this->model->with('products')->where('from_user_id', $userId)->orWhere('to_user_id', $userId)->get();
    }

    public function getThreadsWithProductsByBuyerUser($userId)
    {
        return DB::table('threads as t')->join('thread_product as tp','tp.thread_id','=','t.id')
            ->where('t.from_user_id',$userId)->get();
    }

    public function getThreadsWithBuyingRequestByBuyerUser($userId)
    {
        return DB::table('threads as t')->join('buyingRequest_thread as brt','brt.thread_id','=','t.id')
            ->where('t.from_user_id',$userId)->get();
    }

    public function getProductsWithThreads($userId,$lang)
    {

        return DB::table('products as p')
            ->join('thread_product as tp','tp.product_id','=','p.id')
            ->join('threads as t','t.id','=','tp.thread_id')
            ->leftJoin('messages', 'messages.thread_id', '=', 't.id')
            ->where('t.from_user_id',$userId)
            ->orWhere('t.to_user_id',$userId)
            ->select('p.id','p.'.$lang.'_name as name')
            ->get();
    }
}