<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link rel="icon" type="image/png" href="{{asset('assets/pages/img/favicon.png')}}">
    <link href="https://file.myfontastic.com/cSj93aaG3VLYBUDQWmiU5k/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/packages/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/bootstrap-fileinput/css/fileinput.min.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{asset('assets/user/css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/packages/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}">
@yield('css')
<!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'lang' => \Illuminate\Support\Facades\Lang::Locale()
        ]); ?>
    </script>
    <!-- Hotjar Tracking Code for http://oleb2b.com -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 500366, hjsv: 5};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>
<body>
<div id="app" class="sidebar-container">
    <aside>
        <div class="aside-container">
            <span class="close-aside toggle-aside"><i class="fa fa-close"></i></span>
            <h1><i class="icon-icon-menu-bold"></i><span>{{trans('common.my_oleb2b')}}</span></h1>

            <a href="{{route('portal.home')}}">
                <div class="logo"><img src="{{asset('assets/pages/img/logo.png')}}" alt="Logo" class="img-responsive">
                </div>
            </a>
            <ul>
                <li>
                    <a class="title" href="{{route('user.dashboard')}}"><i class="icon-dashboard"></i>Dashboard</a>
                </li>
                <li class="select-language">
                    <a class="title" href="#" data-toggle="collapse" data-target="#side-languages"><i
                                class="icon-idioma"></i> Select Language</a>
                    <ul class="sub collapse" id="side-languages">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li class="lang {{$localeCode == Lang::locale() ? 'active': ''}}">
                                <a rel="alternate" hreflang="{{$localeCode}}"
                                   href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                    {{ $properties['native'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a class="title" href="{{route('account.manage.show')}}"><i
                                class="icon-user"></i> {{trans('menus.user.manage_account')}}</a>
                </li>
                <li>
                    <a class="title" href="{{action('User\SuppliersController@edit')}}"><i
                                class="icon-suplier-menber"></i> {{trans('menus.user.manage_company_profile')}}</a>
                </li>
                <li>
                    <a class="title" href="{{route('download.manual')}}"><i class="icon-certi"></i> {{trans('user.exporter_manual')}} </a>
                </li>
                <li>
                    <a class="title" href="#"><i class="icon-icon-mensaje-2"></i> {{trans('menus.user.messages')}} <span
                                class="sr-only">(current)</span></a>
                    <ul class="sub">
                        <li class="{{\Request::url()==route('user.messages.products.index')?'active':''}}"><a
                                    href="{{route('user.messages.products.index')}}">{{trans('menus.user.generals')}}</a>
                            <span class="badge badge-green notification-count pull-right background-orange"
                                  id="menu-messages-products-count">0</span></li>
                        <li class="{{\Request::url()==route('user.messages.buying-request.index')?'active':''}}"><a
                                    href="{{route('user.messages.buying-request.index')}}">{{trans('menus.user.for_buying_requests')}}</a>
                            <span class="badge badge-green notification-count pull-right background-orange"
                                  id="menu-messages-buying-request-count">0</span></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="title"><i class="icon-proveedores"></i>{{trans('menus.user.for_buyers')}}<span
                                class="caret"></span> </a><i class="fa fa-check-circle-o float-right"></i>
                    <ul class="sub">
                        <li class="{{\Request::url()==route('user.buying-request.index')?'active':''}}">
                            <a href="{{action('User\BuyingRequestController@index')}}">{{trans('menus.user.manage_buying_request')}}</a>
                            <span class="badge badge-green notification-count pull-right background-orange"
                                  id="menu-buying-request-count">0</span>
                        </li>
                        <li class="{{\Request::url()==route('user.saved-products.index')?'active':''}}">
                            <a href="{{route('user.saved-products.index')}}">{{trans('menus.user.saved_products')}}</a>
                            <span class="badge badge-green notification-count pull-right background-orange"
                                  id="menu-saved-products-count">0</span>
                        </li>
                        {{-- <li class="{{\Request::url()==action('User\SuppliersController@edit')?'active':''}}">
                             <a href="{{action('User\SuppliersController@edit')}}">{{trans('menus.user.manage_company_profile')}}</a>
                             <span class="badge badge-green notification-count pull-right background-orange"
                                   id="menu-saved-products-count"><i class="fa fa-warning"></i></span>
                         </li>--}}
                    </ul>
                </li>
                @if(!Auth::user()->isSupplier())
                    <li>
                        <a href="#" class="title"><i class="icon-suplier"></i> {{trans('menus.user.for_suppliers')}}
                            <span class="caret"></span></a>
                        <ul class="sub">
                            <li><a href="#">{{trans('menus.user.manage_products')}}</a></li>
                            {{--  <li><a href="#">{{trans('menus.user.manage_company_profile')}}</a></li>--}}
                            <li><a href="#">{{trans('menus.user.subaccounts')}}</a></li>
                            <li><a href="#">{{trans('menus.user.saved_buying_request')}}</a></li>
                        </ul>
                    </li>
                @else
                    <li>
                        <a href="#" class="title"><i class="icon-suplier"></i> {{trans('menus.user.for_suppliers')}}
                            <span class="caret"></span>
                        </a> {!! Auth::user()->isSupplier() ? '<i class="fa fa-check-circle-o"></i>' : '' !!}
                        <ul class="sub">
                            <li class="{{\Request::url()==route('user.products.index')?'active':''}}"><a
                                        href="{{route('user.products.index')}}">{{trans('menus.user.manage_products')}}</a>
                                <span class="badge badge-green notification-count pull-right background-orange"
                                      id="menu-products-count">0</span>

                            </li>
                            {{-- <li class="{{\Request::url()==action('User\SuppliersController@edit')?'active':''}}"><a
                                         href="{{action('User\SuppliersController@edit')}}">{{trans('menus.user.manage_company_profile')}}</a>
                             </li>--}}
                            @if(\Auth::user()->isPrincipalAccount())
                                <li class="{{\Request::url()==action('User\SubAccountsController@index')?'active':''}}">
                                    <a href="{{action('User\SubAccountsController@index')}}">{{trans('menus.user.subaccounts')}}</a>
                                    <span class="badge badge-green notification-count pull-right background-orange"
                                          id="menu-subaccounts-count">0</span></li>
                            @endif
                            <li class="{{\Request::url()==route('user.saved-buying-request.index')?'active':''}}"><a
                                        href="{{route('user.saved-buying-request.index')}}">{{trans('menus.user.saved_buying_request')}}</a>
                                <span class="badge badge-green notification-count pull-right background-orange"
                                      id="menu-saved-buying-request-count">0</span></li>
                        </ul>
                    </li>
                @endif
                <li><a class="boton-azul btn-block text-center" href="{{route('user.memberships.index')}}"><i
                                class="fa fa-star"></i> {{trans('menus.user.memberships')}} - {{ trans('memberships.'.strtolower(auth()->user()->activeMembership()->en_name ))}}</a></li>
            </ul>
        </div>
    </aside>
    <div class="page-wrapper">
        <ul class="select-lang">
            <li class="toggle-aside"><i class="icon-icon-menu-bold"></i></li>
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li class="lang {{$localeCode == Lang::locale() ? 'active': ''}}">
                    <a rel="alternate" hreflang="{{$localeCode}}"
                       href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                        {{ $properties['native'] }}
                    </a>
                </li>
            @endforeach
            <li class="salir">
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{trans('menus.user.logout')}} <i class="fa fa-sign-out"></i>
                </a>
                <form id="logout-form" action="{{ action('Auth\LoginController@logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
            <li class="user-authenticated">
                <small>{{trans('portal.greeting')}}, <strong>{{Auth::user()->names}}</strong></small>
            </li>
        </ul>
        @if(Auth::user()->status == \App\Library\Constants::USER_STATUS_EMAIL_CONFIRM)
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-warning">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {!! trans('messages.we_have_sent_you_an_email') !!} <a
                                    href="{{route('web.resend-email-confirmation',Auth::user()->id)}}"
                                    id="validate-email">{!! trans('messages.send_again') !!}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(Auth::user()->supplier)
            @if(!Auth::user()->supplier->images || !Auth::user()->supplier->factory_detail_complete || Auth::user()->supplier->certifications->isEmpty())
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-warning">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {!! trans('messages.data_missing') !!}
                                <a href="{{route('company.profile.show')}}" >{!! trans('messages.profile_images_missing') !!}</a>
                                @if(!Auth::user()->supplier->factory_detail_complete)
                                    ,
                                    <a href="{{route('company.profile.show',['section' => 'factory'])}}" >{!! trans('messages.factory_details_images_missing') !!}</a>
                                @endif
                                @if(Auth::user()->supplier->certifications->isEmpty())
                                    ,
                                    <a href="{{route('company.profile.show',['section' => 'certifications'])}}">{!! trans('messages.certifications_images_missing') !!}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        @yield('content')
        <footer id="footer">
            <div class="row">
                <div class="col-xs-12">
                    <ul>
                        <li>©OleB2B property of Olebaba.com SAC. All rights reserved</li>
                        <li class="pull-right">Version: 1.0</li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>

<!-- Scripts -->
{{--<script src="/js/app.js"></script>--}}
<script src="{{asset('assets/packages/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/packages/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
<script src="{{asset('assets/packages/bootstrap-fileinput/themes/fa/theme.js')}}"></script>
{{--<script src="{{asset('assets/packages/bootstrap-fileinput/js/plugins/sortable.min.js')}}"></script>--}}
<script src="{{asset('assets/packages/bootstrap-fileinput/js/locales/es.js')}}"></script>
<script src="{{asset('assets/pages/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/packages/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/packages/moment/min/moment.min.js')}}"></script>
<script src="{{asset('assets/packages/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('assets/user/js/scripts.js')}}"></script>
<script src="https://js.pusher.com/3.2/pusher.min.js"></script>
@include('global-partials.avisos')
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $.get('{{route('api.messages.products.count')}}', function (data) {
        if (data) {
            $("#menu-messages-products-count").text(data.count);
        }
    });
    $.get('{{route('api.messages.buyingrequest.count')}}', function (data) {
        if (data) {
            $("#menu-messages-buying-request-count").text(data.count);
        }
    });


    $.get('{{route('api.buying-request.count')}}', function (data) {
        if (data) {
            $("#menu-buying-request-count").text(data.count);
        }
    });

    $.get('{{route('api.saved.buying.request.count')}}', function (data) {
        if (data) {
            $("#menu-saved-buying-request-count").text(data.count);
        }
    });

    $.get('{{route('api.productFavorite.count')}}', function (data) {
        if (data) {
            $("#menu-saved-products-count").text(data.count);
        }
    });


    $.get('{{route('api.subaccounts.count')}}', function (data) {
        if (data) {
            $("#menu-subaccounts-count").text(data.count);
        }
    });

    $.get('{{route('api.products.count')}}', function (data) {
        if (data) {
            $("#menu-products-count").text(data.count);
        }
    });

    $.get('{{route('api.buyging-request.count')}}', function (data) {
        if (data) {
            $("#menu-saved-buying-request").text(data.count);
        }
    });
    $('#validate-email').on('click', function (e) {
        e.preventDefault();
        var target = $(this).prop('href');
        swal({
                title: '{{trans('alerts.messages.message_send')}}',
                text: '{{trans('alerts.validateEmail.message')}}',
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{{trans('alerts.buttons.ok')}}',
                closeOnConfirm: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.get(target);
                }
            });
    });
</script>
@yield('js')
</body>
</html>
