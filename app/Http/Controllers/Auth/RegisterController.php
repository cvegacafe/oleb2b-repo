<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Library\Constants;
use App\Notifications\WelcomeToOleb2b;
use App\Repositories\User\User;
use App\Services\CountryService;
use App\Services\LanguageService;
use App\Services\MembershipService;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    private $languageService = null;

    private $countryService = null;

    /**
     * Create a new controller instance.
     *
     * @param LanguageService $languageService
     * @param CountryService $countryService
     */
    public function __construct(LanguageService $languageService, CountryService $countryService)
    {
        $this->middleware('guest');
        $this->languageService = $languageService;
        $this->countryService = $countryService;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $languages = $this->languageService->all();
        $countries = $this->countryService->all();

        return view('auth.register', compact('languages', 'countries'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'country_id' => 'required|exists:countries,id',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'names' => 'required|max:80',
            'last_names' => 'required|max:80',
            'office_phone' => 'required|max:25',
            'mobile_phone' => 'required|max:25',
            'address' => 'required|max:200',
            'city' => 'required|max:200',
            'postal_code' => 'required|max:20',
            'other_languages' => 'max:100'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'country_id' => $data['country_id'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'names' => $data['names'],
            'last_names' => $data['last_names'],
            'company' => $data['company'],
            'type' => $data['type'],
            'status' => Constants::USER_STATUS_EMAIL_CONFIRM,
//            'office_phone' => $data['office_phone'],
//            'mobile_phone' => $data['mobile_phone'],
//            'address' => $data['address'],
//            'city' => $data['city'],
//            'postal_code' => $data['postal_code'],
//            'other_languages' => $data['other_languages'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request $request
     * @param MembershipService $membershipService
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function register(Request $request, MembershipService $membershipService)
    {
        try {
            $validator = Validator::make($request->all(), [
                "names" => "required|max:80",
                "last_names" => "required|max:80",
                "email" => 'required|email|unique:users,email',
                "password" => 'required|confirmed',
                "country_id" => "required|exists:countries,id",
                "company" => "max:250",
//                "office_phone" => "required|max:25",
//                "mobile_phone" => "required|max:25",
//                "address" => "required|max:200",
//                "city" => "required|max:60",
//                "postal_code" => "required|max:20",
//                "languages" => "required|existsArray:languages,id",
//                "other_languages" => "max:100",
//                "terms_and_conditions" => "required",
            ]);

            if ($validator->fails()) {
                return redirect()->route('portal.home', ['show' => 'register'])->withErrors($validator)
                    ->withInput();
            }
            if ($request->has('checkbox_both')) {
                $request['type'] = User::TYPE_BOTH;
            } else {
                if ($request->has('checkbox_supplier')) {
                    $request['type'] = User::TYPE_SUPPLIER;
                } else {
                    $request['type'] = User::TYPE_BUYER;
                }
            }

            $user = $this->create($request->all());

            if ($request->has('languages')) {
                $user->languages()->attach($request->get('languages'));
            }

            if ($request->has('markets')) {
                $user->markets()->attach($request->get('markets'));
            }

            $basicMembership = $membershipService->find(1);

            $user->memberships()->attach($basicMembership->id, [
                'main_rotation_banners' => $basicMembership->main_rotation_banners,
                'inner_rotation_banners' => $basicMembership->inner_rotation_banners,
                'product_posting' => $basicMembership->product_posting,
                'amount' => 0,
                'frequency' => 'A',
                'is_active' => true,
                'finish_at' => Carbon::now()->addYears(30)->toDateTimeString()
            ]);

            $user->notify(new WelcomeToOleb2b($user));
            event(new Registered($user));
            $this->guard()->login($user);

        } catch (Exception $e) {
            if (isset($user) && $user) {
                $user->memberships()->detach();
                $user->languages()->detach();
                $user->markets()->detach();
                $user->delete();
            }

            Log::error($e);
            Aviso::error(trans('alerts.messages.no_registered_error'));
        }

        return redirect($this->redirectPath());
    }
}
