<?php
return [
    'user' => [
        'manage_account' => 'My account',
        'messages' => 'Messages',
        'for_buyers' => 'For Buyers',
        'manage_buying_request' => 'Manage Buying Requests',
        'for_suppliers' => 'For Suppliers',
        'manage_products' => 'My products',
        'manage_company_profile' => 'Company Profile',
        'subaccounts' => 'Sub Accounts',
        'saved_products' => 'Saved Products',
        'saved_buying_request' => 'Saved Buying Requests',
        'contact_us' => 'Contact Us',
        'logout' => 'Logout',
        'generals' => 'General',
        'memberships' => 'Memberships',
        'create_company_profile' => 'Create Company Profile',
        'for_buying_requests' => 'For Buying Requests'
    ],
    'footer' => [
        'customer_service' => 'Customer Service',
        'contact_us' => 'Contact Us',
        'terms_use' => 'Terms of Use',
        'privacy_politics' => 'Privacy Policy',
        'about_us' => 'About Us',
        'about_oleb2b' => 'About Oleb2b.com',
        'sitemap' => 'Site Map ',
        'buy_on_oleb2b' => 'Buy On Oleb2b.com',
        'get_quotations' => 'Get Quotations',
        'sell_on_oleb2b' => 'Sell On Oleb2b.com',
        'supplier_memberships' => 'Supplier Memberships',
        'follow_us' => 'Follow Us'
    ],
    'responsive' => [
        'my_account' => ''
    ]
];