<?php namespace App\Repositories\Currency;

use App\Repositories\BaseRepository;

class CurrencyRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new Currency();
        parent::__construct();
    }

}