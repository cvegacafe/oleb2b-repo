@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            @if($buying_request->is_approved)
                <div class="alert alert-success">Cotizacion Aprobada</div>
            @else
                <div class="alert alert-warning">Cotizacion Sin Aprobar</div>
            @endif
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group col-md-12">
                            <label for="description" class="col-md-12 control-label">{{trans('products.product_photos')}}</label>
                            <div class="col-md-12">
                                <img src="{{asset($buying_request->image)}}" alt="" >
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="name" class="col-md-4 control-label">{{trans('products.name')}} (ESP)</label>
                            <div class="col-md-6">
                                {{$buying_request->es_name}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="name" class="col-md-4 control-label">{{trans('products.name')}} (ING)</label>
                            <div class="col-md-6">
                                {{$buying_request->en_name}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="name" class="col-md-4 control-label">{{trans('products.name')}} (RU)</label>
                            <div class="col-md-6">
                                {{$buying_request->ru_name}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description" class="col-md-4 control-label">{{trans('products.description')}} (ESP)</label>
                            <div class="col-md-6">
                                {{$buying_request->es_description}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description" class="col-md-4 control-label">{{trans('products.description')}} (ING)</label>
                            <div class="col-md-6">
                                {{$buying_request->en_description}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="description" class="col-md-4 control-label">{{trans('products.description')}} (RU)</label>
                            <div class="col-md-6">
                                {{$buying_request->ru_description}}
                            </div>
                        </div>
                        <br>
                        <hr>

                        <div class="form-group col-md-12">
                            <label for="category" class="col-md-4 control-label">{{trans('products.product_supercategory')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->subcategory->category->supercategory->name}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="subcategory_id" class="col-md-4 control-label">{{trans('products.product_category')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->subcategory->category->name}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="subcategory_id" class="col-md-4 control-label">{{trans('products.product_subcategory')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->subcategory->name}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="country_id" class="col-md-4 control-label">{{trans('products.place_origin')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->country->name}}
                            </div>
                        </div>



                        <div class="form-group col-md-12">
                            <label for="price" class="col-md-4 control-label">{{trans('products.price')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->order_qty}}   {{$buying_request->measure->name}}
                            </div>
                        </div>


                        <div class="form-group col-md-12">
                            <label for="color" class="col-md-4 control-label">{{trans('products.qty')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->start_date}} -   {{$buying_request->end_date}}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="ports" class="col-md-4 control-label">{{trans('products.ports')}}</label>
                            <div class="col-md-6">
                                {{$buying_request->ports}}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{action('Admin\BuyingRequestController@edit',$buying_request)}}" class="btn btn-info">Editar</a>
                                @if($buying_request->is_approved)
                                    <form action="{{route('admin.buying-request.disable',$buying_request)}}" method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-warning">Deshabilitar</button>
                                    </form>
                                @else
                                    <form action="{{route('admin.buying-request.enable',$buying_request)}}" method="POST">
                                        <input type="hidden" name="_method" value="PUT">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-success">Habilitar</button>
                                    </form>
                                @endif
                                <form action="{{action('Admin\BuyingRequestController@destroy',$buying_request)}}" method="POST" onsubmit="return confirm('Seguro que desea eliminar {{$buying_request->es_name}}')">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
