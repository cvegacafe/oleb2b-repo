<?php
return [
    'user' => [
        'manage_account' => 'Mi Cuenta',
        'messages' => 'Mensajes',
        'for_buyers' => 'Para Compradores',
        'manage_buying_request' => 'Administrar Solicitudes De Cotización',
        'for_suppliers' => 'Para Vendedores',
        'manage_products' => 'Mis Productos',
        'manage_company_profile' => 'Perfil De Empresa',
        'subaccounts' => 'Sub Cuentas',
        'saved_products' => 'Productos Guardados',
        'saved_buying_request' => 'Solicitudes De Cotización Guardadas',
        'contact_us' => 'Contáctenos',
        'memberships' => 'Membresías',
        'logout' => 'Salir',
        'generals' => 'General',
        'create_company_profile' => 'Crear perfil de empresa',
        'for_buying_requests' => 'De Solicitudes de Cotización'
    ],
    'footer' => [
        'customer_service' => 'Servicio Al Cliente',
        'contact_us' => 'Contáctenos',
        'terms_use' => 'Términos de Uso',
        'privacy_politics' => 'Políticas de Privacidad',
        'about_us' => 'Sobre Nosotros',
        'about_oleb2b' => 'Sobre Oleb2b.com',
        'sitemap' => 'Mapa Del Sitio',
        'buy_on_oleb2b' => 'Compre En Oleb2b.com',
        'get_quotations' => 'Reciba Una Cotizacion',
        'sell_on_oleb2b' => 'Venda En Oleb2b.com',
        'supplier_memberships' => 'Membresias Para Vendedores',
        'follow_us' => 'Siguenos En'
    ]
];