@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new Measurement</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{action('Admin\MeasurementsController@store')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Type</label>
                                <div class="col-md-6">
                                    <select name="type" id="" class="form-control">
                                        @foreach(\App\Library\Constants::MEASUREMENT_TYPES as $item)
                                            <option value="{{$item['id']}}">{{$item['name']}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"  required autofocus>
                                </div>
                            </div>
                            <h3>Translates</h3>
                            @foreach($languages as $language)
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">{{$language->name}}</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="languages[{{$language->id}}]" required autofocus>
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection