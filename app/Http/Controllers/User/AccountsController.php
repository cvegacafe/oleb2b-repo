<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\User\User;
use App\Services\CountryService;
use App\Services\FileService;
use App\Services\LanguageService;
use App\Services\MarketService;
use App\Services\UserService;
use Exception;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AccountsController extends Controller
{

    /**
     * Show user data account
     * GET
     * @param Guard $guard
     * @param CountryService $countryService
     * @param LanguageService $languageService
     * @param MarketService $marketService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageAccount(
        Guard $guard,
        CountryService $countryService,
        LanguageService $languageService,
        MarketService $marketService
    ) {
        $user = $guard->user();
        $countries = $countryService->all();
        $languages = $languageService->all();
        $markets = $marketService->all();

        return view('user.accounts.manage', compact('user', 'countries', 'languages', 'markets'));
    }

    /**
     * update user data account
     * PUT
     * @param User $user
     * @param Request $request
     * @param UserService $userService
     * @param FileService $fileService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function manageAccountUpdate(
        User $user,
        Request $request,
        UserService $userService,
        FileService $fileService
    ) {
        try {
            if ($request->hasFile('avatar')) {
                $fileService->removeFile($user->image);
                $file = $request->file('avatar');
                $pathImage = $fileService->storeUserAvatar($file, Auth::id());
                $request['image'] = $pathImage;
            }

            if ($userService->update($user->id, $request->all())) {
                if ($request->has('languages')) {
                    $user->languages()->sync($request->get('languages'));
                }
                if ($request->has('markets')) {
                    $user->markets()->sync($request->get('markets'));
                }
            }

            Aviso::guardado(trans('validation.custom.messages.record_updated'));

        } catch (Exception $e) {
            Log::error($e);
            Aviso::guardado(trans('validation.custom.messages.record_updated_error'));
        }

        return redirect()->route('account.manage.show');
    }

    /**
     * @param Request $request
     * @param UserService $userService
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postChangePassword(Request $request, UserService $userService)
    {

        $validator = Validator::make($request->all(), [
            "old_password" => 'required',
            "new_password" => 'required|max:128|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->route('account.manage.show', ['show' => 'change-password-form'])
                ->withErrors($validator)
                ->withInput();
        }

        $password = $request->new_password;

        if (Hash::check($request->old_password, Auth::user()->password)) {
            $rs = $userService->update(Auth::id(), ['password' => bcrypt($password)]);
            if ($rs) {
                Aviso::guardado(trans('alerts.password.password_updated'));
            } else {
                Aviso::noGuardado(trans('alerts.password.password_updated_error'));
            }
        } else {
            Aviso::noGuardado(trans('alerts.password.password_current_invalid'));
            return redirect()->route('account.manage.show', ['show' => 'change-password-form']);
        }

        return redirect()->route('account.manage.show');

    }

}
