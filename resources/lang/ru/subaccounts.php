<?php
return [
    'add_new_subaccount' => 'Добавить новую учетную запись',
    'update_subaccount' => 'Update SubAccount',
    'markets' => 'Рынки',
    'validation_edit' => 'Этот раздел может быть редактирован только :owner'
];