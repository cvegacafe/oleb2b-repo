<?php namespace App\Repositories\CompanyType;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class CompanyType extends Model
{
    public $table = 'companyTypes';

    public $fillable = ['en_name', 'es_name', 'ru_name'];

    public $timestamps = false;

    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }
}
