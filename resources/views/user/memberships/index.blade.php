@extends('layouts.app')
@section('content')
    @include('global-partials.membership-content')
    @include('user.memberships.payu-form')
@endsection
@section('js')
    <script>
        function sendPlanSelection(membershipId, el) {

            var content = $(el).parent().parent().find('.modal-body');

            var plan = content.find('select option:selected');

            if(plan.attr('disabled')=='disabled'){

                var form_group = content.find('.form-group');

                form_group.addClass('has-error');

                var errorMsg = content.find('#error-msg');

                errorMsg.text('Debe seleccionar un plan');

                return;
            }

            var request = $.ajax({
                url: '{{route('api.payu.signature')}}',

                method: "POST",

                data: {

                    membership_id: membershipId,

                    frequency: plan.val(),

                    _token: window.Laravel.csrfToken,

                    lang: window.Laravel.lang
                },

                dataType: "json",

                async: false
            });

            request.done(function (data) {
                var form = $('#payu-form');

                form.find('#signature').val(data.signature);

                form.find('#description').val(data.description);

                form.find('#referenceCode').val(data.referenceCode);

                form.find('#amount').val(data.amount);

                form.find('#currency').val(data.currency);

                form.submit();

                return true;
            });

            request.fail(function (jqXHR, textStatus) {

                console.log(jqXHR);

                console.log("Request failed: " + textStatus);
            });
        }
    </script>
@endsection