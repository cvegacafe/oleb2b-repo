<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subcategory_id'=>'required|exists:subcategories,id',
            'name'=>'required|max:150',
            'description'=>'required|max:10000',
            'type'=>'max:50',
            'color'=>'max:50',
            'images'=>'required|array',
            'images.*'=>'image',
            'country_id'=>'required|exists:countries,id',
            'supply_ability'=>'required|numeric',
            'supply_measure_id'=>'required|exists:measurements,id',
            'supply_ability_frecuency'=>'required|in:DAY,MONTH,YEAR',
            'moq'=>'required|numeric',
            'moq_measure_id'=>'required|exists:measurements,id',
            'delivery_time'=>'required|numeric|max:999',
            'price'=>'numeric',
            'currency_id'=>'required|exists:currencies,id',
            'sale_unit_measure_id'=>'required|exists:measurements,id',
            'delivery_terms'=>'required|array',
            'payment_terms'=>'required|array',
            'ports'=>'array',
            'user_id'=>'exists:users,id'
        ];
    }
}
