<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Services\MembershipService;
use App\Services\MessengerService;
use App\Services\OrderService;
use App\Services\PayUService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MembershipsController extends Controller
{
    private $membershipService = null;

    public function __construct(MembershipService $membershipService)
    {
        $this->membershipService = $membershipService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param PayUService $payUService
     * @return \Illuminate\Http\Response
     */
    public function index(PayUService $payUService)
    {
        $memberships = $this->membershipService->all();
        $advancedMembership = $memberships->find(2);
        $premiumMembership = $memberships->find(3);
        $payuParameters = $payUService->getParameters();

        return view('user.memberships.index', compact('advancedMembership', 'premiumMembership', 'payuParameters'));
    }

    public function payuConfirmation(
        Request $request,
        PayUService $payUService,
        OrderService $orderService,
        MessengerService $messengerService
    ) {
        try {
            \Debugbar::disable();
            Log::info($request->all());
            $payUService->confirmation($request, $orderService, $messengerService);

        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function payuResponse(Request $request, PayuService $payuService)
    {
        $data = $payuService->pageResponse($request);
        $msg = $data['msg'];
        $subtitle = $data['subtitle'];
        $title = $data['title'];
        Aviso::success($msg);
        return redirect()->route('portal.home');
    }


}
