@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">{{trans('buyingRequest.edit_buying_request')}}</div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{route('user.buying-request.update',$buyingRequest)}}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="category" class="control-label">{{trans('products.product_supercategory')}}</label>
                                        <select name="category" id="supercategory" class="form-control required" >
                                            <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                            @foreach($superCategories as $superCategory)
                                                @if($buyingRequest->subcategory->category->supercategory->id == $superCategory->id)
                                                    <option value="{{$superCategory->id}}" selected>{{$superCategory->name}}</option>
                                                @else
                                                    <option value="{{$superCategory->id}}">{{$superCategory->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        <label for="subcategory_id" class="control-label">{{trans('products.product_category')}}</label>
                                        <select name="subcategory_id" id="category" class="form-control required" >
                                            @foreach($buyingRequest->subcategory->category->supercategory->categories as $categories)
                                                @if($buyingRequest->subcategory->category->id == $categories->id)
                                                    <option value="{{$categories->id}}" selected>{{$categories->name}}</option>
                                                @else
                                                    <option value="{{$categories->id}}">{{$categories->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
                                        <label for="subcategory_id" class="control-label">{{trans('products.product_subcategory')}}</label>
                                        <select name="subcategory_id" id="subcategory_id" class="form-control required" >
                                            @foreach($buyingRequest->subcategory->category->subcategories as $subcategories)
                                                @if($buyingRequest->subcategory_id == $subcategories->id)
                                                    <option value="{{$subcategories->id}}" selected>{{$subcategories->name}}</option>
                                                @else
                                                    <option value="{{$subcategories->id}}">{{$subcategories->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('subcategory_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subcategory_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="control-label">{{trans('products.name')}}</label>
                                        <input id="name" type="text" class="form-control" name="name" value="{{$buyingRequest->name}}" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="description" class="control-label">{{trans('products.description')}}</label>
                                        <textarea name="description" id="description" cols="40" rows="5" class="form-control">{{$buyingRequest->description}}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="start_date" class="control-label">{{trans('products.start_date')}}</label>
                                                <div class="form-group">
                                                    <div class='input-group date date_picker'>
                                                        <input type='text' class="form-control" name="start_date"/>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label for="end_date" class="control-label">{{trans('products.end_date')}}</label>
                                                <div class="form-group">
                                                    <div class='input-group date date_picker'>
                                                        <input type='text' class="form-control" name="end_date"/>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="country_id" class="control-label">Country of delivery</label>
                                        <select name="country_id" id="country_id" class="form-control required" >
                                            <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                            @foreach($countries as $country)
                                                @if($buyingRequest->country_id == $country->id)
                                                    <option value="{{$country->id}}" selected>{{$country->name}}</option>
                                                @else
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group{{ $errors->has('moq') ? ' has-error' : '' }}">
                                        <label for="order_qty" class="control-label">Order quantity</label>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <input id="order_qty" type="number" class="form-control" name="order_qty" value="{{$buyingRequest->order_qty}}" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <select name="measure_id" id="measure_id" class="form-control required" >
                                                    @foreach($measurements as $measure)
                                                        @if($buyingRequest->measure_id == $measure->id)
                                                            <option value="{{$measure->id}}" selected>{{$measure->name}}</option>
                                                        @else
                                                            <option value="{{$measure->id}}" >{{$measure->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if ($errors->has('moq'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('moq') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('payment_terms') ? ' has-error' : '' }}">
                                        <label for="mobile_phone" class="control-label">{{trans('products.payment_terms')}}</label>
                                        <div class="checkbox-list">
                                            @foreach($paymentTerms as $paymentTerm)
                                                @if($buyingRequest->paymentTerms->contains($paymentTerm->id))
                                                    <label class="checkbox-inline">
                                                        <input id="{{$paymentTerm->id}}"  type="checkbox" name="payment_terms[]" value="{{$paymentTerm->id}}"/>&nbsp;{{$paymentTerm->name}}
                                                    </label>
                                                @else
                                                    <label class="checkbox-inline">
                                                        <input id="{{$paymentTerm->id}}"  checked type="checkbox" name="payment_terms[]" value="{{$paymentTerm->id}}"/>&nbsp;{{$paymentTerm->name}}
                                                    </label>
                                                @endif
                                            @endforeach
                                            @if ($errors->has('payment_terms'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('payment_terms') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('ports') ? ' has-error' : '' }}">
                                        <label for="ports" class="control-label">{{trans('products.ports')}}</label>
                                        <input id="ports" type="text" class="form-control" name="port" value="{{$buyingRequest->ports}}" >
                                        @if ($errors->has('ports'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('ports') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                        <label for="languages" class="control-label">{{trans('products.languages')}}</label>
                                        <div class="checkbox-list">
                                            @foreach($languages as $language)
                                                @if($buyingRequest->languages->contains($language->id))
                                                <label class="checkbox-inline">
                                                    <input id="{{$language->id}}"  checked type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->original_name}}
                                                </label>
                                                @else
                                                    <label class="checkbox-inline">
                                                        <input id="{{$language->id}}"  type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->original_name}}
                                                    </label>
                                                @endif
                                            @endforeach
                                            @if ($errors->has('languages'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('languages') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                        <label for="description" class="control-label">{{trans('products.product_photos')}}</label>
                                        <input type="file" name="photo" class="form-control">
                                        @if ($errors->has('photo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('photo') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary">
                                            {{trans('common.forms.buttons.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        var PATH_SUPERCATEGORY = '{{route('api.categories.getBySupercategory')}}';
        var PATH_CATEGORY = '{{route('api.subcategories.getByCategory')}}';

        $(function(){

            $('#supercategory').on('change',function(e){

                e.preventDefault();

                $('#category').append("<option value='0'>{{trans('common.loading')}}</option>");

                var supercategory_id = $(this).find(':selected').val();

                var request = $.ajax({
                    url: PATH_SUPERCATEGORY,
                    method: "GET",
                    data: {supercategory_id:supercategory_id,language:'{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {
                    $('#category').find('option').remove();
                    $('#subcategory_id').find('option').remove();
                    if(data.length > 0){
                        $.each(data, function(index, element) {
                            $('#category').append("<option value='"+ element.id +"'>"+element.trans_name+"</option>");
                        });
                    }

                    $('#category').trigger('change');
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            $('#category').on('change',function(e){

                e.preventDefault();

                $('#subcategory_id').append("<option value='0'>{{trans('common.loading')}}</option>");

                var category_id = $(this).find(':selected').val();

                var request = $.ajax({
                    url: PATH_CATEGORY,
                    method: "GET",
                    data: {category_id:category_id,language:'{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {

                    $('#subcategory_id').find('option').remove();

                    if(data.length > 0){
                        $.each(data, function(index, element) {
                            $('#subcategory_id').append("<option value='"+ element.id +"'>"+element.trans_name+"</option>");
                        });
                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });
        });
    </script>
@endsection