<?php namespace App\DTOs;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;


/**
 * Created for oleb2b by emmanuel <emmanuelbarturen@gmail.com>.
 * Date: 28/03/18
 */
class SearchableDTO
{
    /**
     * @var LengthAwarePaginator
     */
    private $products;
    /**
     * @var array
     */
    private $countries;
    /**
     * @var array
     */
    private $categories;

    /**
     * SearchableDTO constructor.
     * @param LengthAwarePaginator $products
     * @param Collection $countries
     * @param Collection $categories
     */
    public function __construct(LengthAwarePaginator $products, Collection $countries, Collection $categories)
    {
        $this->products = $products;
        $this->countries = $countries;
        $this->categories = $categories;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getProducts(): LengthAwarePaginator
    {
        return $this->products;
    }

    /**
     * @return Collection
     */
    public function getCountries(): Collection
    {
        return $this->countries;
    }

    /**
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }


}