<?php namespace App\Http\Requests;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Http\FormRequest;

class CreateSubAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Guard $guard)
    {
        return $guard->user()->supplier->pivot->is_principal;
    }

    /* public function all()
         {
             $data = parent::all();
             $data['parent_user_id'] = Auth::id();
             return $data;
         }*/
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'names' => 'required',
            'last_names' => 'required',
            'email' => 'required',
            'languages'=>'required|array',
           /* 'markets'=>'required|array',*/
        ];
    }
}
