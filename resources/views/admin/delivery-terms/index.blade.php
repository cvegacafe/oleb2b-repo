@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Delivery Terms :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Descripcion Espanol</th>
                        <th>Descripcion Ingles</th>
                        <th>Descripcion Ruso</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($deliveryTerms as $deliveryTerm)
                        <tr>
                            <td><b>{{$deliveryTerm->id}}</b></td>
                            <td>{{$deliveryTerm->name}}</td>
                            <td>{{$deliveryTerm->es_description}}</td>
                            <td>{{$deliveryTerm->en_description}}</td>
                            <td>{{$deliveryTerm->ru_description}}</td>
                            <td>
                                <a href="{{action('Admin\DeliveryTermsController@edit',$deliveryTerm)}}"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="fa fa-pencil"></span></button></a>
                                <form action="{{action('Admin\DeliveryTermsController@destroy',$deliveryTerm)}}" method="POST" onsubmit="return confirm('Esta seguro q desea eliminar {{$deliveryTerm->name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs" ><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <form action="{{action('Admin\DeliveryTermsController@store')}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="text" name="name" class="form-control" placeholder="Nombre"></td>
                            <td><input type="text" name="es_description" class="form-control" placeholder="descripcion en espanol"></td>
                            <td><input type="text" name="en_description" class="form-control" placeholder="descripcion en ingles"></td>
                            <td><input type="text" name="ru_description" class="form-control" placeholder="descripcion en ruso"></td>
                            <td colspan="2"><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
                        </tr>
                        </tfoot>
                    </form>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection