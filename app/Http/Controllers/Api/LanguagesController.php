<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiAllLanguagesRequest;
use App\Services\LanguageService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class LanguagesController extends Controller
{
    private $languageService = null;

    public function __construct(LanguageService $languageService)
    {
        $this->languageService = $languageService;
    }

    public function getLanguages(Request $request)
    {
        $languages = $this->languageService->getLanguagesOriginalName();
        return response()->json(['languages'=>$languages]);
    }
}
