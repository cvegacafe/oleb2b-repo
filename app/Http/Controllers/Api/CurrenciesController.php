<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CurrencyEnableRequest;
use App\Services\CurrencyService;

class CurrenciesController extends Controller
{
    private $currencyService = null;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    public function enable(CurrencyEnableRequest $currencyEnableRequest)
    {
        $val = $currencyEnableRequest->get('checked') === 'true' ? true : false;
        $response = $this->currencyService->update(
            $currencyEnableRequest->get('id'),
            ['enabled' => $val]
        );

        return response()->json(['response' => $response, 'value' => $val]);
    }
}
