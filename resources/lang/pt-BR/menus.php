<?php
return [
    'admin' => [

    ],
    'user' => [
        'manage_account' => 'Manage Account',
        'messages' => 'Messages',
        'for_buyers' => 'For Buyers',
        'manage_buying_request' => 'Manage Buying Request',
        'for_suppliers' => 'For Suppliers',
        'manage_products' => 'Manage Products',
        'manage_company_profile' => 'Manage Company Profile',
        'subaccounts' => 'Sub Accounts',
        'saved_products' => 'Saved Products',
        'saved_buying_request' => 'Saved Buying Request',
        'contact_us' => 'Contact Us',
        'logout' => 'Logout'
    ],
    'portal' => [

    ],
];