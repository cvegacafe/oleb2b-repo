@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Sub-Categorias :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Categoria</th>
                            <th>Espanol</th>
                            <th>Ingles</th>
                            <th>Ruso</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($subcategories as $subcategory)
                        <tr>
                            <td><b>{{$subcategory->id}}</b></td>
                            <td><b>{{$subcategory->category->es_name}}</b></td>
                            <td>{{$subcategory->es_name}}</td>
                            <td>{{$subcategory->en_name}}</td>
                            <td>{{$subcategory->ru_name}}</td>

                            <td>
                                <form action="{{action('Admin\SubcategoriesController@destroy',$subcategory)}}" method="POST" onsubmit="return confirm('Esta seguro q desea eliminar {{$subcategory->es_name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs" ><span class="fa fa-times"></span></button>
                                </form>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection