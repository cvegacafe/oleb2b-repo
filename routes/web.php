<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

if (env('APP_ENV') == 'local') {
    include 'tests.php';
}

Carbon\Carbon::setLocale(Illuminate\Support\Facades\Lang::locale());


Route::get('sitemap', ['uses' => 'SitemapController@index']);
Route::get('sitemap/products', ['uses' => 'SitemapController@products', 'as' => 'sitemap.products']);
Route::get('sitemap/statics', ['uses' => 'SitemapController@statics', 'as' => 'sitemap.statics']);
/*
|--------------------------------------------------------------------------
| Portal Routes
|--------------------------------------------------------------------------
*/


Route::get('/', function () {
    return redirect()->route('portal.home');
});

Route::get('landing-page', function () {
    return view('landings.latam-exporta');
});

//ruta de prueba envio mail

/*Route::get('/test/mail/{lang}', function ($lang = 'es') {
    app()->setLocale($lang);
    $user = User::find(1);
    if (!$user) {
        return 'no existe el correo';
    } else {
        $user->notify(new \App\Notifications\WelcomeToOleb2b($user));
        $user->notify(new \App\Notifications\SendEmailForNewMessage($user, 'http://www.google.com'));
        $user->notify(new \App\Notifications\ResetPasswordNotification(csrf_token()));
        $membership = \App\Repositories\Membership\Membership::find(1);
        $user->notify(new \App\Notifications\NewPurchasedMembership($user, $membership));
        $buyerrequest = \App\Repositories\BuyingRequest\BuyingRequest::find(1);
        if (!$buyerrequest) {
            $buyerrequest = factory(\App\Repositories\BuyingRequest\BuyingRequest::class)->create();
        }
        $super = \App\Repositories\Supercategory\Supercategory::first();
        $user->notify(new \App\Notifications\NewBuyingRequestForSuppliers($super, $buyerrequest, $lang, $user));
        echo 'enviado';
    }

});*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect']
], function () {
    Auth::routes();
    Route::get('/', ['as' => 'portal.home', 'uses' => 'HomeController@index']);

    Route::get('/categories', ['as' => 'portal.categories', 'uses' => 'HomeController@categories']);
    Route::get('/product/{slug}', ['as' => 'portal.singleProduct', 'uses' => 'HomeController@singleProduct']);
    Route::get('/buying-request', ['as' => 'portal.buyingRequests', 'uses' => 'HomeController@buyingRequests']);

    Route::get('/search/{q?}-' . trans('common.in') . '-{currentCountries?}', ['as' => 'portal.search', 'uses' => 'HomeController@search']);
    Route::post('/build-search', ['as' => 'portal.search.build', 'uses' => 'HomeController@buildSearch']);

    Route::post('/contactSupplier',
        ['as' => 'portal.contactSupplier', 'uses' => 'HomeController@postContactSupplier'])->middleware('auth');
    Route::post('/contactBuyer',
        ['as' => 'portal.contactBuyer', 'uses' => 'HomeController@postContactBuyer'])->middleware('auth');
    Route::get('/contact', ['as' => 'portal.contact', 'uses' => 'HomeController@contact']);
    Route::post('/contact', ['as' => 'portal.contact.post', 'uses' => 'HomeController@postContact']);
    Route::get('/terms-of-use', ['as' => 'portal.terms-use', 'uses' => 'HomeController@termsUse']);
    Route::get('/privacy-politics', ['as' => 'portal.privacy-politics', 'uses' => 'HomeController@PrivacyPolitics']);
    Route::get('/memberships', ['as' => 'portal.memberships', 'uses' => 'HomeController@memberships']);
    Route::get('download-certificate/{idCertificate}',
        ['as' => 'web.certificate.download', 'uses' => 'HomeController@downloadCertificate']);
    Route::get('/about', ['uses' => 'HomeController@about', 'as' => 'web.about']);
    Route::get('/email-confirmation/{token}/{email}',
        ['uses' => 'HomeController@emailConfirmation', 'as' => 'web.email-confirmation']);
    Route::get('/resend-confirmation-email/{id}',
        ['uses' => 'HomeController@resendEmailConfirmation', 'as' => 'web.resend-email-confirmation']);
});

/*
|--------------------------------------------------------------------------
| Intranet Routes
|--------------------------------------------------------------------------
*/
Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/my-oleb2b',
    'namespace' => 'User',
    'middleware' => ['auth', 'localeSessionRedirect', 'localizationRedirect']
], function () {
    Route::get('/dashboard', ['as' => 'user.dashboard', 'uses' => 'CommonController@index']);
    Route::get('/manage-account', ['as' => 'account.manage.show', 'uses' => 'AccountsController@manageAccount']);
    Route::post('change-password',
        ['as' => 'account.manage.change-password', 'uses' => 'AccountsController@postChangePassword']);
    Route::put('/manage-account/{user}',
        ['as' => 'account.manage.update', 'uses' => 'AccountsController@manageAccountUpdate']);
    Route::get('sub-accounts', ['as' => 'user.sub-accounts.index', 'uses' => 'SubAccountsController@index']);
    Route::get('sub-accounts/create', [
        'as' => 'user.sub-accounts.create',
        'uses' => 'SubAccountsController@create'
    ])->middleware('hasAvailableAccounts');
    Route::post('sub-accounts/create', [
        'as' => 'user.sub-accounts.store',
        'uses' => 'SubAccountsController@store'
    ])->middleware('hasAvailableAccounts');
    Route::delete('sub-accounts/{user}',
        ['as' => 'user.sub-accounts.destroy', 'uses' => 'SubAccountsController@destroy']);
    Route::get('sub-accounts/{user}/edit', ['as' => 'user.sub-accounts.edit', 'uses' => 'SubAccountsController@edit']);
    Route::put('sub-accounts/{user}', ['as' => 'user.sub-accounts.update', 'uses' => 'SubAccountsController@update']);
    Route::get('/manage-company-profile', ['as' => 'company.profile.show', 'uses' => 'SuppliersController@edit']);
    Route::post('/manage-company-profile/', ['as' => 'company.profile.store', 'uses' => 'SuppliersController@store']);
    Route::put('/manage-company-profile/{supplier}',
        ['as' => 'company.profile.update', 'uses' => 'SuppliersController@update']);
    Route::get('/products', ['as' => 'user.products.index', 'uses' => 'ProductsController@index']);
    Route::get('/products/create', [
        'as' => 'user.products.create',
        'uses' => 'ProductsController@create'
    ])->middleware(['canSupplierPostingProducts', 'hasAvailableProducts']);
    Route::post('/products/create', [
        'as' => 'user.products.create.post',
        'uses' => 'ProductsController@store'
    ])->middleware(['canSupplierPostingProducts', 'hasAvailableProducts']);
    Route::get('/products/{slug}/edit', ['as' => 'user.products.edit', 'uses' => 'ProductsController@edit']);
    Route::put('/products/{slug}', ['as' => 'user.products.update', 'uses' => 'ProductsController@update']);
    Route::delete('/products/{slug}', ['as' => 'user.products.delete', 'uses' => 'ProductsController@destroy']);
    Route::get('buying-request/', ['as' => 'user.buying-request.index', 'uses' => 'BuyingRequestController@index']);
    Route::get('buying-request/create',
        ['as' => 'user.buying-request.create', 'uses' => 'BuyingRequestController@create']);
    Route::post('buying-request/create',
        ['as' => 'user.buying-request.store', 'uses' => 'BuyingRequestController@store']);
    Route::get('buying-request/{buyingRequest}/edit',
        ['as' => 'user.buying-request.edit', 'uses' => 'BuyingRequestController@edit']);
    Route::put('buying-request/{buyingRequest}',
        ['as' => 'user.buying-request.update', 'uses' => 'BuyingRequestController@update']);
    Route::delete('buying-request/{buyingRequest}',
        ['as' => 'user.buying-request.destroy', 'uses' => 'BuyingRequestController@destroy']);
    Route::get('saved-products', ['as' => 'user.saved-products.index', 'uses' => 'FavoriteProductsController@index']);
    Route::delete('saved-products/{product}',
        ['as' => 'user.saved-products.destroy', 'uses' => 'FavoriteProductsController@destroy']);
    Route::get('saved-buying-request',
        ['as' => 'user.saved-buying-request.index', 'uses' => 'SavedBuyingRequestsController@index']);
    Route::delete('saved-buying-request/{buyingRequest}',
        ['as' => 'user.saved-buying-request.destroy', 'uses' => 'SavedBuyingRequestsController@destroy']);
    Route::get('/memberships', ['as' => 'user.memberships.index', 'uses' => 'MembershipsController@index']);
    Route::post('/memberships/upgrade/{membership}',
        ['as' => 'user.memberships.upgrade', 'uses' => 'MembershipsController@upgrade']);
    Route::get('/messages/products',
        ['as' => 'user.messages.products.index', 'uses' => 'MessagesController@indexProducts']);
    Route::get('/messages/buying-request',
        ['as' => 'user.messages.buying-request.index', 'uses' => 'MessagesController@indexBuyingRequest']);
    Route::get('buy-response', ['as' => 'web.payu-response', 'uses' => 'MembershipsController@payuResponse']);
    Route::get('download-file/{attachmentId}',
        ['as' => 'web.file.download', 'uses' => 'MessagesController@downloadAttachment']);
    Route::get('download-manual',
        ['as' => 'download.manual', 'uses' => 'CommonController@downloadManual']);
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/login', ['as' => 'admin.login', 'uses' => 'LoginController@showLoginForm']);
    Route::post('/login', ['as' => 'admin.login.post', 'uses' => 'LoginController@login']);
    Route::post('/logout', ['as' => 'admin.logout', 'uses' => 'LoginController@logout']);
    Route::group(['middleware' => 'admin',], function () {
        Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'LoginController@dashboard']);
        Route::get('/dashboard', ['as' => 'admin.dashboard', 'uses' => 'LoginController@dashboard']);
        Route::get('/h-messages', 'LoginController@messages');

        Route::resource('users', 'UsersController');
        Route::get('usersDatatable', ['as' => 'userDatatables.data', 'uses' => 'UsersController@allUsers']);
        Route::post('/users/add-membership/{user}',
            ['as' => 'admin.user.membership.add', 'uses' => 'UsersController@addMembershipToUser']);
        Route::resource('countries', 'CountriesController');
        Route::resource('supercategories', 'SupercategoriesController');
        Route::resource('categories', 'CategoriesController');
        Route::resource('subcategories', 'SubcategoriesController', ['except' => ['create', 'show', 'edit']]);
        Route::resource('currencies', 'CurrenciesController', ['except' => ['show', 'create']]);
        Route::resource('measurements', 'MeasurementsController', ['except' => ['show', 'create']]);
        Route::resource('memberships', 'MembershipsController', ['except' => ['show', 'create', 'store', 'destroy']]);
        Route::resource('products', 'ProductsController', ['except' => ['create', 'store']]);
        Route::resource('suppliers', 'SupplierController');
        Route::put('products/enable/{product}',
            ['as' => 'admin.product.enable', 'uses' => 'ProductsController@enable']);
        Route::put('products/disable/{product}',
            ['as' => 'admin.product.disable', 'uses' => 'ProductsController@disable']);
        Route::resource('buying-request', 'BuyingRequestController', ['except' => ['create', 'store']]);
        Route::put('buying-request/enable/{buyingRequest}',
            ['as' => 'admin.buying-request.enable', 'uses' => 'BuyingRequestController@enable']);
        Route::put('buying-request/disable/{buyingRequest}',
            ['as' => 'admin.buying-request.disable', 'uses' => 'BuyingRequestController@disable']);
        Route::resource('delivery-term', 'DeliveryTermsController', ['except' => ['show', 'create']]);
        Route::resource('tax-identification', 'TaxTypesController', ['except' => ['show', 'create']]);

        # News
        Route::get('news', ['uses' => 'NewsController@index', 'as' => 'admin.show.news']);
        Route::post('news', ['uses' => 'NewsController@store', 'as' => 'admin.show.news.post']);
        Route::delete('news/{new}', ['uses' => 'NewsController@destroy', 'as' => 'admin.show.news.destroy']);

        # Sliders
        Route::get('sliders', ['uses' => 'SlidersController@index', 'as' => 'admin.sliders.index']);
        Route::post('slider', ['uses' => 'SlidersController@store', 'as' => 'admin.sliders.store']);
        Route::delete('slider/{slider}', ['uses' => 'SlidersController@destroy', 'as' => 'admin.sliders.destroy']);

        # Oleb2b images
        Route::get('oleb2b-images', ['uses' => 'Oleb2bImagesController@index', 'as' => 'admin.oleb2b-images.index']);
        Route::post('oleb2b-images', ['uses' => 'Oleb2bImagesController@store', 'as' => 'admin.oleb2b-images.store']);
        Route::delete('oleb2b-images/{image}',
            ['uses' => 'Oleb2bImagesController@destroy', 'as' => 'admin.oleb2b-images.destroy']);

    });
});

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
*/
/*Funciones ajax, pasar a archivo api cuando se use webtookens*/

Route::group(['prefix' => 'api/v1', 'namespace' => 'Api'], function () {
    Route::get('/categories/bySupercategory',
        ['as' => 'api.categories.getBySupercategory', 'uses' => 'CategoriesController@getCategoriesBySupercategoryId']);
    Route::get('/subcategories/byCategory',
        ['as' => 'api.subcategories.getByCategory', 'uses' => 'SubcategoriesController@getSubcategoriesByCategoryId']);
    Route::put('/currency/enable', ['uses' => 'CurrenciesController@enable', 'as' => 'api.currency.enable']);
    Route::put('/country/enable', ['uses' => 'CountriesController@enable', 'as' => 'api.country.enable']);
    Route::get('/countries',
        ['uses' => 'CountriesController@getAllCountriesByGroup', 'as' => 'api.country.all.byGroup']);
    Route::get('/languages', ['uses' => 'LanguagesController@getLanguages', 'as' => 'api.languages.all']);
    Route::get('/markets', ['uses' => 'MarketsController@getMarkets', 'as' => 'api.markets.all']);
    Route::get('/saved-buying-request-count',
        ['uses' => 'SavedBuyingRequestsController@savedBuyingRequestCount', 'as' => 'api.buyging-request.count']);
    Route::get('/sub-account/count',
        ['uses' => 'SubAccountsController@subAccountCount', 'as' => 'api.subaccounts.count']);
    Route::post('/product/favorite',
        ['uses' => 'FavoriteProductsController@create', 'as' => 'api.productFavorite.create']);
    Route::post('/product/favorite/count',
        ['uses' => 'FavoriteProductsController@allFavoriteCount', 'as' => 'api.productFavorite.count']);
    Route::post('/product/image/delete/{product}',
        ['uses' => 'ProductsController@deleteImage', 'as' => 'api.productImage.delete']);
    Route::get('/product/count', ['uses' => 'ProductsController@productsCount', 'as' => 'api.products.count']);
    Route::post('/product/favorite/delete',
        ['uses' => 'FavoriteProductsController@remove', 'as' => 'api.productFavorite.remove']);
    Route::get('/product/favorite/count',
        ['uses' => 'FavoriteProductsController@allFavoriteCount', 'as' => 'api.productFavorite.count']);
    Route::get('notifications', ['uses' => 'NotificationsController@all', 'as' => 'api.notifications']);
    Route::post('get-signature', ['uses' => 'PayUController@getSignature', 'as' => 'api.payu.signature']);
    Route::get('/saved-buying-request/count',
        ['uses' => 'SavedBuyingRequestsController@savedBuyingRequestCount', 'as' => 'api.saved.buying.request.count']);
    Route::post('/buying-request/saving',
        ['uses' => 'SavedBuyingRequestsController@create', 'as' => 'api.saved-buying-request.create']);
    Route::post('/buying-request/saving/delete',
        ['uses' => 'SavedBuyingRequestsController@remove', 'as' => 'api.saved-buying-request.remove']);
    Route::get('/buying-request/count',
        ['uses' => 'BuyingRequestController@buyingRequestCount', 'as' => 'api.buying-request.count']);
    Route::get('/messages/products/unread/count',
        ['uses' => 'MessagesController@countProductMessages', 'as' => 'api.messages.products.count']);
    Route::get('/messages/buying-request/unread/count',
        ['uses' => 'MessagesController@countBuyingRequestMessages', 'as' => 'api.messages.buyingrequest.count']);
    //  Chat Messages Module  \\
    Route::get('get-messages', ['uses' => 'MessagesController@getLastMessages', 'as' => 'api.messages.last']);
    Route::post('send-message', ['uses' => 'MessagesController@postSendMessage', 'as' => 'api.messages.send']);
    Route::post('auth', ['uses' => 'MessagesController@postAuth', 'as' => 'api.messages.auth']);

    # Supplier
    Route::delete('/supplier/cert/delete',
        ['uses' => 'CertificatesController@destroy', 'as' => 'api.certificates.delete']);
});

Route::post('payu-confirmation',
    ['as' => 'web.payu-confirmation', 'uses' => 'User\MembershipsController@payuConfirmation']);
