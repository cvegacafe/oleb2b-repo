<div class="col-xs-12 col-sm-4 col-lg-2">
    <figure>
        <img src="{{asset($buyingRequest->image)}}" alt="">
    </figure>
</div>
<div class="col-xs-12 col-sm-3 product">
    <ul>
        <li class="name">{{$buyingRequest->name}}</li>
        <li>{{$buyingRequest->description}}</li>
        <li><strong>{{trans('buyingRequest.order_quantity')}}:</strong> {{$buyingRequest->order_qty}} {{$buyingRequest->measure->name}}</li>
        <li><strong>{{trans('buyingRequest.country_of_delivery')}}:</strong> {{$buyingRequest->country->name}} </li>
    </ul>
</div>
<div class="col-xs-12 col-sm-4 col-lg-4 supplier">
    <button data-toggle="collapse" data-target="#datos-empresa">{{trans('buyingRequest.buyer_info')}} <i class="fa fa-caret-down"></i></button>
    <ul id="datos-empresa" class="collapse">
        {{--El nombre de la empresa solo lo pueden ver los premium o advanced user--}}
        @if($buyingRequest->user->supplier)
            @if(\Auth::check() && \Auth::user()->isPremium())
                <li class="name">{{$buyingRequest->user->supplier->company_name}}</li>
            @else
                <li class="name">{{trans('common.not_available')}}</li>
            @endif

            <li><strong>{{trans('user.form.country')}}:</strong> {{$buyingRequest->user->supplier->country->name}}</li>
            <li><strong>{{trans('buyingRequest.start_date')}}:</strong> {{$buyingRequest->formatted_start_date}}</li>
            <li><strong>{{trans('buyingRequest.end_date')}}:</strong> {{$buyingRequest->formatted_end_date}}</li>
        @else
            <li class="name">{{trans('buyingRequest.default_name_no_supplier')}}</li>
            <li><strong>{{trans('user.form.country')}}:</strong> {{$buyingRequest->user->country->name}}</li>
            <li><strong>{{trans('buyingRequest.start_date')}}:</strong> {{$buyingRequest->formatted_start_date}}</li>
            <li><strong>{{trans('buyingRequest.end_date')}}:</strong> {{$buyingRequest->formatted_end_date}}</li>
        @endif
    </ul>
</div>