<div class="modal fade" role="dialog" id="SupplierModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" role="form"  action="{{route('portal.contactSupplier')}}" id="SupplierFormHtml" enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{trans('portal.contact_supplier')}}</strong>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 id="product_name"></h2>
                        <img src="{{old('input_product_photo')}}" alt="" id="product_photo" class="img-responsive">
                    </div>
                </div>
                <input type="hidden" id="product_id" name="product_id" value="{{old('product_id')}}">
                <input type="hidden" id="input_product_photo" name="input_product_photo">
                <input type="hidden" value="{{\Request::url()}}" name="current_url">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                    <select name="country_id" id="country_id" class="form-control required" required>
                                        <option disabled selected>{{trans('products.country_of_delivery')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{old('country_id')==$country->id ? 'selected':''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group{{ $errors->has('port') ? ' has-error' : '' }}">
                                    <input id="ports" type="text" class="form-control" name="port" value="{{old('port')}}" placeholder="{{trans('products.ports')}}" required>
                                    @if ($errors->has('port'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('port') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group{{ $errors->has('order_qty') ? ' has-error' : '' }}">
                                    <input id="moq" type="number" min="0" class="form-control number" name="order_qty" value="{{old('order_qty')}}" required placeholder="{{trans('products.qty')}}">
                                    @if ($errors->has('order_qty'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('order_qty') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group{{ $errors->has('measure_id') ? ' has-error' : '' }}">
                                    <select name="measure_id" id="measure_id" class="form-control required" required>
                                        @foreach($measurements as $measure)
                                            <option value="{{$measure->id}}" {{old('measure_id')==$country->id ? 'selected':''}}>{{$measure->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('measure_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('measure_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('payment_terms') ? ' has-error' : '' }} text-center">
                            <label for="payment_terms[]" class="formas-title">{{trans('products.payment_terms')}}</label>
                            <ul class="inline-checkbox-label">
                                @foreach($paymentTerms as $paymentTerm)
                                    <li>
                                        <label class="checkbox-inline"  data-toggle="tooltip" title="{{$paymentTerm->description}}">
                                            <input id="{{$paymentTerm->id}}" {{in_array($paymentTerm->id,old('payment_terms',[]))?'checked':''}}  type="checkbox" name="payment_terms[]" value="{{$paymentTerm->id}}"/>&nbsp;{{$paymentTerm->name}}
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                            @if ($errors->has('payment_terms'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('payment_terms') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                {{--<input type="file" name="file" value="{{old('file')}}">--}}
                                <div id="filesContainer">
                                    <div class="text-center">
                                        <button id="seleccionar-fotos" type="button" class="boton-azul-no-shadow"><i class="fa fa-plus"></i> Agregar Archivos</button>
                                    </div>
                                    <div class="preview-files">

                                    </div>
                                </div>
                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="idioma1" class="tab-pane fade in active">
                            <h4 class="idioma-proveedor">{{trans('portal.supplier_speak_languages')}}: <strong id="languages">Language</strong></h4>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <textarea name="description" id="description" cols="40" rows="5" class="form-control" maxlength="1000" placeholder="{{trans('portal.your_message')}}">{{old('description')}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                @endif
                                <span  id="contador"></span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="boton-azul" data-dismiss="modal">{{trans('common.forms.buttons.close')}}</button>
                <button type="submit" class="boton-naranja">{{trans('common.forms.buttons.send')}}</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->