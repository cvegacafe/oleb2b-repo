<?php namespace App\Http\Controllers;

use App\Http\Requests\contactBuyerRequest;
use App\Http\Requests\ContactSupplierRequest;
use App\Jobs\SetVisitedCategoryJob;
use App\Jobs\SetVisitedSubCategoryJob;
use App\Library\Aviso;
use App\Library\Constants;
use App\Notifications\WelcomeToOleb2b;
use App\Repositories\Oleb2bImage\Oleb2bImage;
use App\Services\AttachmentService;
use App\Services\BuyingRequestService;
use App\Services\CategoryService;
use App\Services\CertificationService;
use App\Services\CountryService;
use App\Services\EmailService;
use App\Services\FavoriteProductService;
use App\Services\FileService;
use App\Services\LanguageService;
use App\Services\MeasureService;
use App\Services\MembershipService;
use App\Services\MessageService;
use App\Services\MessengerService;
use App\Services\NewsService;
use App\Services\Oleb2bImageService;
use App\Services\PaymentTermService;
use App\Services\ProductService;
use App\Services\SavedBuyingRequestService;
use App\Services\SliderService;
use App\Services\SupercategoryService;
use App\Services\ThreadService;
use App\Services\UserService;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * @var SupercategoryService|null
     */
    private $superCategoryService = null;
    /**
     * @var LanguageService|null
     */
    private $languageService = null;

    /**
     * HomeController constructor.
     * @param SupercategoryService $superCategoryService
     * @param LanguageService $languageService
     */
    public function __construct(SuperCategoryService $superCategoryService, LanguageService $languageService)
    {
        $this->superCategoryService = $superCategoryService;
        $this->languageService = $languageService;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @param CountryService $countryService
     * @param ProductService $productService
     * @param FavoriteProductService $favoriteProductService
     * @param MeasureService $measureService
     * @param PaymentTermService $paymentTermService
     * @param LanguageService $languageService
     * @param SupercategoryService $superCategoryService
     * @param ThreadService $threadService
     * @param NewsService $newsService
     * @param SliderService $sliderService
     * @param Oleb2bImageService $oleb2bImageService
     * @return \Illuminate\Http\Response
     */
    public function index(
        Request $request,
        CountryService $countryService,
        ProductService $productService,
        FavoriteProductService $favoriteProductService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        LanguageService $languageService,
        SupercategoryService $superCategoryService,
        ThreadService $threadService,
        NewsService $newsService,
        SliderService $sliderService,
        Oleb2bImageService $oleb2bImageService
    ) {

        $countries = $countryService->getAllEnableForSuppliers();
        $featuredProducts = $productService->featuredProducts(25);

        if (Auth::check() && !Auth::user()->suppliers->isEmpty()) {
            $myProducts = $productService->getProductsBySupplier(Auth::user()->supplier, 999999);
        } else {
            $myProducts = collect([]);
        }

        $allCountries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $languages = $languageService->all();
        $superCategories = $superCategoryService->all();
        $authUser = Auth::check();

        if ($authUser) {
            $favoritesProducts = $favoriteProductService->getFavoritesByUser(Auth::id());
            $threads = $threadService->getThreadsWithProductsByBuyerUser(Auth::id());

        } else {
            $favoritesProducts = collect([]);
            $threads = collect([]);
        }

        $show = $request->get('show', null);
        $status = $request->get('status', null);

        $news = $newsService->all()->sortByDesc('created_at');

        $sliders = $sliderService->getByLang(5);
        $backgroundImages = $oleb2bImageService->getBySection(Oleb2bImage::SECTION_BACKGROUND);
        $partnersImages = $oleb2bImageService->getBySection(Oleb2bImage::SECTION_PARTNERS);
        $eventsImages = $oleb2bImageService->getBySection(Oleb2bImage::SECTION_EVENTS);
        $cclImages = $oleb2bImageService->getBySection(Oleb2bImage::SECTION_CCL);

        return view('pages.home',
            compact('countries', 'featuredProducts', 'favoritesProducts', 'authUser', 'allCountries',
                'measurements', 'paymentTerms', 'languages', 'superCategories', 'show', 'status', 'myProducts',
                'productsInThreads', 'threads', 'news', 'sliders', 'backgroundImages', 'partnersImages', 'eventsImages',
                'cclImages'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories()
    {
        $superCategories = $this->superCategoryService->all();

        return view('pages.categorias', compact('superCategories'));
    }

    /**
     * @param $slug
     * @param Request $request
     * @param ProductService $productService
     * @param FavoriteProductService $favoriteProductService
     * @param MeasureService $measureService
     * @param PaymentTermService $paymentTermService
     * @param LanguageService $languageService
     * @param CountryService $countryService
     * @param ThreadService $threadService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function singleProduct(
        $slug,
        Request $request,
        ProductService $productService,
        FavoriteProductService $favoriteProductService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        LanguageService $languageService,
        CountryService $countryService,
        ThreadService $threadService
    ) {
        $product = $productService->findBySlug($slug);

        if (!$product) {
            Aviso::modelNotFound();
            return redirect()->route('user.dashboard');
        }
        $allCountries = $countryService->all();
        $allMeasurements = $measureService->all();
        $allPaymentTerms = $paymentTermService->all();
        $allLanguages = $languageService->all();
        $deliveryTerms = $product->deliveryTerms;
        $paymentTerms = $product->paymentTerms;
        $user = $product->user;
        $supplier = $user->supplier;
        $productsBySupplier = $productService->getProductsBySupplier($supplier, 5, $product->id);
        $relatedProducts = $productService->getRelatedProducts($product->subcategory, 10, $product->id);
        $authUser = Auth::check();

        if (Auth::check() && !Auth::user()->suppliers->isEmpty()) {
            $myProducts = $productService->getProductsBySupplier(Auth::user()->supplier, 999999);
            $isMyProduct = $myProducts->contains('id', $product->id);
        } else {
            $myProducts = collect([]);
            $isMyProduct = false;
        }

        if ($authUser) {
            $favoritesProducts = $favoriteProductService->getFavoritesByUser(Auth::id());
            $threads = $threadService->getThreadsWithProductsByBuyerUser(Auth::id());

        } else {
            $favoritesProducts = collect([]);
            $threads = collect([]);
        }

        $show = $request->get('show', null);

        return view('pages.singleProduct',
            compact('product', 'deliveryTerms', 'paymentTerms', 'supplier', 'productsBySupplier',
                'relatedProducts', 'favoritesProducts', 'authUser', 'allCountries', 'allMeasurements',
                'allPaymentTerms', 'allLanguages', 'isMyProduct', 'threads', 'myProducts', 'show', 'user'));
    }

    /**
     * @param BuyingRequestService $buyingRequestService
     * @param Request $request
     * @param CountryService $countryService
     * @param MeasureService $measureService
     * @param LanguageService $languageService
     * @param PaymentTermService $paymentTermService
     * @param SavedBuyingRequestService $savedBuyingRequestService
     * @param ThreadService $threadService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buyingRequests(
        BuyingRequestService $buyingRequestService,
        Request $request,
        CountryService $countryService,
        MeasureService $measureService,
        LanguageService $languageService,
        PaymentTermService $paymentTermService,
        SavedBuyingRequestService $savedBuyingRequestService,
        ThreadService $threadService
    ) {

        $search = $request->get('q', '');
        $buyingRequests = $buyingRequestService->allApprovedPaginate(4, $search);
        $allCountries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $languages = $languageService->all();
        $authUser = Auth::check();

        if ($authUser) {
            $savedBuyingRequest = $savedBuyingRequestService->getSavedBuyingRequestByUser(Auth::id());
            $threads = $threadService->getThreadsWithBuyingRequestByBuyerUser(Auth::id());
            $myBuyingRequest = $buyingRequestService->getUserBuyingRequest(Auth::id());
        } else {
            $savedBuyingRequest = collect([]);
            $threads = collect([]);
            $myBuyingRequest = collect([]);
        }

        $show = $request->get('show', null);

        return view('pages.buyingRequests',
            compact('buyingRequests', 'search', 'allCountries', 'measurements', 'paymentTerms', 'languages',
                'savedBuyingRequest', 'authUser', 'threads', 'myBuyingRequest', 'show'));

    }

    /**
     * @param string $q
     * @param string $currentCountries
     * @param Request $request
     * @param ProductService $productService
     * @param CountryService $countryService
     * @param FavoriteProductService $favoriteProductService
     * @param MeasureService $measureService
     * @param PaymentTermService $paymentTermService
     * @param LanguageService $languageService
     * @param ThreadService $threadService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(
        string $q,
        string $currentCountries,
        Request $request,
        ProductService $productService,
        CountryService $countryService,
        FavoriteProductService $favoriteProductService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        LanguageService $languageService,
        ThreadService $threadService
    ) {
        $allCountries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $languages = $languageService->all();

        /*$categoryId = $request->get('category', null);
        if ($categoryId) {
            $this->dispatch(new SetVisitedCategoryJob($categoryId));
        }*/

        $authUser = Auth::check();

        if ($authUser && !Auth::user()->suppliers->isEmpty()) {
            $myProducts = $productService->getProductsBySupplier(Auth::user()->supplier, 999999);
        } else {
            $myProducts = collect([]);
        }

        if ($authUser) {
            $favoritesProducts = $favoriteProductService->getFavoritesByUser(Auth::id());
            $threads = $threadService->getThreadsWithProductsByBuyerUser(Auth::id());
        } else {
            $favoritesProducts = collect([]);
            $threads = collect([]);
        }

        $searchResult = $productService->searchApprovedPaginate($q, $currentCountries, $request->get('c'), 10);
        $products = $searchResult->getProducts();
        $categoriesInSearch = $searchResult->getCategories();
        $countriesInSearch = $searchResult->getCountries();

        return view('pages.products-search',
            compact('products', 'featuredProducts', 'search', 'categoriesInSearch', 'countriesInSearch', 'myProducts',
                'authUser', 'favoritesProducts', 'allCountries', 'measurements', 'paymentTerms', 'languages', 'threads',
                'q', 'currentCountries'));

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function buildSearch(Request $request)
    {
        $countries = trans('common.all');
        $q = empty($request->get('q')) ? trans('dashboard.products') : $request->get('q');
        return redirect()->route('portal.search', ['q' => $q, 'countries' => $countries]);
    }

    /**
     * @param contactBuyerRequest $request
     * @param BuyingRequestService $buyingRequestService
     * @param ThreadService $threadService
     * @param MessengerService $messengerService
     * @param MessageService $messageService
     * @param FileService $fileService
     * @param AttachmentService $attachmentService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postContactBuyer(
        contactBuyerRequest $request,
        BuyingRequestService $buyingRequestService,
        ThreadService $threadService,
        MessengerService $messengerService,
        MessageService $messageService,
        FileService $fileService,
        AttachmentService $attachmentService
    ) {
        try {

            if ($request->hasFile('file')) {
                $path = $fileService->storeContactSupplier($request->file('file'), Auth::id());
            } else {
                $path = null;
            }

            DB::beginTransaction();
            $buyingRequest = $buyingRequestService->find($request->get('buyingRequest_id'));

            $thread = $threadService->create([
                'type' => Constants::THREAD_HAS_BUYING_REQUEST,
                'from_user_id' => Auth::id(),
                'to_user_id' => $buyingRequest->user_id
            ]);

            $thread->buyingRequests()->attach($buyingRequest->id, ['message' => $request->get('message')]);

            $newMessage = [
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'body' => $request->get('message', trans('products.greeting_buying_request'))
            ];

            $message = $messageService->create($newMessage);

            if ($path != null) {
                $newAttachment = [
                    'message_id' => $message->id,
                    'path' => $path,
                    'type' => $request->file('file')->getClientOriginalExtension(),
                    'name' => $request->file('file')->getClientOriginalName(),
                ];

                $attachmentService->create($newAttachment);

            }

            $url = route('user.messages.buying-request.index',
                ['buying-request' => $buyingRequest->id, 'thread' => $thread->id]);

            $messengerService->sendMessageToBuyer($thread->toUser, Lang::locale(), $url);
            DB::commit();
            Aviso::success(trans('alerts.messages.message_send'));
        } catch (Exception $e) {

            DB::rollback();
            Log::error($e);
            Aviso::error(trans('alerts.messages.message_not_send'));
            return redirect()->back();
        }

        return redirect()->route('user.messages.buying-request.index',
            ['buying-request' => $buyingRequest->id, 'thread' => $thread->id]);
    }

    /**
     * @param ContactSupplierRequest $request
     * @param ProductService $productService
     * @param MessengerService $messengerService
     * @param ThreadService $threadService
     * @param FileService $fileService
     * @param MessageService $messageService
     * @param AttachmentService $attachmentService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postContactSupplier(
        ContactSupplierRequest $request,
        ProductService $productService,
        MessengerService $messengerService,
        ThreadService $threadService,
        FileService $fileService,
        MessageService $messageService,
        AttachmentService $attachmentService
    ) {
        try {

            if ($request->hasFile('file')) {
                $path = $fileService->storeContactSupplier($request->file('file'), Auth::id());
            } else {
                $path = null;
            }

            $product = $productService->find($request->get('product_id'));

            DB::beginTransaction();

            $thread = $threadService->create([
                'type' => Constants::THREAD_HAS_PRODUCT,
                'from_user_id' => Auth::id(),
                'to_user_id' => $product->user_id
            ]);

            $thread->products()->attach($product->id, [
                'country_id' => $request->get('product_id'),
                'measure_id' => $request->get('measure_id'),
                'ports' => $request->get('port', null),
                'order_qty' => $request->get('order_qty'),
                'message' => $request->get('description'),
                'payment_terms' => json_encode($request->get('payment_terms')),
                'files' => json_encode($path)
            ]);

            $newMessage = [
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'body' => $request->get('description', trans('products.greeting_contact_supplier'))
            ];

            $message = $messageService->create($newMessage);

            if ($path != null) {

                $newAttachment = [
                    'message_id' => $message->id,
                    'path' => $path,
                    'type' => $request->file('file')->getClientOriginalExtension(),
                    'name' => uniqid('File_' . Auth::id() . '_'),
                ];

                $attachmentService->create($newAttachment);

            }

            $url = route('user.messages.products.index', ['product' => $product->id, 'thread' => $thread->id]);
            $messengerService->sendMessageToSupplier($thread->toUser, Lang::locale(), $url);
            DB::commit();
            Aviso::success(trans('alerts.messages.message_send'));
            return redirect()->route('user.messages.products.index',
                ['product' => $product->id, 'thread' => $thread->id]);

        } catch (Exception $e) {

            DB::rollback();
            Log::error($e);
            Aviso::error(trans('alerts.messages.message_not_send'));
        }

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('pages.contact');
    }

    /**
     * @param Request $request
     * @param EmailService $emailService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postContact(Request $request, EmailService $emailService)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $subject = $request->get('subject', 'Mensaje desde oleb2b.com');
        $message = $request->get('message');
        $emailService->sendContactForm($name, $email, $subject, $message);
        Aviso::guardado('Mensaje enviado!');

        return redirect()->route('portal.contact');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function termsUse()
    {
        return view('pages.terms_of_use.' . Lang::locale());
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function PrivacyPolitics()
    {
        return view('pages.privacy_politics.' . Lang::locale());
    }

    /**
     * @param MembershipService $membershipService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function memberships(MembershipService $membershipService)
    {
        if (Auth::check()) {
            return redirect()->route('user.memberships.index');
        }

        $memberships = $membershipService->all();
        $advancedMembership = $memberships->find(2);
        $premiumMembership = $memberships->find(3);

        return view('pages.memberships', compact('advancedMembership', 'premiumMembership'));
    }

    /**
     * @param $idCertification
     * @param FileService $fileService
     * @param CertificationService $certificationService
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadCertificate(
        $idCertification,
        FileService $fileService,
        CertificationService $certificationService
    ) {
        try {
            $certification = $certificationService->find($idCertification);

            if (!$certification) {
                Aviso::error(trans('alerts.messages.attachment_not_found'));
            }

            $path = public_path($certification->image);

            return $fileService->downloadFile($path,
                $certification->supplier->company_name . '.' . $certification->image_extension);

        } catch (FileNotFoundException $e) {
            Log::error($e);
        } catch (Exception $e) {
            Log::error($e);
        }

        Aviso::error(trans('alerts.messages.attachment_not_found'));
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view('pages.about_us.' . Lang::locale());
    }

    /**
     * @param $token
     * @param $email
     * @param UserService $userService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function emailConfirmation($token, $email, UserService $userService)
    {
        $user = $userService->findByEmail($email);

        if (!$user) {
            Aviso::error(trans(''));
            return redirect()->route('portal.home');
        }

        if ($user->email_confirmacion == $token) {
            $user->status = Constants::USER_STATUS_ACTIVE;//Pasa a estar activo
            $user->save();
            Aviso::guardado(trans('alerts.validateEmail.message_confirm'));

        } else {
            Aviso::guardado('Enlace no valido');
        }

        return redirect()->route('portal.home');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resendEmailConfirmation()
    {
        $user = Auth::user();
        $user->notify(new WelcomeToOleb2b($user));

        return redirect()->back();
    }
}
