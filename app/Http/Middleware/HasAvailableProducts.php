<?php namespace App\Http\Middleware;

use App\Library\Aviso;
use Closure;
use Illuminate\Support\Facades\Auth;

class HasAvailableProducts
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $membership = Auth::user()->activeMembership();

        if($membership && $membership->pivot->product_posting == 0){
            Aviso::error(trans('alerts.middlewares.has_not_product_available'));
            return redirect()->back();
        }

        return $next($request);
    }
}
