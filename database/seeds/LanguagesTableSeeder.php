<?php

use App\Repositories\Language\Language;
use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create([
            'id'=>1,
            'original_name'=>'English',
            'abbreviation'=>'en',
            'es_name'=>'Ingles',
            'en_name'=>'English',
            'ru_name'=>'английский'
        ]);

        Language::create([
            'id'=>2,
            'original_name'=>'Español',
            'abbreviation'=>'es',
            'es_name'=>'Español',
            'en_name'=>'Spanish',
            'ru_name'=>'испанский'
        ]);

        Language::create([
            'id'=>3,
            'original_name'=>'Русский ',
            'abbreviation'=>'ru',
            'es_name'=>'Ruso',
            'en_name'=>'Russian',
            'ru_name'=>'Русский'
        ]);

        Language::create([
            'id'=>4,
            'original_name'=>'Português',
            'abbreviation'=>'pt-BR',
            'es_name'=>'Portugues',
            'en_name'=>'Russian',
            'ru_name'=>'Русский'
        ]);
    }
}
