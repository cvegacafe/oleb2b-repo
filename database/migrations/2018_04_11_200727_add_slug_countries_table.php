<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlugCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->string('ru_slug')->after('enabled_for_supplier')->nullable();
            $table->string('en_slug')->after('enabled_for_supplier')->nullable();
            $table->string('es_slug')->after('enabled_for_supplier')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('ru_slug');
            $table->dropColumn('en_slug');
            $table->dropColumn('es_slug');
        });
    }
}
