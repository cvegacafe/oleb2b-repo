<?php namespace App\Repositories\Subcategory;

use App\Repositories\Category\Category;
use App\Repositories\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class Subcategory
 * @package App\Repositories\Subcategory
 */
class Subcategory extends Model
{
    /**
     * @var string
     */
    public $table = 'subcategories';
    /**
     * @var array
     */
    public $fillable = ['es_name', 'en_name', 'ru_name', 'category_id','visited'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }
}
