<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\Slider\Slider;
use App\Services\FileService;
use App\Services\SliderService;
use Illuminate\Http\Request;

/**
 * Class SlidersController
 * @package App\Http\Controllers\Admin
 */
class SlidersController extends Controller
{
    /**
     * @var SliderService
     */
    private $sliderService;

    /**
     * SlidersController constructor.
     */
    public function __construct()
    {
        $this->sliderService = new SliderService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $sliders = $this->sliderService->paginate();
        return view('admin.sliders.index', compact('sliders'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($file = $request->file('image')) {
                $disc = 'sliders';
                $path_res = $file->storePubliclyAs('/' . 0,
                    uniqid(0 . '_' . time() . '_') . '.' . $file->getClientOriginalExtension(), $disc);

                $path = 'uploads/images/' . $disc . '/' . $path_res;
                $request['path'] = $path;
                (new SliderService())->create($request->all());
            }
        } else {
            Aviso::noGuardado();
        }
        return redirect()->route('admin.sliders.index');
    }

    /**
     * @param Slider $slider
     * @param FileService $fileService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Slider $slider, FileService $fileService)
    {
        $fileService->removeFile($slider->path);
        $slider->delete();
        return redirect()->route('admin.sliders.index');
    }
}
