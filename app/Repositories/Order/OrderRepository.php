<?php namespace App\Repositories\Order;

use App\Repositories\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Order();
        parent::__construct();
    }
}