@extends('layouts.pages')
@section('title')
    {{ ucfirst($product->name) }}
@endsection
@section('description')
    {{ str_limit($product->description,180) }}
@endsection
@section('content')
    <div class="single-product-title category{{$product->subcategory->category->supercategory->id}}">
        <h2>
            <a href="">{{$product->subcategory->category->supercategory->name}}</a>
            <small><a href="#">{{trans('portal.categories')}} ></a></small>
        </h2>
    </div>
    <section class="container product-description">
        <div class="row">
            <div class="col-xs-12 col-md-6 product-description__carousel">
                <div class="images">
                    @if(isset($product->pictures[1]))
                        <div class="nav">
                            @if(isset($product->pictures[0]))
                                @foreach($product->pictures as $picture)
                                    <figure class="nav__photo">
                                        <img src="{{asset($picture)}}" alt="" class="img-responsive">
                                    </figure>
                                @endforeach
                            @else
                                <figure class="nav__photo">
                                    <img src="{{asset($product->main_photo)}}" alt="" class="img-responsive">
                                </figure>
                            @endif
                        </div>
                    @endif
                    <div class="gallery {{isset($product->pictures[1]) ? '' : 'alone'}}">
                        @if(isset($product->pictures[0]))
                            @foreach($product->pictures as $picture)
                                <figure>
                                    <img src="{{asset($picture)}}" alt="" class="img-responsive">
                                </figure>
                            @endforeach
                        @else
                            <figure>
                                <img src="{{asset($product->main_photo)}}" alt="" class="img-responsive">
                            </figure>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="descripcion">
                            <h4>{{trans('products.description')}}:</h4>
                            <p>{{$product->description}}</p>
                            <div class="toggle">
                                <span class="toggle__show">ver más</span>
                                <span class="toggle__hide">ver menos</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="row detalle">
                    <div class="col-xs-12 col-md-6">
                        <h1>{{$product->name}}</h1>
                        @if(auth()->guard('admin')->check())
                            <a target="_blank" href="{{ route('products.show', $product->id) }}" class="">Ver Producto
                                <i class="fa fa-external-link"></i> </a>
                        @endif
                        <span class="precio"><span
                                    class="text-roboto">{{$product->price}} </span>{{$product->currency->currency_code}}
                            / {{$product->saleUnitMeasure->name}}</span>
                        <ul>
                            <li><strong>{{trans('products.place_origin')}}:</strong> {{$product->country->name}}</li>
                            <li><strong>{{trans('products.moq')}}
                                    :</strong> {{$product->moq}} {{$product->moqMeasure->name}}</li>
                            <li><strong>{{trans('products.delivery_time')}} ({{trans('common.days')}}
                                    ):</strong> {{$product->delivery_time}}</li>
                            <li><strong>{{trans('products.type')}}:</strong> {{$product->type}}</li>
                            <li><strong>{{trans('products.supply_ability')}}
                                    :</strong> {{$product->supply_ability}} {{$product->supplyMeasure->name}}
                                / {{$product->supply_ability_frecuency}}</li>
                            <li><strong>{{trans('products.color')}}:</strong> {{$product->color}}</li>
                            @if($product->ports && is_array($product->ports))
                                <li><strong>{{trans('products.ports')}}:</strong> {{implode(", ",$product->ports)}}</li>
                            @endif
                        </ul>
                        @if($product->is_organic)
                            <figure class="is_organic">
                                <img src="{{asset('assets/pages/img/organic_vf.png')}}">
                            </figure>
                        @endif
                    </div>
                    <div class="col-xs-12 col-md-6 empresa">
                        <div class="toggle">
                            <span>{{trans('portal.company_details')}}</span>
                            <i class="fa fa-caret-down"></i>
                        </div>
                        <div class="datos">
                            <h1>{{$supplier->company_name}}</h1>
                            @if(auth()->guard('admin')->check())
                                <a target="_blank" href="{{ route('users.show',$user) }}" class="">Ver Proveedor <i
                                            class="fa fa-external-link"></i> </a>
                            @endif
                            <ul>
                                <li><strong>{{trans('user.form.country')}}:</strong> {{$supplier->country->name}}</li>
                                <li><strong>{{trans('products.delivery_terms')}}
                                        :</strong> {{$deliveryTerms->implode('name',' ,')}}</li>
                                <li><strong>{{trans('products.payment_terms')}}
                                        :</strong> {{$paymentTerms->implode('name',' ,')}}</li>
                                <li><strong>{{trans('products.sale_manager_name')}}
                                        : </strong> {{$product->user->names}} {{$product->user->last_names}}</li>
                                <li><strong>{{trans('products.office_number')}}:</strong> <span
                                            class="text-roboto">{{$product->user->office_phone}}</span></li>
                                <li><strong>{{trans('products.mobile_number')}}:</strong> <span
                                            class="text-roboto">{{$product->user->mobile_phone}}</span></li>
                                <li><strong>{{trans('products.languages')}}
                                        :</strong> {{$product->user->languages->implode('abbreviation',', ')}}</li>
                                @if(!$product->user->markets->isEmpty())
                                    <li><strong>{{trans('products.markets')}}
                                            :</strong> {{$product->user->markets->implode('name',', ')}}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <hr>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                  <span class="precio2"><small>{{trans('products.price')}}:</small>
                                      @if($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_FIXED)
                                          <span class=" text-roboto">{{$product->price}}</span>
                                          {{$product->currency->currency_code}} / {{$product->supplyMeasure->name}}
                                      @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_ASK)
                                          {{trans('priceTypes.ask_seller')}}
                                      @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_RANGE)
                                          <span class=" text-roboto">{{$product->price_range}}</span>
                                          {{$product->currency->currency_code}} / {{$product->supplyMeasure->name}}
                                      @endif
                              </span>
                            </div>
                            <div class="col-xs-12 col-md-6 botones">
                                @if($isMyProduct)
                                    <a href="{{route('user.products.edit',$product)}}">
                                        <button>
                                            {{trans('products.edit_product')}}
                                        </button>
                                    </a>
                                @elseif($threads->contains('product_id',$product->id))
                                    <a href="{{route('user.messages.products.index', ['product' => $product->id, 'thread' => $threads->where('product_id',$product->id)->first()->id])}}">
                                        <button>
                                            {{trans('menus.user.messages')}}
                                        </button>
                                    </a>
                                @else
                                    <button onClick="contactWithSupplier('{{$product->id}}','{{$product->name}}','{{asset($product->main_photo)}}','{{$product->user->languages->implode(\Lang::locale().'_name',' ,')}}')"
                                            id="contactWithSupplier">
                                        {{trans('portal.contact_supplier')}}
                                    </button>
                                    <div class="botones">
                                        <div class="botones__guardar">
                                            <a class="toggleFavorite">{{ trans('portal.save_for_later') }}</a>
                                            <button class="contacta toggleFavorite {{ auth()->check() && $favoritesProducts->contains('product_id', $product->id) ? 'active': '' }}"
                                                    data-product-id="{{$product->id}}">
                                                <i class="fa fa-heart"></i>
                                            </button>
                                        </div>

                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="company-details">
        <div class="nav">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">{{trans('portal.company_details')}}</a>
                            </li>
                            @if($supplier->images)
                                <li><a data-toggle="tab" href="#menu1">{{trans('portal.photos_and_logos')}}</a></li>
                            @endif
                            @if($supplier->factoryDetail)
                                <li><a data-toggle="tab" href="#menu2">{{trans('portal.factory_details')}}</a></li>
                            @endif
                            @if($supplier->certifications)
                                <li><a data-toggle="tab"
                                       href="#menu3">{{trans('portal.certifications_and_trademarks')}}</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <div class="row">
                        <div class="col-xs-12 col-md-8">
                            <ul class="detalles">
                                <li>
                                    <strong>{{trans('common.forms.suppliers.company_name')}}</strong>: {{$supplier->company_name}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.company_description')}}</strong>: {{$supplier->CompanyDescription}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.location')}}</strong>: {{$supplier->country->name}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.company_legal_address')}}</strong>: {{$supplier->company_legal_address}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.postal_code')}}</strong>: {{$supplier->postal_code}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.tax_number')}} </strong>: {{$supplier->taxtype->name}} {{$supplier->tax_number}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.company_type')}}</strong>: {{$supplier->companyType->name}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.member_trade_organization')}}</strong>: {{$supplier->member_trade_organization}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.year_company_registered')}}</strong>: {{$supplier->year_company_registered}}
                                </li>
                                <li>
                                    <strong>{{trans('common.forms.suppliers.number_employees')}}</strong>:{{$supplier->number_employees}}
                                </li>
                                <li><strong>{{trans('common.forms.suppliers.website')}}</strong>: <a
                                            href="{{$supplier->website}}" target="_blank">{{$supplier->website}}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-8 col-xs-offset-2 col-md-offset-0 col-md-4">
                            <div class="membership">
                                @php
                                    $membershipName = $supplier->users->where('pivot.is_principal',true)->first()->activeMembership()->en_name ?? 'Basic';
                                @endphp
                                <img src="{{asset('assets/pages/img/icon_'.$membershipName.'.jpg')}}"
                                     alt="">
                                <h5>{{trans('memberships.'.strtolower($membershipName))}}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade fotos-logos">
                    @if($supplier->images)
                        @foreach($supplier->images as $image)
                            <figure class="showInLightbox">
                                <img src="{{$image}}">
                            </figure>
                        @endforeach
                    @endif
                </div>
                <div id="menu2" class="tab-pane fade">
                    @if($supplier->factoryDetail)
                        <ul class="detalles">
                            <li>
                                <strong>{{trans('common.forms.suppliers.factory_address')}}</strong>: {{$supplier->factoryDetail->factory_address}}
                            </li>
                            <li>
                                <strong>{{trans('common.forms.suppliers.factory_size')}}</strong>: {{$supplier->factoryDetail->factory_size}}
                            </li>
                            <li>
                                <strong>{{trans('common.forms.suppliers.contract_manufacturing')}}</strong>: {{$supplier->factoryDetail->oem_service_offered?trans('common.forms.suppliers.oem_service_offered'):''}} {{$supplier->factoryDetail->design_service_offered?trans('common.forms.suppliers.design_service_offered'):''}}
                            </li>
                            <li>
                                <strong>{{trans('common.forms.suppliers.qty_employees_QC')}}</strong>: {{$supplier->factoryDetail->number_qc_staff}}
                            </li>
                            <li>
                                <strong>{{trans('common.forms.suppliers.qty_employees_product_design')}}</strong>: {{$supplier->factoryDetail->number_rd_staff}}
                            </li>
                            <li>
                                <strong>{{trans('common.forms.suppliers.qty_employees_production_lines')}}</strong>: {{$supplier->factoryDetail->number_production_lines}}
                            </li>
                            <li>
                                <strong>{{trans('common.forms.suppliers.qty_annual_output_value')}}</strong>: {{$supplier->factoryDetail->annual_output_value ? $supplier->factoryDetail->annual_output_value : 0}}
                                USD
                            </li>
                        </ul>
                    @endif
                </div>
                <div id="menu3" class="tab-pane fade certificaciones">
                    <div class="row">
                        @foreach($supplier->certifications as $certification)
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <ul class="detalles">
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.type_certification')}}</strong>: {{$certification->type_certification}}
                                            </li>
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.issued_by')}}</strong>:{{$certification->issued_by}}
                                            </li>
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.start_date')}}</strong>:{{$certification->start_date}}
                                            </li>
                                            <li>
                                                <strong>{{trans('common.forms.suppliers.expiration_date')}}</strong>: {{$certification->expiration_date}}
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="certificado-img">
                                            <figure>
                                                <img src="{{asset('assets/pages/img/'.$certification->image_extension.'.png')}}">
                                                <a href="{{route('web.certificate.download',$certification->id)}}"><span><i
                                                                class="fa fa-download"></i></span></a>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="other">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3>{{trans('portal.more_products_from_seller')}}</h3>
                </div>
            </div>
            <div class="row">
                <div class="{{$productsBySupplier->isEmpty()?'':'carrousel-related-product'}}">
                    @forelse($productsBySupplier as $product)
                        <div class="item">
                            <a href="{{route('portal.singleProduct',$product->slug)}}">
                                <div class="producto">
                                    <div class="body">
                                        <div class="nombre titulo-3" title="{{$product->name}}">
                                            {{ str_limit($product->name, 40) }}
                                        </div>
                                        <figure class="thumb">
                                            <img src="{{asset($product->main_photo)}}" alt="" class="img-responsive">
                                        </figure>
                                        <div class="minimo titulo-5">
                                            {{trans('products.moq')}}: {{$product->moq}} {{$product->moqMeasure->name}}
                                        </div>
                                        <div class="proveniencia">
                                            {{$product->country_name}}
                                        </div>
                                        <div class="precio text-roboto">
                                            @if($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_FIXED)
                                                {{$product->price}}
                                            @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_ASK)
                                                {{trans('priceTypes.ask_seller')}} <br>
                                            @elseif($product->price_type == \App\Repositories\Product\Product::TYPE_PRICE_RANGE)
                                                {{$product->price_range}}
                                            @endif

                                            {{$product->currency->currency_code}}
                                            / {{$product->saleUnitMeasure->name}}
                                        </div>
                                        <div class="origin">
                                            <small>
                                                <strong>
                                                    {{trans('products.place_origin')}}: {{$product->country->name}}
                                                </strong>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                        <div class="col-xs-12 text-center">
                            <h4>{{trans('products.products_by_supplier_not_found')}}</h4>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <section class="destacados">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <h3>{{trans('portal.related_products')}}</h3>
                </div>
            </div>
            <div class="row">
                @forelse($relatedProducts as $product)
                    <div class="col-xs-12 col-xsm-6 col-md-4 col-lg-15">
                        <div class="producto">
                            <div class="header">
                                @if($authUser)
                                    @if($myProducts->contains('id',$product->id))
                                        <a href="{{route('user.products.edit',$product)}}">
                                            <button class="edit">
                                                {{trans('products.edit_product')}}
                                            </button>
                                        </a>
                                    @elseif($threads->contains('product_id',$product->id))
                                        <a href="{{route('user.messages.products.index', ['product' => $product->id, 'thread' => $threads->where('product_id',$product->id)->first()->id])}}">
                                            <button class="edit">
                                                {{trans('menus.user.messages')}}
                                            </button>
                                        </a>
                                    @else
                                        <button onclick="contactWithSupplier('{{$product->id}}','{{$product->name}}','{{asset($product->main_photo)}}','{{$product->user->languages->implode(\Lang::locale().'_name',' ,')}}')">
                                            {{trans('portal.contact_supplier')}}
                                        </button>
                                    @endif

                                    @if($favoritesProducts->contains('product_id',$product->id))
                                        <button data-product-id="{{$product->id}}" class="toggleFavorite active"><i
                                                    class="fa fa-heart"></i></button>
                                    @else
                                        <button data-product-id="{{$product->id}}" class="toggleFavorite"><i
                                                    class="fa fa-heart"></i></button>
                                    @endif
                                @else
                                    <button onclick="showAuth()">{{trans('portal.contact_supplier')}}</button>
                                    <button class="toggleFavorite" data-product-id="{{$product->id}}"><i
                                                class="fa fa-heart"></i></button>
                                @endif
                            </div>
                            <a href="{{route('portal.singleProduct',$product->slug)}}">
                                <div class="body">
                                    <div class="nombre titulo-3" title=" {{$product->name}}">
                                        {{ str_limit($product->name, 40) }}
                                    </div>
                                    <figure class="thumb">
                                        <img src="{{asset($product->main_photo)}}" alt="{{$product->name}}"
                                             class="img-responsive">
                                    </figure>
                                    <div class="minimo titulo-5">
                                        {{trans('products.moq')}}: {{$product->moq}} {{$product->moqMeasure->name}}
                                    </div>
                                    <div class="proveniencia">
                                        {{$product->country_name}}
                                    </div>
                                    <div class="precio">
                                        {{$product->price}} {{$product->currency->currency_code}}
                                        x {{$product->saleUnitMeasure->name}}
                                    </div>
                                    <div class="origin">
                                        <small>
                                            <strong>
                                                {{trans('products.place_origin')}}: {{$product->country->name}}
                                            </strong>
                                        </small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @empty
                    <div class="col-xs-12 text-center">
                        <h4>{{trans('products.related_products_not_found')}}</h4>
                    </div>
                @endforelse
            </div>
        </div>
    </section>
    @include('pages.includes.supplier-contact-popup',[
    'countries' => $allCountries,
    'measurements'=>$allMeasurements,
    'paymentTerms' => $allPaymentTerms,
    'languages' => $allLanguages
    ])
@endsection
@section('scripts')
    <script>
        function contactWithSupplier(product_id, product_name, product_photo, languages) {
            if (IS_AUTH) {
                let modal = $('#SupplierModal');
                modal.find('#product_id').val(product_id);
                modal.find('#product_name').text(product_name);
                modal.find('#product_photo').attr('src', product_photo);
                modal.find('#input_product_photo').val(product_photo);
                modal.find('#languages').text(languages);
                modal.modal();
            } else {
                showAuth();
            }
        }

        $(function () {
            var max_chars = 250;

            if (window.location.hash === '#contact') {
                $('#contactWithSupplier').click();
            }
            $('#description').keyup(function () {
                var chars = $(this).val().length;
                var diff = max_chars - chars;
                $('#contador').html(diff);
            });
        })
    </script>
@endsection