<?php
return [
    'welcome_to_oleb2b' => 'Добро пожаловать на Oleb2b.com',
    'new_buying_request' => 'Nueva oportunidad de venta!',
    'change_password_title' => 'Для изменения пароля перейдите по ссылке и следуйте указаниям системы.',
    'change_password_btn' => 'Замена пароля',
    'change_password_message' => 'Если Вы не можете нажать на кнопку «Замена пароля», скопируйте и вставьте ссылку ниже в строку Вашего браузера',
    'button_not_working'=>'Не работает кнопка? Вставьте ссылку в Ваш браузер',
    'oleb2b_team'=>'Команда OleB2B',
    'regards'=>'С уважением,',
    'mail_confirm' => [
        'title' => 'Добро пожаловать на Oleb2b.com<br><br> Пожалуйста, подтвердите Ваш адрес эл. почты по ссылке ',
        'action_btn' => 'Подтвердить'
    ],
    'mail_purchase' => [
        'subject' => 'Подтверждение Категории Поставщика',
        'title' => 'Ваш пакет услуг <strong>:membershipName</strong> <br><br> Просим Вас отправить инструкции по изготовлению рекламного Баннера по адресу :',
        'action_btn' => 'Dashboard',
        'banner_message' => 'Рекламные баннеры будут размещены на сайте в течение 72 часов после получения Ваших инструкций и подтверждения пакета услуг.<br> Спасибо, что Вы с нами!'
    ],
    'mail_message' => [
        'title' => 'Вам пришло сообщение на сайте OleB2B.com.',
        'action_btn' => 'Прочитать'
    ],
    'buying_request' => [
        'subject' => 'Новый запрос КП в Категории :supercategoryName на  OleB2B.com.',
        'title' => 'Новый запрос КП в категории <b>:supercategoryName</b> на  OleB2B.com.<br><br> Для просмотра всех запросов КП, зайдите на www.OleB2B.com в раздел ЗАПРОСЫ КП.',
        'action_btn' => 'Перейти на OleB2B.com'
    ]
];