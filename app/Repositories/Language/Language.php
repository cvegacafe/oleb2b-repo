<?php namespace App\Repositories\Language;

use App\Repositories\Country\Country;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Language extends Model
{
    public $table = 'languages';
    public $fillable = ['name', 'abbreviation'];

    public function countries()
    {
        $this->hasMany(Country::class);
    }

    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }
}
