<?php namespace App\Notifications;

use App\Repositories\BuyingRequest\BuyingRequest;
use App\Repositories\Supercategory\Supercategory;
use App\Repositories\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewBuyingRequestForSuppliers extends Notification
{
    use Queueable;

    private $buyingRequest, $lang, $supercategory, $user;

    /**
     * Create a new notification instance.
     * @param Supercategory $supercategory
     * @param BuyingRequest $buyingRequest
     * @param $lang
     * @param User $user
     */
    public function __construct(Supercategory $supercategory, BuyingRequest $buyingRequest, $lang, User $user)
    {
        $this->buyingRequest = $buyingRequest;
        $this->lang = $lang;
        $this->supercategory = $supercategory;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $actionUrl = url($this->lang . '/buying-request') . '?q=' . $this->buyingRequest->nameByLang($this->lang);
        $supercategoryName = $this->supercategory->nameByLang($this->lang);
        $userName = $this->user->names;
        return (new MailMessage)->view('mails.new-buying-request',
            compact('actionUrl', 'supercategoryName', 'userName'))->subject(trans('mails.buying_request.subject').' - OleB2B.com');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
