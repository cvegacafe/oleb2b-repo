<?php namespace App\Jobs;

use App\Services\CategoryService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetVisitedCategoryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $categoryId;

    /**
     * Create a new job instance.
     * @param $categoryId
     */
    public function __construct($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Execute the job.
     *
     * @param CategoryService $categoryService
     */
    public function handle(CategoryService $categoryService)
    {
        $category = $categoryService->find($this->categoryId);

        if($category){

            $category->visited = $category->visited + 1;

            $category->save();
        }

    }
}
