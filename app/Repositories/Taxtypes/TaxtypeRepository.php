<?php namespace App\Repositories\Taxtypes;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:12 PM
 */
use App\Repositories\BaseRepository;

class TaxtypeRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Taxtypes();
        parent::__construct();
    }
}