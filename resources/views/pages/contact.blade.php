@extends('layouts.pages')
@section('title')
    Contact
@endsection
@section('content')
    <div class="container-fluid contact-top">
        <div class="col-xs-12">
            <h1>{{trans('portal.contact_us')}}</h1>
        </div>
    </div>
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <form action="{{route('portal.contact.post')}}" method="POST" class="form-orizontal">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="name">{{trans('portal.contact_us_name')}}</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="form-group">
                            <label for="email">{{trans('portal.contact_us_email')}}</label>
                            <input type="email"  class="form-control" name="email" id="email" required>
                        </div>
                        <div class="form-group">
                            <label for="subject">{{trans('portal.contact_us_subject')}}</label>
                            <input type="text" class="form-control" name="subject" id="subject">
                        </div>
                        <div class="form-group">
                            <label for="message">{{trans('portal.contact_us_message ')}}</label>
                            <textarea name="message" id="message" cols="30" rows="10" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <button class="boton-naranja" type="submit">{{trans('portal.contact_us_send')}}</button>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="carrousel-contactos">
                        <figure>
                            <img src="{{asset('assets/pages/img/contacto-comercio-internacional.png')}}" alt="" style="margin-top: 25px" class="img-responsive visible-md visible-lg">
                        </figure>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection