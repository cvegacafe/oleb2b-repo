<?php namespace App\Policies;

use App\Repositories\User\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can delete the user.
     *
     * @param User $user
     * @param User $user
     * @return mixed
     */
    public function deleteSubAccount(User $user, User $subuser)
    {
        if($user->id == $subuser->id){
            return false;
        }

        if ($subuser->isPrincipalAccount()) {
            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function userPremium(User $user)
    {
        return $user->isPremium();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isPrincipalAccount(User $user)
    {
        return $user->isPrincipalAccount();
    }

}
