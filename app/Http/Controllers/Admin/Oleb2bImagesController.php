<?php namespace App\Http\Controllers\Admin;

use App\Library\Aviso;
use App\Repositories\Oleb2bImage\Oleb2bImage;
use App\Services\FileService;
use App\Services\Oleb2bImageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Oleb2bImagesController extends Controller
{
    /**
     * @var Oleb2bImageService
     */
    private $oleb2bImageService;

    /**
     * SlidersController constructor.
     */
    public function __construct()
    {
        $this->oleb2bImageService = new Oleb2bImageService();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $images = $this->oleb2bImageService->paginate();
        return view('admin.oleb2b-images.index', compact('images'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            if ($file = $request->file('image')) {
                $disc = 'oleb2b_images';
                $path_res = $file->storePubliclyAs('/' . 0,
                    uniqid(0 . '_' . time() . '_') . '.' . $file->getClientOriginalExtension(), $disc);

                $path = 'uploads/images/' . $disc . '/' . $path_res;
                $request['path'] = $path;
                (new Oleb2bImageService())->create($request->all());
            }
        } else {
            Aviso::noGuardado();
        }
        return redirect()->route('admin.oleb2b-images.index', ['show' => str_slug($request->get('section'))]);
    }


    /**
     * @param Oleb2bImage $image
     * @param FileService $fileService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Oleb2bImage $image, FileService $fileService)
    {
        $fileService->removeFile($image->path);
        $image->delete();
        return redirect()->route('admin.oleb2b-images.index');
    }
}
