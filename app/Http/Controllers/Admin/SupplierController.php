<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\SupplierUpdateRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Supplier\Supplier;
use App\Services\SupplierService;
use App\Services\CompanyTypeService;
use App\Services\CountryService;
use App\Services\TaxTypeService;
//use App\Services\SupercategoryService;


use Exception;
class SupplierController extends Controller
{
    private $supplierService;

    public function __construct(SupplierService $supplierService)
    {
    	$this->supplierService = $supplierService;
    }
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$suppliers = $this->supplierService->paginate(5);
    	return view('admin.suppliers.index',compact('suppliers'));
    }
     /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
     public function show(Supplier $supplier)
     {
     	return view('admin.suppliers.show',compact('supplier'));
     }

     public function edit(
        Supplier $supplier,
        CompanyTypeService $companytypeService,
        CountryService $countryService,
        TaxTypeService $taxtypeService
        )
    {
        $companytypes = $companytypeService->all();
        $countries = $countryService->all();
        $taxtypes = $taxtypeService->all();
        return view('admin.suppliers.edit',compact('supplier','companytypes','countries','taxtypes'));

     }
     public function update(
        SupplierUpdateRequest $request,$id        
        ){
        $supplier = Supplier::findOrfail($id);
        //$supplierService->update($supplier->id,$request->all());
        $supplier->update($request->all());
        $supplier->save();
        return redirect()->action('Admin\SupplierController@index');
     }
     


}
