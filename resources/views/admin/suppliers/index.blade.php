@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Proveedores :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Company Type</th>
                        <th>Company Name</th>
                        <th>Description</th>
                        <th>Member Trade Organization</th>
                        <th>Country</th>
                        <th>Type Tax</th>
                        <th>Tax Number</th>
                        <th>Address Company</th>
                        <th>Postal code</th>
                        <th>Website</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach($suppliers as $supplier)
                        <tr>
                            <td>{{$i++ }}</td>
                            <td>
                                {{ $supplier->companytype->en_name }}
                                
                            </td>
                            <td>{{ $supplier->company_name }}</td>
                            <td>
                                {{ $supplier->en_company_description }}
                            </td>
                            <td>
                                {{$supplier->member_trade_organization }}
                            </td>
                            <td>
                                {{$supplier->country->en_name }}
                            </td>
                            <td>
                                {{$supplier->taxtype->name}}
                            </td>
                            <td>
                                {{$supplier->tax_number}}
                            </td>
                            <td>
                                {{$supplier->company_legal_address}}
                            </td>
                            <td>
                                {{$supplier->postal_code}}
                            </td>
                            <td>
                                {{$supplier->website}}
                            </td>
                            <td>
                                <a href="{{action('Admin\SupplierController@edit',$supplier)}}"><i class="fa fa-pencil-square-o"></i> </a>
                            </td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
                {{ $suppliers->links() }}
            </div>
        </div>
    </div>
@endsection
