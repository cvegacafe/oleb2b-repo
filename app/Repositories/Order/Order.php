<?php namespace App\Repositories\Order;

use App\Repositories\Membership\Membership;
use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'orders';

    public $fillable = ['signature','reference_code','user_id','membership_id','frequency','amount','description','status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function membership()
    {
        return $this->belongsTo(Membership::class);
    }
}
