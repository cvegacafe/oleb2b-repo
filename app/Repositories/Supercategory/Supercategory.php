<?php namespace App\Repositories\Supercategory;

use App\Repositories\Category\Category;
use App\Repositories\Subcategory\Subcategory;
use App\Repositories\Supplier\Supplier;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class Supercategory
 * @package App\Repositories\Supercategory
 */
class Supercategory extends Model
{
    /**
     * @var string
     */
    public $table = 'supercategories';

    /**
     * @var array
     */
    public $fillable = ['es_name', 'en_name', 'ru_name', 'visited'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function subcategories()
    {
        return $this->hasManyThrough(Subcategory::class, Category::class);
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }

    /**
     * @param $lang
     * @return mixed
     */
    public function nameByLang($lang)
    {
        return $this->getAttribute(strtolower($lang) . '_name');
    }
}
