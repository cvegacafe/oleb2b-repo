<?php
return [
    'admin' => [

    ],

    'user' => [
        'manage_account' => 'Мой Профиль',
        'messages' => 'Сообщения',
        'for_buyers' => 'Покупателям',
        'manage_buying_request' => 'Редактировать Запросы КП',
        'for_suppliers' => 'Поставщикам',
        'manage_products' => 'Мои товары',
        'manage_company_profile' => 'Информация о компании',
        'subaccounts' => 'Дополнительные учетные записи',
        'saved_products' => 'Сохраненные товары',
        'saved_buying_request' => 'Сохраненные Запросы КП',
        'contact_us' => 'Связаться С Нами',
        'logout' => 'выйти',
        'generals' => 'Общие',
        'memberships' => 'КАТЕГОРИИ ПОСТАВЩИКОВ',
        'create_company_profile' => 'Crear perfil de empresa',
        'for_buying_requests' => 'По Запросам КП'
    ],
    'footer' => [
        'customer_service' => 'Обслуживание Клиентов',
        'contact_us' => 'Связаться С Нами',
        'terms_use' => 'Условия эксплуатации',
        'privacy_politics' => 'Политика конфиденциальности',
        'about_us' => 'О Нас',
        'about_oleb2b' => 'Об Oleb2b.com',
        'sitemap' => 'Карта Сайта',
        'buy_on_oleb2b' => 'Покупка На Oleb2b.com',
        'get_quotations' => 'Разместить Запрос',
        'sell_on_oleb2b' => 'Продажа На Oleb2b.com',
        'supplier_memberships' => 'Категории Поставщиков',
        'follow_us' => 'С нами в'
    ]
];