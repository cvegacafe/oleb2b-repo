@extends('layouts.app')
@section('content')
    <div class="container-fluid">
    </div>
    <section id="mensajes">
        <div class="container-fluid h100">
            <div class="row h100">
                <div class="col-xs-12 col-md-4 h100">
                    <div class="productos-mensajes">
                        <div class="header">
                            <h3>Tus productos</h3>
                        </div>
                        <div class="body">
                            <div class="inbox">
                                <div class="header">
                                    <h3> <i class="fa fa-caret-left"></i> Todos los mensajes</h3>
                                </div>
                                <div class="container-messagges">

                                    <div class="single-message">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>Cristian Vega</h3>
                                                <small><strong>Infinibyte EIRL</strong></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>Cristian Vega</h3>
                                                <small><strong>Infinibyte EIRL</strong></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>Cristian Vega</h3>
                                                <small><strong>Infinibyte EIRL</strong></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>Cristian Vega</h3>
                                                <small><strong>Infinibyte EIRL</strong></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>Cristian Vega</h3>
                                                <small><strong>Infinibyte EIRL</strong></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-message">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>Cristian Vega</h3>
                                                <small><strong>Infinibyte EIRL</strong></small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @foreach($threads as $thread)
                                @if($thread->type == \App\Library\Constants::THREAD_HAS_PRODUCT)
                                <div class="producto">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <figure>
                                                <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                            </figure>
                                        </div>
                                        <div class="col-xs-8">
                                            <h3>{{$thread->product->name}}</h3>
                                            <ul>
                                                <li><strong>Fecha:</strong> {{$thread->created_at}}</li>
                                                <li><strong>Desde:</strong> {{$thread->country->name}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @elseif($thread->type == \App\Library\Constants::THREAD_HAS_BUYING_REQUEST)
                                    <div class="producto">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <figure>
                                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" >
                                                </figure>
                                            </div>
                                            <div class="col-xs-8">
                                                <h3>:D</h3>
                                                <ul>
                                                    <li><strong>Fecha:</strong> 88/88/8888</li>
                                                    <li><strong>Desde:</strong> Perú</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-8 h100">
                    <div class="chat">
                        <div class="mensajes">
                            <div class="mensaje-unico buyer">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico supplier">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico buyer">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico supplier">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico buyer">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico supplier">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico buyer">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico supplier">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico buyer">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                            <div class="mensaje-unico supplier">
                                <figure class="logo-empresa">
                                    <img src="{{asset('images/uploads/products/1/1-1-300x309.jpg')}}" alt="Logo empresa">
                                </figure>
                                <div class="texto">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dignissimos dolores dolorum eaque explicabo facilis incidunt ipsum laborum minima neque nulla quibusdam quos, repudiandae? Ea id ipsam itaque rerum soluta.
                                </div>
                            </div>
                        </div>
                        <div class="input-mensaje">
                            <div class="adjunto">
                                <i class="fa fa-paperclip"></i>
                            </div><div class="box">
                                <textarea name="" id="" cols="30" rows="10" placeholder="Escribe un mensaje ... "></textarea>
                            </div><div class="enviar">
                                <button class="boton-naranja">Enviar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
    @section('js')
        <script>
            $('.producto').on('click',function(){
                showMessages();
            });
            $('.productos-mensajes .header').on('click',function(){
                hideMessages();
            });

            function showMessages(){
                $('.inbox').addClass('active');
            }
            function hideMessages(){
                $('.inbox').removeClass('active');
            }
        </script>
    @endsection