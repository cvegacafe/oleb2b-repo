<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */


use App\Repositories\Currency\CurrencyRepository;

class CurrencyService extends BaseService
{

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->mainRepo = $currencyRepository;
        parent::__construct();
    }
}