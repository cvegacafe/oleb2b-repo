<?php namespace App\Repositories\Thread;

use App\Repositories\BuyingRequest\BuyingRequest;
use App\Repositories\Country\Country;
use App\Repositories\Message\Message;
use App\Repositories\Product\Product;
use App\Repositories\User\User;
use App\Services\CountryService;
use App\Services\MeasureService;
use App\Services\PaymentTermService;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{

  public $fillable = ['type', 'from_user_id', 'to_user_id'];

  public function buyingRequests()
  {
    return $this->belongsToMany(BuyingRequest::class, 'buyingRequest_thread', 'thread_id', 'buyingRequest_id')->withTimestamps();
  }

  public function products()
  {
    return $this->belongsToMany(Product::class, 'thread_product', 'thread_id', 'product_id')
      ->withPivot(['country_id', 'measure_id', 'ports', 'order_qty', 'message', 'payment_terms', 'files'])->withTimestamps();
  }

  public function fromUser()
  {
    return $this->belongsTo(User::class, 'from_user_id', 'id');
  }

  public function toUser()
  {
    return $this->belongsTo(User::class, 'to_user_id', 'id');
  }

  public function messages()
  {
    return $this->hasMany(Message::class);
  }

  public function getProductAttribute()
  {
    return $this->products()->first();
  }

  public function getBuyingRequestAttribute()
  {
    return $this->buyingRequests()->first();
  }

  public function getPivotMeasureAttribute()
  {
    $measureService = app(MeasureService::class);

    return $measureService->find($this->product->pivot->measure_id);
  }

  public function getPivotCountryAttribute()
  {
    $countryService = app(CountryService::class);

    return $countryService->find($this->product->pivot->country_id);
  }

  public function getPivotPaymentTermsAttribute()
  {
    $paymentTermsService = app(PaymentTermService::class);

    $ids = json_decode($this->product->pivot->payment_terms);

    if (empty($ids)) return [];

    return $paymentTermsService->getManyById($ids);
  }

  public function getCountryAttribute()
  {
    $countryId = $this->product->pivot->country_id;

    return Country::find($countryId);
  }

  public function newMessages($userId)
  {
    $messages = $this->messages->filter(function ($value, $key) use ($userId) {
      return $value->read_at == null && $value->user_id != $userId;
    });

    return $messages->count();
  }

  public function getSupplierAttribute()
  {
    return User::find($this->product->user_id);
  }

  public function getBuyerAttribute()
  {
    $buyer = $this->fromUser != $this->supplier ? $this->fromUser : $this->toUser;
    return $buyer;
  }
}
