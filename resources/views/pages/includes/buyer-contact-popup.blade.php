<div class="modal fade" role="dialog" id="BuyerModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" role="form"  action="{{route('portal.contactBuyer')}}" id="BuyerFormHtml" enctype="multipart/form-data" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{trans('buyingRequest.contact_buyer')}}</strong>
            </div>
            <div class="modal-body">
                    {{ csrf_field() }}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-6 text-center">
                            <img src="{{old('input_product_photo')}}" alt="" id="product_photo" class="img-responsive" width="200px">
                            <input type="hidden" id="input_product_photo" name="input_product_photo">
                            <input type="hidden" id="buyingRequest_id" name="buyingRequest_id">
                            <input type="hidden" name="input_description" id="input_description">
                            <input type="hidden" name="input_start_date" id="input_start_date">
                            <input type="hidden" name="input_end_date" id="input_end_date">
                            <input type="hidden" name="input_buyer_name" id="input_buyer_name">
                            <input type="hidden" name="input_country" id="input_country">
                        </div>
                        <div class="col-xs-6">
                            <h2 id="product_name"></h2>
                            <strong>{{trans('buyingRequest.product_description')}}:</strong>
                            <p id="description">{{old('input_description')}}</p>
                            <div class="row">
                                <div class="col-xs-6">
                                    <strong>{{trans('buyingRequest.start_date')}}:</strong>
                                    <p id="start_date">{{old('input_start_date')}}</p>
                                </div>
                                <div class="col-xs-6">
                                    <strong>{{trans('buyingRequest.end_date')}}:</strong>
                                    <p id="close_date">{{old('input_end_date')}}</p>
                                </div>
                            </div>
                            <strong>{{trans('buyingRequest.buyer')}}:</strong>
                            <p id="buyer_name">{{old('input_buyer_name')}}</p>
                            <strong>{{trans('user.form.country')}}</strong>
                            <p id="buyer_country">{{old('input_country')}}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="idioma1" class="tab-pane fade in active">
                            <div class="text-center">
                                <small class="idioma-proveedor">{{trans('buyingRequest.this_buyer_speak')}}: <strong id="languages">Language</strong></small>
                            </div>
                            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                <textarea name="message" id="message" cols="40" rows="5" maxlength="250"  class="form-control" placeholder="Mensaje">{{old('message')}}</textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('message') }}</strong>
                                        </span>
                                @endif
                                <span  id="contador"></span>
                            </div>
                            <div id="filesContainer" class="form-group">
                                <button id="seleccionar-fotos" type="button" class="boton-azul-no-shadow"><i class="fa fa-plus"></i> Agregar Archivos</button>
                                <div class="preview-files">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="boton-azul" data-dismiss="modal">{{trans('common.forms.buttons.close')}}</button>
                <button type="submit" class="boton-naranja">{{trans('common.forms.buttons.send')}}</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->