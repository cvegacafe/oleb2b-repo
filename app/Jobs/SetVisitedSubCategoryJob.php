<?php namespace App\Jobs;

use App\Services\SubcategoryService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SetVisitedSubCategoryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $subcategoryId;

    /**
     * Create a new job instance.
     * @param $subcategoryId
     */
    public function __construct($subcategoryId)
    {
        $this->subcategoryId = $subcategoryId;
    }

    /**
     * Execute the job.
     *
     * @param SubcategoryService $subcategoryService
     */
    public function handle(SubcategoryService $subcategoryService)
    {
        $subcategory = $subcategoryService->find($this->subcategoryId);

        if($subcategory){

            $subcategory->visited = $subcategory->visited + 1;

            $subcategory->save();
        }

    }
}
