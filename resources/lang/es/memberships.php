<?php
return [
    'basic' => 'BÁSICA',
    'advanced' => 'AVANZADA',
    'premium' => 'PREMIUM',

    'cost' => 'COSTO',
    'product_posting' => 'PUBLICACION DE PRODUCTOS ',
    'banner_main_page' => "BANNER EN PAGINA PRINCIPAL*",
    'banner_inner_pages' => "BANNER EN PAGINAS SECUNDARIAS**",
    'featured_products' => "DESTAQUE DE PRODUCTOS",
    'ability_quote_buying_request' => "COTIZACION DE SOLICITUDES DE COTIZACION",
    'subaccounts' => 'CUENTAS SECUNDARIAS***',

    'disclaimer1'=>'Promoción válida solamente para planes anuales. El monto total de la membresia sera cargado a su tarjeta al momento de la compra. La duración de la membresía es de 12 meses + 3 meses gratis. No aplica reembolso. ',

    'choose_plan' => 'Escoge tu plan',
    'active' => 'Activo',
    'unlimited' => 'Ilimitada',
    'average_rotation' => 'Tiempo promedio de rotacion :time min/mes',
    'cond_1' => '*EL BANNER SERA DISEÑADO POR LA AGENCIA CREATIVA  OLEBABA.COM S.A.C, 4 BANNERS POR AÑO: ROTACION CADA TRES MESES. EL PRIMER BANNER ESTARA LISTO Y EN LINEA DENTRO DE LAS PRIMERAS 72 HORAS DE CONFIRMAR LA MEMBRESIA 	',
    'cond_2' => '**EL BANNER SERA DISEÑADO POR LA AGENCIA CREATIVA  OLEBABA.COM S.A.C, 4 BANNERS POR AÑO: ROTACION CADA TRES MESES. EL PRIMER BANNER ESTARA LISTO Y EN LINEA DENTRO DE LAS PRIMERAS 72 HORAS DE CONFIRMAR LA MEMBRESIA 	',
    'cond_3' => '***ESTA FUNCION PERMITE TENER DIFERENTES PERSONAS DE CONTACTO POR PRODUCTO O TERRITORIO DE VENTAS	 '
];