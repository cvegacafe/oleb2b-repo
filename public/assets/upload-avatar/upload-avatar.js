/**
 * Created by Emmanuel on 25/06/2017.
 */
(function () {
    var uploader = document.createElement('input'),
        image = document.getElementById('img-result'),
        wrapper = document.getElementById('upload-avatar-wrapper');

    uploader.type = 'file';
    uploader.accept = 'image/*';
    uploader.style.display = 'none';
    console.log(wrapper.dataset);
    var inputName = wrapper.dataset.inputName ? wrapper.dataset.inputName :'image';
    uploader.setAttribute('name', inputName);
    image.onclick = function () {
        event.preventDefault();
        uploader.click();
    };
    wrapper.appendChild(uploader);
    uploader.onchange = function () {
        var reader = new FileReader();
        reader.onload = function (evt) {
            image.classList.remove('no-image');
            image.style.backgroundImage = 'url(' + evt.target.result + ')';
            // document.querySelector('.show-button').style.display = 'block';
            //document.querySelector('.upload-result__content').innerHTML = JSON.stringify(request, null, '  ');
        };
        reader.readAsDataURL(uploader.files[0]);

    };

    /* document.querySelector('.hide-button').onclick = function () {
     document.querySelector('.upload-result').style.display = 'none';
     };*/

    /* document.querySelector('.show-button').onclick = function () {
     document.querySelector('.upload-result').style.display = 'block';
     };*/
})();