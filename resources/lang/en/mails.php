<?php
return [
    'welcome_to_oleb2b' => 'Welcome to Oleb2b.com',
    'new_buying_request' => 'Nueva oportunidad de venta!',
    'change_password_title' => ' To change your password please follow the link',
    'change_password_btn' => 'Change Password',
    'change_password_message' => 'If you can not click the button “Change password “ copy and paste the link below into your browser',
    'button_not_working'=>'Button not working? Paste this link into your browser',
    'oleb2b_team'=>'OleB2B Team',
    'regards'=>'Regards,',
    'mail_confirm' => [
        'title' => 'Thank you for registering at www.OLEB2B.com.<br><br> Please confirm your email address using the link below',
        'action_btn' => ' Confirm email'
    ],
    'mail_purchase' => [
        'subject' => 'Membership confirmed',
        'title' => 'Your <strong>:membershipName</strong> membership is confirmed.<br><br> Please send us guide lines for the banner design to this email:',
        'action_btn' => 'Dashboard',
        'banner_message' => ' The banners will be uploaded within 72 hours after receiving instructions. Thank you for being with us!'
    ],
    'mail_message' => [
        'title' => 'You have a new message at OleB2B.com',
        'action_btn' => 'Check'
    ],
    'buying_request' => [
        'subject' => 'A new buying request has been posted on OleB2B.com',
        'title' => 'A new buying request has been posted on OleB2B.com in <b>:supercategoryName</b> <br><br> Please visit OleB2B.com to check out the new buying requests and start submitting your quotes.',
        'action_btn' => 'Go to OleB2B.com'
    ]
];