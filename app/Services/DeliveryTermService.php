<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:25 PM
 */

use App\Repositories\DeliveryTerm\DeliveryTermRepository;

class DeliveryTermService extends BaseService
{

    public function __construct(DeliveryTermRepository $deliveryTermRepository)
    {
        $this->mainRepo = $deliveryTermRepository;
        parent::__construct();
    }
}