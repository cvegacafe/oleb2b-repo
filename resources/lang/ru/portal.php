<?php

return [
    // Home
    'main_slogan' => 'Платформа В2В для Российских и Латиноамериканских экспортеров',
    'search' => 'Поиск',
    'product' => 'Товары',
    'supplier' => 'Поставщики',
    'search_products' => 'Поиск товаров на русском',
    'categories' => 'Категории',
    'search_by_country' => 'Поиск по странам',
    'buying_request' => 'Запросы Покупателей',
    'contact_us_subject' => 'Тема',
    'contact_us_name' => 'Имя',
    'contact_us_email' => 'Имейл',
    'contact_us_message ' => 'Сообщение',
    'contact_us_send' => 'Отправить',
    'featured_products' => 'Популярные Товары',
    'contact_supplier' => 'Связаться',
    'not_featured_products' => 'НЕТ ПОПУЛЯРНЫХ ТОВАРОВ',
    'how_it_works' => 'Как это работает?',
    'see_more' => 'подробнее',
    'for' => '',
    'new' => 'Новым',
    'free' => 'бесплатно',
    'buyers' => 'Покупателям',
    'users' => 'Пользователям',
    'suppliers' => 'Поставщикам',
    'latest_news' => 'Новости',
    'trade_organizations' => 'Торговые Организации и АГЕНТСТВА',
    'trade_shows_and_events' => 'Выставки И Конференции',
    'partners' => 'Партнеры',
    'sale_online_is_easy' => 'Продажа Онлайн - Легко',
    'join_free' => 'Регистрация',
    'customer_support' => 'клиент',
    'free_to_start_online_trading' => 'НАЧАТЬ ТОРГОВЛЮ НА САЙТЕ',
    'already_member' => 'УЖЕ ЗАРЕГИСТРИРОВАЛИСЬ',
    'member' => '',
    'sign_in' => 'ВОЙТИ',
    'greeting' => 'Здравствуйте',
    'enter_at_your_account' => 'Зайдите в профиль',
    'remember_me' => 'Запомни меня',
    'forgot_password' => 'Забыли пароль?',
    'i_accept' => 'я принимаю',
    'upload_products'=>'Добавьте товары',
    //Show Single Product
    'saved'=> 'СОХРАНЕНО',

    'error_403' => 'Не разрешено',
    'error_404' => 'Не найдено',
    'error_503' => 'Внутренняя ошибка сайта',

    'error_403_text' => 'Доступ запрещен',
    'error_404_text' => 'По Вашему запросу ничего не найдено',
    'error_503_text' => 'Al parecer algo ha salido mal.',

    'company_details' => 'Информация О Компании',
    'photos_and_logos' => 'Фото/логотипы Компании',
    'factory_details' => 'О Фабрике',
    'certifications_and_trademarks' => 'Сертификаты И Торговые Марки',
    'save_for_later' => 'Сохранить',
    'request_quote' => 'Котировка',
    'related_products' => 'ПОДОБНЫЕ ТОВАРЫ',
    'more_products_from_seller' => 'ДРУГИЕ ТОВАРЫ ОТ ЭТОГО ПОСТАВЩИКА',
    'contact_us' => 'Связаться С Нами',
    'search_buying_request' => 'Поиск в запросах КП',
    'terms_of_use' => 'Условия эксплуатации',

    'supplier_speak_languages' => 'ЭТОТ ПОСТАВЩИК ГОВОРИТ НА ЯЗЫКАХ',
    'your_message' => 'ваше сообщение',
    /*seccion como funciona*/
    'suppliers_card' => [
        'for_suppliers' => 'ПОСТАВЩИКАМ',
        'register_company' => 'Регистрируйте Вашу компанию',
        'upload_products' => 'Добавляйте новые товары',
        'membership_plan' => 'Выбирайте категорию поставщика',
        'view_buying_request' => 'Осуществляйте поиск по Запросам КП',
        'improve_your_sales' => 'Увеличивайте продажи с помощью OleB2B.com'
    ],

    'new_users_card' => [
        'new_users' => 'НОВЫМ ПОЛЬЗОВАТЕЛЯМ',
        'register' => 'Зарегистрируйтесь',
        'benefits' => ' Пользуйтесь возможностями платформы OleB2B.com',
        'new_tool' => 'Добавьте новый канал продаж для Вашей компании'
    ],

    'buyers_card' => [
        'for_buyers' => 'ПОКУПАТЕЛЯМ',
        'find_new_products' => 'Поиск новых товаров и поставщиков',
        'posting_buying_request' => 'Размещайте Ваши Запросы КП и получайте предложения',
        'improve_sourcing' => ' Более эффективный процесс закупок с OleB2B.com'
    ]
];
