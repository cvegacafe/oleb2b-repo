<?php namespace App\Repositories\DeliveryTerm;

use App\Repositories\BaseRepository;

class DeliveryTermRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new DeliveryTerm();
        parent::__construct();
    }
}