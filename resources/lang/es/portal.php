<?php

return [
    // Home
    'main_slogan' => 'Plataforma de venta B2B para exportadores de America Latina y Rusia',
    'search' => 'Buscar',
    'product' => 'Producto',
    'supplier' => 'Proveedor',
    'search_products' => 'Buscar productos en español',
    'categories' => 'Categorías',
    'search_by_country' => 'Búsqueda por país',
    'buying_request' => 'Solicitudes de cotización',
    'contact_us_subject' => 'Asunto',
    'contact_us_name' => 'Nombre',
    'contact_us_email' => 'Correo Electrónico',
    'contact_us_mobile' => 'Mobile',
    'contact_us_message ' => 'Mensaje',
    'contact_us_send' => 'Contact Us',
    'featured_products' => 'Productos destacados',
    'contact_supplier' => 'Contactar vendedor',
    'not_featured_products' => 'Aun no hay productos destacados',
    'how_it_works' => '¿Cómo funciona?',
    'see_more' => 'Ver más',
    'for' => 'Para',
    'free' => 'Gratis',
    'new' => 'Nuevos',
    'buyers' => 'Compradores',
    'users' => 'Usuarios',
    'suppliers' => 'Vendedores',
    'latest_news' => 'Noticias',
    'trade_organizations' => 'Camaras de Comercio y Agencias',
    'trade_shows_and_events' => 'Ferias y Eventos',
    'partners' => 'Socios',
    'sale_online_is_easy' => 'Vender en Línea es Fácil',
    'join_free' => 'Registrate',
    'customer_support' => 'Atención al cliente',
    'free_to_start_online_trading' => '¡Empiece a vender en línea es',
    'already_member' => '¿Ya estas ',
    'member' => 'registrado',
    'sign_in' => 'Ingresar',
    'greeting' => 'Hola',
    'enter_at_your_account' => 'Ingresa a tu cuenta',
    'forgot_password' => '¿Olvidaste tu contraseña?',
    'remember_me' => 'Recuerdame',
    'i_accept' => 'Acepto los',
    'upload_products'=>'Sube tus productos',
    //Show Single Product

    'error_403' => 'Prohibido',
    'error_404' => 'No encontrado',
    'error_503' => 'Error interno',

    'error_403_text' => 'Acceso denegado.',
    'error_404_text' => 'No encontramos lo que estás buscando.',
    'error_503_text' => 'Al parecer algo ha salido mal.',

    'company_details' => 'Detalle de la empresa',
    'photos_and_logos' => 'Fotos y logo',
    'factory_details' => 'Detalles de fábrica',
    'certifications_and_trademarks' => 'Certificaciones y Trademarks',
    'save_for_later' => 'Guardar para después',
    'saved' => 'Guardado',
    'request_quote' => 'solicitar cotización',
    'related_products' => 'Productos relacionados',
    'more_products_from_seller' => 'Más productos de este vendedor',
    'contact_us' => 'Contáctenos',

    'search_buying_request' => 'Buscar solicitudes de cotización',
    'terms_of_use' => 'Términos de uso',

    'supplier_speak_languages' => 'Este proveedor habla los siguientes idiomas',
    'your_message' => 'Tu mensaje',
    /*seccion como funciona*/
    'suppliers_card' => [
        'for_suppliers' => 'Para Vendedores',
        'register_company' => 'Registre su empresa',
        'upload_products' => 'Suba la información de sus productos',
        'membership_plan' => 'Seleccione su tipo de membresía',
        'view_buying_request' => 'Revise las Solicitudes de Cotización',
        'improve_your_sales' => 'Incremente sus ventas con OleB2B.com'
    ],

    'new_users_card' => [
        'new_users' => 'NUEVOS USUARIOS',
        'register' => 'Registrese',
        'benefits' => 'Compruebe todos los beneficios que la plataforma OleB2B.com tiene para su empresa',
        'new_tool' => 'Añada una nueva herramienta a su empresa'
    ],

    'buyers_card' => [
        'for_buyers'=>'COMPRADORES',
        'find_new_products' => 'Encuentre nuevos productos y vendedores',
        'posting_buying_request' => 'Publique sus Solicitudes de Cotización y reciba cotizaciones',
        'improve_sourcing' => 'Mejore su capacidad para comprar productos de nuevos vendedores con OleB2B.com'
    ]
];
