@extends('layouts.admin3')
@section('content')
    <form action="{{action('Admin\UsersController@update',$user)}}" method="POST" onsubmit="return confirm('Seguro que desea guardar estos datos?')">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
    <div class="col-xs-12">
        <div class="form-group">
            <a href="{{action('Admin\UsersController@show',$user)}}" class="btn btn-default pull-right">Canclear</a>
            <button type="submit" class="btn btn-success pull-right">Guardar</button>
        </div>
    </div>
    <div class="col-xs-12" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Usuario: </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4 " align="center">
                        <img alt="User Pic" src="{{asset($user->image)}}" class="img-circle img-responsive">
                        <label><input type="checkbox" name="remove_image" class="form-control"> Quitar imagen</label>
                    </div>
                    <div class=" col-md-4">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>

                                <td>Nombres:</td>
                                <td class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                                    <input type="text" name="names" value="{{$user->names}}" class="form-control">
                                    @if ($errors->has('names'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('names') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('last_names') ? ' has-error' : '' }}">
                                <td>Apellidos:</td>
                                <td>
                                    <input type="text" name="last_names" value="{{$user->last_names}}" class="form-control">
                                    @if ($errors->has('last_names'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_names') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                <td>Tel&eacute;fono:</td>
                                <td>
                                    <input type="text" name="office_phone" value="{{$user->office_phone}}" class="form-control">
                                    @if ($errors->has('office_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('office_phone') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                                <td>Celular:</td>
                                <td>
                                    <input type="text" name="mobile_phone" value="{{$user->mobile_phone}}" class="form-control">
                                    @if ($errors->has('mobile_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mobile_phone') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                            <tr class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                <td>Empresa:</td>
                                <td>
                                    <input type="text" name="company" value="{{$user->company}}" class="form-control">
                                    @if ($errors->has('company'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                                <td>Dirección:</td>
                                <td>
                                    <input type="text" name="address" value="{{$user->address}}" class="form-control">
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                <td>Codigo Postal:</td>
                                <td>
                                    <input type="text" name="postal_code" value="{{$user->postal_code}}" class="form-control">
                                    @if ($errors->has('postal_code'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('postal_code') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class=" col-md-4">
                        <table class="table table-user-information">
                            <tbody>
                            <tr class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <td>Email:</td>
                                <td>
                                    <input type="email" name="email" value="{{$user->email}}" id="email" class="form-control" disabled>
                                    <label><input type="checkbox" name="change_email" id="change_email"> cambiar email</label>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <td>Ciudad:</td>
                                <td>
                                    <input type="text" name="city" value="{{$user->city}}" class="form-control">
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                <td><label for="country_id">Pais:</label></td>
                                <td>
                                    <select name="country_id" id="country_id" class="form-control">
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{$country->id == $user->country->id ? 'selected' : ''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country_id') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                <td>Idiomas:</td>
                                <td>
                                    <ul class="inline-checkbox">
                                        @foreach($languages as $language)
                                            <li>
                                                <label class="checkbox-inline">
                                                    <input id="{{$language->id}}"  {{in_array($language->id,$user->languages->pluck('id')->toArray())?'checked':''}}  type="checkbox" name="languages[]" value="{{$language->id}}"/>&nbsp;{{$language->name}}
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @if ($errors->has('languages'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('languages') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr class="form-group{{ $errors->has('other_languages') ? ' has-error' : '' }}">
                                <td>Otros Idiomas:</td>
                                <td>
                                    <input type="text" name="other_languages" value="{{$user->other_languages}}" class="form-control">
                                    @if ($errors->has('other_languages'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('other_languages') }}</strong>
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6" >
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Información de última membresia </h3>
            </div>
            @if($lastMembership)
                <div class="panel-body">
                    <div class="row">
                        <div class=" col-md-12 col-lg-12 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <input type="hidden" name="membership_user_id" value="{{$lastMembership->pivot->id}}">
                                    <td>Membresía:</td>
                                    <td class="form-group{{ $errors->has('membership_id') ? ' has-error' : '' }}">
                                        <select name="membership_id" id="membership_id" class="form-control">
                                            @foreach($memberships as $membership)
                                                <option value="{{$membership->id}}" {{$membership->id == $lastMembership->id ? 'selected' : ''}}>{{$membership->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('membership_id'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('membership_id') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="form-group{{ $errors->has('membership_product_posting') ? ' has-error' : '' }}">
                                    <td>Cantidad Publicaciones:</td>
                                    <td>
                                        <input type="text" name="membership_product_posting" value="{{$lastMembership->pivot->product_posting}}" class="form-control">
                                        @if ($errors->has('membership_product_posting'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('membership_product_posting') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="form-group{{ $errors->has('membership_amount') ? ' has-error' : '' }}">
                                    <td>Precio:</td>
                                    <td>
                                        <input type="text" name="membership_amount" value="{{$lastMembership->pivot->amount}}" class="form-control">
                                        @if ($errors->has('membership_amount'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('membership_amount') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="form-group{{ $errors->has('membership_frequency') ? ' has-error' : '' }}">
                                    <td>Frecuencia:</td>
                                    <td>
                                        <select name="membership_frequency" id="membership_frequency" class="form-control">
                                            <option value="M" {{strtoupper($lastMembership->pivot->frequency) == 'M' ? 'selected' : ''}}>Mensual</option>
                                            <option value="A" {{strtoupper($lastMembership->pivot->frequency) == 'A' ? 'selected' : ''}}>Anual</option>
                                        </select>
                                        @if ($errors->has('membership_frequency'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('membership_frequency') }}</strong>
                                        </span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="form-group{{ $errors->has('membership_created_at') ? ' has-error' : '' }}">
                                    <td>Fecha de inicio</td>
                                    <td>
                                        @if($lastMembership->pivot->created_at!= null)
                                            <input type="date" name="membership_created_at" class="form-control" value="{{$lastMembership->pivot->created_at->format('Y-m-d')}}">
                                        @else
                                            <input type="date" name="membership_created_at" class="form-control">
                                        @endif

                                            @if ($errors->has('membership_created_at'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('membership_created_at') }}</strong>
                                        </span>
                                            @endif
                                       </td>
                                </tr>
                                <tr class="form-group{{ $errors->has('membership_finish_at') ? ' has-error' : '' }}">
                                    <td>Fecha de finalización</td>
                                    <td>
                                        @if($lastMembership->pivot->finish_at!=null)
                                            <input type="date" name="membership_finish_at" class="form-control" value="{{Carbon\Carbon::parse($lastMembership->pivot->finish_at)->format('Y-m-d')}}">
                                        @else
                                            <input type="date" name="membership_finish_at" class="form-control">
                                        @endif
                                            @if ($errors->has('membership_finish_at'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('membership_finish_at') }}</strong>
                                        </span>
                                            @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <tr>
                    <td>Este usuario no tiene ninguna membresia adquirida</td>
                </tr>
            @endif
        </div>
    </div>
    </form>
@endsection
@section('js')
    <script>
        $('#change_email').on('change',function(){
            if($(this).is(':checked')){
                $('#email').attr('disabled',false);
            }else{
                $('#email').attr('disabled',true);
            }
        })
    </script>
    @endsection