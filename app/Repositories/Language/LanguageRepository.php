<?php namespace App\Repositories\Language;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class LanguageRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Language();
        parent::__construct();
    }

    public function getByLangFluent($lang)
    {
        return DB::table('languages')
            ->select('id',$lang.'_name as name')
            ->get();
    }

    public function getLangByOriginalName()
    {
        return DB::table('languages')
            ->select('id','original_name as name')
            ->get();
    }
}