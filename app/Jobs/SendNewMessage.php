<?php namespace App\Jobs;

use App\Library\Constants;
use App\Notifications\SendEmailForNewMessage;
use App\Services\ThreadService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNewMessage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $userId, $threadId;

    /**
     * Create a new job instance.
     *
     * @param $threadId
     * @param $userId
     */
    public function __construct($threadId, $userId)
    {
        $this->threadId = $threadId;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @param ThreadService $threadService
     */
    public function handle(ThreadService $threadService)
    {
        $thread = $threadService->find($this->threadId);

        if ($thread) {

            if ($this->userId == $thread->from_user_id) {

                $newMessages = $thread->newMessages($thread->to_user_id);

                $user = $thread->toUser;

                if ($thread->type == Constants::THREAD_HAS_PRODUCT) {

                    $product = $thread->product;

                    $url = route('user.messages.products.index', ['product' => $product->id, 'thread' => $thread->id]);

                } else {

                    $buyingRequest = $thread->buyingRequest;

                    $url = route('user.messages.buying-request.index',
                        ['buying-request' => $buyingRequest->id, 'thread' => $thread->id]);
                }


            } else {

                $newMessages = $thread->newMessages($thread->from_user_id);

                $user = $thread->fromUser;

                if ($thread->type == Constants::THREAD_HAS_PRODUCT) {

                    $product = $thread->product;

                    $url = route('user.messages.products.index', ['product' => $product->id, 'thread' => $thread->id]);

                } else {

                    $buyingRequest = $thread->buyingRequest;

                    $url = route('user.messages.buying-request.index',
                        ['buying-request' => $buyingRequest->id, 'thread' => $thread->id]);
                }


            }

            if ($newMessages < 2) {

                $user->notify(new SendEmailForNewMessage($user, $url));

            }

        }
    }
}
