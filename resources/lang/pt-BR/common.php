<?php
return [
    'day' => 'xx',
    'days' => 'xx',
    'month' => 'xx',
    'months' => 'xx',
    'year' => 'xx',
    'years' => 'xx',
    'forms' => [
        'register' => [
            'first_name' => 'xxxx',
            'last_names' => 'xxx',
            'email_address' => 'xxx',
            'country' => 'xx',
            'office_phone' => 'xxx',
            'mobile_phone' => 'x',
            'address' => 'xxx',
            'city' => 'xxxx',
            'postal_code' => 'xxx',
            'languages' => 'xxxxx',
            'others_languages' => 'xxxx',
        ],
        'buttons' => [
            'save' => 'xxx',
            'update' => 'xxx',
            'delete' => 'xx',
        ],
        'inputs' => [
            'select' => 'Selecionar'
        ]

    ],
];