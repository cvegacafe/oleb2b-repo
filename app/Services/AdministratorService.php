<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:15 PM
 */

use App\Repositories\Administrator\AdministratorRepository;

class AdministratorService extends BaseService
{

    public function __construct(AdministratorRepository $administratorRepository)
    {
        $this->mainRepo = $administratorRepository;
        parent::__construct();
    }
}