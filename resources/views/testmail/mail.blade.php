<!DOCTYPE html>
<html style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: sans-serif;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 10px;-webkit-tap-highlight-color: rgba(0,0,0,0);">
<head style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<title style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">Mail</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">

</head>
<style style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
*{
	box-sizing: border-box;
}
	#cabecera{
		font-family: "Arial", sans-serif;
		font-size: 40px;
		font-weight: bold;
	}
	#parrafo{
		font-family: arial;
		font-size: 16px;
		margin-bottom: 40px;
	}
	#logo{
		margin-top:-90px;
		opacity: 0.85;
		width: 220px;
	}
	#header{
		opacity: 0.85;
		margin-right: auto;
		margin-left: auto;
	}
	.header{
		padding-bottom: 100px;
		margin-top: 25px;
		
	}
	#boton{
		border-radius: 30px;
		background-color: #FF430A;
		opacity: 0.75e;
		height: 50px;
		width: 200px;
		margin-bottom: 80px;
	}
	#informe2{
		font-family: "Arial", sans-serif;
		font-size: 18px;

	}
	#url{
		font-family: "Arial",sans-serif;
		font-weight: bold;
		font-size: 20px;
		margin-left: 65px;
		color:black !important;
	}
</style>
<body style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: 0;font-family: &quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size: 14px;line-height: 1.42857143;color: #333;background-color: #fff;">
	<div class="container" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;">
		<div class="header" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-bottom: 100px;margin-top: 25px;">
			<img class="img-responsive center-block" id="header" src="http://oleb2b.app/assets/emails/header.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;max-width: 100%!important;height: auto;margin-right: auto;margin-left: auto;opacity: 0.85;">
			<img class="img-responsive center-block" id="logo" src="http://oleb2b.app/assets/pages/img/logo-blanco.png" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;max-width: 100%!important;height: auto;margin-right: auto;margin-left: auto;margin-top: -90px;opacity: 0.85;width: 220px;">
		</div>
			<h1 id="cabecera" class="text-center" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: .67em 0;font-size: 40px;font-family: &quot;Arial&quot;, sans-serif;font-weight: bold;line-height: 1.1;color: inherit;margin-top: 20px;margin-bottom: 10px;text-align: center;">Hola</h1>
		
		<p id="parrafo" class="text-center" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;text-align: center;font-family: arial;font-size: 16px;margin-bottom: 40px;">Haga clic en el siguiente boton para restablecer su contraseña</p>
			<div class="text-center" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;text-align: center;">
				<button href="" class="btn btn-warning" id="boton" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: 0;font: inherit;color: #fff;overflow: visible;text-transform: none;-webkit-appearance: button;cursor: pointer;font-family: inherit;font-size: 14px;line-height: 1.42857143;display: inline-block;padding: 6px 12px;margin-bottom: 80px;font-weight: 400;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent;border-radius: 30px;background-color: #FF430A;border-color: #eea236;opacity: 0.75e;height: 50px;width: 200px;">Cambiar Contraseña</button>
			</div>
		<p class="text-center" id="informe2" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;text-align: center;font-family: &quot;Arial&quot;, sans-serif;font-size: 18px;">Si tiene problemas al hacer clic en el boton "cambiar de contraseña", copie y pegue la URL que aparece en su navegador web: </p>
		<p class="text-left" id="url" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;text-align: left;font-family: &quot;Arial&quot;,sans-serif;font-weight: bold;font-size: 20px;margin-left: 65px;font-color: black !important;"><a href="http://oleb2b.com/pass-word/ret/5a900cf67e34ab9f73ab1a60e8f9d01026d037b0d265119b413de165643fac7d" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: transparent;color: #337ab7;text-decoration: underline;">http://oleb2b.com/pass-<br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">word/ret/5a900cf67e34ab9f73ab1a60e8f9d01026d037b0d265119b413de165<br style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">643fac7d</a></p>
	</div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"></script>
</body>
</html>;


