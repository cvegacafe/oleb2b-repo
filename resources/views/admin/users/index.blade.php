@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Usuarios :</h1>
                </div>
            </div>
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Email</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Estado</th>
                    <th>Pais</th>
                    <th>Membresia</th>
                    <th>Productos</th>
                    <th>Acciones</th>
                </tr>
                </thead>
            </table>

            {{--<div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Email</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Estado</th>
                        <th>Pais</th>
                        <th>Membresia</th>
                        <th>Productos</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td><b>{{$loop->iteration}}</b></td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->names}}</td>
                            <td>{{$user->last_names}}</td>
                            <td>{{$user->status_name}}</td>
                            <td>{{$user->country->name}}</td>
                            @if($user->activeMembership())
                            <td>{{$user->activeMembership()->es_name }} [vence en {{$user->LastFinishAt}} dias]</td>
                            @else
                                <td>Sin membresia</td>
                            @endif
                            <td>{{$user->products->count()}}</td>
                                <td>
                                    <a href="{{action('Admin\UsersController@show',$user)}}" title="ver informacion del usuario"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="fa fa-eye"></span></button></a>
                                </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
                {{ $users->links() }}
            </div>--}}
        </div>
    </div>
@endsection
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection
@section('scripts')
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: false,
                ajax: '{!! route('userDatatables.data') !!}',
                order: [[ 0, "desc" ]],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'email', name: 'email' },
                    { data: 'names', name: 'names' },
                    { data: 'last_names', name: 'last_names' },
                    { data: 'status_name', name: 'status_name' },
                    { data: 'country.es_name', name: 'country.es_name' },
                    { data: 'active_membership', name: 'active_membership' },
                    { data: 'product_count', name: 'product_count' },
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection