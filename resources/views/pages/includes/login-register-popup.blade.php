<div class="auth-background">
    <div class="content-auth">
        <div class="header">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#ingresar">
                        <i class="fa fa-address-card-o"></i>
                        <span>{{trans('portal.sign_in')}}</span>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#registrarse">
                        <i class="fa fa-user-plus"></i>
                        <span>{{trans('portal.join_free')}}</span>
                    </a>
                </li>
            </ul>
            <i class="close fa fa-close"></i>
        </div>
        <div class="body">
            <div class="tab-content">
                <div id="ingresar" class="tab-pane fade in active">
                    <h3 id="msg-title">{{trans('portal.greeting')}}! {{trans('portal.enter_at_your_account')}}</h3>
                    <div class="row">
                        <div class="col-xs-12" id="login-form">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ action('Auth\LoginController@login') }}">
                                <input type="hidden" name="redirect_back" id="redirect_back"
                                       value="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"}}">
                                {{ csrf_field() }}
                                @if(isset($errors))
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        @endif
                                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                                            <div class="sub active">
                                                <input type="email" name="email" value="{{ old('email') }}"
                                                       placeholder="{{trans('user.form.email_address')}}" required
                                                       autofocus>
                                                @if (old('email'))
                                                    <span class="help-block">
                                                    <strong>{{ trans('auth.failed') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                                            <div class="sub">
                                                <input type="password"
                                                       placeholder="{{trans('user.form.password')}}" name="password"
                                                       required>
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                                            <div class="remember">
                                                <label>
                                                    {{trans('portal.remember_me')}} <input type="checkbox"
                                                                                           name="remember">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                                            <button type="submit">
                                                {{trans('portal.sign_in')}}
                                            </button>
                                            <a class="btn btn-link" href="#" id="forgot-password">
                                                {{trans('portal.forgot_password')}}
                                            </a>
                                        </div>
                                    </div>
                            </form>
                        </div>
                        <div class="col-xs-12" id="reset-password" style="display: none">
                            <form class="form-horizontal" role="form" method="POST"
                                  action="{{ action('Auth\ForgotPasswordController@sendResetLinkEmail') }}">
                                <input type="hidden" name="redirect_back"
                                       value="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"}}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email"
                                           class="col-md-4 control-label">{{trans('user.form.email_address')}}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email"
                                               value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{trans('passwords.send_password_reset_link')}}
                                        </button>
                                        <a class="btn btn-link " href="#" id="return-to-login">
                                            {{trans('passwords.back_to_login')}}
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="registrarse" class="tab-pane fade">
                    <form class="form-horizontal" v-on:submit="onSubmit" role="form" method="POST"
                          action="{{ action('Auth\RegisterController@register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12 col-md-6 ">
                                <div class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i>
                                            <input id="name" v-model="register.names"
                                                   type="text"
                                                   name="names"
                                                   value="{{ old('names') }}" required autofocus
                                                                                                  placeholder="{{trans('user.form.first_name')}}">
                                        </div>
                                        @if ($errors->has('names'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('names') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('last_names') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i>
                                            <input id="last_names" type="text" v-model="register.last_names" name="last_names" value="{{ old('last_names') }}"
                                                   required autofocus placeholder="{{trans('user.form.last_names')}}">
                                        </div>
                                        @if ($errors->has('last_names'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_names') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i>
                                            <input id="company" type="text" name="company" v-model="register.company"  value="{{ old('company') }}" required
                                                                                                  placeholder="{{trans('user.form.company')}}">
                                        </div>
                                        @if ($errors->has('company'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('company') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <i class="fa fa-asterisk requiredasterisk"></i>
                                        <select name="country_id" v-model="selected_country_id"
                                                class="form-control required selectpicker">
                                            <option value="0" selected disabled>{{trans('user.form.country')}}</option>
                                            <optgroup label="{{trans('user.can_sell_buy')}}">
                                                <option v-for="country in countries_can_sell" v-bind:value="country.id">
                                                    @{{country.name}}
                                                </option>
                                            </optgroup>
                                            <optgroup label="{{trans('user.can_buy')}}">
                                                <option v-for="country in countries_only_buy" v-bind:value="country.id">
                                                    @{{country.name}}
                                                </option>
                                            </optgroup>
                                        </select>
                                        @if ($errors->has('country_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('country_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 ">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i>
                                            <input type="email" name="email" v-model="register.email" value="{{ old('email') }}"
                                                   required placeholder="{{trans('user.form.email_address')}}">
                                        </div>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i>
                                            <input id="password" type="password" name="password" required
                                                   placeholder="{{trans('user.form.password')}}">
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i>
                                            <input id="password-confirm" type="password" name="password_confirmation"
                                                   required placeholder="{{trans('user.form.confirm_password')}}">
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group" v-bind:class="[checkOptions()? 'has-error': '' ]">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-xs-12 col-md-3 col-md-offset-1" v-if="show_buyer">
                                                <label for="checkbox_buyer" class=" checkbox-inline terms">
                                                    <input id="checkbox_buyer" type="checkbox" v-model="is_buyer"
                                                           name="checkbox_buyer"
                                                           class="checkbox"> {{trans('common.i_buy')}}
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-3 "
                                                 v-if="show_supplier">
                                                <label for="checkbox_supplier" class=" checkbox-inline terms">
                                                    <input id="checkbox_supplier" v-model="is_supplier" type="checkbox"
                                                           name="checkbox_supplier"
                                                           class="checkbox"> {{trans('common.i_sell')}}
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-md-3" v-if="show_both">
                                                <label for="checkbox_both" class=" checkbox-inline terms">
                                                    <input id="checkbox_both" type="checkbox" v-model="is_both"
                                                           name="checkbox_both"
                                                           class="checkbox"> {{trans('common.i_sell_buy')}}
                                                </label>
                                            </div>
                                        </div>
                                        <span class="help-block text-center" v-if="checkOptions() && selected_country_id !== 0">
                                                <strong>Debe seleccionar una opción</strong>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            @push('scripts')
                                <script>
                                    vm.$data.register.names = "{{ old('names') }}";
                                    vm.$data.register.last_names = "{{ old('last_names') }}";
                                    vm.$data.register.company = "{{ old('company') }}";
                                    vm.$data.register.email = "{{ old('email') }}";
                                </script>
                            @endpush
                            {{--<div class="col-xs-12  col-lg-6">
                                <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                           <i class="fa fa-asterisk requiredasterisk"></i><input id="office_phone" type="number" name="office_phone" min="0"
                                                   value="{{ old('office_phone') }}" required
                                                   placeholder="{{trans('user.form.office_phone')}}">
                                        </div>
                                        @if ($errors->has('office_phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('office_phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i><input id="mobile_phone" type="number" name="mobile_phone" min="0"
                                                   value="{{ old('mobile_phone') }}" required
                                                   placeholder="{{trans('user.form.mobile_phone')}}">
                                        </div>
                                        @if ($errors->has('mobile_phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mobile_phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                         <i class="fa fa-asterisk requiredasterisk"></i><input id="address" type="text" name="address" value="{{ old('address') }}"
                                                   required placeholder="{{trans('user.form.address')}}">
                                        </div>
                                        @if ($errors->has('mobile_phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mobile_phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                          <i class="fa fa-asterisk requiredasterisk"></i><input id="city" type="text" name="city" value="{{ old('city') }}" required
                                                   placeholder="{{trans('user.form.city')}}">
                                        </div>
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i><input id="postal_code" type="text" name="postal_code"
                                                   value="{{ old('postal_code') }}" required placeholder="{{trans('user.form.postal_code')}}">
                                        </div>
                                        @if ($errors->has('postal_code'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('postal_code') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                    <label for="postal_code"
                                           class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">{{trans('user.form.languages')}}</label>
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="checkbox-list">
                                         <i class="fa fa-asterisk requiredasterisklabel"></i><label class="checkbox-inline" v-for="language in languages">
                                                <input type="checkbox" name="languages[]" v-bind:value="language.id"/>&nbsp; @{{language.name}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('other_languages') ? ' has-error' : '' }}">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-offset-1 col-lg-10">
                                        <div class="sub">
                                            <i class="fa fa-asterisk requiredasterisk"></i><input id="other_languages" type="text" name="other_languages"
                                                   value="{{ old('other_languages') }}" placeholder="{{trans('user.form.others_languages')}}">
                                        </div>
                                        @if ($errors->has('other_languages'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('other_languages') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>--}}
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6 col-md-offset-3  col-lg-offset-3">
                                        <label for="terms_conditions" class=" checkbox-inline terms">
                                            <input id="terms_conditions" type="checkbox" name="terms_and_conditions"
                                                   class="checkbox" required> {{trans('portal.i_accept')}} <a
                                                    href="{{route('portal.terms-use')}}"
                                                    style="color:blue;text-decoration: underline">{{trans('portal.terms_of_use')}}</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-md-6 col-lg-offset-3 ">
                                        <button type="submit" :disabled="sending" class="btn btn-primary btn-block">
                                            {{trans('portal.join_free')}} <i v-if="sending" class="fa fa-refresh fa-spin"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>