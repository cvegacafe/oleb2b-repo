<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'button_change_password' => 'Change Password',
    'old_password_input' => 'Enter your old password',
    'new_password_input' => 'Enter new password',
    'new_password_input_confirmation' => 'Confirm new Password',
    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'send_password_reset_link' => "Send Password Reset Link",
    'back_to_login' => "Back to login",
    'message_mail_change_password'=>'to change your password please follow the LINK',
    'message_paste_browser'=>'If you’re having trouble clicking the " :name " button, copy and paste the URL below into your web browser:'
];
