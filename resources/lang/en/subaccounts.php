<?php
return [
    'add_new_subaccount' => 'Add a New SubAccount',
    'update_subaccount' => 'Update SubAccount',
    'markets' => 'Markets',
    'validation_edit' => 'This infromation can be edited by: :owner'
];