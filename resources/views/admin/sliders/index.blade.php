@extends('layouts.admin3')
@section('content')
    <div class="row">
        <a class="btn btn-primary" data-toggle="modal" href="#modal-id">Nuevo</a>
        <div class="modal fade" id="modal-id">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Nueva Imagen para slider</h4>
                    </div>
                    <form action="{{route('admin.sliders.store')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="modal-body ">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' :'' }}">
                                        <label for="title" class=" control-label">Titulo*</label>
                                        <input type="text" class="form-control input-sm" id="title" name="title"
                                               value="" required>
                                        @if($errors->has('title'))
                                            <small class="text-danger">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div><!-- /form-group -->
                                    <div class="form-group {{ $errors->has('duration') ? 'has-error' :'' }}">
                                        <label for="duration" class="control-label">Duración (Segundos)</label>
                                        <input type="number" class="form-control input-sm" id="duration"
                                               name="duration" value="3">
                                        @if($errors->has('duration'))
                                            <small class="text-danger">{{ $errors->first('duration') }}</small>
                                        @endif
                                    </div><!-- /form-group -->
                                    <div class="form-group {{ $errors->has('lang') ? 'has-error' :'' }}">
                                        <label for="lang" class="control-label">Idioma</label>
                                        <select name="lang" id="lang" class="form-control" required>
                                            <option value="es">Español</option>
                                            <option value="en">Inglés</option>
                                            <option value="ru">Ruso</option>
                                        </select>
                                        @if($errors->has('lang'))
                                            <small class="text-danger">{{ $errors->first('lang') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('url') ? 'has-error' :'' }}">
                                        <label for="url" class="control-label">Url de Redirección (opcional)</label>
                                        <input type="text" class="form-control input-sm" id="url" name="url">
                                        @if($errors->has('url'))
                                            <small class="text-danger">{{ $errors->first('url') }}</small>
                                        @endif
                                    </div><!-- /form-group -->
                                    <div class="form-group {{ $errors->has('image') ? 'has-error' :'' }}">
                                        <label for="image" class="control-label">Imagen* (820x370)</label>
                                        <input type="file" class="form-control input-sm" id="image" name="image">
                                        @if($errors->has('image'))
                                            <small class="text-danger">{{ $errors->first('image') }}</small>
                                        @endif
                                    </div><!-- /form-group -->
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button class="btn btn-primary">Guardar</button>
                        </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <br><br>
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Sliders:</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Nombre</th>
                        <th>Duración</th>
                        <th>Idioma</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sliders as $slider)
                        <tr>
                            <td>
                                <img src="{{asset($slider->path)}}" alt="" style="width: 200px"
                                     class="img-responsive"><br>
                            </td>
                            <td>
                                <p>{{$slider->title}}</p>
                            </td>
                            <td>
                                <p>{{$slider->duration}}</p>
                            </td>
                            <td>
                                <p>{{$slider->lang}}</p>
                            </td>
                            <td>
                                <form action="{{ route('admin.sliders.destroy',$slider) }}" method="POST" onsubmit="return confirm('Seguro que desea eliminar la imagen?')">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-danger">Eliminar</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
                {{ $sliders->links() }}
            </div>
        </div>
    </div>
@endsection