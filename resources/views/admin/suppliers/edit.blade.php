@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('common.forms.suppliers.company_details')}}</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{action('Admin\SupplierController@update',$supplier)}}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="category" class="col-md-4 control-label">{{trans('common.forms.suppliers.company_type')}}</label>
                                <div class="col-md-6">
                                    <select name="companytype" class="form-control required" >
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($companytypes as $comptype)
                                            <option value="{{$comptype->id}}" {{$comptype->id==$supplier->companytype->id?'selected':''}}>{{$comptype->en_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="company_name" class="col-md-4 control-label">{{trans('common.forms.suppliers.company_name')}} </label>
                                <div class="col-md-6">
                                    <input  type="text" class="form-control" name="company_name" value="{{ $supplier->company_name }}" required>
                                    @if ($errors->has('company_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                    @endif
                                </div>    
                            </div>

                            <div class="form-group{{ $errors->has('es_company_description') ? ' has-error' : '' }}">
                                <label for="es_name" class="col-md-4 control-label">{{trans('common.forms.suppliers.company_description')}} ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <textarea id="es_name" type="text" class="form-control" name="es_company_description" required>{{ $supplier->es_company_description}}</textarea>
                                    @if ($errors->has('es_company_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_company_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_company_description') ? ' has-error' : '' }}">
                                <label for="en_name" class="col-md-4 control-label">{{trans('common.forms.suppliers.company_description')}} ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <textarea id="en_name" type="text" class="form-control" name="en_company_description" required>{{ $supplier->en_company_description }}</textarea>
                                    @if ($errors->has('en_company_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_company_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_company_description') ? ' has-error' : '' }}">
                                <label for="ru_name" class="col-md-4 control-label">{{trans('common.forms.suppliers.company_description')}} ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <textarea id="ru_name" type="text" class="form-control" name="ru_company_description" required>{{$supplier->ru_company_description }}</textarea>
                                    @if ($errors->has('ru_company_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_company_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="member_trade_organization" class="col-md-4 control-label">{{trans('common.forms.suppliers.member_trade_organization')}} </label>
                                <div class="col-md-6">
                                    <input  type="text" class="form-control" name="member_trade_organization" value="{{ $supplier->member_trade_organization }}" required>
                                    @if ($errors->has('member_trade_organization'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('member_trade_organization') }}</strong>
                                    </span>
                                    @endif
                                </div>    
                            </div>

                            
                            <div class="form-group">
                                <label for="country_id" class="col-md-4 control-label">{{trans('products.place_origin')}}</label>
                                <div class="col-md-6">
                                    <select name="country_id" id="country_id" class="form-control required" >
                                        <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{$country->id==$supplier->country_id?'selected':''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="taxtype_id" class="col-md-4 control-label">{{trans('common.forms.suppliers.tax_type')}}</label>
                                <div class="col-md-6">
                                    <select name="taxtype_id" id="taxtype_id" class="form-control required">
                                        <option value="0" selected>{{trans('commin.forms.inputs.select')}}
                                        </option>
                                        @foreach ($taxtypes as $taxtype)
                                            <option value="{{$taxtype->id}}" {{$taxtype->id==$supplier->taxtype_id?'selected':''}}>{{$taxtype->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('tax_number') ? ' has-error' : '' }}">
                                <label for="tax_number" class="col-md-4 control-label">{{trans('common.forms.suppliers.tax_number')}} </label>
                                <div class="col-md-6">
                                    <input id="tax_number" type="text" class="form-control" name="tax_number" value="{{$supplier->tax_number}}" >
                                    @if ($errors->has('tax_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tax_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('company_legal_address') ? ' has-error' : '' }}">
                                <label for="company_legal_address" class="col-md-4 control-label">{{trans('common.forms.suppliers.company_legal_address')}}</label>
                                <div class="col-md-6">
                                    <input id="company_legal_address" type="text" class="form-control" name="company_legal_address" value="{{$supplier->company_legal_address}}" >
                                    @if ($errors->has('en_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_legal_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                <label for="postal_code" class="col-md-4 control-label">{{trans('common.forms.suppliers.postal_code')}} </label>
                                <div class="col-md-6">
                                    <input id="postal_code" type="text" class="form-control" name="postal_code" value="{{$supplier->postal_code}}" >
                                    @if ($errors->has('postal_code'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">{{trans('common.forms.suppliers.website')}} </label>
                                <div class="col-md-6">
                                    <input id="website" type="text" class="form-control" name="es_color"  value="{{$supplier->website}}" >
                                    @if ($errors->has('website'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('year_company_registered') ? ' has-error' : '' }}">
                                <label for="year_company_registered" class="col-md-4 control-label">{{trans('common.forms.suppliers.year_company_registered')}} </label>
                                <div class="col-md-6">
                                    <input id="year_company_registered" type="text" class="form-control" name="year_company_registered"  value="{{$supplier->year_company_registered}}" >
                                    @if ($errors->has('year_company_registered'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('year_company_registered') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('number_employees') ? ' has-error' : '' }}">
                                <label for="number_employees" class="col-md-4 control-label">{{trans('common.forms.suppliers.number_employees')}} </label>
                                <div class="col-md-6">
                                    <input id="number_employees" type="text" class="form-control" name="number_employees"  value="{{$supplier->number_employees }}" >
                                    @if ($errors->has('number_employees'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('number_employees') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{trans('common.forms.buttons.save')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection