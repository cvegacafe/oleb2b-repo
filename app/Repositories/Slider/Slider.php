<?php namespace App\Repositories\Slider;

use Illuminate\Database\Eloquent\Model;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed path
 */
class Slider extends Model
{
    protected $fillable = ['title', 'path', 'url', 'duration', 'lang'];
    public $timestamps = false;
}
