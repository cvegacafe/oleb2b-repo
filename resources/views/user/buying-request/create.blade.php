@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-oleb2b">
                    <div class="panel-heading">{{trans('buyingRequest.submit_buying_request')}}</div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{action('User\BuyingRequestController@store')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label for="supercategory" class="control-label">{{trans('products.product_supercategory')}}</label>
                                        <select name="supercategory" id="supercategory" class="form-control required" >
                                            <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                            @foreach($superCategories as $superCategory)
                                            <option value="{{$superCategory->id}}" {{old('supercategory') == $superCategory->id ? 'selected' : ''}}>{{$superCategory->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        <label for="category" class="control-label">{{trans('products.product_category')}}</label>
                                        <select name="category" id="category" class="form-control required" >
                                            @if(old('category'))
                                                <option value="{{old('category')}}" selected>{{$categories->where('id',old('category'))->first()->toArray()[Lang::locale().'_name']}}</option>
                                            @else
                                                <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('subcategory_id') ? ' has-error' : '' }}">
                                        <label for="subcategory_id" class="control-label">{{trans('products.product_subcategory')}}</label>
                                        <select name="subcategory_id" id="subcategory_id" class="form-control required" >
                                            @if(old('subcategory_id'))
                                                <option value="{{old('subcategory_id')}}" selected>{{$subcategories->where('id',old('subcategory_id'))->first()->toArray()[Lang::locale().'_name']}}</option>
                                            @else
                                                <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                            @endif
                                        </select>
                                        @if ($errors->has('subcategory_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('subcategory_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="control-label">{{trans('products.name')}}</label>
                                        <input id="name" type="text" class="form-control" name="name" value="{{old('name')}}" required>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="description" class="control-label">{{trans('products.description')}}</label>
                                        <textarea name="description" id="description" maxlength="250" cols="40" rows="5" class="form-control">{{old('description')}}</textarea>
                                        <small>Max 250 caracteres</small>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="start_date" class="control-label">{{trans('products.start_date')}}</label>
                                                <div class="form-group">
                                                    <div class='input-group date date_picker'>
                                                        <input type='text' class="form-control" name="start_date" id="start_date" value="{{old('start_date')}}" />
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <label for="end_date" class="control-label">{{trans('products.end_date')}}</label>
                                                <div class="form-group">
                                                    <div class='input-group date date_picker'>
                                                        <input type='text' id="end_date" class="form-control" name="end_date" value="{{old('end_date')}}"/>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
                                        <label for="country_id" class="control-label"> {{trans('buyingRequest.country_of_delivery')}}</label>
                                        <select name="country_id" id="country_id" class="form-control required" >
                                            <option value="0" selected>{{trans('common.forms.inputs.select')}}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}" {{ old('country_id') == $country->id ? 'selected' : '' }}>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country_id'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('country_id') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group{{ $errors->has('moq') ? ' has-error' : '' }}">
                                        <label for="order_qty" class="control-label">{{trans('buyingRequest.order_quantity')}}</label>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <input id="order_qty" min="0" type="number" class="form-control" name="order_qty" value="{{old('order_qty')}}" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <select name="measure_id" id="measure_id" class="form-control required" >
                                                    @foreach($measurements as $measure)
                                                        <option value="{{$measure->id}}" {{ old('measure_id') == $measure->id ? 'selected' : '' }}>{{$measure->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if ($errors->has('moq'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('moq') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('payment_terms') ? ' has-error' : '' }}">
                                        <label for="mobile_phone" class="control-label">{{trans('products.payment_terms')}}</label>
                                        <div class="checkbox-list">
                                            @foreach($paymentTerms as $paymentTerm)
                                                <label class="checkbox-inline">
                                                    <input id="{{$paymentTerm->id}}"  type="checkbox" name="payment_terms[]" value="{{$paymentTerm->id}}" {{in_array($paymentTerm->id,old('payment_terms',[]))? 'checked' : ''}}/>&nbsp;{{$paymentTerm->name}}
                                                </label>
                                            @endforeach
                                            @if ($errors->has('payment_terms'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('payment_terms') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('ports') ? ' has-error' : '' }}">
                                        <label for="ports" class="control-label">{{trans('products.ports')}}</label>
                                        <input id="ports" type="text" class="form-control" name="port" value="{{old('port')}}" >
                                        @if ($errors->has('ports'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('ports') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                        <label for="languages" class="control-label">{{trans('products.languages')}}</label>
                                        <div class="checkbox-list">
                                            @foreach($languages as $language)
                                                <label class="checkbox-inline">
                                                    <input id="{{$language->id}}"  type="checkbox" name="languages[]" value="{{$language->id}}" {{in_array($language->id,old('languages',[]))? 'checked' : ''}}/>&nbsp;{{$language->original_name}}
                                                </label>
                                            @endforeach
                                            @if ($errors->has('languages'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('languages') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                                        <label for="description" class="control-label">{{trans('products.product_photos')}}</label>
                                        <input type="file" name="photo" class="form-control">
                                        @if ($errors->has('photo'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('photo') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary">
                                            {{trans('common.forms.buttons.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>

        var PATH_SUPERCATEGORY = '{{route('api.categories.getBySupercategory')}}';
        var PATH_CATEGORY = '{{route('api.subcategories.getByCategory')}}';

        $(function(){
            $('#supercategory').on('change',function(e){

                e.preventDefault();

                $('#category').append("<option value='0'>{{trans('common.loading')}}</option>");

                var supercategory_id = $(this).find(':selected').val();

                var request = $.ajax({
                    url: PATH_SUPERCATEGORY,
                    method: "GET",
                    data: {supercategory_id:supercategory_id,language:'{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {

                    $('#category').find('option').remove();

                    $('#subcategory_id').find('option').remove();

                    if(data.length > 0){
                        $.each(data, function(index, element) {
                            $('#category').append("<option value='"+ element.id +"'>"+element.trans_name+"</option>");
                        });
                    }
                    $('#category').trigger('change');
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });

            $('#category').on('change',function(e){

                e.preventDefault();

                $('#subcategory_id').append("<option value='0'>{{trans('common.loading')}}</option>");

                var category_id = $(this).find(':selected').val();

                var request = $.ajax({
                    url: PATH_CATEGORY,
                    method: "GET",
                    data: {category_id:category_id,language:'{{Lang::locale()}}'},
                    dataType: "json"
                });

                request.done(function (data) {

                    $('#subcategory_id').find('option').remove();

                    if(data.length > 0){
                        $.each(data, function(index, element) {
                            $('#subcategory_id').append("<option value='"+ element.id +"'>"+element.trans_name+"</option>");
                        });

                    }

                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });
        });
    </script>
    @endsection