<?php namespace App\Notifications;

use App\Repositories\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeToOleb2b extends Notification
{
    use Queueable;

    private $user, $password;

    /**
     * Create a new notification instance.
     * @param User $user
     * @param null $password
     */
    public function __construct(User $user, $password = null)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $this->user->email_confirmacion = str_replace('/', '-', bcrypt(uniqid('Udachi')));
        $this->user->save();
        $userName = $this->user->names;
        $confirmationUrl = route('web.email-confirmation', [$this->user->email_confirmacion, $this->user->email]);
        $password = $this->password;
        return (new MailMessage)->view('mails.welcome',
            compact('userName', 'confirmationUrl', 'password'))->subject(trans('mails.welcome_to_oleb2b').' - OleB2B.com');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
