<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\Category\Category;
use App\Services\CategoryService;
use App\Services\LanguageService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{

    private $categoryService, $languageService;

    public function __construct(CategoryService $categoryService, LanguageService $languageService)
    {
        $this->categoryService = $categoryService;
        $this->languageService = $languageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->categoryService->all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = $this->languageService->all();

        return view('admin.categories.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $this->categoryService->create($request->all());
            Aviso::guardado();

        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    /* public function edit(Category $category)
     {
         $languages  = $this->languageService->all();
         return view('admin.categories.edit',compact('category','languages'));
     }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Category $category
     * @param CategoryService $categoryService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category, CategoryService $categoryService)
    {
        $categoryService->update($category->id, $request->except(['_token', '_method']));
        Aviso::actualizado();
        /*try {
            if ($this->categoryService->update($category->id, ['name' => $request->get('name')])) {
                foreach ($request->get('languages', []) as $key => $item) {
                    if ($category->languages->contains($key))
                        $category->languages()->updateExistingPivot($key, ['translation' => $item]);
                    else
                        $category->languages()->attach($key, ['translation' => $item]);
                }
            }
            Aviso::actualizado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noActualizado();
        }*/
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        try {

            $category->subcategories()->delete();
            $category->delete();
            Aviso::eliminado();

        } catch (Exception $e) {

            Log::error($e);
            Aviso::noEliminado();
        }
        return redirect()->action('Admin\CategoriesController@index');
    }
}
