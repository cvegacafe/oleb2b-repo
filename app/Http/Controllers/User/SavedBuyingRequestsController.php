<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\BuyingRequest\BuyingRequest;
use App\Services\CountryService;
use App\Services\LanguageService;
use App\Services\MeasureService;
use App\Services\PaymentTermService;
use App\Services\SavedBuyingRequestService;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SavedBuyingRequestsController extends Controller
{
    private $savedBuyingRequestService = null;

    public function __construct(SavedBuyingRequestService $savedBuyingRequestService)
    {
        $this->savedBuyingRequestService = $savedBuyingRequestService;
    }

    public function index(
        CountryService $countryService,
        MeasureService $measureService,
        PaymentTermService $paymentTermService,
        LanguageService $languageService
    ) {
        $allCountries = $countryService->all();
        $measurements = $measureService->all();
        $paymentTerms = $paymentTermService->all();
        $languages = $languageService->all();
        $savedBuyingRequests = $this->savedBuyingRequestService->getSavedBuyingRequestByUser(Auth::id());
        $buyingRequests = collect([]);

        foreach ($savedBuyingRequests as $savedBuyingRequest) {
            $buyingRequests->push($savedBuyingRequest->buyingRequest);
        }
        return view('user.savedBuyingRequest.index',
            compact('buyingRequests', 'allCountries', 'measurements', 'paymentTerms', 'languages'));
    }

    public function destroy(BuyingRequest $buyingRequest)
    {
        try {

            $savedbr = $this->savedBuyingRequestService->findSavedBuyingRequest($buyingRequest->id, Auth::id());

            if ($savedbr == null) {
                abort(503);
            }

            $savedbr->delete();
            Aviso::eliminado(trans('validation.custom.messages.record_deleted'));
        } catch (Exception $e) {
            Log::error($e);
            Aviso::eliminado(trans('validation.custom.messages.record_deleted_error'));
        }

        return redirect()->route('user.saved-buying-request.index');
    }
}
