@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row cabecera">
            <div class="col-xs-12 col-sm-8">
                <h3 class="titulo-1 titulo-dashboard">{{trans('menus.user.subaccounts')}}:</h3>
            </div>
            <div class="col-xs-12 col-sm-4 text-right">
                <a href="{{action('User\SubAccountsController@create')}}" class="boton boton-azul-no-shadow"> {{trans('subaccounts.add_new_subaccount')}} </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-oleb2b">
                    <div class="table-responsive">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('user.form.email_address')}}</th>
                                <th>{{trans('user.form.first_name')}}</th>
                                <th>{{trans('user.form.last_names')}}</th>
                                <th>{{trans('common.status')}}</th>
                                <th>{{trans('common.type')}}</th>
                                <th>{{trans('common.forms.buttons.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td><b>{{$loop->iteration}}</b></td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->names}}</td>
                                    <td>{{$user->last_names}}</td>
                                    <td>{{$user->status_name}}</td>
                                    <td>{{$user->typeSubAccount}}</td>
                                    <td>
                                        <a href="{{action('User\SubAccountsController@edit',$user)}}">
                                            <button class="btn btn-primary btn-xs" title="{{trans('common.forms.buttons.edit')}}"><span class="fa fa-pencil"></span></button>
                                        </a>
                                        @if(!$user->isPrincipalAccount())
                                            <form action="{{route('user.sub-accounts.destroy',$user)}}" method="POST" class="sweet-confirm"
                                                  data-title="{{trans('alerts.subaccounts.title_delete')}}"
                                                  data-message="{{trans('alerts.subaccounts.message_confirm',['name'=>$user->names])}}"
                                                  data-ok-button="{{trans('alerts.buttons.ok')}}">
                                                <input type="hidden" name="_method" value="DELETE">
                                                {{csrf_field()}}
                                                <button class="btn btn-primary btn-xs" title="{{trans('common.forms.buttons.delete')}}"><span class="fa fa-times"></span></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection