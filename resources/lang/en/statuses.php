<?php
return [
    'products' => [
        \App\Repositories\Product\Product::STATUS_CHECKED => 'Revisado',
        \App\Repositories\Product\Product::STATUS_NEW => 'Nuevo',
        \App\Repositories\Product\Product::STATUS_EDITED => 'Editado',
    ],
    'sections' => [
        \App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_BACKGROUND => 'Fondo (1920 x 536)',
        \App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_CCL => 'Camara de Comercio (209 x 96)',
        \App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_EVENTS => 'Feria y Eventos (209 x 125)',
        \App\Repositories\Oleb2bImage\Oleb2bImage::SECTION_PARTNERS => 'Socios (209 x 125)',
    ]
];