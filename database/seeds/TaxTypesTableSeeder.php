<?php

use App\Repositories\Taxtypes\Taxtypes;
use Illuminate\Database\Seeder;

class TaxTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $txRUC = Taxtypes::create(['id' => 1, 'name' => 'RUC']);
        $txRUC->countries()->attach([
            67 => ['digits' => 13],//Ecuador
            160 => ['digits' => 14],//Nicaragua
            173 => ['digits' => 9],//Paraguay
            174 => ['digits' => 11],//Peru
        ]);

        $txCUIIT = Taxtypes::create(['id' => 2, 'name' => 'CUIT']);
        $txCUIIT->countries()->attach([
            11 => ['digits' => 10],//Argentina
        ]);

        $txNit = Taxtypes::create(['id' => 3, 'name' => 'NIT']);
        $txNit->countries()->attach([
            27 => ['digits' => 10],//Bolivia
            52 => ['digits' => 10],//Colombia
            93 => ['digits' => 9],//Guatemala
            171 => ['digits' => 14],//Panama
            241 => ['digits' => 10],//Venezuela
            69 => ['digits' => 10],//el salvador
        ]);

        $taxCNPJ = Taxtypes::create(['id' => 4, 'name' => 'CNPJ']);
        $taxCNPJ->countries()->attach([
            32 => ['digits' => 14],//Brazil
        ]);

        $taxRUT = Taxtypes::create(['id' => 5, 'name' => 'RUT']);
        $taxRUT->countries()->attach([
            48 => ['digits' => 9],//Chile
            238 => ['digits' => 12],//Uruguay
        ]);


        $taxNITE = Taxtypes::create(['id' => 6, 'name' => 'NITE']);
        $taxNITE->countries()->attach([
            57 => ['digits' => 10],//Costa Rica
        ]);

        $txEIN = Taxtypes::create(['id' => 7, 'name' => 'EIN']);
        $txEIN->countries()->attach([
            59 => ['digits' => 9],//Cuba
            179 => ['digits' => 7],//Puerto Rico
        ]);

        $txRNC = Taxtypes::create(['id' => 8, 'name' => 'RNC']);
        $txRNC->countries()->attach([
            66 => ['digits' => 9],//Republica dominicana
        ]);

        $txRTN = Taxtypes::create(['id' => 9, 'name' => 'RTN']);
        $txRTN->countries()->attach([
            101 => ['digits' => 14],//Honduras
        ]);

        $txRFC = Taxtypes::create(['id' => 10, 'name' => 'RFC']);
        $txRFC->countries()->attach([
            145 => ['digits' => 12],//Mexico
        ]);

        $txNHH = Taxtypes::create(['id' => 11, 'name' => 'ИНН']);
        $txNHH->countries()->attach([
            183 => ['digits' => 12],//Rusia
        ]);
    }
}
