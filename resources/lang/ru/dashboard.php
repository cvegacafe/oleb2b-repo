<?php

return [
    'company_info' => 'О Компании',
    'products' => 'Товары',
    'certificates' => 'Сертификаты',
    'buying_requests' => 'Запросы Кп',
    'company_photos' => 'Фото Компании',
    'advanced' => 'Профессонал',
    'update' => 'Обновить',
    'submit' => 'Разместить',
    'add_products' => 'Добавить',
    'add_photos' => 'Добавить Фото',
    'upgrade' => 'Выбрать Другой',
    'membership' => 'членство'
];

