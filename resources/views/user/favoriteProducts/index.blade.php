@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="titulo-1 titulo-dashboard"><i class="fa fa-heart" style="font-size: 0.7em; color: #a2998e"></i> {{trans('menus.user.saved_products')}}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @include('global-partials.avisos')
                <section class="buying-requests">
                    @foreach($products as $product)
                        <div class="row single-request">
                            <div class="col-xs-12 col-sm-4 col-lg-2">
                                <a href="{{route('portal.singleProduct',$product->slug)}}" target="_blank">
                                    <figure>
                                        <img src="{{asset($product->main_photo)}}" alt="">
                                    </figure>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-4 product">
                                <ul>
                                    <li class="name"><a href="{{route('portal.singleProduct',$product->slug)}}" target="_blank">{{$product->name}}</a></li>
                                    <li>{{$product->description}}</li>
                                    <li><strong>{{trans('user.form.country')}}: </strong> {{$product->country->name}}</li>
                                    <li><strong>{{trans('products.moq')}}:</strong> {{$product->moq}} {{$product->moqMeasure->name}}</li>
                                    <li><strong>{{trans('products.price')}}:</strong> {{$product->price}} {{$product->currency->currency_code}}</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-lg-3 supplier">
                                <button data-toggle="collapse" data-target="#datos-empresa">{{trans('products.supplier_info')}} <i class="fa fa-caret-down"></i></button>
                                <ul id="datos-empresa" class="collapse">
                                    <li class="name">{{$product->user->supplier->company_name}}</li>
                                    <li><strong>{{trans('user.form.country')}}:</strong> {{$product->user->supplier->country->name}}</li>
                                    <li><strong>{{trans('products.membership')}}: {{$product->user->activeMembership()->name}} </strong></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-lg-3 buttons" >
                                @if($myProducts->contains('id',$product->id))
                                    <a href="{{route('user.products.edit',$product)}}">
                                        <button class="btn-block contact">
                                            {{trans('products.edit_product')}}
                                        </button>
                                    </a>
                                @elseif($threads->contains('product_id',$product->id))
                                    <a href="{{route('user.messages.products.index', ['product' => $product->id, 'thread' => $threads->where('product_id',$product->id)->first()->id])}}">
                                        <button class="btn-block contact">
                                            {{trans('menus.user.messages')}}
                                        </button>
                                    </a>
                                @else
                                    <button class="btn-block contact" onclick="contactWithSupplier('{{$product->id}}','{{$product->name}}','{{asset($product->main_photo)}}','{{$product->user->languages->implode(\Lang::locale().'_name',' ,')}}')">
                                        {{trans('portal.contact_supplier')}}
                                    </button>
                                @endif
                                <form action="{{route('user.saved-products.destroy',$product)}}" method="POST" class="sweet-confirm"
                                      data-title="{{trans('alerts.favoriteProducts.title_delete')}}"
                                      data-message="{{trans('alerts.favoriteProducts.message_confirm',['name'=>$product->name])}}"
                                      data-ok-button="{{trans('common.forms.buttons.delete')}}"
                                      data-cancel-button="{{trans('common.forms.buttons.cancel')}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{csrf_field()}}
                                    <button class="btn-block save"  >{{trans('common.forms.buttons.delete')}}</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </section>
            </div>
        </div>
    </div>
    @include('pages.includes.supplier-contact-popup',[
        'countries' => $allCountries,
        'measurements'=>$measurements,
        'paymentTerms' => $paymentTerms,
        'languages' => $languages
        ])
@endsection
@section('js')
    <script>
        function contactWithSupplier(product_id,product_name,product_photo,languages){
            var modal = $('#SupplierModal');
            modal.find('#product_id').val(product_id);
            modal.find('#product_name').text(product_name);
            modal.find('#product_photo').attr('src',product_photo);
            modal.find('#languages').text(languages);
            modal.find('#SupplierFormHtml')[0].reset();
            modal.modal();
        }
    </script>
@endsection