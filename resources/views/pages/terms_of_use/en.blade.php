@extends('layouts.pages')
@section('title')
    {{trans('portal.terms_of_use')}}
@endsection
@section('content')
    <div class="container-fluid contact-top">
        <div class="col-xs-12">
            <h1>{{trans('portal.terms_of_use')}}</h1>
        </div>
    </div>
    <section class="contact">
        <div class="container terms-list">
            <div class="row ">
                <h4>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY.
                    <small>(Updated on March 1， 2017)</small>
                </h4>
                <p>Welcome to <strong>www.oleb2b.com</strong>. These Terms of Use describe the terms and conditions applicable to your
                    access and use of the website at <strong>www.oleb2b.com (“Site”)</strong> . This document is a legally binding
                    agreement between you as the user(s) of the Site (referred to as “you”, “your” or “User”
                    hereinafter) and the Oleb2b.com entity listed in clause 2.1 below (referred to as “we”, “our” or
                    “Oleb2b.com” or “OleB2B.com S.A.C.” hereinafter).
                </p>

                <ol>
                    <li>
                        <strong> Application and Acceptance of the Terms</strong>
                        <ol>
                            <li>Your use of the Site and Oleb2b.com’s services, software and products (collectively the
                                as the “Services” hereinafter) is subject to the terms and conditions contained in this
                                document as well as the Privacy Policy , the Product Listing Policy and any other rules
                                and policies of the Sites that Oleb2b.com may publish from time to time. This document
                                and such other rules and policies of the Site are collectively referred to below as the
                                “Terms”. By accessing the Site or using the Services, you agree to accept and be bound
                                by the Terms. Please do not use the Services or the Site if you do not accept all of the
                                Terms.
                            </li>
                            <li>You may not use the Services and may not accept the Terms if (a) you are not of legal
                                age to form a binding contract with Oleb2b.com, or (b) you are not permitted to receive
                                any Services under the laws of Republic of Peru or other countries / regions including
                                the country / region in which you are resident or from which you use the Services.
                            </li>
                            <li>You acknowledge and agree that Oleb2b.com may amend any Terms at any time by posting the
                                relevant amended and restated Terms on the Sites. By continuing to use the Services or
                                the Sites, you agree that the amended Terms will apply to you.
                            </li>
                            <li>If Oleb2b.com has posted or provided a translation of the
                                English/Spanish/Russia/Portuguese language version of the Terms, you agree that the
                                translation is provided for convenience only and that the Spanish language version will
                                govern your uses of the Services or the Site.
                            </li>
                            <li>You may be required to enter into a separate agreement, whether online or offline, with
                                Oleb2b.com or our affiliate for any Service (“Additional Agreements”). If there is any
                                conflict or inconsistency between the Terms and an Additional Agreement, the Additional
                                Agreement shall take precedence over the Terms only in relation to that Service
                                concerned.
                            </li>
                            <li>The Terms may not otherwise be modified except in writing by an authorized officer of
                                Oleb2b.com.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong> Provision of Services</strong>
                        <ol>
                            <li>The Oleb2b.com contracting entity that you are contracting with is OleB2B.com S.A.C.
                                Peru if you are a registered member of the Site. And who you agree may invoice you for
                                their part of the Services.
                            </li>
                            <li>You must register as a member on the Site in order to access and use some Services.
                                Further, Oleb2b.com reserves the right, without prior notice, to restrict access to or
                                use of certain Services (or any features within the Services) to paying Users or subject
                                to other conditions that Oleb2b.com may impose in our discretion.
                            </li>
                            <li>Services (or any features within the Services) may vary for different regions and
                                countries. No warranty or representation is given that a particular Service or feature
                                or function thereof or the same type and extent of the Service or features and functions
                                thereof will be available for Users. Oleb2b.com may in our sole discretion limit, deny
                                or create different level of access to and use of any Services (or any features within
                                the Services) with respect to different Users.
                            </li>
                            <li>Oleb2b.com may launch, change, upgrade, impose conditions to, suspend, or stop any
                                Services (or any features within the Services) without prior notice except that in case
                                of a fee-based Service, such changes will not substantially adversely affect the paying
                                Users in using that Service.
                            </li>
                            <li>Some Services may be provided by OleB2B.com S.A.C.’s affiliates on behalf of
                                OleB2B.com S.A.C
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Users Generally</strong>
                        <ol>
                            <li>As a condition of your access to and use of the Sites or Services, you agree that you
                                will comply with all applicable laws and regulations when using the Sites or Services
                            </li>
                            <li>You agree to use the Site or Services solely for your own private and internal purposes.
                                You agree that (a) you will not copy, reproduce, download, re-publish, sell, distribute
                                or resell any Services or any information, text, images, graphics, video clips, sound,
                                directories, files, databases or listings, etc available on or through the Site (the
                                “Site Content”), and (b) you will not copy, reproduce, download, compile or otherwise
                                use any Site Content for the purposes of operating a business that competes with
                                Oleb2b.com, or otherwise commercially exploiting the Site Content. Systematic retrieval
                                of Site Content from the Sites to create or compile, directly or indirectly, a
                                collection, compilation, database or directory (whether through robots, spiders,
                                automatic devices or manual processes) without written permission from Oleb2b.com is
                                prohibited. Use of any content or materials on the Site for any purpose not expressly
                                permitted in the Terms is prohibited.
                            </li>
                            <li>You must read Oleb2b.com’s Privacy Policy which governs the protection and use of
                                personal information about Users in the possession of Oleb2b.com and our affiliates. You
                                accept the terms of the Privacy Policy and agree to the use of the personal information
                                about you in accordance with the Privacy Policy.
                            </li>
                            <li>Oleb2b.com may allow Users to access to content, products or services offered by third
                                parties through hyperlinks (in the form of word link, banners, channels or otherwise),
                                API or otherwise to such third parties' web sites. You are cautioned to read such web
                                sites' terms and conditions and/or privacy policies before using the Site. You
                                acknowledge that Oleb2b.com has no control over such third parties' web sites, does not
                                monitor such web sites, and shall not be responsible or liable to anyone for such web
                                sites, or any content, products or services made available on such web sites.
                            </li>
                            <li>You agree not to undertake any action to undermine the integrity of the computer systems
                                or networks of Oleb2b.com and/or any other User nor to gain unauthorized access to such
                                computer systems or networks.
                            </li>
                            <li>You agree not to undertake any action which may undermine the integrity of Oleb2b.com’s
                                feedback system, such as leaving positive feedback for yourself using secondary Member
                                IDs or through third parties or by leaving unsubstantiated negative feedback for another
                                User.
                            </li>
                            <li>By posting or displaying any information, content or material (“User Content”) on the
                                Site or providing any User Content to Oleb2b.com or our representative(s), you grant an
                                irrevocable, perpetual, worldwide, royalty-free, and sub-licensable (through multiple
                                tiers) license to Oleb2b.com to display, transmit, distribute, reproduce, publish,
                                duplicate, adapt, modify, translate, create derivative works, and otherwise use any or
                                all of the User Content in any form, media, or technology now known or not currently
                                known in any manner and for any purpose which may be beneficial to the operation of the
                                Site, the provision of any Services and/or the business of the User. You confirm and
                                warrant to Oleb2b.com that you have all the rights, power and authority necessary to
                                grant the above license.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Member Accounts</strong>
                        <ol>
                            <li>User must be registered on the Site to access or use some Services (a registered User is
                                also referred to as a “Member” below). Except with Oleb2b.com’s approval, one User may
                                only register one member account on the Site. Oleb2b.com may cancel or terminate a
                                User’s member account if Oleb2b.com has reasons to suspect that the User has
                                concurrently registered or controlled two or more member accounts. Further, Oleb2b.com
                                may reject User’s application for registration for any reason.
                            </li>
                            <li>Upon registration on the Site, Oleb2b.com shall assign an account and issue a member ID
                                and password (the latter shall be chosen by a registered User during registration) to
                                each registered User. An account may have a web-based email account with limited storage
                                space for the Member to send or receive emails.
                            </li>
                            <li>A set of Member ID and password is unique to a single account. Each Member shall be
                                solely responsible for maintaining the confidentiality and security of your Member ID
                                and password and for all activities that occur under your account. No Member may share,
                                assign, or permit the use of your Member account, ID or password by another person
                                outside of the Member’s own business entity. Member agrees to notify Oleb2b.com
                                immediately if you become aware of any unauthorized use of your password or your account
                                or any other breach of security of your account.
                            </li>
                            <li>Member agrees that all activities that occur under your account (including without
                                limitation, posting any company or product information, clicking to accept any
                                Additional Agreements or rules, subscribing to or making any payment for any services,
                                sending emails using the email account) will be deemed to have been authorized by the
                                Member.
                            </li>
                            <li>Member acknowledges that sharing of your account with other persons, or allowing
                                multiple users outside of your business entity to use your account (collectively,
                                "multiple use"), may cause irreparable harm to Oleb2b.com or other Users of the Site.
                                Member shall indemnify Oleb2b.com, our affiliates, directors, employees, agents and
                                representatives against any loss or damages (including but not limited to loss of
                                profits) suffered as a result of the multiple use of your account. Member also agrees
                                that in case of the multiple use of your account or Member’s failure to maintain the
                                security of your account, Oleb2b.com shall not be liable for any loss or damages arising
                                from such a breach and shall have the right to suspend or terminate Member’s account
                                without liability to Member.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Member’s Responsibilities</strong>
                        <ol>
                            <li>Each Member represents, warrants and agrees that (a) you have full power and authority
                                to accept the Terms, to grant the license and authorization and to perform the
                                obligations hereunder; (b) you use the Site and Services for business purposes only; and
                                (c) the address you provide when registering is the principal place of business of your
                                business entity. For purposes of this provision, a branch or liaison office will not be
                                considered a separate entity and your principal place of business will be deemed to be
                                that of your head office.
                            </li>
                            <li>Member will be required to provide information or material about your entity, business
                                or products/services as part of the registration process on the Site or your use of any
                                Service or the member account. Each Member represents, warrants and agrees that (a) such
                                information and material whether submitted during the registration process or thereafter
                                throughout the continuation of the use of the Site or Service is true, accurate, current
                                and complete, and (b) you will maintain and promptly amend all information and material
                                to keep it true, accurate, current and complete
                            </li>
                            <li>Upon becoming a Member, you consent to the inclusion of the contact information about
                                you in our Buyer Database and authorize Oleb2b.com and our affiliates to share the
                                contact information with other Users or otherwise use your personal information in
                                accordance with the Privacy Policy
                            </li>
                            <li>Each Member represents, warrants and agrees that (a) you shall be solely responsible for
                                obtaining all necessary third party licenses and permissions regarding any User Content
                                that you submit, post or display; (b) any User Content that you submit, post or display
                                does not infringe or violate any of the copyright, patent, trademark, trade name, trade
                                secrets or any other personal or proprietary rights of any third party and (c) you have
                                the right and authority to sell, trade, distribute or export or offer to sell, trade,
                                distribute or export the products or services described in the User Content and such
                                sale, trade, distribution or export or offer does not violate any Third Party Rights.
                            </li>
                            <li>Each Member further represents, warrants and agrees that the User Content that you
                                submit, post or display shall:
                                <ol>
                                    <li>be true, accurate, complete and lawful;</li>
                                    <li>not be false, misleading or deceptive;</li>
                                    <li> not contain information that is defamatory, libelous, threatening or harassing,
                                        obscene, objectionable, offensive, sexually explicit or harmful to minors;
                                    </li>
                                    <li>not contain information that is discriminatory or promotes discrimination based
                                        on race, sex, religion, nationality, disability, sexual orientation or age;
                                    </li>
                                    <li>not violate the Product Listing Policy, other Terms or any applicable Additional
                                        Agreements
                                    </li>
                                    <li>not violate any applicable laws and regulations (including without limitation
                                        those governing export control, consumer protection, unfair competition, or
                                        false advertising) or promote any activities which may violate any applicable
                                        laws and regulations;
                                    </li>
                                    <li>not contain any link directly or indirectly to any other web Sites which
                                        includes any content that may violate the Terms.
                                    </li>
                                </ol>
                            </li>
                            <li>Each Member further represents, warrants and agrees that you shall:
                                <ol>
                                    <li>carry on your activities on the Site in compliance with any applicable laws and
                                        regulations;
                                    </li>
                                    <li>conduct your business transactions with other users of the Site in good faith;
                                    </li>
                                    <li>carry on your activities in accordance with the Terms and any applicable
                                        Additional Agreements;
                                    </li>
                                    <li>not use the Services or Site to defraud any person or entity (including without
                                        limitation sale of stolen items, use of stolen credit/debit cards);
                                    </li>
                                    <li>not impersonate any person or entity, misrepresent yourself or your affiliation
                                        with any person or entity;
                                    </li>
                                    <li>not engage in spamming or phishing;</li>
                                    <li>not engage in any other unlawful activities (including without limitation those
                                        which would constitute a criminal offence, etc) or encourage or abet any
                                        unlawful activities;
                                    </li>
                                    <li>not involve attempts to copy, reproduce, exploit or expropriate Oleb2b.com’s
                                        various proprietary directories, databases and listings;
                                    </li>
                                    <li>not involve any computer viruses or other destructive devices and codes that
                                        have the effect of damaging, interfering with, intercepting or expropriating any
                                        software or hardware system, data or personal information;
                                    </li>
                                    <li>not involve any scheme to undermine the integrity of the data, systems or
                                        networks used by Oleb2b.com and/or any user of the Sites or gain unauthorized
                                        access to such data, systems or networks;
                                    </li>
                                    <li>not engage in any activities that would otherwise create any liability for
                                        Oleb2b.com or our affiliates.
                                    </li>
                                </ol>

                            </li>
                            <li>Member may not use the Services and member account to engage in activities which are
                                identical or similar to Oleb2b.com’s e-commerce marketplace business.
                            </li>
                            <li>If Member provides a business referee, Member represents, warrants and agrees that you
                                have obtained all necessary consents, approvals and waivers from your business partners
                                and associates to (a) act as your business referee; (b) post and publish their contact
                                details and information, reference letters and comments on their behalf; and (c) that
                                third parties may contact such business referees to support claims or statements made
                                about you. You further warrant and agree that all reference letters and comments are
                                true and accurate and third parties may contact the business referees without the need
                                to obtain your consent.
                            </li>
                            <li>Member agrees to provide all necessary information, materials and approval, and render
                                all reasonable assistance and cooperation necessary for Oleb2b.com’s provision of the
                                Services, evaluating whether Member has breached the Terms and/or handling any complaint
                                against the Member. If Member’s failure to do so results in delay in, or suspension or
                                termination of, the provision of any Service, OleB2B.com shall not be obliged to extend
                                the relevant service period nor shall be liable for any loss or damages arising from
                                such delay, suspension or termination.
                            </li>
                            <li>Member acknowledges and agrees that Oleb2b.com shall not be required to actively monitor
                                nor exercise any editorial control whatsoever over the content of any message or
                                material or information created, obtained or accessible through the Services or Sites.
                                Oleb2b.com does not endorse, verify or otherwise certify the contents of any comments or
                                other material or information made by any Member. Each Member is solely responsible for
                                the contents of their communications and may be held legally liable or accountable for
                                the content of their comments or other material or information
                            </li>
                            <li>Member acknowledges and agrees that the Services may only be used by businesses and
                                their representatives for business use and not for individual consumers or for personal
                                use.
                            </li>
                            <li>Member acknowledges and agrees that each Member is solely responsible for observing
                                applicable laws and regulations in its respective jurisdictions to ensure that all use
                                of the Site and Services are in compliance with the same.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong> Breaches by Members</strong>
                        <ol>
                            <li>Oleb2b.com reserves the right in our sole discretion to remove, modify or reject any
                                User Content that you submit to, post or display on the Site which we reasonably believe
                                is unlawful, violates the Terms, could subject Oleb2b.com to liability, or is otherwise
                                found inappropriate in Oleb2b.com’s opinion.
                            </li>
                            <li>If any Member breaches any Terms or if Oleb2b.com has reasonable grounds to believe that
                                any Member is in breach of any the Terms, Oleb2b.com shall have the right to impose a
                                penalty against the Member, or suspend or terminate the Member’s account or subscription
                                of any Service without any liability to the Member. Oleb2b.com shall also have the right
                                to restrict, refuse or ban any and all current or future use of any other Service that
                                may be provided by Oleb2b.com. The penalties that Oleb2b.com may impose include, among
                                others, warning, removing any product listing or other User Content that the Member has
                                submitted, posted or displayed, imposing restrictions on the number of product listings
                                that the Member may post or display, or imposing restrictions on the Member’s use of any
                                features or functions of any Service for such period as Oleb2b.com may consider
                                appropriate in our sole discretion.
                            </li>
                            <li>Without limiting the generality of the provisions of the Terms, a Member would be
                                considered as being in breach of the Terms in any of the following circumstances:
                                <ol>
                                    <li>upon complaint or claim from any third party, Oleb2b.com has reasonable grounds
                                        to believe that such Member has willfully or materially failed to perform your
                                        contract with such third party including without limitation where the Member has
                                        failed to deliver any items ordered by such third party after receipt of the
                                        purchase price, or where the items Member has delivered materially fail to meet
                                        the terms and descriptions outlined in your contract with such third party,
                                    </li>
                                    <li>Oleb2b.com has reasonable grounds to suspect that such Member has used a stolen
                                        credit card or other false or misleading information in any transaction with a
                                        counter party,
                                    </li>
                                    <li>Oleb2b.com has reasonable grounds to suspect that any information provided by
                                        the Member is not current or complete or is untrue, inaccurate, or misleading,
                                        or
                                    </li>
                                    <li>Oleb2b.com believes that the Member’s actions may cause financial loss or legal
                                        liability to Oleb2b.com or our affiliates or any other Users
                                    </li>
                                </ol>
                            </li>
                            <li>Oleb2b.com reserves the right to cooperate fully with governmental authorities, private
                                investigators and/or injured third parties in the investigation of any suspected
                                criminal or civil wrongdoing. Further, Oleb2b.com may disclose the Member's identity and
                                contact information, if requested by a government or law enforcement body, an injured
                                third party, or as a result of a subpoena or other legal action. Oleb2b.com shall not be
                                liable for damages or results arising from such disclosure, and Member agrees not to
                                bring any action or claim against Oleb2b.com for such disclosure.
                            </li>
                            <li>Oleb2b.com may, at any time and in our reasonable discretion, impose limitations on,
                                suspend or terminate the Member’s use of any Service or the Site without being liable to
                                the Member if Oleb2b.com has received notice that the Member is in breach of any
                                agreement or undertaking with any affiliate of Oleb2b.com and such breach involves or is
                                reasonably suspected of involving dishonest or fraudulent activities. Oleb2b.com shall
                                have the right to publish the records of such breach on the Site. Oleb2b.com shall not
                                be required to investigate such breach or request confirmation from the Member.
                            </li>
                            <li>Each Member agrees to indemnify Oleb2b.com, our affiliates, directors, employees, agents
                                and representatives and to hold them harmless, from any and all damages, losses, claims
                                and liabilities (including legal costs on a full indemnity basis) which may arise from
                                your submission, posting or display of any User Content, from your use of the Sites or
                                Services, or from your breach of the Terms.
                            </li>
                            <li>Each Member further agrees that Oleb2b.com is not responsible, and shall have no
                                liability to you or anyone else for any User Content or other material transmitted over
                                the Site, including fraudulent, untrue, misleading, inaccurate, defamatory, offensive or
                                illicit material and that the risk of damage from such material rests entirely with each
                                Member. Oleb2b.com reserves the right, at our own expense, to assume the exclusive
                                defense and control of any matter otherwise subject to indemnification by the Member, in
                                which event the Member shall cooperate with Oleb2b.com in asserting any available
                                defenses.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Transactions Between Buyers and Suppliers</strong>
                        <ol>
                            <li>Through the Site, Oleb2b.com provides electronic web-based platforms for exchanging
                                information between buyers and sellers of products and services. However, for any
                                Services, Oleb2b.com does not represent either the seller or the buyer in specific
                                transactions. Oleb2b.com does not control and is not liable to or responsible for the
                                quality, safety, lawfulness or availability of the products or services offered for sale
                                on the Site or the ability of the sellers to complete a sale or the ability of buyers to
                                complete a purchase.
                            </li>
                            <li>Users are hereby made aware that there may be risks of dealing with people acting under
                                false pretenses. Oleb2b.com uses several techniques to verify the accuracy of certain
                                information our paying Users provide us when they register for a paying membership
                                service on the Site. However, because user verification on the Internet is difficult,
                                Oleb2b.com cannot and does not confirm each User's purported identity (including,
                                without limitation, paying Members). We encourage you to use various means, as well as
                                common sense, to evaluate the integrity of the potential business partners.
                            </li>
                            <li>Each User acknowledges that it is fully assuming the risks of conducting any purchase
                                and sale transactions in connection with using the Site or Services, and that it is
                                fully assuming the risks of liability or harm of any kind in connection with subsequent
                                activity of any kind relating to products or services that are the subject of
                                transactions using the Site. Such risks shall include, but are not limited to,
                                misrepresentation of products and services, fraudulent schemes, unsatisfactory quality,
                                failure to meet specifications, defective or dangerous products, unlawful products,
                                delay or default in delivery or payment, cost miscalculations, breach of warranty,
                                breach of contract and transportation accidents. Such risks also include the risks that
                                the manufacture, importation, export, distribution, offer, display, purchase, sale
                                and/or use of products or services offered or displayed on the Site may violate or may
                                be asserted to violate Third Party Rights, and the risk that User may incur costs of
                                defense or other costs in connection with third parties’ assertion of Third Party
                                Rights, or in connection with any claims by any party that they are entitled to defense
                                or indemnification in relation to assertions of rights, demands or claims by Third Party
                                Rights claimants. Such risks also include the risks that consumers, other purchasers,
                                end-users of products or others claiming to have suffered injuries or harms relating to
                                products originally obtained by Users of the Sites as a result of purchase and sale
                                transactions in connection with using the Sites may suffer harms and/or assert claims
                                arising from their use of such products. All of the foregoing risks are hereafter
                                referred to as "Transaction Risks". Each User agrees that Oleb2b.com shall not be liable
                                or responsible for any damages, claims, liabilities, costs, harms, inconveniences,
                                business disruptions or expenditures of any kind that may arise a result of or in
                                connection with any Transaction Risks.
                            </li>
                            <li>Users are solely responsible for all of the terms and conditions of the transactions
                                conducted on, through or as a result of use of the Site or Services, including, without
                                limitation, terms regarding payment, returns, warranties, shipping, insurance, fees,
                                taxes, title, licenses, fines, permits, handling, transportation and storage.
                            </li>
                            <li>User agrees to provide all information and materials as may be reasonably required by
                                Oleb2b.com in connection with your transactions conducted on, through or as a result of
                                use of the Site or Services. Oleb2b.com has the right to suspend or terminate any User’s
                                account if the User fails to provide the required information and materials.
                            </li>
                            <li>In the event that any User has a dispute with any party to a transaction, such User
                                agrees to release and indemnify Oleb2b.com (and our agents, affiliates, directors,
                                officers and employees) from all claims, demands, actions, proceedings, costs, expenses
                                and damages (including without limitation any actual, special, incidental or
                                consequential damages) arising out of or in connection with such transaction.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Limitation of Liability</strong>
                        <ol>
                            <li>TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SERVICES PROVIDED BY OLEB2B.COM ON OR
                                THROUGH THE SITE ARE PROVIDED "AS IS", "AS AVAILABLE" AND “WITH ALL FAULTS”, AND
                                OLEB2B.COM HEREBY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
                                INCLUDING BUT NOT LIMITED TO, ANY WARRANTIES OF CONDITION, QUALITY, DURABILITY,
                                PERFORMANCE, ACCURACY, RELIABILITY, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
                                ALL SUCH WARRANTIES, REPRESENTATIONS, CONDITIONS, AND UNDERTAKINGS ARE HEREBY EXCLUDED.
                            </li>
                            <li>TO THE MAXIMUM EXTENT PERMITTED BY LAW, OLEB2B.COM MAKES NO REPRESENTATIONS OR
                                WARRANTIES ABOUT THE VALIDITY, ACCURACY, CORRECTNESS, RELIABILITY, QUALITY, STABILITY,
                                COMPLETENESS OR CURRENTNESS OF ANY INFORMATION PROVIDED ON OR THROUGH THE SITES;
                                OLEB2B.COM DOES NOT REPRESENT OR WARRANT THAT THE MANUFACTURE, IMPORTATION, EXPORT,
                                DISTRIBUTION, OFFER, DISPLAY, PURCHASE, SALE AND/OR USE OF PRODUCTS OR SERVICES OFFERED
                                OR DISPLAYED ON THE SITE DOES NOT VIOLATE ANY THIRD PARTY RIGHTS; AND OLEB2B.COM MAKES
                                NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING ANY PRODUCT OR SERVICE OFFERED
                                OR DISPLAYED ON THE SITE.
                            </li>
                            <li>Any material downloaded or otherwise obtained through the Site is done at each User's
                                sole discretion and risk and each User is solely responsible for any damage to
                                Oleb2b.com’s computer system or loss of data that may result from the download of any
                                such material. No advice or information, whether oral or written, obtained by any User
                                from Oleb2b.com or through or from the Site shall create any warranty not expressly
                                stated herein.
                            </li>
                            <li>The Site may make available to User services or products provided by independent third
                                parties. No warranty or representation is made with regard to such services or products.
                                In no event shall Oleb2b.com and our affiliates be held liable for any such services or
                                products.
                            </li>
                            <li>Each User hereby agrees to indemnify and save Oleb2b.com, our affiliates, directors,
                                officers and employees harmless, from any and all losses, claims, liabilities (including
                                legal costs on a full indemnity basis) which may arise from such User's use of the Site
                                or Services (including but not limited to the display of such User's information on the
                                Site) or from your breach of any of the terms and conditions of the Terms. Each User
                                hereby further agrees to indemnify and save Oleb2b.com, our affiliates, directors,
                                officers and employees harmless, from any and all losses, damages, claims, liabilities
                                (including legal costs on a full indemnity basis) which may arise from User's breach of
                                any representations and warranties made by User to Oleb2b.com, including but not limited
                                to those set forth in Section 5 hereunder.
                            </li>
                            <li>Each User hereby further agrees to indemnify and save Oleb2b.com, our affiliates,
                                directors, officers and employees harmless, from any and all losses, damages, claims,
                                liabilities (including legal costs on a full indemnity basis) which may arise, directly
                                or indirectly, as a result of any claims asserted by Third Party Rights claimants or
                                other third parties relating to products offered or displayed on the Site. Each User
                                hereby further agrees that Oleb2b.com is not responsible and shall have no liability to
                                you, for any material posted by others, including defamatory, offensive or illicit
                                material and that the risk of damages from such material rests entirely with each User.
                                Oleb2b.com reserves the right, at our own expense, to assume the exclusive defense and
                                control of any matter otherwise subject to indemnification by you, in which event you
                                shall cooperate with Oleb2b.com in asserting any available defenses.
                            </li>
                            <li>Oleb2b.com shall not be liable for any special, direct, indirect, punitive, incidental
                                or consequential damages or any damages whatsoever (including but not limited to damages
                                for loss of profits or savings, business interruption, loss of information), whether in
                                contract, negligence, tort, equity or otherwise or any other damages resulting from any
                                of the following.
                                <ol>
                                    <li>the use or the inability to use the Site or Services;</li>
                                    <li>any defect in goods, samples, data, information or services purchased or
                                        obtained from a User or any other third party through the Site;
                                    </li>
                                    <li>violation of Third Party Rights or claims or demands that User's manufacture,
                                        importation, export, distribution, offer, display, purchase, sale and/or use of
                                        products or services offered or displayed on the Site may violate or may be
                                        asserted to violate Third Party Rights; or claims by any party that they are
                                        entitled to defense or indemnification in relation to assertions of rights,
                                        demands or claims by Third Party Rights claimants;
                                    </li>
                                    <li> unauthorized access by third parties to data or private information of any
                                        User;
                                    </li>
                                    <li>statements or conduct of any User of the Site; or;</li>
                                    <li>any matters relating to Services however arising, including negligence.</li>
                                </ol>
                            </li>
                            <li>Notwithstanding any of the foregoing provisions, the aggregate liability of Oleb2b.com,
                                our employees, agents, affiliates, representatives or anyone acting on our behalf with
                                respect to each User for all claims arising from the use of the Site or Services during
                                any calendar year shall be limited to the greater of (a) the amount of fees the User has
                                paid to Oleb2b.com or our affiliates during the calendar year and (b) the maximum amount
                                permitted in the applicable law. The preceding sentence shall not preclude the
                                requirement by the User to prove actual damages. All claims arising from the use of the
                                Sites or Services must be filed within one (1) year from the date the cause of action
                                arose or such longer period as prescribed under any applicable law governing this Term
                                of Use
                            </li>
                            <li>The limitations and exclusions of liability to you under the Terms shall apply to the
                                maximum extent permitted by law and shall apply whether or not Oleb2b.com has been
                                advised of or should have been aware of the possibility of any such losses arising.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>Force Majeure</strong>
                        <ol>
                            <li>Under no circumstances shall Oleb2b.com be held liable for any delay or failure or
                                disruption of the content or services delivered through the Sites resulting directly or
                                indirectly from acts of nature, forces or causes beyond our reasonable control,
                                including without limitation, Internet failures, computer, telecommunications or any
                                other equipment failures, electrical power failures, strikes, labor disputes, riots,
                                insurrections, civil disturbances, shortages of labor or materials, fires, flood,
                                storms, explosions, acts of God, war, governmental actions, orders of domestic or
                                foreign courts or tribunals or non-performance of third parties.
                            </li>
                        </ol>
                    </li>
                    <li><strong>Intellectual Property Rights</strong>
                        <ol>
                            <li>All legal notices or demands to or upon Oleb2b.com shall be made in writing and sent to
                                OleB2B.com S.A.C. personally, by courier, certified mail to the following entity and
                                address: OleB2B.com S.A.C. 480 Francisco Graña, Lima 17, Peru. The notices shall be
                                effective when they are received by OleB2B.com S.A.C. in any of the above-mentioned
                                manner.
                            </li>
                            <li>All legal notices or demands to or upon a User shall be effective if either delivered
                                personally, sent by courier, certified mail, by facsimile or email to the last-known
                                correspondence, fax or email address provided by the User to Oleb2b.com, or by posting
                                such notice or demand on an area of the Site that is publicly accessible without a
                                charge. Notice to a User shall be deemed to be received by such User if and when
                                <ol>
                                    <li>Oleb2b.com is able to demonstrate that communication, whether in physical or
                                        electronic form, has been sent to such User, or
                                    </li>
                                    <li>Immediately upon Oleb2b.com posting such notice on an area of the Site that is
                                        publicly accessible without charge.
                                    </li>
                                </ol>
                            </li>
                            <li>You agree that all agreements, notices, demands, disclosures and other communications
                                that Oleb2b.com sends to you electronically satisfy the legal requirement that such
                                communication should be in writing.
                            </li>
                        </ol>
                    </li>
                    <li>
                        <strong>General Provisions</strong>
                        <ol>
                            <li>Subject to any Additional Agreements, the Terms constitute the entire agreement between
                                you and Oleb2b.com with respect to and govern your use of the Site and Services,
                                superseding any prior written or oral agreements in relation to the same subject matter
                                herein.
                            </li>
                            <li>Oleb2b.com and you are independent contractors, and no agency, partnership, joint
                                venture, employee-employer or franchiser-franchisee relationship is intended or created
                                by the Terms.
                            </li>
                            <li>If any provision of the Terms is held to be invalid or unenforceable, such provision
                                shall be deleted and the remaining provisions shall remain valid and be enforced.
                            </li>
                            <li>Headings are for reference purposes only and in no way define, limit, construe or
                                describe the scope or extent of such section.
                            </li>
                            <li>Oleb2b.com’s failure to enforce any right or failure to act with respect to any breach
                                by you under the Terms will not constitute a waiver of that right nor a waiver of
                                Oleb2b.com’s right to act with respect to subsequent or similar breaches.
                            </li>
                            <li>Oleb2b.com shall have the right to assign the Terms (including all of our rights,
                                titles, benefits, interests, and obligations and duties in the Terms to any person or
                                entity (including any affiliates of Oleb2b.com). You may not assign, in whole or part,
                                the Terms to any person or entity.
                            </li>
                            <li>If you are from Republic of Peru or outside of Republic of Peru, The Terms shall be
                                governed by the laws of Republic of Peru.
                            </li>
                        </ol>
                    </li>
                </ol>
            </div>
        </div>

    </section>
@endsection