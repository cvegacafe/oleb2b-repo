@if(isset($errors) && $errors->any())
    <div class="alert alert-danger" role="alert">
        <p>Por Favor Corrija los siguientes errores</p>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{str_replace(' id ',' ',$error)}}</li>
            @endforeach
        </ul>
    </div>
@endif