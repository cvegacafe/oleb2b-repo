<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Library\Aviso;
use App\Repositories\News\News;
use App\Services\NewsService;
use Illuminate\Http\Request;

/**
 * Class NewsController
 * @package App\Http\Controllers\Admin
 */
class NewsController extends Controller
{
    /**
     * @param NewsService $newsService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(NewsService $newsService)
    {
        $news = $newsService->all()->reverse();
        return view('admin.news.index', compact('news'));
    }

    /**
     * @param NewsRequest $request
     * @param NewsService $newsService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(NewsRequest $request, NewsService $newsService)
    {
        $news = $newsService->create($request->all());
        if ($news) {
            Aviso::guardado();
        } else {
            Aviso::noGuardado();
        }

        return redirect()->route('admin.show.news');
    }

    /**
     * @param News $new
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(News $new)
    {
        $new->delete();
        Aviso::eliminado();
        return redirect()->route('admin.show.news');
    }

}
