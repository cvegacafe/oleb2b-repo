<?php

use App\Repositories\Certification\Certification;
use App\Repositories\FactoryDetail\FactoryDetail;
use App\Repositories\Supplier\Supplier;
use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = \App\Repositories\Country\Country::all();
        $taxtypes  = \App\Repositories\Taxtypes\Taxtypes::all();
        $categories= \App\Repositories\Supercategory\Supercategory::all();
        $measures = \App\Repositories\Measure\Measure::all();
        $companyTypes  = \App\Repositories\CompanyType\CompanyType::all();
        factory(Supplier::class,40)->create([
            'country_id'=>$countries->random(1)->first()->id,
            'taxtype_id'=>$taxtypes->random(1)->first()->id,
            'supercategory_id'=>$categories->random(1)->first()->id,
            'company_type'=>$companyTypes->random()->id
        ])->each(function($supplier)use($measures){
            $supplier->factoryDetail()->save(factory(FactoryDetail::class)->make(['supplier_id'=>$supplier->id]));
            $supplier->certifications()->save(factory(Certification::class)->make(['supplier_id'=>$supplier->id]));
        });

    }
}
