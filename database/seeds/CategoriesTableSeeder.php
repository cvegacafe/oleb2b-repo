<?php

use App\Repositories\Category\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Supercategory::create(['id'=>1,'es_name'=>'Agricultura Y Alimentos','en_name'=>'Agriculture & Food','ru_name'=>'Продукты питания и Сельское хозяйство']);
        Category::create(['id'=>1,'supercategory_id'=>1,'en_name'=>'Agriculture & Food','es_name'=>'Agricultura Y Alimentos','ru_name'=>'Продукты питания и Сельское хозяйство']);
        Category::create(['id'=>2,'supercategory_id'=>1,'en_name'=>'Food & Beverage ','es_name'=>'Comida Y Bebidas','ru_name'=>'Продукты питания и напитки']);

        //Supercategory::create(['id'=>2,'es_name'=>'Agricultura E Insumos','en_name'=>'Agriculture and Farming supplies','ru_name'=>'Агропром и Животноводство']);
        Category::create(['id'=>3,'supercategory_id'=>2,'en_name'=>'Agriculture and Farming supplies','es_name'=>'Agricultura E Insumos','ru_name'=>'Агропром и Животноводство']);

        //Supercategory::create(['id'=>3,'es_name'=>'Ropa, Textiles y Accesorios','en_name'=>'Apparel, Textiles, Accessories','ru_name'=>'Одежда, Текстиль, Аксессуары']);
        Category::create(['id'=>4,'supercategory_id'=>3,'en_name'=>'Apparel','es_name'=>'Ropa','ru_name'=>'Одежда']);
        Category::create(['id'=>5,'supercategory_id'=>3,'en_name'=>'Textile & Leather Products','es_name'=>'Piel Y Textil','ru_name'=>'Текстиль и кожаные изделия']);
        Category::create(['id'=>6,'supercategory_id'=>3,'en_name'=>'Fashion Accessories','es_name'=>' Accesorios De Moda','ru_name'=>'Аксессуары']);
        Category::create(['id'=>7,'supercategory_id'=>3,'en_name'=>'Watches, Jewelry, Eyewear ','es_name'=>'Relojería, Joyería Y Lentes','ru_name'=>'Часы, очки, бижутерия']);

        //Supercategory::create(['id'=>4,'es_name'=>'Automóviles y Transporte','en_name'=>'Auto and Transportation','ru_name'=>'Автомобили и транспорт']);
        Category::create(['id'=>8,'supercategory_id'=>4,'en_name'=>'Automobiles & Motorcycles','es_name'=>'Automóviles Y Motocicletas','ru_name'=>'Авто и мото транспорт']);
        Category::create(['id'=>9,'supercategory_id'=>4,'en_name'=>'Transportation ','es_name'=>'Transporte','ru_name'=>'Транспорт']);

        //Supercategory::create(['id'=>5,'es_name'=>'Electrónicos','en_name'=>'Electronics','ru_name'=>'Электроника']);
        Category::create(['id'=>10,'supercategory_id'=>5,'en_name'=>'Computer Hardware & Software','es_name'=>'Informática Hardware Y Software','ru_name'=>'Програмное обеспечение и комплектующие ПК']);
        Category::create(['id'=>11,'supercategory_id'=>5,'en_name'=>'Home Appliance','es_name'=>'Electrodomésticos  ','ru_name'=>'Бытовая техника']);
        Category::create(['id'=>12,'supercategory_id'=>5,'en_name'=>'Consumer Electronics','es_name'=>'Electrónica De Consumo ','ru_name'=>'Бытовая электроника']);
        Category::create(['id'=>13,'supercategory_id'=>5,'en_name'=>'Secrurity and Safety equipment','es_name'=>'Seguridad Y Protección ','ru_name'=>'Системы безопасности и охраны труда']);

        //Supercategory::create(['id'=>6,'es_name'=>'Regalos y Juguetes','en_name'=>'Gifts &Toys','ru_name'=>'Игрушки и Подарки']);
        Category::create(['id'=>14,'supercategory_id'=>6,'en_name'=>'Gifts & Grafts','es_name'=>'Regalos Y Artesanía','ru_name'=>' Подарки и ремесла']);
        Category::create(['id'=>15,'supercategory_id'=>6,'en_name'=>'Toys & Hobbies ','es_name'=>'Juguetes Y Hobbies ','ru_name'=>'Игрушки и Хобби']);

        //Supercategory::create(['id'=>7,'es_name'=>'Deporte y Entretenimiento','en_name'=>'Sport & Entertainment Goods','ru_name'=>'Спорт и развлечения']);
        Category::create(['id'=>16,'supercategory_id'=>7,'en_name'=>'Sport & Entertainment Goods','es_name'=>'Articulos De Deporte Y Entretenimiento','ru_name'=>'Спорт и развлечения']);

        //Supercategory::create(['id'=>8,'es_name'=>'Hogar, Iluminación y Construcción','en_name'=>'Home, Construction, Light','ru_name'=>'Товары для дома, Строительство, Освещение']);
        Category::create(['id'=>17,'supercategory_id'=>8,'en_name'=>'Construction ','es_name'=>'Construcción E Inmobiliaria','ru_name'=>'Строительные товары']);
        Category::create(['id'=>18,'supercategory_id'=>8,'en_name'=>'Home & Garden','es_name'=>'Hogar Y Jardín ','ru_name'=>'Дом и сад ']);
        Category::create(['id'=>19,'supercategory_id'=>8,'en_name'=>'Lights & Lighting','es_name'=>'Luces E Iluminación ','ru_name'=>'Освещение']);
        Category::create(['id'=>20,'supercategory_id'=>8,'en_name'=>'Furniture','es_name'=>'Mobiliario ','ru_name'=>'Мебель']);

        //Supercategory::create(['id'=>9,'es_name'=>'Salud y Belleza','en_name'=>'Health and Beauty','ru_name'=>'Красота и Здоровье']);
        Category::create(['id'=>21,'supercategory_id'=>9,'en_name'=>'Health and Medical','es_name'=>'Salud Y Medicina ','ru_name'=>'Медицина и здоровье']);
        Category::create(['id'=>22,'supercategory_id'=>9,'en_name'=>'Beauty and Personal Care','es_name'=>'Belleza Y Cuidado Personal ','ru_name'=>'Красота и Здоровье']);

        //Supercategory::create(['id'=>10,'es_name'=>'Bolsos, Calzados y Accesorios','en_name'=>'Bags, shoes & accessories','ru_name'=>'Сумки, Обувь и Аксессуары']);
        Category::create(['id'=>23,'supercategory_id'=>10,'en_name'=>'Luggage, Bags & Cases','es_name'=>'Bolsos, Maletas Y Fundas ','ru_name'=>'Сумки, чемоданы и кейсы']);
        Category::create(['id'=>24,'supercategory_id'=>10,'en_name'=>'Shoes and accessories','es_name'=>'Calzado Y Accesorios ','ru_name'=>'Обувь и Аксессуары']);

        //Supercategory::create(['id'=>11,'es_name'=>'Equipamiento electrónico, componentes','en_name'=>'Electrical equipment, components','ru_name'=>'Электрооборудование и телекоммуникации']);
        Category::create(['id'=>25,'supercategory_id'=>11,'en_name'=>'Electrical Equipment & Supplies ','es_name'=>'Equipamiento Eléctrico Y Suministros ','ru_name'=>'Электрооборудование и комплектующие']);
        Category::create(['id'=>26,'supercategory_id'=>11,'en_name'=>'Electronic Compnents & Supplies','es_name'=>'Componentes Electrónicos Y Suministros ','ru_name'=>'Электрооборудование и комплектующие']);
        Category::create(['id'=>27,'supercategory_id'=>11,'en_name'=>'Telecommunication','es_name'=>'Telecomunicación','ru_name'=>'Телекоммуникации ']);

        //Supercategory::create(['id'=>12,'es_name'=>'Embalaje, publicitario, oficina','en_name'=>'Packaging, advertising, office','ru_name'=>'Упаковка, реклама и канцелярия']);
        Category::create(['id'=>28,'supercategory_id'=>12,'en_name'=>'Packaging & Printing ','es_name'=>'Empaquetado E Impresión ','ru_name'=>'Упаковочные материалы, Печать']);
        Category::create(['id'=>29,'supercategory_id'=>12,'en_name'=>'Office & School Supplies ','es_name'=>'Oficina Y Colegios Suministros ','ru_name'=>'Офисные и школьные товары']);
        Category::create(['id'=>30,'supercategory_id'=>12,'en_name'=>'Service Equipment','es_name'=>'Suministros De Servicio ','ru_name'=>'Сервисное оборудование']);

        //Supercategory::create(['id'=>13,'es_name'=>'Químicos y plásticos','en_name'=>'Chemicals and plastics & Rubber','ru_name'=>'Химия, пластик и резина']);
        Category::create(['id'=>31,'supercategory_id'=>13,'en_name'=>'Rubber & Plastics','es_name'=>'Plásticos, Gomas Y Cauchos ','ru_name'=>'Резина и пластик']);
        Category::create(['id'=>32,'supercategory_id'=>13,'en_name'=>'Energy','es_name'=>'Energía ','ru_name'=>'Энергия и Энергеника']);
        Category::create(['id'=>33,'supercategory_id'=>13,'en_name'=>'Chemicals','es_name'=>'Productos Químicos ','ru_name'=>'Химикаты']);

        //Supercategory::create(['id'=>14,'es_name'=>'Minerales y metalurgia','en_name'=>'Minerals and metallurgy','ru_name'=>'Минералы и металлургия']);
        Category::create(['id'=>34,'supercategory_id'=>14,'en_name'=>'Minerals & Metallurgy','es_name'=>'Metalurgia Y Minería ','ru_name'=>'Минералы и металлургия']);

        //Supercategory::create(['id'=>15,'es_name'=>'Maquinaria, partes y herramientas','en_name'=>'Machinery, Industrial Parts & Tools','ru_name'=>'Оборудование и инструменты']);
        Category::create(['id'=>35,'supercategory_id'=>15,'en_name'=>'Machinery','es_name'=>'Maquinaria','ru_name'=>'Оборудование ']);
        Category::create(['id'=>36,'supercategory_id'=>15,'en_name'=>'Industrial Parts & Fabrication Services','es_name'=>'Partes Industriales Y Servicios De Fabricación ','ru_name'=>'Запчасти для промышленности и услуги по производству ']);
        Category::create(['id'=>37,'supercategory_id'=>15,'en_name'=>'Tools ','es_name'=>'Herramientas ','ru_name'=>'Инструменты']);
        Category::create(['id'=>38,'supercategory_id'=>15,'en_name'=>'Hardware','es_name'=>'Hardware ','ru_name'=>'Комплектующие  и расходные материалы']);
        Category::create(['id'=>39,'supercategory_id'=>15,'en_name'=>'Measurement & Analysis Instruments ','es_name'=>'Medición E Instrumentos De Análisis ','ru_name'=>'Приборы для измерений и анализа']);



    }
}
