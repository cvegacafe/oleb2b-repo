<?php
return [
    'add_new_subaccount' => 'Agregar subcuenta',
    'update_subaccount' => 'Actualizar subcuenta',
    'markets' => 'Mercados',
    'validation_edit' => 'Esta sección solo puede ser editada por su administrador: :owner'
];