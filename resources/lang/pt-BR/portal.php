<?php

return [
    // Home
    'main_slogan' => 'Seu parceiro comercial na América Latina e na Rússia',
    'search' => 'Pesquisa',
    'product' => 'Produto',
    'supplier' => 'Provedor',
    'search_products' => 'Procurar produtos',
    'categories' => 'Categorias',
    'search_by_country' => 'Pesquisa por país',
    'buying_request' => 'Solicitações de cotação',
    'featured_products' => 'Produtos em destaque',
    'contact_supplier' => 'Contactar fornecedor',
    'not_featured_products' => 'Não há produtos em destaque',
    'how_it_works' => 'Como funciona?',
    'see_more' => 'Ver mais',
    'for' => 'Para',
    'new' => 'novo',
    'buyers' => 'Compradores',
    'users' => 'Usuários',
    'suppliers' => 'Fornecedores',
    'latest_news' => 'Notícia',
    'trade_organizations' => 'Câmaras de Comércio',
    'trade_shows_and_events' => 'Feiras e Eventos',
    'sale_online_is_easy' => 'Vender online é fácil',
    'join_free' => 'Register',
    'customer_support' => 'Assistência ao Consumidor',
    //Show Single Product

    'company_details' => 'Detalhe da empresa',
    'photos_and_logos' => 'Fotos e logotipo',
    'factory_details' => 'Detalhes Fábrica',
    'certifications_and_trademarks' => 'Certificados e Marcas Registadas',
    'save_for_later' => 'Guardar para mais tarde',
    'request_quote' => 'cotação',
    'related_products' => 'Produtos relacionados',
    'more_products_from_seller' => 'Mais produtos deste vendedor'

];
