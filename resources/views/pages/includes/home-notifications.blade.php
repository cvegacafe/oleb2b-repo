<div class="dropdown" id="async-notifications"  style="display: none">
    <a data-hover="dropdown" href="#" class="dropdown-toggle">
        <i class="fa fa-bell fa-fw"></i>
        <span class="badge badge-green notification-count">@{{ notifications.length }}</span>
    </a>
    <ul class="dropdown-menu active dropdown">
        <li class="header"><p>Tienes <strong class="notification-count">@{{ notifications.length  }}</strong> notificaciones</p></li>
        <li>
            <div class="dropdown-slimscroll">
                <ul>
                    <li v-for="notification in notifications" >
                        <a v-bind:href ="notification.url "  target="_blank">
                            <span class="message">
                                <i class="fa fa-envelope-o fa-lg"></i>
                                @{{ notification.title }}
                            </span>
                            <span class="fecha small">@{{ notification.created_at }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="last text-center cerrar">Cerrar</li>
    </ul>
</div>