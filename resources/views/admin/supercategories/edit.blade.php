@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar: {{$supercategory->name}}</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{action('Admin\SupercategoriesController@update',$supercategory)}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $supercategory->name }}" required autofocus>
                                </div>
                            </div>
                            <h3>Traducciones</h3>
                            @foreach($languages as $language)
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">{{$language->name}}</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="languages[{{$language->id}}]"
                                               value="{{$supercategory->languages->contains($language->id)? $supercategory->languages()->where('language_id',$language->id)->first()->pivot->translation:'' }}"
                                               required autofocus>
                                    </div>
                                </div>
                                @endforeach
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Actualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection