<?php namespace App\Repositories\User;

use App\Library\Constants;
use App\Notifications\ResetPasswordNotification;
use App\Repositories\Country\Country;
use App\Repositories\Language\Language;
use App\Repositories\Market\Market;
use App\Repositories\Membership\Membership;
use App\Repositories\Message\Message;
use App\Repositories\Notifications\Notification;
use App\Repositories\Product\Product;
use App\Repositories\Supplier\Supplier;
use App\Repositories\Thread\Thread;
use Carbon\Carbon;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, CanResetPassword, softDeletes;

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'email',
        'password',
        'names',
        'last_names',
        'email_confirmacion',
        'status',
        'office_phone',
        'mobile_phone',
        'address',
        'city',
        'postal_code',
        'other_languages',
        'company',
        'image',
        'type'
    ];

    const TYPES = [self::TYPE_BOTH, self::TYPE_BUYER, self::TYPE_SUPPLIER];
    const TYPE_BUYER = 'buyer';
    const TYPE_SUPPLIER = 'supplier';
    const TYPE_BOTH = 'both';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'status_name',
        'active_membership',
        'product_count'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'language_user', 'user_id', 'language_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function memberships()
    {
        return $this->belongsToMany(Membership::class, 'membership_user', 'user_id', 'membership_id')
            ->withPivot('main_rotation_banners', 'inner_rotation_banners', 'product_posting', 'amount', 'frequency',
                'finish_at', 'is_active')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function suppliers()
    {
        return $this->belongsToMany(Supplier::class, 'supplier_user', 'user_id',
            'supplier_id')->withPivot('is_principal')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class, 'from_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function markets()
    {
        return $this->belongsToMany(Market::class, 'market_user', 'user_id', 'market_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * @param string $directory
     * @return string
     */
    public function mainImage($directory = '')
    {
        $supplier = $this->supplier;

        if ($supplier) {
            if (isset($supplier->images[0])) {
                return $directory . $supplier->images[0];
            }
        }

        if ($this->image) {
            return $directory . $this->image;
        }

        return $directory . Constants::USER_DEFAULT_IMAGE;
    }

    // Accesors\\

    /**
     * @return mixed
     */
    public function getSupplierAttribute()
    {
        return $this->suppliers()->first();
    }

    /**
     * @param $value
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getCompanyAttribute($value)
    {
        return $value == null ? trans('buyingRequest.default_name_no_supplier') : $value;
    }

    /**
     * @return string
     */
    public function getTypeSubAccountAttribute()
    {
        $pivotRow = $this->supplier->users->where('pivot.user_id', $this->id)->first();
        if ($pivotRow) {
            return $pivotRow->pivot->is_principal ? 'Principal' : 'Empleado';
        }

        return 'No pertenece a empresa';
    }

    /**
     * @return string
     */
    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case Constants::USER_STATUS_ACTIVE :
                return 'Activo';
            case Constants::USER_STATUS_BLOCKED :
                return 'Bloqueado';
            case Constants::USER_STATUS_EMAIL_CONFIRM :
                return 'Email no validado';
            default :
                return 'No Reconocido';
        }
    }

    public function getProductCountAttribute()
    {
        return $this->products()->count();
    }

    public function getActiveMembershipAttribute()
    {
        $m =  $this->activeMembership();
        if($m){
            return $m->name;
        }
        return trans('memberships.basic');
    }

    /**
     * @return bool
     */
    public function isSupplier()
    {
        return $this->country->enabled_for_supplier ? true : false;
    }

    /**
     * @return int
     */
    public function getLastFinishAtAttribute()
    {
        $lastMembership = $this->activeMembership();
        return Carbon::now()->diffInDays(Carbon::parse($lastMembership->pivot->finish_at));
    }

    /**
     * @param $value
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value == null ? asset(Constants::USER_DEFAULT_IMAGE) : asset($value);
    }

    // Funciones Adicionales \\

    /**
     * @throws \Exception
     */
    public function lastMembership()
    {
        throw new \Exception('User ActiveMembership del modelo User en vez de lastMembership');
    }

    /**
     * @return mixed
     */
    public function activeMembership()
    {
        return $this->memberships->where('pivot.is_active', 1)->first();
    }

    /**
     * @return bool
     */
    public function isPrincipalAccount()
    {
        if (!$this->supplier) {
            return false;
        }
        $pivotRow = $this->supplier->users->where('pivot.user_id', $this->id)->first();

        if ($pivotRow) {
            return $pivotRow->pivot->is_principal;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isPremium()
    {
        $lastMembership = $this->activeMembership();

        if (!$lastMembership) {
            return false;
        }

        if ($lastMembership->id != Constants::MEMBERSHIP_PREMIUM) {
            return false;
        }

        if (Carbon::now()->gt(Carbon::parse($lastMembership->pivot->finish_at))) {
            return false;
        }

        return true;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->names . ' ' . $this->last_names;
    }
}
