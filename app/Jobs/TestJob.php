<?php namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $data['name'] = 'test';
        $data['email'] = 'test@oleb2b2.com';
        $data['subject'] = 'testing jobs';
        $data['message'] = 'Testing job';

        $mailer->send('mails.contact-form', ['data' => $data],
            function ($m) {
                $m->to('rockerosperu@gmail.com', 'Emmanuel Barturen')->subject('Test Jobs');
            });
        \Log::info('test job ejecutado');
    }
}
