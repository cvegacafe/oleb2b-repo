<?php
return [
    'buttons' => [
        'ok' => 'OK'
    ],

    'messages' => [
        'message_send' => 'Message sent',
        'message_not_send' => 'Message not sent',
        'attachment_not_found' => 'Attached file not found',
        'no_registered_error' => 'Couldn\'t register the user',
        'membership_pay_success' => 'Payment successful. Your membership :name'
    ],

    'middlewares' => [
        'has_not_product_available' => 'Maximum of products added',
        'supplier_data_required' => 'To upload products please fill in your Company Information'
    ],

    'password' => [
        'password_updated' => 'Password updated',
        'password_updated_error' => 'Pasword not updated',
        'password_current_invalid' => 'Current password is incorrect',
    ],

    'subaccounts' => [
        'title_delete' => 'Delete Sub-Account',
        'message_confirm' => 'Are you sure you want to delete user :name ?'
    ],

    'buyingRequest' => [
        'title_delete' => 'Delete the Buying Request',
        'message_confirm' => 'Are you sure you want to delete this Buying Request :name ?',
        'deleted' => 'Buying request deleted!',
        'deleted_error' => 'Couldn\'t delete the Buying Request'
    ],

    'products' => [
        'title_delete' => 'Delete product',
        'message_confirm' => 'Delete product :name ?',
        'deleted' => 'Product deleted',
        'deleted_error' => 'Product couldn\'t be deleted'
    ],

    'favoriteProducts' => [
        'title_delete' => 'Delete saved product',
        'message_confirm' => 'Delete saved product: :name ?'
    ],
    'validateEmail' => [
        'message' => 'We have sent you an email to confirm your email address',
        'message_confirm' => 'Email validated',
    ],
    'delete_certificate' => [
        'message' => 'Delete this certificate?',
        'message_confirm' => ' ',
    ],
];