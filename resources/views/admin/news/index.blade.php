@extends('layouts.admin3')
@section('content')
    @include('global-partials.validation-errors')
    <a class="btn btn-primary" data-toggle="modal" href="#modal-id">Nueva Noticia</a>
    <div class="modal fade" id="modal-id">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Nueva Noticia</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.show.news.post')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        <div class="form-group">
                            <label for="title">Enlace</label>
                            <input type="text" class="form-control" name="link">
                        </div>
                        <div class="form-group">
                            <label for="title">Descripción corta</label>
                            <input type="text" class="form-control" name="short_description">
                        </div>
                        <div class="form-group">
                            <label for="title">Imagen previa</label>
                            <input type="text" class="form-control" name="image">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="row">
        <table class="table table-bordred table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>short_description</th>
                <th>Accionesa</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($news as $new)
                <tr>
                    <td>
                        <a href="{{$new->link}}" target="_blank">
                            <img src="{{$new->image}}" alt="" style="width: 500px">
                        </a>
                    </td>
                    <td>{{$new->title}}</td>
                    <td>{{$new->short_description}}</td>
                    <td>
                        <form action="{{route('admin.show.news.destroy',$new)}}" method="POST"
                              onsubmit="return confirm('Seguro que desea eliminar?')">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" class="btn btn-danger">Borrar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="clearfix"></div>
    </div>
@endsection