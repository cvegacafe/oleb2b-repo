@extends('layouts.pages')
@section('title')
    Memberships
@endsection
@section('content')

    <div class="container">
        @include('global-partials.membership-content')
    </div>
    <div class="registrate" style="margin-top: 30px;">
        <div class="centro">
            <h3>{{trans('portal.sale_online_is_easy')}}</h3>
            <button onclick="showRegisterForm()">{{trans('portal.join_free')}}</button>
        </div>
    </div>
@endsection