<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Oleb2b - @yield('title')</title>
    <link rel="stylesheet" href="{{asset('assets/pages/css/styles.css')}}?v=1.2">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="description"
          content="B2B portal for Latin American and Russian suppliers. Find new products at best prices in Russia and LATAM countries. ">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link href="{{ asset('css/fontastic_icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/packages/slick-carousel/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/dropzone/dist/dropzone.css')}}">
    <link rel="icon" type="image/png" href="{{asset('assets/pages/img/favicon.png')}}">
    <link rel="stylesheet"
          href="{{asset('assets/packages/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}?v=1.1">

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-97378714-1', 'auto');
        ga('send', 'pageview');
    </script>

    @yield('styles')
    <script>
                @if(Auth::check())
        var IS_AUTH = true;
                @else
        var IS_AUTH = false;
        @endif
    </script>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-579BNTQ');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for http://oleb2b.com -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 500366, hjsv: 5};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-579BNTQ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
    <div class="top-top-nav {{ Request::url() == route('portal.home') ? 'home':'' }} {{ Request::url() == route('portal.search') ? 'search':'' }}">
        <div class="slider-bg" style="display: none; ">
            @php
                $backgrounds = isset($backgroundImages) ? $backgroundImages : [];
            @endphp
            @forelse($backgrounds as $image)
                <div class="item-slide">
                    <img src="{{asset($image->path)}}" alt="Exportación de {{$image->title}}">
                </div>
            @empty
                <div class="item-slide">
                    <img src="{{asset('assets/pages/img/top-bg/ropa-3.jpg')}}" alt="Exportación de avocado">
                </div>
                <div class="item-slide">
                    <img src="{{asset('assets/pages/img/top-bg/limon.jpg')}}" alt="Exportación de avocado">
                </div>
                <div class="item-slide">
                    <img src="{{asset('assets/pages/img/top-bg/ropa-2.jpg')}}" alt="moda peruana de exportación">
                </div>
                <div class="item-slide">
                    <img src="{{asset('assets/pages/img/top-bg/vino.jpg')}}" alt="Vino peruano">
                </div>
                <div class="item-slide">
                    <img src="{{asset('assets/pages/img/top-bg/uva.jpg')}}" alt="Uva peruana">
                </div>
            @endforelse
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6">
                    <div class="idiomas">
                        <ul>
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li class="{{$localeCode==\Lang::locale()?'active':''}}">
                                    <a rel="alternate" hreflang="{{$localeCode}}"
                                       href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                        {{ ucfirst($properties['native']) }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="tp-menu">
                        <ul>
                            @if(Auth::check())
                                <li><span>{{trans('portal.greeting')}},</span> {{Auth::user()->names}}</li>
                                <li>
                                    <span href="{{ url('/logout') }}"
                                          onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        {{trans('menus.user.logout')}}
                                    </span>
                                    <form id="logout-form" action="{{ action('Auth\LoginController@logout') }}"
                                          method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @else
                                {{--  <li>{{trans('portal.customer_support')}} <i class="fa fa-caret-down"></i></li>--}}
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top-nav {{ Request::url() == route('portal.home') ? 'home':'' }} {{ Request::url() == route('portal.search') ? 'search':'' }}">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 col-sm-2 col-toggle">
                    <div class="toggle-menu">
                        <i class="icon-menu"></i>
                    </div>
                </div>
                @if (!Auth::check())
                    <div class="col-md-6 col-mid">
                        <div class="top-text">
                            <span>{{trans('portal.free_to_start_online_trading')}} {{trans('portal.free')}}!  </span>
                            <button class="showRegister">{{trans('portal.join_free')}}</button>
                        </div>
                    </div>
                    <div class="col-md-0 logo-nuevo">
                        <a href="{{route('portal.home')}}">
                            <img src="{{asset('assets/pages/img/logo.png')}}" alt="" class="img-responsive logo-negro">
                            <img src="{{asset('assets/pages/img/logo-blanco.png')}}" alt=""
                                 class="img-responsive logo-blanco">
                        </a>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-sm-offset-1 logueo col-md-offset-0 col-md-6">
                        <div class="col-mid ingreso">
                            <div class="top-text">
                                <span>{{trans('portal.already_member')}} {{trans('portal.member')}}?</span>
                                <button class="showAuth azul">{{trans('portal.sign_in')}}</button>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xs-8 col-sm-8 col-md-0 logo-nuevo">
                        <a href="{{route('portal.home')}}">
                            <img src="{{asset('assets/pages/img/logo.png')}}" alt="" class="img-responsive logo-negro">
                            <img src="{{asset('assets/pages/img/logo-blanco.png')}}" alt=""
                                 class="img-responsive logo-blanco">
                        </a>
                    </div>
                    <div class="col-xs-2 col-sm-2 logueo col-md-offset-6 col-md-6">
                        <div class="myoleb2b">
                        <!--<span>
                                Hola, {{-- Auth::user()->names --}}
                                </span>-->
                            <a href="{{route('user.dashboard')}}">
                                <span>{{trans('common.my_oleb2b')}}</span> <i class="auth icon-user"></i>
                            </a>
                        </div>
                    </div>
                    {{--<div class="col-xs-2 col-sm-2 col-sm-offset-5 col-lg-offset-5 notifications-top">--}}
                    {{--@include('pages.includes.home-notifications')--}}
                    {{--</div>--}}
                @endif
            </div>
        </div>
    </div>
</header>

{{--Responsive--}}
<div class="mobile-side-menu">
    <div class="header">
        <div class="close"><i class="fa fa-close"></i></div>
        <figure>
            <img src="{{asset('assets/pages/img/logo.png')}}" alt="Oleb2b" class="logo">
        </figure>
        <div class="buttons">
            <strong>{{trans('common.my_oleb2b')}}</strong>
            @if(!Auth::check())
                <div class="row">
                    <div class="col-xs-6">
                        <a class="btn btn-default btn-block"
                           onclick="showRegisterForm()">{{trans('portal.join_free')}}</a>
                    </div>
                    <div class="col-xs-6">
                        <a class="btn btn-default btn-block" onclick="showAuth()">{{trans('portal.sign_in')}}</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="body">
        <div class="panel-group" id="accordion">
            <div class="panel">
                <div class="heading item0">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <span class="icon-idioma"></span> {{trans('user.form.languages')}}</a>
                        <i class="fa fa-chevron-down"></i>
                        <span class="current">{{Lang::locale()}}</span>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li class="{{$localeCode==\Lang::locale()?'active':''}}">
                                    <a rel="alternate" hreflang="{{$localeCode}}"
                                       href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                        {{ ucfirst($properties['native']) }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="heading item1">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            <span class="fa fa-user"></span>{{trans('account')}}
                        </a>
                        <i class="fa fa-chevron-down"></i>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Mensajes</a></li>
                            <li><a href="#">Mis productos Guardados</a></li>
                            <li><a href="#">Mis Solicitudes de compra</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel">
                <div class="heading item2">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            <span class="fa fa-question-circle"></span> {{trans('menus.footer.customer_service')}}</a>
                        <i class="fa fa-chevron-down"></i>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                            <li>
                                <a href="{{route('portal.privacy-politics')}}">{{trans('menus.footer.privacy_politics')}}</a>
                            </li>
                            <li><a href="{{route('portal.terms-use')}}">{{trans('menus.footer.terms_use')}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="redes">
                <h4>{{trans('menus.footer.follow_us')}} : </h4>
                <a href="#"><span class="fb"><i class="fa fa-facebook"></i></span></a>
                <a href="#"><span class="tw"><i class="fa fa-twitter"></i></span></a>
                <a href="#"><span class="ld"><i class="fa fa-linkedin"></i></span></a>
            </div>
        </div>
    </div>
</div>
{{--Responsive--}}
@if(isset($errors))
    @include('pages.includes.login-register-popup')
@endif
@yield('content')
<footer>
    <div class="container-fluid main-footer">
        <div class="row">
            <div class="col-md-15">
                <ul>
                    <li class="title"><i class="fa fa-angle-right"></i> {{trans('menus.footer.customer_service')}}</li>
                    <li><a href="{{route('portal.contact')}}">{{trans('menus.footer.contact_us')}}</a></li>
                    <li><a href="{{route('portal.terms-use')}}">{{trans('menus.footer.terms_use')}}</a></li>
                    <li><a href="{{route('portal.privacy-politics')}}">{{trans('menus.footer.privacy_politics')}}</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-15">
                <ul>
                    <li class="title"><i class="fa fa-angle-right"></i> {{trans('menus.footer.about_us')}}</li>
                    <li><a href="{{route('web.about')}}">{{trans('menus.footer.about_oleb2b')}}</a></li>
                    <li>{{trans('menus.footer.sitemap')}}</li>
                </ul>
            </div>
            <div class="col-md-15">
                <ul>
                    <li class="title"><i class="fa fa-angle-right"></i> {{trans('menus.footer.buy_on_oleb2b')}}</li>
                    <li><a href="{{route('portal.categories')}}">{{trans('products.by_category')}}</a></li>
                    @if(Auth::check())
                        <li>
                            <a href="{{action('User\BuyingRequestController@create')}}">{{trans('menus.footer.get_quotations')}}</a>
                        </li>
                    @else
                        <li><a href="#" class="showAuth">{{trans('menus.footer.get_quotations')}}</a></li>
                    @endif

                </ul>
            </div>
            <div class="col-md-15">
                <ul>
                    <li class="title"><i class="fa fa-angle-right"></i> {{trans('menus.footer.sell_on_oleb2b')}}</li>
                    @if(Auth::check())
                        <li>
                            <a href="{{route('user.memberships.index')}}"> {{trans('menus.footer.supplier_memberships')}}</a>
                        </li>
                    @else
                        <li>
                            <a href="{{route('portal.memberships')}}"> {{trans('menus.footer.supplier_memberships')}}</a>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="col-md-15">
                <span class="title"><i class="fa fa-angle-right"></i> {{trans('menus.footer.follow_us')}} :</span>
                <div class="redes">
                    <a href="https://www.facebook.com/oleb2b/"><span class="fb"><i
                                    class="fa fa-facebook"></i></span></a>
                    <a href="https://twitter.com/oleb2b"><span class="tw"><i class="fa fa-twitter"></i></span></a>
                    <a href=""><span class="gp"><i class="fa fa-google-plus"></i></span></a>
                    <a href=""><span class="ln"><i class="fa fa-linkedin"></i></span></a>
                </div>
            </div>

        </div>
    </div>
    <div class="container sub-footer">
        <div class="row">
            <div class="col-xs-12 col-xmd-5 col-md-2">
                <figure class="logo">
                    <img src="{{asset('assets/pages/img/logo.png')}}" alt="" class="img-responsive">
                </figure>
            </div>
            <div class="col-xs-12 col-xmd-7">
                <div class="copyrights">
                    ©OleB2B property of Olebaba.com SAC. <span>All rights reserved</span>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="img-lightbox">
    <figure>
        <img src="https://placehold.it/1200x600">
    </figure>
</div>
<script>
    var setFavoriteURL = '{{route('api.productFavorite.create')}}';
    var removeFavoriteURL = '{{route('api.productFavorite.remove')}}';
    var _TOKEN = '{{csrf_token()}}';
</script>

<script src="{{asset('assets/packages/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('assets/pages/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/packages/slick-carousel/slick/slick.min.js')}}"></script>
<script src="https://js.pusher.com/3.2/pusher.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="{{asset('assets/packages/vue/vue-resource.min.js')}}"></script>
<script src="{{asset('assets/packages/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/packages/dropzone/dist/dropzone.js')}}"></script>
<script src="{{asset('assets/pages/js/scripts.js')}}?v=1.0.0"></script>
@include('global-partials.avisos')
@yield('scripts')
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = _TOKEN;
    var resourceCountries = Vue.resource('{{route('api.country.all.byGroup')}}');
    var resourceLanguages = Vue.resource('{{route('api.languages.all')}}');
    console.log('languages: ', resourceLanguages)
    new Vue({
        mounted: function () {
            console.log('vue mounted')
            var self = this;
            resourceCountries.get({lang: '{{\Lang::locale()}}'}).then(function (response) {
                self.countries_can_sell = response.data.sell_and_buy;
                self.countries_only_buy = response.data.only_buy;
            });
            resourceLanguages.get().then(function (response) {
                self.languages = response.data.languages;
                console.log('get languages: ', self.languages)
            });
            /* resourceMarkets.get({lang: '{{\Lang::locale()}}'}).then(function (response) {
                self.markets = response.data.markets;
            });*/
        },
        el: '#registrarse',
        data: {
            countries_can_sell: [],
            countries_only_buy: [],
            languages: [],
            sending: false,
            is_buyer: false,
            show_buyer: false,
            is_supplier: false,
            show_supplier: false,
            is_both: false,
            show_both: false,
            selected_country_id: 0,
            register: {
                names: '',
                last_names: '',
                company: '',
                email: ''
            }


        },
        watch: {
            selected_country_id: function () {
                if (this.selected_country_id === 0) {
                    this.is_buyer = false;
                    this.is_supplier = false;
                    this.is_both = false;
                    this.show_buyer = false;
                    this.show_supplier = false;
                    this.show_both = false;
                } else {
                    this.is_buyer = true;
                    this.show_buyer = true;
                    for (index = 0; index < this.countries_can_sell.length; ++index) {
                        if (this.countries_can_sell[index].id === this.selected_country_id) {
                            this.is_supplier = true;
                            this.is_both = true;
                            this.show_supplier = true;
                            this.show_both = true;
                            return;
                        } else {
                            this.is_supplier = false;
                            this.is_both = false;
                            this.show_supplier = false;
                            this.show_both = false;
                        }
                    }
                }
            }
        },
        methods: {
            onSubmit: function (event) {
                if (this.selected_country_id === 0) {
                    event.preventDefault();
                    return true;
                } else {
                    if (this.checkOptions()) {
                        console.log(1);
                        event.preventDefault();
                        return false;
                    } else {
                        console.log(2);
                        this.sending = true;
                        return true;
                    }
                }

            },
            checkOptions: function () {
                return !this.is_buyer && !this.is_supplier && !this.is_both;
            }
        }
    });

    $(function () {
        $('#forgot-password').on('click', function () {
            $('#login-form').hide();
            $('#reset-password').show();
            $('#ingresar #msg-title').text(' ');
        });
        $('#return-to-login').on('click', function () {
            $('#login-form').show();
            $('#reset-password').hide();
            $('#ingresar #msg-title').text('{{trans('portal.greeting')}}! {{trans('portal.enter_at_your_account')}}');
        });
    });
</script>

@if(isset($show) && $show == 'login')
    <script>
        var top = $('header').height();
        var bg = $('.auth-background');
        $('body').css('overflow', 'hidden');
        $('.toggle-auth.icono .auth').hide();
        $('.toggle-auth.icono .fa').show();
        $('.content-auth .header ul li').removeClass('active');
        $('.content-auth .header ul li a[href="#ingresar"]').parent().addClass('active');
        $('#registrarse').removeClass('in active');
        $('#ingresar').addClass('in active');

        @if(isset($status) && $status == 'expired')
        $('#ingresar #msg-title').text('{{trans('validation.custom.general.expired')}}');
        @endif
        bg.fadeIn();

        if ($(window).width() < 992) {
            bg.css('top', top);
        }
    </script>
@elseif(isset($show) && $show == 'register')
    <script>
        var top = $('header').height();
        var bg = $('.auth-background');
        $('body').css('overflow', 'hidden');

        $('.toggle-auth.icono .auth').hide();
        $('.toggle-auth.icono .fa').show();

        $('.content-auth .header ul li').removeClass('active');
        $('.content-auth .header ul li a[href="#registrarse"]').parent().addClass('active');

        $('#ingresar').removeClass('in active');
        $('#registrarse').addClass('in active');
        bg.fadeIn();
        if ($(window).width() < 992) {
            bg.css('top', top);
        }
    </script>
@elseif(isset($show) && $show == 'reset-password')
    <script>
        var top = $('header').height();
        var bg = $('.auth-background');
        $('body').css('overflow', 'hidden');

        $('.toggle-auth.icono .auth').hide();
        $('.toggle-auth.icono .fa').show();
        $('.content-auth .header ul li').removeClass('active');
        $('.content-auth .header ul li a[href="#ingresar"]').parent().addClass('active');

        $('#registrarse').removeClass('in active');

        $('#ingresar').addClass('in active');

        bg.fadeIn();

        if ($(window).width() < 992) {
            bg.css('top', top);
        }

        $('#login-form').hide();
        $('#reset-password').show();
        $('#ingresar #msg-title').text(' ');
    </script>
@elseif(isset($show) && $show == 'contact-supplier' && old('product_id'))
    <script>
        var modal = $('#SupplierModal');
        modal.modal();
    </script>

@elseif(isset($show) && $show == 'contact-buyer' && old('buyingRequest_id'))
    <script>
        var modal = $('#BuyerModal');
        modal.modal();
    </script>
@endif

@if(Auth::check())
    <script>
        var resourceNotifications = Vue.resource('{{route('api.notifications')}}');
        var vmNotifications = new Vue({
            mounted: function () {
                var self = this;
                resourceNotifications.get({}).then(function (response) {
                    self.notifications = response.data;
                });
            },
            el: '#async-notifications',
            data: {
                notifications: []
            }
        });
    </script>
    <script type="text/javascript">
        (function () {
            Pusher.logToConsole = false;
            var pusherNoti = new Pusher('{{env('PUSHER_KEY')}}', {
                encrypted: true
            });
            var notifications = pusherNoti.subscribe('notifications-{{Auth::id()}}');
            notifications.bind('notification', function (data) {
                //console.log(data);
                vmNotifications.notifications.push({
                    description: data.description,
                    title: data.title,
                    created_at: data.created_at,
                    url: data.url
                });
            });
        })();
    </script>
@endif

</body>
</html>
