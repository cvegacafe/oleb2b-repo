<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Repositories\User\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'country_id'=>$faker->numberBetween(1,21),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('123456'),
        'names' => $faker->name,
        'last_names' => $faker->lastName,
        'email_confirmacion' => bcrypt(str_random(10)),
        'status' => $faker->numberBetween(1,3),
        'office_phone' => $faker->phoneNumber,
        'mobile_phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'city' => $faker->city,
        'postal_code' => $faker->postcode,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Repositories\Product\Product::class, function (Faker\Generator $faker) {

    return [
        'user_id'=>1,
        'subcategory_id' => 0,
        'country_id' => 0,
        'supply_measure_id' => 0,
        'moq_measure_id' => 0,
        'currency_id' => 0 ,
        'es_name'=>'esp-'. $faker->word(5),
        'en_name'=>'ing-'. $faker->word(5),
        'ru_name'=>'ru-'. $faker->word(5),
        'es_description'=>'esp-'. $faker->sentence,
        'en_description'=>'ingles-'. $faker->sentence,
        'ru_description'=>'ruso-'. $faker->sentence,
        'es_color'=>'esp-'. $faker->word(5),
        'en_color'=>'ing-'. $faker->word(5),
        'ru_color'=>'ru-'. $faker->word(5),
        'es_type'=>'esp-'. $faker->word(5),
        'en_type'=>'ing-'. $faker->word(5),
        'ru_type'=>'ru-'. $faker->word(5),
        'es_slug'=>'esp-'.$faker->unique()->slug,
        'en_slug'=>'ingls-'.$faker->unique()->slug,
        'ru_slug'=>'ruso-'.$faker->unique()->slug,
        'pictures' => null,
        'sale_unit_measure_id' => 0,
        'supply_ability' => $faker->randomDigitNotNull,
        'supply_ability_frecuency' => $faker->randomElement(['YEAR','MONTH','DAY']),
        'moq' => $faker->randomDigitNotNull,
        'delivery_time' => $faker->randomDigitNotNull,
        'price' => $faker->randomFloat(2, 1, 10000),
        'ports' => $faker->postcode,
        'is_approved'=>false
    ];
});

$factory->define(App\Repositories\Supplier\Supplier::class, function (Faker\Generator $faker) {

    return [
        'country_id'=>0,
        'taxtype_id' => 0,
        'supercategory_id' => 0,
        'company_type'=>0,
        'company_name' => $faker->company,
        'es_company_description'=>'esp-'. $faker->word,
        'en_company_description'=>'ing-'. $faker->word,
        'ru_company_description'=>'rus-'. $faker->word,
        'member_trade_organization' => $faker->word,
        'tax_number' => $faker->randomNumber(8) ,
        'company_legal_address' => $faker->address,
        'postal_code' => $faker->postcode,
        'website' => $faker->domainName,
        'year_company_registered' => $faker->year($max = 'now'),
        'number_employees' => $faker->numberBetween($min = 5, $max = 1000),
        'images' => $faker->shuffle([$faker->imageUrl,$faker->imageUrl,$faker->imageUrl])
    ];
});

$factory->define(\App\Repositories\FactoryDetail\FactoryDetail::class, function (Faker\Generator $faker) {

    return [
        'supplier_id'=>0,
        'factory_address' => $faker->address,
        'factory_size' => $faker->randomNumber(4),
        'number_qc_staff' => $faker->word,
        'number_rd_staff' => $faker->randomNumber(8) ,
        'number_production_lines' => $faker->randomNumber(2),
        'annual_output_value' => $faker->randomNumber(4),
        'annual_turnover' => null,
        'design_service_offered' => $faker->boolean ,
        'oem_service_offered' => $faker->boolean ,
    ];
});

$factory->define(\App\Repositories\Certification\Certification::class, function (Faker\Generator $faker) {

    return [
        'supplier_id'=>0,
        'type_certification' => $faker->word ,
        'issued_by' => $faker->company,
        'start_date' => $faker->dateTime,
        'expiration_date' => $faker->dateTime,
        'image' => 'uploads/files/pdf-sample.pdf',
        'image_extension'=>'pdf'
    ];
});

$factory->define(\App\Repositories\BuyingRequest\BuyingRequest::class, function (Faker\Generator $faker) {

    return [
        'user_id'=>1,
        'subcategory_id' => 1,
        'es_name'=>$faker->word,
        'es_description'=>$faker->word,
        'en_name'=>$faker->word,
        'en_description'=>$faker->word,
        'ru_name'=>$faker->word,
        'ru_description'=>$faker->word,
        'country_id' => 174,
        'measure_id' => 1,
        'order_qty' => $faker->randomDigitNotNull,
        'image' => null,
        'start_date' => $faker->dateTime,
        'end_date' => $faker->dateTime,
        'ports' => $faker->postcode,
        'is_approved'=>false
    ];
});