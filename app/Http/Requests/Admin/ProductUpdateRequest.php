<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "es_name" => "required",
            "en_name" => "required",
            "ru_name" => "required",
            "es_description" => "required",
            "en_description" => "required",
            "ru_description" => "required",
        ];
    }
}
