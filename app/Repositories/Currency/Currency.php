<?php namespace App\Repositories\Currency;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $table = 'currencies';
    public $fillable = ['currency', 'currency_code', 'enabled'];

}
