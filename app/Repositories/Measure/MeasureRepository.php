<?php namespace App\Repositories\Measure;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:11 PM
 */


use App\Repositories\BaseRepository;

class MeasureRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Measure();
        parent::__construct();
    }

}