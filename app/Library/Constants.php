<?php namespace App\Library;

class Constants
{
    /*Constants Statuses*/
    const USER_STATUS_ACTIVE = 1;
    const USER_STATUS_EMAIL_CONFIRM = 2;
    const USER_STATUS_BLOCKED = 3;

    /*memberships*/
    const MEMBERSHIP_BASIC = 1;
    const MEMBERSHIP_ADVANCED = 2;
    const MEMBERSHIP_PREMIUM = 3;

    const MEMBERSHIP_FREQUENCY = [
        ['id' => 'M', 'es_name' => 'Mensual', 'en_name' => 'Monthly', 'ru_name' => 'ежемесячно'],
        ['id' => 'Y', 'es_name' => 'Anual', 'en_name' => 'Annual', 'ru_name' => 'годовой'],
    ];

    const AVAILABLE_LANG = ['en', 'es', 'ru'];

    /* Payu Transaction */
    const PAYU_TRANSACTION_STATE_APPROVED = 4;
    const PAYU_TRANSACTION_STATE_DECLINED = 6;
    const PAYU_TRANSACTION_STATE_ERROR = 104;
    const PAYU_TRANSACTION_STATE_PENDING = 7;
    /*Payu Confirmation*/
    const PAYU_CONFIRMATION_CODE_APPROVED = 4;
    const PAYU_CONFIRMATION_CODE_DECLINED = 6;
    const PAYU_CONFIRMATION_CODE_EXPIRED = 5;

    /*threads*/
    const THREAD_HAS_BUYING_REQUEST = 1;
    const THREAD_HAS_PRODUCT = 2;

    /*Orders*/
    const ORDER_STATUS_PENDING = 1;
    const ORDER_STATUS_ACCEPTED = 2;
    const ORDER_STATUS_REJECTED = 3;
    const ORDER_STATUS_CANCELED = 4;

    /*FRECUENCY PLAN*/
    const MEMBERSHIP_FREQUENCY_ANNUAL = 'a';
    const MEMBERSHIP_FREQUENCY_MONTHLY = 'm';

    // Default Images \\
    const PRODUCT_DEFAULT_IMAGE = 'assets/pages/img/default-product-image.png';
    const SUPPLIER_DEFAULT_IMAGE = 'assets/pages/img/default-product-image.png';
    const USER_DEFAULT_IMAGE = 'assets/pages/img/user_default.png';
}