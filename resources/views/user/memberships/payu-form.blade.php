<form method="post" action="{{$payuParameters->url}}" id="payu-form">
    <input name="merchantId" type="hidden" value="{{$payuParameters->merchantId}}">
    <input name="accountId" type="hidden" value="{{$payuParameters->accountId}}">
    <input name="description" type="hidden" value="Test PAYU" id="description">
    <input name="referenceCode" type="hidden" value="TestPayU11" id="referenceCode">
    <input name="amount" type="hidden" value="" id="amount">
    <input name="buyerFullName" type="hidden" value="{{Auth::user()->names.' '.Auth::user()->last_names}}">
    <input name="buyerEmail" type="hidden" value="{{Auth::user()->email}}">
    <input name="tax" type="hidden" value="0">
    <input name="taxReturnBase" type="hidden" value="0">
    <input name="currency" type="hidden" value="USD" id="currency">
    <input name="signature" type="hidden" id="signature">
    <input name="test" type="hidden" value="{{$payuParameters->test ? '1' : '0' }}">
    <input name="confirmationUrl" type="hidden" value="{{route('web.payu-confirmation')}}">
    <input name="responseUrl" type="hidden" value="{{route('web.payu-response')}}">
</form>