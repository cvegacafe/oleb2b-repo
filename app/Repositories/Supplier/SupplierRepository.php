<?php namespace App\Repositories\Supplier;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:12 PM
 */
use App\Repositories\BaseRepository;

class SupplierRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Supplier();
        parent::__construct();
    }
}