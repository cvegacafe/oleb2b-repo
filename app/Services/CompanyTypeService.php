<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */
use App\Repositories\CompanyType\CompanyTypeRepository;

class CompanyTypeService extends BaseService
{

    public function __construct(CompanyTypeRepository $companyTypeRepository)
    {
        $this->mainRepo = $companyTypeRepository;
        parent::__construct();
    }
}