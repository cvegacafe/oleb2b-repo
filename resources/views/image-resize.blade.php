<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oleb2b</title>
</head>
<body>
<div class="flex-center position-ref full-height">
    <form action="#" id="soyformulario">
        <input id="imageFile" name="imageFile" type="file" class="imageFile"  accept="image/*"   />
        <input type="button" value="Resize Image"  onclick="ResizeImage()"/>
        <br/>
        <img src="" id="preview"  >
        <img src="" id="output">
    </form>
</div>
<script src="{{asset('assets/packages/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/user/js/fileInput.js')}}"></script>
<script>
    $(document).ready(function() {

        $('#imageFile').change(function(evt) {

            var files = evt.target.files;
            var file = files[0];

            if (file) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('preview').src = e.target.result;
                };
                reader.readAsDataURL(file);
            }
        });
    });

    function ResizeImage() {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var filesToUploads = document.getElementById('imageFile').files;
            var file = filesToUploads[0];
            if (file) {
                console.log(file);
                var reader = new FileReader();
                // Set the image once loaded into file reader
                reader.onload = function(e) {

                    var img = document.createElement("img");
                    img.src = e.target.result;

                    var canvas = document.createElement("canvas");
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0);

                    var MAX_WIDTH = 100;
                    var MAX_HEIGHT = 100;
                    var width = img.width;
                    var height = img.height;

                    if (width > height) {
                        if (width > MAX_WIDTH) {
                            height *= MAX_WIDTH / width;
                            width = MAX_WIDTH;
                        }
                    } else {
                        if (height > MAX_HEIGHT) {
                            width *= MAX_HEIGHT / height;
                            height = MAX_HEIGHT;
                        }
                    }

                    canvas.width = width;
                    canvas.height = height;
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0, width, height);
                    console.log(img);
                    dataurl = canvas.toDataURL(file.type);
                    console.log(dataurl);
                    document.getElementById('output').src = dataurl;
                };
                reader.readAsDataURL(file);
            }
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
    }
</script>
</body>
</html>
