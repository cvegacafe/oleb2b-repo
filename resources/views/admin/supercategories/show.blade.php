@extends('layouts.admin3')
@section('content')
    <div class="row">
        Supercategoria: <h1>{{$supercategory->name}}</h1>
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Categorias :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Espanol</th>
                        <th>Ingles</th>
                        <th>Ruso</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($supercategory->categories as $category)
                        <tr>
                            <td><b>{{$category->id}}</b></td>
                            <td>{{$category->es_name}}
                                <form action="{{action('Admin\CategoriesController@update',$category)}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$category->es_name}}','{{$category->id}}_es_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="es_name" id="{{$category->id}}_es_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$category->en_name}}
                                <form action="{{action('Admin\CategoriesController@update',$category)}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$category->en_name}}','{{$category->id}}_en_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="en_name" id="{{$category->id}}_en_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$category->ru_name}}
                                <form action="{{action('Admin\CategoriesController@update',$category)}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$category->ru_name}}','{{$category->id}}_ru_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="ru_name" id="{{$category->id}}_ru_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <a href="{{action('Admin\CategoriesController@show',$category)}}">
                                    <button class="btn btn-primary btn-xs" title="Subcategorias de categoria"><span
                                                class="fa fa-list"></span></button>
                                </a>
                                {{--
                                                                <a href="{{action('Admin\CategoriesController@edit',$category)}}"><button class="btn btn-primary btn-xs"  ><span class="fa fa-pencil"></span></button></a>
                                --}}
                                <form action="{{action('Admin\SubcategoriesController@destroy',$category)}}"
                                      method="POST"
                                      onsubmit="return confirm('Esta seguro q desea eliminar {{$category->name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs"><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <form action="{{action('Admin\CategoriesController@store')}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="supercategory_id" value="{{$supercategory->id}}">
                        <input type="hidden" name="type" value="normal">
                        <tfoot>
                        <tr>
                            <td></td>
                            <td><input type="text" name="es_name" class="form-control"></td>
                            <td><input type="text" name="en_name" class="form-control"></td>
                            <td><input type="text" name="ru_name" class="form-control"></td>
                            <td>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                            </td>
                        </tr>
                        </tfoot>
                    </form>
                </table>

            </div>

        </div>
        {{--<h4>Add List</h4>
        <form action="{{action('Admin\CategoriesController@store')}}" method="POST">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="supercategory_id" value="{{$supercategory->id}}">
            <input type="hidden" name="type" value="list">
            <td colspan="3"><textarea name="values" id="" cols="40" rows="3" placeholder="element1,element2,element3..." class="form-control"></textarea></td>
            <td><button type="submit" class="btn btn-primary">Agregar</button></td>
        </form>--}}
    </div>
@endsection
@section('scripts')
    <script>
        function cambiarValor(value, element) {
            var newvalue = prompt("Ingrese Nombre", value);
            if (newvalue != null) {
                console.log(element);
                $('#'+element).val(newvalue);
                return true;
            } else {
                return false;
            }
        }
    </script>
@endsection