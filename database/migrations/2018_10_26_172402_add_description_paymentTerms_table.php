<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddDescriptionPaymentTermsTable
 */
class AddDescriptionPaymentTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentTerms', function (Blueprint $table) {
            $table->string('es_description')->after('name')->nullable();
            $table->string('en_description')->after('es_description')->nullable();
            $table->string('ru_description')->after('en_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('es_description');
            $table->dropColumn('en_description');
            $table->dropColumn('ru_description');
        });
    }
}
