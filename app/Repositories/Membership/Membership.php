<?php namespace App\Repositories\Membership;

use App\Repositories\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Membership extends Model
{
    public $table = 'memberships';

    public $fillable = ['es_name', 'en_name', 'ru_name', 'main_rotation_banners', 'inner_rotation_banners', 'product_posting',
        'monthly_price', 'annual_price', 'buying_request_responses', 'subaccounts'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'membership_user', 'user_id', 'membership_id')->withPivot('main_rotation_banners', 'inner_rotation_banners', 'product_posting', 'amount','frequency','finish_at','is_active')->withTimestamps();
    }

    /*public function getFinishAtAttribute($value)
    {
        return Carbon::parse($value);
    }*/

    public function getNameAttribute()
    {
        return $this->nameByLang(Lang::locale());
    }

    public function nameByLang($lang)
    {
        return $this->getAttribute(strtolower($lang) . '_name');
    }

}
