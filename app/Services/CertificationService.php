<?php namespace App\Services;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:26 PM
 */

use App\Repositories\Certification\CertificationRepository;
use App\Repositories\Supplier\Supplier;

class CertificationService extends BaseService
{
    public function __construct(CertificationRepository $certificationRepository)
    {
        $this->mainRepo = $certificationRepository;
        parent::__construct();
    }

    public function createIfAllInputsFill(Supplier $supplier, array $inputs)
    {
        if (empty($inputs['type_certification']) || empty($inputs['issued_by']) || empty($inputs['start_date']) ||
            empty($inputs['expiration_date']) || empty($inputs['image'])) {
            return false;
        }
        $inputs['supplier_id'] = $supplier->id;
        return $this->mainRepo->create($inputs);
    }
}