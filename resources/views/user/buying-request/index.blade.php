@extends('layouts.app')
@section('content')
    <section class="buying-requests">
        <div class="container-fluid">
            <div class="row cabecera">
                <div class="col-xs-12 col-sm-8">
                    <h3 class="titulo-1 titulo-dashboard">{{trans('menus.user.manage_buying_request')}}</h3>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <a class="boton boton-azul submit-buying-request" href="{{action('User\BuyingRequestController@create')}}">{{trans('buyingRequest.submit_buying_request')}}</a>
                </div>
            </div>
            @if (session('info'))
                <div class="row">
                    <div class="col-xs-12" style="margin-top: 15px">
                        <div class="alert alert-info">
                            <strong>Info!</strong> {{ session('info') }}
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    @foreach($buyingRequests as $buyingRequest)
                        <div class="row single-request">
                            @include('global-partials.buying-request-row')
                            <div class="col-xs-12 col-lg-3 buttons" >
                                <a href="{{action('User\BuyingRequestController@edit',$buyingRequest)}}" class=""><button class="contact">{{trans('products.edit')}}</button></a>
                                <form action="{{route('user.buying-request.destroy',$buyingRequest)}}" method="POST" class="sweet-confirm"
                                      data-title="{{trans('alerts.buyingRequest.title_delete')}}"
                                      data-message="{{trans('alerts.buyingRequest.message_confirm',['name'=>$buyingRequest->name])}}"
                                      data-ok-button="{{trans('alerts.buttons.ok')}}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{csrf_field()}}
                                    <button class="save" title="{{trans('common.forms.buttons.delete')}}">{{trans('products.delete')}}</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection