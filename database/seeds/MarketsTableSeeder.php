<?php

use App\Repositories\Market\Market;
use Illuminate\Database\Seeder;

class MarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Market::create(['en_name'=>'Usa', 'es_name'=>'Estados Unidos', 'ru_name'=>'Сша']);
        Market::create(['en_name'=>'Europe', 'es_name'=>'Europa', 'ru_name'=>'Европа']);
        Market::create(['en_name'=>'Middle East', 'es_name'=>'Medio Oriente','ru_name'=>'Ближний Восток']);
        Market::create(['en_name'=>'Russia', 'es_name'=>'Rusia','ru_name'=>'Россия']);
        Market::create(['en_name'=>'Oceania', 'es_name'=>'Oceania','ru_name'=>'Океания']);
        Market::create(['en_name'=>'Canada', 'es_name'=>'Canada','ru_name'=>'Санада']);
        Market::create(['en_name'=>'Asia', 'es_name'=>'Asia','ru_name'=>'Азия']);
        Market::create(['en_name'=>'South America', 'es_name'=>'America del Sur', 'ru_name'=>'Южная Америка']);
        Market::create(['en_name'=>'Central America', 'es_name'=>'Central America', 'ru_name'=>'Центральная Америка']);
    }
}
