@extends('layouts.admin3')
@section('content')
    <div class="row">
        {{--<a href="{{action('Admin\CategoriesController@create')}}" class="btn btn-primary">New</a>--}}
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Categorias :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Espanol</th>
                        <th>Ingles</th>
                        <th>Ruso</th>
                        <th>Subcategorias</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td><b>{{$category->id}}</b></td>
                            <td>{{$category->es_name}}
                                <form action="{{action('Admin\CategoriesController@update',$category)}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$category->es_name}}','{{$category->id}}_es_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="es_name" id="{{$category->id}}_es_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$category->en_name}}
                                <form action="{{action('Admin\CategoriesController@update',$category)}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$category->en_name}}','{{$category->id}}_en_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="en_name" id="{{$category->id}}_en_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$category->ru_name}}
                                <form action="{{action('Admin\CategoriesController@update',$category)}}"
                                      method="POST"
                                      onsubmit="return cambiarValor('{{$category->ru_name}}','{{$category->id}}_ru_name')">
                                    <input type="hidden" name="_method" value="PUT">
                                    <input type="hidden" name="ru_name" id="{{$category->id}}_ru_name">
                                    {{csrf_field()}}
                                    <button class="btn btn-remove btn-primary btn-xs" type="submit"><span
                                                class="fa fa-pencil"></span>
                                    </button>
                                </form>
                            </td>
                            <td>{{$category->subcategories->count()}}</td>
                            <td>
                                <a href="{{action('Admin\CategoriesController@show',$category)}}">
                                    <button class="btn btn-primary btn-xs" title="Categorias de Supercategoria"><span
                                                class="fa fa-list"></span></button>
                                </a>
                                {{--
                                                                <a href="{{action('Admin\CategoriesController@edit',$category)}}"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="fa fa-pencil"></span></button></a>
                                --}}
                                <form action="{{action('Admin\CategoriesController@destroy',$category)}}" method="POST"
                                      onsubmit="return confirm('Esta seguro q desea eliminar {{$category->name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs"><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function cambiarValor(value, element) {
            var newvalue = prompt("Ingrese Nombre", value);
            if (newvalue != null) {
                console.log(element);
                $('#'+element).val(newvalue);
                return true;
            } else {
                return false;
            }
        }
    </script>
@endsection