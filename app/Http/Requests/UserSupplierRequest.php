<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'suppliers.company_name' => 'required|max:250',
            'suppliers.company_description' => 'required|max:150',
            "suppliers.member_trade_organization" => "required|max:50",
            "suppliers.country_id" => "required|exists:countries,id",
            "suppliers.taxtype_id" => "required|exists:taxtypes,id",
            "suppliers.tax_number" => "required|max:30",
            "suppliers.company_legal_address" => "required|max:255",
            "suppliers.postal_code" => "max:10",
            "suppliers.website" => "max:150",
            "suppliers.year_company_registered" => "numeric|max:9999",
            "suppliers.company_type" => "required|exists:companyTypes,id",
            "suppliers.number_employees" => "numeric|max:99999999",
            "suppliers.supercategory_id" => "required|exists:supercategories,id",
            "suppliers.images" => 'file',

            "new_certification.type_certification" => "max:255",
            "new_certification.issued_by" => "max:255",
            "new_certification.start_date" => "date_format:Y-m-d",
            "new_certification.expiration_date" => "date_format:Y-m-d",
            "new_certification.image" => "file",

            "factoryDetails.factory_address" => "max:255",
            "factoryDetails.factory_size" => "numeric|max:9999999",
            "factoryDetails.number_qc_staff" => "numeric|max:9999999",
            "factoryDetails.number_rd_staff" => "numeric|max:9999999",
            "factoryDetails.number_production_lines" => "numeric|max:9999999",
            "factoryDetails.annual_output_value" => "numeric|max:9999999",
            "factoryDetails.measure_id" => "exists:measurements,id"
        ];
    }
}
