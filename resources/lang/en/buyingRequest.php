<?php
return [
    'submit_buying_request' => 'Submit Buying request',
    'edit_buying_request' => 'Edit Buying request',
    'quantity' => 'Quantity',
    'order_quantity' => 'Order Quantity',
    'buyer_from' => 'Buyer from',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'contact_buyer' => 'Contact Buyer',
    'country_of_delivery' => 'Country of Delivery',
    'save_later' => 'Save for Later',
    'default_name_no_supplier' => 'Private',
    'buyer_info' => 'Buyer info',
    'product_description' => 'Product Description',
    'buyer' => 'Buyer',
    'this_buyer_speak' => 'This buyer speaks the following languages',
];