<?php namespace App\Repositories\Slider;

use App\Repositories\BaseRepository;

/**
 * Class SliderRepository
 * @package App\Repositories\Slider
 */
class SliderRepository extends BaseRepository
{
    /**
     * SliderRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Slider();
        parent::__construct();
    }

    /**
     * @param int $max
     * @param string $locale
     * @return
     */
    public function getSliders(int $max, string $locale)
    {
        return $this->model->where('lang', $locale)->take($max)->get();
    }

}