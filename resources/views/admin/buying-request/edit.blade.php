@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Traducciones</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{action('Admin\BuyingRequestController@update',$buying_request)}}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('es_name') ? ' has-error' : '' }}">
                                <label for="es_name" class="col-md-4 control-label">{{trans('products.name')}} ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <input id="es_name" type="text" class="form-control" name="es_name" value="{{$buying_request->es_name}}" required>
                                    @if ($errors->has('es_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_name') ? ' has-error' : '' }}">
                                <label for="en_name" class="col-md-4 control-label">{{trans('products.name')}} ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <input id="en_name" type="text" class="form-control" name="en_name" value="{{$buying_request->en_name}}" required>
                                    @if ($errors->has('en_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_name') ? ' has-error' : '' }}">
                                <label for="ru_name" class="col-md-4 control-label">{{trans('products.name')}} ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <input id="ru_name" type="text" class="form-control" name="ru_name" value="{{$buying_request->ru_name}}" required>
                                    @if ($errors->has('ru_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('es_description') ? ' has-error' : '' }}">
                                <label for="es_description" class="col-md-4 control-label">{{trans('products.description')}} ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <textarea name="es_description" id="es_description" cols="40" rows="5" class="form-control">{{$buying_request->es_description}}</textarea>
                                    @if ($errors->has('es_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_description') ? ' has-error' : '' }}">
                                <label for="en_description" class="col-md-4 control-label">{{trans('products.description')}} ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <textarea name="en_description" id="en_description" cols="40" rows="5" class="form-control">{{$buying_request->en_description}}</textarea>
                                    @if ($errors->has('en_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_description') ? ' has-error' : '' }}">
                                <label for="ru_description" class="col-md-4 control-label">{{trans('products.description')}} ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <textarea name="ru_description" id="ru_description" cols="40" rows="5" class="form-control">{{$buying_request->ru_description}}</textarea>
                                    @if ($errors->has('ru_description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('es_type') ? ' has-error' : '' }}">
                                <label for="es_type" class="col-md-4 control-label">{{trans('products.type')}} ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <input id="es_type" type="text" class="form-control" name="es_type" value="{{$buying_request->es_type}}" >
                                    @if ($errors->has('es_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_type') ? ' has-error' : '' }}">
                                <label for="en_type" class="col-md-4 control-label">{{trans('products.type')}} ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <input id="en_type" type="text" class="form-control" name="en_type" value="{{$buying_request->en_type}}" >
                                    @if ($errors->has('en_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_type') ? ' has-error' : '' }}">
                                <label for="ru_type" class="col-md-4 control-label">{{trans('products.type')}} ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <input id="ru_type" type="text" class="form-control" name="ru_type" value="{{$buying_request->ru_type}}" >
                                    @if ($errors->has('ru_type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_type') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('es_color') ? ' has-error' : '' }}">
                                <label for="city" class="col-md-4 control-label">{{trans('products.color')}} ({{trans('common.languages.spanish')}})</label>
                                <div class="col-md-6">
                                    <input id="es_color" type="text" class="form-control" name="es_color"  value="{{$buying_request->es_color}}" >
                                    @if ($errors->has('es_color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('es_color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('en_color') ? ' has-error' : '' }}">
                                <label for="en_color" class="col-md-4 control-label">{{trans('products.color')}} ({{trans('common.languages.english')}})</label>
                                <div class="col-md-6">
                                    <input id="en_color" type="text" class="form-control" name="en_color"  value="{{$buying_request->en_color}}" >
                                    @if ($errors->has('en_color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('en_color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('ru_color') ? ' has-error' : '' }}">
                                <label for="ru_color" class="col-md-4 control-label">{{trans('products.color')}} ({{trans('common.languages.russian')}})</label>
                                <div class="col-md-6">
                                    <input id="ru_color" type="text" class="form-control" name="ru_color"  value="{{$buying_request->ru_color}}" >
                                    @if ($errors->has('ru_color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ru_color') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{trans('common.forms.buttons.save')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
