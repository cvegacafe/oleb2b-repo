<?php namespace App\Repositories\Product;

use App\Library\Constants;
use App\Repositories\Category\Category;
use App\Repositories\Country\Country;
use App\Repositories\Currency\Currency;
use App\Repositories\DeliveryTerm\DeliveryTerm;
use App\Repositories\Measure\Measure;
use App\Repositories\PaymentTerm\PaymentTerm;
use App\Repositories\Subcategory\Subcategory;
use App\Repositories\Tag\Tag;
use App\Repositories\Thread\Thread;
use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class Product
 * @package App\Repositories\Product
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $user
 * @property mixed $subcategory
 * @property mixed $delivery_terms
 * @property mixed $payment_terms
 * @property mixed $moq_measure
 * @property mixed $currency
 * @property mixed $threads
 * @property mixed $country
 * @property mixed $supply_measure
 * @property mixed $sale_unit_measure
 * @property bool|\Carbon\Carbon|\DateTime|false|float|\Illuminate\Support\Collection|int|mixed|string|static $color
 * @property bool|\Carbon\Carbon|\DateTime|false|float|\Illuminate\Support\Collection|int|mixed|string|static $slug
 * @property bool|\Carbon\Carbon|\DateTime|false|float|\Illuminate\Support\Collection|int|mixed|string|static $name
 * @property bool|\Carbon\Carbon|\DateTime|false|float|\Illuminate\Support\Collection|int|mixed|string|static $type
 * @property bool|\Carbon\Carbon|\DateTime|false|float|\Illuminate\Support\Collection|int|mixed|string|static $description
 * @property mixed $main_photo
 * @property \Illuminate\Container\Container|mixed|string $supply_ability_frecuency
 * @property array $pictures
 * @property array $ports
 * @property mixed $tags
 * @property string $status_color
 * @property mixed status
 * @property \Illuminate\Container\Container|mixed $status_name
 */
class Product extends Model
{
    /**
     * @var string
     */
    public $table = 'products';

    public $fillable = [
        'user_id',
        'subcategory_id',
        'country_id',
        'supply_measure_id',
        'moq_measure_id',
        'currency_id',
        'pictures',
        'sale_unit_measure_id',
        'supply_ability',
        'supply_ability_frecuency',
        'moq',
        'delivery_time',
        'price',
        'ports',
        'es_name',
        'en_name',
        'ru_name',
        'es_description',
        'en_description',
        'ru_description',
        'es_color',
        'en_color',
        'ru_color',
        'es_slug',
        'en_slug',
        'ru_slug',
        'es_type',
        'en_type',
        'ru_type',
        'status',
        'price_type',
        'price_range'
    ];

    const STATUSES = [self::STATUS_NEW, self::STATUS_EDITED, self::STATUS_EDITED];
    const STATUS_NEW = 'new';
    const STATUS_EDITED = 'edit';
    const STATUS_CHECKED = 'checked';

    const TYPE_PRICES = [self::TYPE_PRICE_ASK, self::TYPE_PRICE_FIXED, self::TYPE_PRICE_RANGE];
    const TYPE_PRICE_FIXED = 'fixed';
    const TYPE_PRICE_RANGE = 'price_range';
    const TYPE_PRICE_ASK = 'ask_seller';

    protected $casts = [
        'pictures' => 'array',
        'ports' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deliveryTerms()
    {
        return $this->belongsToMany(DeliveryTerm::class, 'deliveryTerm_product', 'product_id',
            'deliveryTerm_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function paymentTerms()
    {
        return $this->belongsToMany(PaymentTerm::class, 'paymentTerm_product', 'product_id',
            'paymentTerm_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function moqMeasure()
    {
        return $this->belongsTo(Measure::class, 'moq_measure_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplyMeasure()
    {
        return $this->belongsTo(Measure::class, 'supply_measure_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function saleUnitMeasure()
    {
        return $this->belongsTo(Measure::class, 'sale_unit_measure_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function threads()
    {
        return $this->belongsToMany(Thread::class, 'thread_product', 'product_id', 'thread_id')
            ->withPivot([
                'country_id',
                'measure_id',
                'ports',
                'order_qty',
                'message',
                'payment_terms',
                'files'
            ])->withTimestamps();
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return mixed
     */
    public function getSlugAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_slug');
    }

    /**
     * @return mixed
     */
    public function getColorAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_color');
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_name');
    }

    /**
     * @return mixed
     */
    public function getTypeAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_type');
    }

    /**
     * @return mixed
     */
    public function getDescriptionAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_description');
    }

    /**
     * @return string
     */
    public function getMainPhotoAttribute()
    {
        if (isset($this->pictures[0])) {
            return $this->pictures[0];
        } else {
            return asset(Constants::PRODUCT_DEFAULT_IMAGE);
        }
    }

    /**
     * @return string
     */
    public function getStatusColorAttribute()
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'warning';
            case self::STATUS_EDITED:
                return 'warning';
            case self::STATUS_CHECKED:
                return 'success';
            default:
                return 'default';
        }
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getStatusNameAttribute()
    {
        return trans('statuses.products.' . $this->status);
    }

    /**
     * @param $value
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getSupplyAbilityFrecuencyAttribute($value)
    {
        switch ($value) {
            case 'YEAR':
                return trans('common.year');
            case 'MONTH' :
                return trans('common.month');
            case 'DAY':
                return trans('common.day');
            default:
                return 'No implementado';
        }
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return Lang::locale() . '_slug';
    }

    /**
     * @param $userId
     * @return int
     */
    public function newMessages($userId)
    {
        $messagesCount = 0;
        $threads = $this->threads;
        if ($threads->isEmpty()) {
            return 0;
        }

        foreach ($threads as $thread) {

            $messages = $thread->newMessages($userId);
            $messagesCount += $messages;
        }

        return $messagesCount;
    }

    /**
     * @param Product $category
     * @param string $old
     * @return string
     */
    public function buildUriCategoryParameters(Product $category, $old)
    {
        if (is_null($old)) {
            return $category->id;
        }
        $categories = array_filter(explode('-', $old));
        $categories[] = $category->id;
        return implode('-', $categories);
    }

    /**
     * @param Product $country
     * @param string $old
     * @return string
     */
    public function buildUriCountryParameters(Product $country, $old)
    {
        if ($old == trans('common.all')) {
            return $country->full_name;
        }
        $countries = array_filter(explode('-', $old));
        $countries[] = $country->full_name;
        return implode('-', $countries);
    }
}
