<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('es_name',50);
            $table->string('en_name',50);
            $table->string('ru_name',50);
            $table->unsignedTinyInteger('main_rotation_banners');
            $table->unsignedTinyInteger('inner_rotation_banners');
            $table->integer('product_posting');
            $table->decimal('monthly_price');
            $table->decimal('annual_price');
            $table->boolean('buying_request_responses')->default(false);
            $table->unsignedTinyInteger('subaccounts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
