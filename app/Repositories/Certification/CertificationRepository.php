<?php namespace App\Repositories\Certification;


use App\Repositories\BaseRepository;

class CertificationRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new Certification();
        parent::__construct();
    }
}