<?php namespace App\Repositories\Notifications;

use App\Repositories\User\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    protected $fillable = ['user_id', 'icon', 'es_title', 'en_title', 'ru_title', 'read_at', 'url'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function trans_title($lang)
    {
        return $this->getAttribute(strtolower($lang) . '_title');
    }
}
