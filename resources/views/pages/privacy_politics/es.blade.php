@extends('layouts.pages')
@section('title')
    Contact
@endsection
@section('content')
    <div class="container-fluid contact-top">
        <div class="col-xs-12">
            <h1>{{trans('menus.footer.privacy_politics')}}</h1>
        </div>
    </div>
    <section class="contact">
        <div class="container">
            <div class="row">
                <strong>Política de privacidad</strong>

                (Actualizado 1 de marzo de 2017)

                OleB2B.com (el &quot;Sitio&quot;) es una plataforma de comercio electrónico que se utiliza predominantemente

                por las entidades empresariales para facilitar el comercio electrónico y tal uso no implica generalmente

                la recolección de información personal de los individuos. OleB2B.com reconoce la importancia de la

                privacidad, así como la importancia de mantener la confidencialidad de la información personal. Esta

                política de privacidad se aplica a todos los productos y servicios proporcionados por nosotros y

                establece la forma en que podemos recopilar, usar y divulgar información en relación con los usuarios

                del sitio.

                Puede usar nuestros servicios y productos a través de un dispositivo móvil, ya sea a través de

                aplicaciones móviles o sitios web optimizados para móviles. Esta política de privacidad se aplica también

                a dicha utilización de nuestros servicios y productos.

                Todos los términos no definidos en este documento tendrán el significado que se les atribuye en los

                Términos de uso del sitio, que se puede encontrar aquí.
                &nbsp;

                &nbsp;
                <li><strong> RECOLECCION DE INFORMACIÓN</strong></li>
                &nbsp;
                <ol>
                    <li>Su privacidad es importante para nosotros y hemos tomado medidas para asegurar que no

                        recolectamos información de usted más de la requerida para poner a su disposición nuestros servicios y

                        proteger su cuenta.</li>
                    <li>La información incluyendo, pero no limitado a, nombre de usuario, dirección, número de teléfono,

                        número de fax, dirección de correo electrónico, sexo, fecha y / o año de nacimiento y las preferencias

                        del usuario (&quot;Información de Registro&quot;) puede ser requerido en el momento que el usuario se registra en

                        el Sitio.</li>
                    <li>Registramos y recolectamos detalles de las actividades de los usuarios en el Sitio. La información

                        sobre estas transacciones, incluyendo, pero no limitado a, los tipos y las especificaciones de los

                        productos, información de precios y la entrega y cualquier registro de conflicto comercial ( &quot;actividades

                        de información&quot;) puede ser recolectada cuando las transacciones de compra y venta se llevan a cabo o

                        son facilitadas a través del Sitio.</li>
                    <li>De vez en cuando, podemos recopilar información acerca de nuestros usuarios y usuarios potenciales

                        durante las ferias comerciales, eventos de la industria y otros. La información que se puede recoger en

                        estos lugares pueden incluir, pero no se limitan a, nombre de usuario, dirección, número de teléfono,

                        número de fax y dirección de correo electrónico (&quot;Información del Evento&quot;).</li>
                    <li>Registrar y recopilar registros de las actividades de compra y navegación de los usuarios en nuestra

                        plataforma, incluyendo pero no limitado a direcciones IP, patrones de navegación y pautas de

                        comportamiento de los compradores. Además, recopilamos información estadística sobre el sitio y los

                        visitantes al Sitio, incluyendo, pero no limitado a, las direcciones IP, software de navegador, sistema

                        operativo, los atributos de software y hardware, páginas vistas, número de sesiones y de visitantes

                        únicos (en conjunto &quot;Visualización de la información &quot;).</li>
                    <li>Registro de la Información, información de la cuenta, actividades de información, información del

                        evento y la información de navegación en general, se refieren a las entidades comerciales y se

                        denominan en conjunto Datos de Negocio (&quot;Datos de Negocio&quot;). En la medida y sólo en la medida en

                        que constituyen datos de identificación personal de los individuos, dicha información se denomina

                        Datos Personales como (&quot;Datos Personales&quot;).</li>
                    <li>Es obligatorio para los usuarios del Sitio proporcionar ciertos de Datos de Negocio y Datos Personales

                        (como se especifica en el momento de la recolección). En el caso de que los usuarios no proporcionen

                        ningún o suficientes Datos de Negocio y / o Datos Personales marcados como obligatorios, es posible

                        que no será posible completar el proceso de registro o proporcionar a dichos usuarios con nuestros

                        productos o servicios.</li>
                </ol>
                &nbsp;

                <li><strong> USO DE DATOS PERSONALES</strong></li>
                &nbsp;

                Si usted proporciona información personal a nosotros, se considera que usted nos ha autorizado para

                recoger, conservar y utilizar de que los datos personales para los siguientes propósitos:
                <ol>
                    <li>verificar su identidad;</li>
                    <li>verificar su elegibilidad para registrarse como usuario del Sitio;</li>
                    <li>procesar su registro como usuario, que le proporciona un log-in ID para el sitio y el mantenimiento y

                        gestión de su registro;</li>
                    <li>proporcionarle servicio al cliente y responder a sus preguntas, comentarios, reclamaciones o

                        controversias;</li>
                    <li>facilitarle la comunicación entre compradores y vendedores en el sitio;</li>
                    <li>realizar trabajos de investigación o análisis estadístico con el fin de mejorar el contenido y el diseño

                        de los Sitios, para mejorar nuestra oferta de productos y servicios y para fines comerciales y

                        promocionales;</li>
                    <li>sujeto a la obtención de su consentimiento en la forma que sea necesario de acuerdo a la ley

                        aplicable, podemos usar su nombre, número de teléfono, dirección de residencia, dirección de correo

                        electrónico y número de fax (“Datos de Marketing&quot;) para proporcionar notificaciones, encuestas, alertas

                        de productos, comunicaciones y otros materiales de marketing a usted en relación a los bienes y

                        servicios ofrecidos por nosotros en el sitio incluyendo membresías Avanzada, Premium y Gratuita;</li>
                    <li>Si usted voluntariamente ingresa cualquier información al Sitio para su publicación en el sitio a través

                        de las herramientas de publicación, incluyendo, pero no limitado a, perfil de empresa, catálogo de

                        productos, compradores potenciales, y los foros de discusión, entonces se considera que ha dado su

                        consentimiento para la publicación de dicha información en el sitio ( &quot;información voluntaria&quot;); y</li>
                    <li>Dar tal consentimiento puede ser necesario para cualquiera de los fines anteriormente mencionados

                        o requerido por la ley o con respecto a cualquier reclamación o posibles reclamaciones presentadas

                        contra nosotros.</li>
                </ol>
                &nbsp;

                <li><strong> DIVULGACIÓN DE DATOS PERSONALES</strong></li>

                &nbsp;
                <ol>
                    <li>Asimismo, acepta que podemos dar a conocer y transferir (ya sea dentro o fuera de la jurisdicción de

                        la entidad OleB2B.com que se contrata con) sus datos personales a los proveedores de servicios

                        contratados por nosotros para ayudarnos a proporcionarle nuestros servicios (incluyendo pero no

                        limitado a la entrada de datos, gestión de bases de datos, promociones, productos y servicios de alertas,

                        servicios de entrega, servicios de extensión de pago, y la autenticación de suscripciones y servicios de

                        verificación) ( &quot;Proveedores de Servicio&quot;). Estos proveedores de servicios tienen la obligación de guardar

                        la confidencialidad para nosotros, y sólo se les permite utilizar sus datos personales en relación con los

                        fines especificados en B.1 a B.9 anteriormente, y no para sus propios fines (incluyendo marketing

                        directo).</li>
                    <li>El usuario acepta que podemos dar a conocer y transferir (ya sea dentro o fuera de la jurisdicción de

                        la entidad OleB2B.com que se contrata con), para los fines especificados en B.1 a B.9 anteriormente,

                        sus datos personales empresas afiliadas del grupo OleB2B.com.</li>
                    <li>Cuando sea necesario también podemos revelar y transferir (ya sea dentro o fuera de la jurisdicción

                        de la entidad OleB2B.com que está contratando) sus datos personales a nuestros asesores

                        profesionales, agencias de aplicación de la ley, aseguradoras, el gobierno y regulación y otras

                        organizaciones para los fines especificados en B.9 anteriormente.</li>
                    <li>Todos los datos personales suministrados por usted serán recopilados y almacenados por nosotros y

                        serán accesibles por nuestros empleados, los proveedores de servicios contratados por nosotros y

                        terceras partes contempladas en C.2 y C.3 anterior, a favor o en relación con cualquiera de las fines

                        indicados en B.1 a B.9 anteriormente.</li>
                    <li>Podemos compartir su información de cuenta con bancos o proveedores para permitir que sus

                        transacciones en el sitio se completen satisfactoriamente. Si bien tenemos la tecnología y los

                        procedimientos internos requeridos para mantener su información de cuenta y otros datos personales a

                        salvo de intrusos, no hay ninguna garantía de que dicha tecnología o procedimientos puedan eliminar

                        todos los riesgos de robo, pérdida o mal uso.</li>
                    <li>Podemos proporcionar información estadística a terceros, pero cuando lo hacemos, no

                        proporcionamos información de identificación personal sin su permiso.</li>
                    <li>relaciones que hemos establecido con otras partes y sitios web para poder ofrecerle el beneficio de

                        los productos y servicios que no ofrecen. Ofrecemos acceso a estas otras partes y sus sitios web, ya sea

                        mediante el uso de hipervínculos a estos sitios desde los Sitios o mediante el ofrecimiento de &quot;co-

                        marca&quot; sitios en los que tanto nosotros como otras partes comparten el mismo localizador de recursos,

                        nombre de dominio uniforme o páginas dentro de un nombre de dominio en Internet. En algunos casos,

                        es posible que deba enviar información personal para registrarse o solicitar productos o servicios

                        prestados por dichos terceros o socios de marca compartida. Esta política de privacidad no se aplica a

                        estos sitios de terceros o sitios de marca. Las políticas de privacidad de esas otras partes pueden diferir

                        de las nuestras, y no tenemos control sobre la información que envíe a dichos terceros. Usted debe leer

                        la política de privacidad de referencia de estos sitios de terceros y sitios de marca antes de responder a

                        cualquier ofertas, productos o servicios anunciados por dichas partes.</li>
                </ol>
                &nbsp;

                <li><strong>DERECHO DE ACCESO A DATOS PERSONALES</strong></li>

                &nbsp;

                En virtud de las leyes aplicables, usted tiene el derecho de solicitar el acceso a la información personal

                en poder de nosotros y de solicitar la corrección de esta.

                Si usted tiene alguna pregunta sobre esta Política de Privacidad o si desea acceder o corregir sus datos

                personales, puede enviar su solicitud por escrito a la siguiente dirección:

                OleB2B.com S.A.C. en Calle Francisco Graña 480, Lima 17, Perú.

                De conformidad con las leyes aplicables, nos reservamos el derecho de cobrarle una tarifa razonable

                para el procesamiento de cualquier acceso a datos o solicitud de corrección.
                &nbsp;

                <li><strong> COOKIES</strong></li>

                &nbsp;

                <p>Utilizamos &quot;cookies&quot; para almacenar información específica acerca de usted y el seguimiento de sus

                    visitas al sitio. Es lo usual que los sitios web utilicen cookies para mejorar la identificación de sus

                    usuarios.</p>

                <p>Una &quot;cookie&quot; es una pequeña cantidad de datos que se envía a su navegador y se almacena en el disco

                    duro de su ordenador. Una cookie puede ser enviada al disco duro de su ordenador sólo si accede al

                    sitio usando un ordenador. Si no desactiva o borra la cookie, cada vez que utilice el mismo equipo para

                    acceder al Sitio, nuestros servidores web será notificado de su visita al Sitio y a su vez pueden tener

                    conocimiento de su visita y el patrón de su uso.</p>

                <p>En general, utilizamos cookies para identificarlo y nos permitirá i) acceder a su información de registro o

                    información de cuenta para que no tenga que volver a entrar en él; ii) recopilar información estadística

                    sobre el uso de los usuarios; iii) la investigación visitando patrones y ayudar anuncios diana, según los

                    intereses del usuario; iv) ayudar a nuestros socios para realizar un seguimiento de las visitas del usuario

                    al Sitio; y v) seguimiento del progreso y la participación en promociones.</p>

                <p>
                    Se puede determinar si y cómo una cookie será aceptada en la configuración del navegador que se

                    instala en el ordenador que está utilizando para acceder al sitio. Si lo desea, puede cambiar dichas

                    configuraciones. Al establecer sus preferencias en el navegador, puede aceptar todas las cookies o

                    puede optar por recibir una notificación cuando se envía una cookie o se puede optar por rechazar

                    todas las cookies. Si usted rechaza todas las cookies seleccionando la función de desactivar cookies en su

                    navegador, es posible que tenga que volver a introducir la información en el sitio con más frecuencia y

                    ciertas características del sitio pueden no estar disponibles.
                </p>

                &nbsp;

                <li><strong> MENORES DE EDAD</strong></li>

                &nbsp;

                El Sitio y su contenido no están dirigidos a menores de edad (menores de 18 años) y no tenemos la

                intención de vender cualquiera de nuestros productos o servicios a menores de edad. Sin embargo, no

                tenemos manera de distinguir la edad de las personas que acceden a nuestro sitio. Si un menor nos ha

                proporcionado información personal sin consentimiento de los padres o tutor, el padre o tutor debe

                comunicarse con nuestro Departamento Legal en la dirección que figura en el apartado D anterior para

                eliminar la información.

                &nbsp;

                <li><strong> MEDIDAS DE SEGURIDAD</strong></li>

                &nbsp;

                <p>Empleamos métodos de seguridad comercialmente razonables para evitar el acceso no autorizado al

                    Sitio, para mantener la exactitud de los datos y asegurar el uso correcto de la información que tenemos.</p>

                <p>Para los usuarios registrados del sitio, su información de registro de cuenta (si lo hay) se puede ver y

                    editar a través de su cuenta, que está protegida por una contraseña. Le recomendamos que no revele su

                    contraseña a nadie. Nuestro personal nunca le pedirá su contraseña en una llamada telefónica o un

                    correo electrónico no solicitado. Si comparte un ordenador con otras personas, no se debe optar por

                    guardar su información de inicio de sesión (por ejemplo, ID de usuario y contraseña) en ese equipo

                    compartido. Recuerde salir de su cuenta y cerrar la ventana de su navegador cuando haya terminado su

                    sesión.</p>

                <p>No hay transmisión de datos a través de Internet o cualquier red inalámbrica que se pueda garantizar

                    que sea perfectamente segura. Como resultado, mientras tratamos de proteger su información, no

                    podemos garantizar la seguridad de ninguna información que nos transmita y usted lo hace bajo su

                    propio riesgo.</p>

                &nbsp;

                <li><strong> Modificaciones a esta POLÍTICA DE PRIVACIDAD</strong></li>

                &nbsp;

                Cualquier cambio a esta Política de Privacidad será comunicado por nosotros publicando en el Sitio la

                nueva Política de Privacidad. Una vez publicada en los Sitios se hará efectiva inmediatamente. Usted

                acepta que cualquier información que tenemos sobre usted (como se describe en esta Política de

                Privacidad, recogida antes o después de entrada en vigor la nueva Política de Privacidad) se regirá por la

                última versión de la política de privacidad.

                &nbsp;

                <li><strong> SUS COMENTARIOS</strong></li>

                &nbsp;

                Agradecemos sus comentarios respecto a nuestra política de privacidad y cualquier comentario sobre

                los servicios que ofrecemos a usted. Puede enviarnos sus comentarios a calle Francisco Grana 480 Lima

                17, Peru
            </div>
        </div>

    </section>
@endsection