<?php

return [

    'my_products' => 'Productos',
    'complete_in' => 'Completar en Español',
    'complete' => 'Completo',
    'incomplete' => 'Incompleto',
    'form' => [
        'first_name' => 'Nombres',
        'last_names' => 'Apellidos',
        'password' => 'Contrase&ntilde;a',
        'confirm_password' => 'Repetir Contrase&ntilde;a',
        'email_address' => 'Correo Electrónico',
        'country' => 'País',
        'office_phone' => 'Número De Teléfono  Ej: +51 123 4567',
        'mobile_phone' => 'Número De Celular  Ej: +51 123 4567',
        'address' => 'Dirección',
        'city' => 'Ciudad',
        'postal_code' => 'Código Postal',
        'languages' => 'Idiomas',
        'others_languages' => 'Otros Idiomas',
        'company' => 'Nombre de la Empresa'
    ],
    'can_sell_buy' => 'Pueden Vender/Comprar',
    'can_buy' => 'Pueden Comprar',
    'update_image' => 'Subir imágen',
    'button-add-photos' => 'Agregar Logo/foto',
    'exporter_manual' => 'Manual del Exportador'

];
