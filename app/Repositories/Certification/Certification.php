<?php namespace App\Repositories\Certification;

use App\Repositories\Supplier\Supplier;
use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    public $table = 'certifications';

    public $fillable = ['supplier_id', 'type_certification', 'issued_by', 'start_date', 'expiration_date', 'image','image_extension'];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function getImageAttribute($value)
    {
        return $value ? $value : 'assets/pages/img/default-product-image.png';
    }
}
