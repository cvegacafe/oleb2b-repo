<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SupplierUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'=>'string|required|max:255',
            'es_company_description'=>'required',
            'en_company_description'=>'required',
            'ru_company_description'=>'required',
            'member_trade_organization'=>'required|max:255',
            'tax_number'=>'required',
            'company_legal_address'=>'string|required|max:255',
            'postal_code'=>'string|max:10',
            'website'=>'url',
            'year_company_registered'=>'numeric',
            'number_employees'=> 'required|numeric'
        ];
    }
}
