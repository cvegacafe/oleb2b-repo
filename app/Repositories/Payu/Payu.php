<?php namespace App\Repositories\Payu;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payu extends Model
{
    use SoftDeletes;

    public $table = 'payu';

    protected $fillable = [
        'reference_pol',
        'currency',
        'is_test',
        'transaction_date',
        'cc_holder',
        'description',
        'value',
        'email_buyer',
        'response_message_pol',
        'transaction_id',
        'sign',
        'billing_address',
        'ip',
        'payment_method_name',
        'shipping_country',
        'commision_pol',
        'commision_pol_currency',
        'billing_city',
        'reference_sale',
        'is_test'
    ];

}
