<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\MembershipService;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Response;

/**
 * Class CommonController
 * @package App\Http\Controllers\User
 */
class CommonController extends Controller
{
    /**
     * @param Guard $guard
     * @param MembershipService $membershipService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Guard $guard, MembershipService $membershipService)
    {
        $user = $guard->user();
        $completeCompanyInfo = false;
        $completeCompanyPhotos = false;
        $productsCount = 0;
        $hasCertifications = false;
        $supplier = $user->supplier;

        if (!$membership = $user->memberships->last()) {
            $basicMembership = $membershipService->find(1);
            $user->memberships()->attach($basicMembership->id, [
                'main_rotation_banners' => $basicMembership->main_rotation_banners,
                'inner_rotation_banners' => $basicMembership->inner_rotation_banners,
                'product_posting' => $basicMembership->product_posting,
                'amount' => 0,
                'frequency' => 'A',
                'is_active' => true,
                'finish_at' => Carbon::now()->addYears(30)->toDateTimeString()
            ]);
        }

        if ($supplier) {
            $completeCompanyInfo = true;

            if ($supplier->original_logo) {
                $completeCompanyPhotos = true;
            }

            if (!$supplier->certifications->isEmpty()) {
                $hasCertifications = true;
            }
        }

        if (!$user->products->isEmpty()) {
            $productsCount = $user->products->count();
        }

        return view('user.dashboard',
            compact('completeCompanyInfo', 'completeCompanyPhotos', 'hasCertifications', 'productsCount'));
    }

    /**
     * @return mixed
     */
    public function downloadManual()
    {
        $file = public_path() . "/pdfs/Manual_de_exportador_OleB2B.com.pdf";
        return Response::download($file, 'Manual_Export_OleB2B.com.pdf', ['Content-Type: application/pdf']);
    }
}