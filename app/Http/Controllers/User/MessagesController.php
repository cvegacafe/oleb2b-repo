<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Services\AttachmentService;
use App\Services\BuyingRequestService;
use App\Services\FileService;
use App\Services\NotificationService;
use App\Services\ProductService;
use App\Services\ThreadService;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MessagesController extends Controller
{
  private $threadService = null;

  public function __construct(ThreadService $threadService)
  {
    $this->threadService = $threadService;
  }

  /**
   * @param Request $request
   * @param ProductService $productService
   * @param NotificationService $notificationService
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function indexProducts(
    Request $request,
    ProductService $productService,
    NotificationService $notificationService
  )
  {
    $user = auth()->user();
    $topics = $productService->getProductsHasThreads($user->id);
    if ($request->has('product') && $request->has('thread')) {
      $product = $productService->find($request->get('product'));
      $isSupplier = $product->user_id === $user->id;
      $notificationService->setNotificationViewed(route('user.messages.products.index',
        ['product' => $request->get('product'), 'thread' => $request->get('thread')]), $user->id);
    }
    $type = 'products';

    return view('user.messages.messages', compact('topics', 'type', 'isSupplier'));
  }

  /**
   * @param Request $request
   * @param BuyingRequestService $buyingRequestService
   * @param NotificationService $notificationService
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function indexBuyingRequest(
    Request $request,
    BuyingRequestService $buyingRequestService,
    NotificationService $notificationService
  )
  {
    $topics = $buyingRequestService->getBuyingRequestHasThreads(Auth::id());

    if ($request->has('buying-request') && $request->has('thread')) {

      $notificationService->setNotificationViewed(route('user.messages.buying-request.index',
        ['buying-request' => $request->get('buying-request'), 'thread' => $request->get('thread')]),
        Auth::id());
    }

    $type = 'buying-request';

    return view('user.messages.messages', compact('topics', 'type'));
  }

  public function downloadAttachment(
    $idAttachment,
    FileService $fileService,
    AttachmentService $attachmentService
  )
  {
    try {
      $attachment = $attachmentService->find($idAttachment);

      if (!$attachment) {
        Aviso::error(trans('alerts.messages.attachment_not_found'));
      }

      $this->authorize('view', $attachment);
      $path = public_path($attachment->path);

      return $fileService->downloadFile($path, $attachment->name);

    } catch (FileNotFoundException $e) {
      Log::error($e);
      return view('error.404');

    } catch (Exception $e) {
      Log::error($e);
      return view('error.404');

    }
  }
}

