<?php namespace App\Repositories\Administrator;

use App\Repositories\BaseRepository;

class AdministratorRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new Administrator();
        parent::__construct();
    }
}