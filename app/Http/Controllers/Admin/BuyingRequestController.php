<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Repositories\BuyingRequest\BuyingRequest;
use App\Services\BuyingRequestService;
use App\Services\FileService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BuyingRequestController extends Controller
{
    private $buyingRequestService;

    public function __construct(BuyingRequestService $buyingRequestService)
    {
        $this->buyingRequestService = $buyingRequestService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buyingRequests = $this->buyingRequestService->all();

        return view('admin.buying-request.index', compact('buyingRequests'));
    }

    /**
     * Display the specified resource.
     *
     * @param BuyingRequest $buying_request
     * @return \Illuminate\Http\Response
     */
    public function show(BuyingRequest $buying_request)
    {
        return view('admin.buying-request.show', compact('buying_request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param BuyingRequest $buying_request
     * @return \Illuminate\Http\Response
     */
    public function edit(BuyingRequest $buying_request)
    {
        return view('admin.buying-request.edit', compact('buying_request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->buyingRequestService->update($id, $request->all());
        Aviso::actualizado();

        return redirect()->action('Admin\BuyingRequestController@show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param BuyingRequest $buying_request
     * @param FileService $fileService
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(BuyingRequest $buying_request,FileService $fileService)
    {
        try {
            if ($buying_request->image) {
                $fileService->removeFile($buying_request->image);
            }

            $buying_request->delete();
            Aviso::eliminado(trans('alerts.buyingRequest.deleted'));
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado(trans('alerts.buyingRequest.deleted'));
        }

        return redirect()->action('Admin\BuyingRequestController@index');
    }

    public function enable(BuyingRequest $buyingRequest)
    {
        $buyingRequest->is_approved = true;
        $buyingRequest->save();

        return redirect()->action('Admin\BuyingRequestController@show', $buyingRequest);
    }

    public function disable(BuyingRequest $buyingRequest)
    {
        $buyingRequest->is_approved = false;
        $buyingRequest->save();

        return redirect()->action('Admin\BuyingRequestController@show', $buyingRequest);
    }
}
