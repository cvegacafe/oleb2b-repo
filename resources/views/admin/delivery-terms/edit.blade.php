@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit: {{$deliveryTerm->name}}</div>
                    <div class="panel-body">
                        @if(isset($errors) && $errors->any())
                            <div class="alert alert-danger" role="alert">
                                <p>Por Favor Corrija los siguientes errores</p>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{str_replace(' id ',' ',$error)}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{action('Admin\DeliveryTermsController@update',$deliveryTerm)}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $deliveryTerm->name }}" required autofocus>
                                </div>
                            </div>
                            <h3>Descripciones</h3>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Espanol</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="es_description" value="{{$deliveryTerm->en_description}}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Ingles</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="en_description" value="{{$deliveryTerm->en_description}}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Ruso</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="ru_description" value="{{$deliveryTerm->ru_description}}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Actualizar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection