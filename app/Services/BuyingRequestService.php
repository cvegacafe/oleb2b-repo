<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */

use App\Repositories\BuyingRequest\BuyingRequestRepository;
use Stichoza\GoogleTranslate\TranslateClient;
use Illuminate\Support\Facades\Lang;

class BuyingRequestService extends BaseService
{

    public function __construct(BuyingRequestRepository $buyingRequestRepository)
    {
        $this->mainRepo = $buyingRequestRepository;
        parent::__construct();
    }

    public function getUserBuyingRequest($userId)
    {
        return $this->mainRepo->getBuyingRequestByUser($userId);
    }

    public function allApprovedPaginate($ElementsByPage = 10, $q = '')
    {
        return $this->mainRepo->allApprovedPaginate($ElementsByPage, $q, Lang::locale());
    }

    public function getBuyingRequestHasThreads($userId)
    {
         return  $this->mainRepo->getBuyingRequestByUserThreads($userId);
    }

}