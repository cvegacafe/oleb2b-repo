<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;

class SubAccountsController extends Controller
{
    public function subAccountCount(UserService $userService)
    {
        if (Auth::user()->isPrincipalAccount()) {
            $users = $userService->indexSubAccounts(Auth::user()->supplier, Auth::id());
            return response()->json(['response' => 'ok', 'count' => $users->count()-1]);
        }

        return response()->json(['response' => 'ok', 'count' => 0]);
    }
}
