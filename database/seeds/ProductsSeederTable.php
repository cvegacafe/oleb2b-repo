<?php

use App\Repositories\Product\Product;
use Illuminate\Database\Seeder;

class ProductsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = \App\Repositories\Subcategory\Subcategory::all();
        $countries = \App\Repositories\Country\Country::where('enabled_for_supplier',true)->get();
        $measures = \App\Repositories\Measure\Measure::all();
        $currencies = \App\Repositories\Currency\Currency::all();
        $deliveryTerms = \App\Repositories\DeliveryTerm\DeliveryTerm::all();
        $paymentTerms = \App\Repositories\PaymentTerm\PaymentTerm::all();
        $languages = \App\Repositories\Language\Language::all();

        factory(Product::class,20)->create([
            'user_id'=>1,
            'subcategory_id' => $subcategories->random()->id,
            'country_id' => $countries->random()->id,
            'supply_measure_id' => $measures->random()->id,
            'moq_measure_id' => $measures->random()->id,
            'currency_id' => $currencies->random()->id ,
            'sale_unit_measure_id' => $measures->random()->id,
            'is_approved'=>true
        ])->each(function ($product)use($deliveryTerms,$paymentTerms,$languages) {
            $product->deliveryTerms()->saveMany($deliveryTerms->random(4));
            $product->paymentTerms()->saveMany($paymentTerms->random(2));
        });
        $this->command->info('Many Products for supplier@oleb2b.com has been created!...');
    }
}
