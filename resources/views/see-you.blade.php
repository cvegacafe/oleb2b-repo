<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Oleb2b</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
</head>
<body>
<div class="flex-center position-ref full-height">
    <table class="table" border="1">
        <thead>
        <tr>
            <th>#</th>
            <th>Producto</th>
            <th>usuario</th>
            <th>Mensaje</th>
            <th>Fecha</th>
        </tr>
        </thead>
        <tbody>
        @foreach($threads as $thread)
            @foreach($thread->messages as $message)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$thread->product->es_name ?? ''}}</td>
                    <td>{{$message->user->full_name ?? ''}}</td>
                    <td width="600px">{{$message->body}}</td>
                    <td>{{$message->created_at->format('d-m-Y')}}</td>
                </tr>
            @endforeach

        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
