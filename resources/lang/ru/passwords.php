<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'button_change_password' => 'Изменить пароль',
    'old_password_input' => 'Введите старый пароль',
    'new_password_input' => 'Введите новый пароль',
    'new_password_input_confirmation' => 'Введите новый пароль повторно',
    'password' => 'Пароль должен быть минимум 6 знаков и совпадать с подтверждением.',
    'reset' => 'Ваш пароль изменен!',
    'sent' => 'Мы отправили Вам ссылку для замены пароля!',
    'token' => 'Неверный токен для замены пароля.',
    'user' => "Пользователь с данным имейл адресом не найден.",
    'send_password_reset_link' => "Отправить имейл для восстановления пароля",
    'back_to_login' => "Войти",
    'message_mail_change_password'=>'для изменения пароля перейдите по ссылке и следуйте указаниям системы',
    'message_paste_browser'=>'If you’re having trouble clicking the " :name " button, copy and paste the URL below into your web browser:'
];
