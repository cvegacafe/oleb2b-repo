<?php

use App\Repositories\Supercategory\Supercategory;
use Illuminate\Database\Seeder;

class SupercategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         Supercategory::create(['id'=>1,'es_name'=>'Agricultura Y Alimentos','en_name'=>'Agriculture & Food','ru_name'=>'Продукты питания и Сельское хозяйство']);
         Supercategory::create(['id'=>2,'es_name'=>'Agricultura E Insumos','en_name'=>'Agriculture and Farming supplies','ru_name'=>'Агропром и Животноводство']);
         Supercategory::create(['id'=>3,'es_name'=>'Ropa, Textiles y Accesorios','en_name'=>'Apparel, Textiles, Accessories','ru_name'=>'Одежда, Текстиль, Аксессуары']);

        Supercategory::create(['id'=>4,'es_name'=>'Automóviles y Transporte','en_name'=>'Auto and Transportation','ru_name'=>'Автомобили и транспорт']);

         Supercategory::create(['id'=>5,'es_name'=>'Electrónicos','en_name'=>'Electronics','ru_name'=>'Электроника']);
         Supercategory::create(['id'=>6,'es_name'=>'Regalos y Juguetes','en_name'=>'Gifts &Toys','ru_name'=>'Игрушки и Подарки']);
         Supercategory::create(['id'=>7,'es_name'=>'Deporte y Entretenimiento','en_name'=>'Sport & Entertainment Goods','ru_name'=>'Спорт и развлечения']);
         Supercategory::create(['id'=>8,'es_name'=>'Hogar, Iluminación y Construcción','en_name'=>'Home, Construction, Light','ru_name'=>'Товары для дома, Строительство, Освещение']);
         Supercategory::create(['id'=>9,'es_name'=>'Salud y Belleza','en_name'=>'Health and Beauty','ru_name'=>'Красота и Здоровье']);
         Supercategory::create(['id'=>10,'es_name'=>'Bolsos, Calzados y Accesorios','en_name'=>'Bags, shoes & accessories','ru_name'=>'Сумки, Обувь и Аксессуары']);
         Supercategory::create(['id'=>11,'es_name'=>'Equipamiento electrónico, componentes','en_name'=>'Electrical equipment, components','ru_name'=>'Электрооборудование и телекоммуникации']);
         Supercategory::create(['id'=>12,'es_name'=>'Embalaje, publicitario, oficina','en_name'=>'Packaging, advertising, office','ru_name'=>'Упаковка, реклама и канцелярия']);
         Supercategory::create(['id'=>13,'es_name'=>'Químicos y plásticos','en_name'=>'Chemicals and plastics & Rubber','ru_name'=>'Химия, пластик и резина']);
         Supercategory::create(['id'=>14,'es_name'=>'Minerales y metalurgia','en_name'=>'Minerals and metallurgy','ru_name'=>'Минералы и металлургия']);
         Supercategory::create(['id'=>15,'es_name'=>'Maquinaria, partes y herramientas','en_name'=>'Machinery, Industrial Parts & Tools','ru_name'=>'Оборудование и инструменты']);

    }
}
