<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ApiContactSupplierFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    public function all()
    {
        $data = parent::all();
        //\Log::info($data); //mostrar en log los parametros enviados
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'country_id' => 'required|exists:countries,id',
            'order_qty' => 'required|numeric',
            'measure_id' => 'required|exists:measurements,id',
            'port' => 'max:200',
            'payment_terms' => 'required|Array',
        ];
    }
}
