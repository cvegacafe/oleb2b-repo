<?php namespace App\Policies;

use App\Repositories\Attachment\Attachment;
use App\Repositories\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AttachmentPolicy
{
    use HandlesAuthorization;

    public function view(User $user,Attachment $attachment)
    {
        $thread = $attachment->message->thread;
        return $thread->from_user_id == $user->id || $thread->to_user_id == $user->id;
    }

}
