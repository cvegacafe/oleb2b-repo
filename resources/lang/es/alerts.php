<?php
return [
    'buttons' => [
        'ok' => 'Aceptar'
    ],

    'messages' => [
        'message_send' => 'Mensaje enviado',
        'message_not_send' => 'No se pudo enviar el mensaje',
        'attachment_not_found' => 'Archivo adjunto no encontrado',
        'no_registered_error' => 'No se pudo registrar al usuario',
        'membership_pay_success' => 'Pago exitoso. Su membersia :name'
    ],

    'middlewares' => [
        'has_not_product_available' => 'Ha posteado el maximo de productos para su membresia',
        'supplier_data_required' => 'Para agregar productos por favor llena la información de la empresa '
    ],

    'password' => [
        'password_updated' => 'La contraseña ha sido actualizada',
        'password_updated_error' => 'La contraseña no ha podido ser actualizada',
        'password_current_invalid' => 'La contraseña actual no es correcta',
    ],

    'subaccounts' => [
        'title_delete' => 'Eliminar Sub-Cuenta',
        'message_confirm' => '¿Seguro que desea eliminar al usuario :name ?'
    ],

    'buyingRequest' => [
        'title_delete' => 'Eliminar Solicitud de Cotizaci&oacute;n',
        'message_confirm' => '¿Seguro que desea eliminar la Solicitud de Cotización :name ?',
        'deleted' => ' Solicitud de Cotización eliminada!',
        'deleted_error' => 'No se pudo eliminar  la Solicitud de Cotización!'
    ],

    'products' => [
        'title_delete' => 'Eliminar Producto',
        'message_confirm' => '¿Seguro que desea eliminar el producto :name ?',
        'deleted' => 'Producto eliminado!',
        'deleted_error' => 'No se pudo eliminar el producto!'
    ],

    'favoriteProducts' => [
        'title_delete' => 'Eliminar Producto Guardado',
        'message_confirm' => '¿Seguro que desea eliminar el producto guardado :name ?'
    ],
    'validateEmail' => [
        'message' => 'Hemos enviado un correo de validacion',
        'message_confirm' => 'Correo validado',
    ],
    'delete_certificate' => [
        'message' => ' Eliminar este certificado?',
        'message_confirm' => ' ',
    ],
];