<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ContactSupplierRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    public function all()
    {
        $data = parent::all();
        //\Log::info($data); //mostrar en log los parametros enviados
        return $data;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|exists:products,id',
            'country_id' => 'required|exists:countries,id',
            'measure_id' => 'required|exists:measurements,id',
            'port' => 'max:150',
            'order_qty' => 'required|numeric|max:100000000',
            'payment_terms' => 'array|existsArray:paymentTerms,id',
            'description' => 'max:250',
            'file' => 'mimes:jpeg,png,doc,docx,xls,xlsx,ppt,pptx|max:2000'
        ];
    }

    public function getRedirectUrl()
    {
        $data = parent::all();
        return $data['current_url'].'?show=contact-supplier';
    }
}
