@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Paises :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Español</th>
                        <th>Ingles</th>
                        <th>Ruso</th>
                        <th>Abbreviacion</th>
                        <th>Disponible para Proveedores</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($countries as $country)
                        <tr>
                            <td><b>{{$country->id}}</b></td>
                            <td>{{$country->es_name}}</td>
                            <td>{{$country->en_name}}</td>
                            <td>{{$country->ru_name}}</td>
                            <td>{{$country->abbreviation}}</td>
                            <td><input type="checkbox" {{$country->enabled_for_supplier?'checked':''}} id="{{$country->id}}"/> <label for="{{$country->id}}" style="cursor:hand">{{$country->enabled_for_supplier?'habilitado':'deshabilitado'}}</label> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>

        var path = '{{route('api.country.enable')}}';

        $(function(){
            $('input[type=checkbox]').on('click',function(e){
                e.preventDefault();
                var checked = false;
                var self = $(this);
                var id = $(this).attr('id');
                if ($(this).is(':checked')){
                     checked = true;
                }
                var request = $.ajax({
                    url: path,
                    method: "POST",
                    data: {_token: window.Laravel.csrfToken,checked:checked,id:id,_method:'put'},
                    dataType: "json"
                });
                request.done(function (data) {
                    if(data.response==true){
                        self.prop('checked',data.value);
                        if(data.value){
                            self.parent().find('label').text('habilitado');
                        }else{
                            self.parent().find('label').text('inahabilitado');
                        }

                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });
        });
    </script>
    @endsection