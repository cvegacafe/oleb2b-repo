<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Library\Aviso;
use App\Services\DeliveryTermService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DeliveryTermsController extends Controller
{
    private $deliveryTermService;

    public function __construct(DeliveryTermService $deliveryTermService)
    {
        $this->deliveryTermService = $deliveryTermService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveryTerms = $this->deliveryTermService->all();

        return view('admin.delivery-terms.index', compact('deliveryTerms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        $languages = $this->languageService->all();
        return view('admin.delivery-terms.create',compact('languages'));
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->deliveryTermService->create($request->all());
            Aviso::guardado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }
        return redirect()->action('Admin\DeliveryTermsController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deliveryTerm = $this->deliveryTermService->find($id);

        return view('admin.delivery-terms.edit', compact('deliveryTerm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $deliveryTerm = $this->deliveryTermService->find($id);
            $this->deliveryTermService->update($deliveryTerm->id, $request->all());
            Aviso::actualizado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noActualizado();
        }

        return redirect()->action('Admin\DeliveryTermsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $deliveryTerm = $this->deliveryTermService->find($id);
            $deliveryTerm->delete();
            Aviso::eliminado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado();
        }

        return redirect()->action('Admin\DeliveryTermsController@index');
    }
}
