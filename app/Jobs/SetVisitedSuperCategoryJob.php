<?php namespace App\Jobs;

use App\Repositories\Supercategory\Supercategory;
use App\Services\SupercategoryService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SetVisitedSuperCategoryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $superCategoryId;

    /**
     * Create a new job instance.
     *
     * @param Supercategory $superCategory
     */
    public function __construct($superCategoryId)
    {
        $this->superCategoryId = $superCategoryId;
    }

    /**
     * Execute the job.
     *
     * @param SupercategoryService $supercategoryService
     */
    public function handle(SupercategoryService $supercategoryService)
    {
        $superCategory = $supercategoryService->find($this->superCategoryId);

        if ($superCategory) {

            $superCategory->visited = $superCategory->visited + 1;

            $superCategory->save();
        }

    }
}
