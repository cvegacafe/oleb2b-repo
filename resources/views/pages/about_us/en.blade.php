@extends('layouts.pages')
@section('title')
    About Us
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                {{--<h1>About Us</h1>--}}
                <div class="carrousel">
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-01.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-02.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-03.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-04.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-05.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-06.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-07.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-08.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-12.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-13.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-14.png')}}" class="img-responsive">
                    </figure>
                    <figure>
                        <img src="{{asset('assets/pages/img/nosotros/en/oleb2b-15.png')}}" class="img-responsive">
                    </figure>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('styles')
    <style>
        .carrousel i{
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            color: #FF6A31;
            font-size: 60px;
            cursor: pointer;
        }
        .carrousel .prev{
            left: -40px;
        }
        .carrousel .next{
            right: -40px;
        }
    </style>
@endsection
@section('scripts')
    <script>
        $(function(){
            $('.carrousel').slick({
                slidesToShow: 1,
                prevArrow: '<i class="fa fa-angle-left prev"></i>',
                nextArrow: '<i class="fa fa-angle-right next"></i>',

            });
        });
    </script>
@endsection