<?php
return [
    'add_new_product' => 'Add a New Product',
    'product_category' => 'Product Category',
    'product_subcategory' => 'Product SubCategory',
    'name' => 'Name',
    'description' => 'Description',
    'product_photos' => 'Product Photos',
    'place_origin' => 'Place Of Origin',
    'supply_ability' => 'Supply Ability',
    'moq' => 'MOQ',
    'delivery_time' => 'Delivery Time',
    'price' => 'Price',
    'delivery_terms' => 'Delivery Terms',
    'payment_terms' => 'Payment Terms',
    'type' => 'Type',
    'color' => 'Color',
    'ports' => 'Ports',
    'sale_manager_name' => 'Sale Manager Name'


];