<?php namespace App\Http\Requests;

use App\Library\Constants;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class upgradeMembershipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $frequencies = collect(Constants::MEMBERSHIP_FREQUENCY)->pluck('id')->implode(',');
        return [
            'frequency' => 'required|in:'.$frequencies
        ];
    }
}
