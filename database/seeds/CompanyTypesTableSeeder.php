<?php

use App\Repositories\CompanyType\CompanyType;
use Illuminate\Database\Seeder;

class CompanyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyType::create(['en_name'=>'Manufacturer','es_name'=>'Fabricante','ru_name'=>'ПРОИЗВОДИТЕЛЬ']);
        CompanyType::create(['en_name'=>'Trading Company','es_name'=>'Trader','ru_name'=>'ТОРГОВАЯ КОМПАНИЯ']);
    }
}
