<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Library\Aviso;
use App\Repositories\Subcategory\Subcategory;
use App\Services\SubcategoryService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class SubcategoriesController
 * @package App\Http\Controllers\Admin
 */
class SubcategoriesController extends Controller
{

    /**
     * @var SubcategoryService
     */
    private $subcategoryService;

    /**
     * SubcategoriesController constructor.
     * @param SubcategoryService $subcategoryService
     */
    public function __construct(SubcategoryService $subcategoryService)
    {
        $this->subcategoryService = $subcategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = $this->subcategoryService->all();
        return view('admin.subcategories.index', compact('subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $this->subcategoryService->create($request->all());
            Aviso::guardado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noGuardado();
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Subcategory $subcategory
     * @return \Illuminate\Http\Response
     */
    /*public function edit(Subcategory $subcategory)
    {
        $languages = $this->languageService->all();
        return view('admin.subcategories.edit', compact('subcategory', 'languages'));
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Subcategory $subcategory
     * @param SubcategoryService $subcategoryService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory, SubcategoryService $subcategoryService)
    {
        $subcategoryService->update($subcategory->id, $request->except(['_method', '_token']));
        Aviso::actualizado();
        /*  try {
              $this->subcategoryService->update($subcategory->id, $request->all());
              Aviso::actualizado();
          } catch (Exception $e) {
              Log::error($e);
              Aviso::noActualizado();
          }*/
        return redirect()->action('Admin\CategoriesController@show', $subcategory->category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Subcategory $subcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        try {
            $subcategory->delete();
            Aviso::eliminado();
        } catch (Exception $e) {
            Log::error($e);
            Aviso::noEliminado();
        }

        return redirect()->back();
    }
}
