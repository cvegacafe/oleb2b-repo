@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row cabecera">
            <div class="col-xs-12 col-sm-8">
                <h3 class="titulo-1 titulo-dashboard">{{trans('menus.user.manage_products')}}</h3>
            </div>
            <div class="col-xs-12 col-sm-4">
                <a href="{{route('user.products.create')}}" class="boton-azul-no-shadow pull-right" >{{trans('products.add_new_product')}}</a>
            </div>
        </div>

        @include('global-partials.avisos')
        <section class="buying-requests">
            <div class="row">
                @foreach($products as $product)
                    <div class="col-xs-12">
                        @include('global-partials.product-row',[
                        'product'=>$product,
                        'link1'=>['text'=>trans('products.edit_product'),'url'=>route('user.products.edit',$product)],
                        'link2'=>['text'=>trans('products.delete'),'url'=>route('user.products.edit',$product)],
                        'deleteForm'=>[
                            'action'=>route('user.products.delete',$product->slug),
                            'button'=>trans('common.forms.buttons.delete'),
                            'product_name'=>$product->name
                            ]])
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection