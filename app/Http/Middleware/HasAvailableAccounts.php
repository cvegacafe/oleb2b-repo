<?php namespace App\Http\Middleware;

use App\Library\Aviso;
use Closure;
use Illuminate\Support\Facades\Auth;

class HasAvailableAccounts
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUser = Auth::user();
        $currentMembership = $currentUser->activeMembership();
        $usedSubAccounts = $currentUser->suppliers->first()->users->count();

        if ($currentMembership && ($currentMembership->subaccounts >= $usedSubAccounts)) {
            return $next($request);
        }

        Aviso::error(trans('common.messages.not_have_subAccounts_available'));
        return redirect()->route('user.sub-accounts.index');
    }
}
