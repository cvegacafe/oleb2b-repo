<?php

return [

    'my_products' => 'Products',
    'complete_in' => 'Complete in English',
    'complete' => 'complete',
    'incomplete' => 'incomplete',
    'form' => [
        'first_name' => 'First Name',
        'last_names' => 'Last Name',
        'password' => 'Password',
        'confirm_password' => 'Confirm Password',
        'email_address' => 'Email',
        'country' => 'Country',
        'office_phone' => 'Office Phone Ex: +51 123 4567',
        'mobile_phone' => 'Mobile Phone',
        'address' => 'Address',
        'city' => 'City',
        'postal_code' => 'Postal Code',
        'languages' => 'Languages',
        'others_languages' => 'Other Languages',
        'company' => 'Company Name'
    ],
    'can_sell_buy' => 'Can sell/buy',
    'can_buy' => 'Can buy',
    'update_image' => 'Update image',
    'button-add-photos' => 'Upload a logo/photo',
    'exporter_manual' => 'Exporter Manual'
];
