@extends('layouts.admin3')
@section('content')
    <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <div class="row">
                    <h1 class="col-xs-8 panel-title">Identificacion tributaria :</h1>
                </div>
            </div>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Paises</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($taxtypes as $taxtype)
                        <tr>
                            <td><b>{{$taxtype->id}}</b></td>
                            <td>{{$taxtype->name}}</td>
                            <td>{{$taxtype->countries->implode('en_name',', ')}}</td>
                            <td>
                                <a href="{{action('Admin\TaxTypesController@edit',$taxtype)}}"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="fa fa-pencil"></span></button></a>
                                <form action="{{action('Admin\TaxTypesController@destroy',$taxtype)}}" method="POST" onsubmit="return confirm('Esta seguro q desea eliminar {{$taxtype->name}}')">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-primary btn-xs" ><span class="fa fa-times"></span></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <form action="{{action('Admin\TaxTypesController@store')}}" method="POST">
                        {{csrf_field()}}
                        <tfoot>
                            <tr>
                                <td></td>
                                <td><input type="text" name="name" class="form-control"></td>
                                <td>
                                    @foreach($countries as $country)
                                        <label for="c-{{$country->id}}">{{$country->en_name}}</label>
                                        <input id="c-{{$country->id}}" type="checkbox" name="countries[]" value="{{$country->id}}" > &nbsp;&nbsp;
                                    @endforeach
                                </td>
                                <td><button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button></td>
                            </tr>
                        </tfoot>
                    </form>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        var path = '{{route('api.currency.enable')}}';

        $(function(){
            $('.chk2').on('click',function(e){
                e.preventDefault();
                var checked = false;
                var self = $(this);
                var id = $(this).attr('id');
                if ($(this).is(':checked')){
                    checked = true;
                }
                var request = $.ajax({
                    url: path,
                    method: "POST",
                    data: {_token: window.Laravel.csrfToken,checked:checked,id:id,_method:'put'},
                    dataType: "json"
                });
                request.done(function (data) {
                    if(data.response==true){
                        self.prop('checked',data.value);
                        if(data.value){
                            self.parent().find('label').text('habilitado');
                        }else{
                            self.parent().find('label').text('inahabilitado');
                        }

                    }
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log(jqXHR);
                    console.log("Request failed: " + textStatus);
                });
            });
        });
    </script>
@endsection
