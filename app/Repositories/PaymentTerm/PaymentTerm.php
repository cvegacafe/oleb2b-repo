<?php namespace App\Repositories\PaymentTerm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

/**
 * Class PaymentTerm
 * @package App\Repositories\PaymentTerm
 */
class PaymentTerm extends Model
{
    /**
     * @var string
     */
    public $table = 'paymentTerms';

    /**
     * @return mixed
     */
    public function getDescriptionAttribute()
    {
        return $this->getAttribute(strtolower(Lang::locale()) . '_description');
    }
}

