<!doctype html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Oleb2b</title>
</head>
<body>
<table cellspacing="0"
       cellpadding="0"
       align="center"
       bgcolor="#ffffff"
       width="621"
       style="width:470px;
       overflow: hidden;
       box-shadow: 0px 0 120px 3px rgba(0, 0, 0, 0.15);
       border-radius: 10px;
       margin: 40px auto 10px;">
    <tr>
        <td align="center" bgcolor="#ff6a31" style="border-left: 1px solid #e8e8e8;
                                                    border-right: 1px solid #e8e8e8;">
            <img src="{{asset('mail/header.jpg')}}" width="470" height="89" alt="" style="
                    margin:0px;
                    border:0px;
                    width: 100%;
                    display: block;"/>
        </td>
    </tr>
    <tr>
        <td  style="
        padding:  25px 15px 20px 15px;
        border-left: 1px solid #e8e8e8;
        border-right: 1px solid #e8e8e8;
        ">
            <table>
                <tr>
                    <td style="
                            padding: 0 10px 0 10px;
                            font-family: Arial, sans-serif;
                            font-size:14px;
                            color:#333333">
                        <span style="text-align: center; font-size: 25px;">
                            <b>{{trans('portal.greeting')}}, {{$userName}}</b>
                        </span>
                        <br>
                        <br/>
                        {{trans('mails.mail_message.title')}}
                        <br/>
                        <br/>
                        <br/>
                        <a href="{{ $actionUrl }}" style="
                                    display: inline-block;
                                    text-decoration: none">
                            <table cellspacing="0" cellpadding="0" bgcolor="#ff6a31" style="border-radius: 29px;">
                                <tr>
                                    <td align="center">
                                        <img src="{{asset('mail/radius-btn-1.jpg')}}" alt="" style="display: block">
                                    </td>
                                    <td align="justify" style="
                                        padding: 12px 15px 12px 15px;
                                        font-family: Arial, sans-serif;
                                        font-size:15px;
                                        color:#ffffff;
                                        text-align: center">
                                        <b>{{trans('mails.mail_message.action_btn')}}</b>
                                    </td>
                                    <td align="center">
                                        <img src="{{asset('mail/radius-btn-2.jpg')}}" alt="" style="display: block">

                                    </td>
                                </tr>
                            </table>
                        </a>
                        <br/><br/>
                        <hr style="opacity: 0.2;">
                        <p>{{trans('mails.button_not_working')}}</p>
                        <p style="color: blue;text-decoration: underline;">{{ $actionUrl }}</p>
                        <hr style="opacity: 0.2;">
                        <br>
                        <p>{{trans('mails.regards')}}<br> {{trans('mails.oleb2b_team')}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#333333" style=" padding: 25px 35px 15px 35px;">

            <table style="width: 100%;">
                <tr>
                    <td style="
                               color: #ffffff;
                               font-family: Arial, sans-serif;
                               font-size:12px;
                               width: 50%;">
                        © 2017 Oleb2b. All rights reserved.
                    </td>

                    <td align="right" style="text-align: right;width: 50%;">
                        <a href="https://www.facebook.com/oleb2b/" style="text-decoration: none;display: inline-block;">
                            <img src="{{asset('mail/ico-facebook.jpg')}}" alt="Facebook" style="
                                                                     display: block;
                                                                     border: 0">
                        </a>
                       {{-- <a href="#" style="text-decoration: none;display: inline-block;">
                            <img src="{{asset('mail/ico-youtube.jpg')}}" alt="Youtube" style="
                                                                     display: block;
                                                                     border: 0">
                        </a>--}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
            <img src="{{asset('mail/border-bottom.jpg')}}" alt="" style="display: block">
        </td>
    </tr>

</table>
</body>

</html>
