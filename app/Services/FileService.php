<?php namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager as Image;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:28 PM
 */
class FileService
{
    const DISC_PRODUCTS = 'products';
    const DISC_BUYING_REQUEST = 'buying_request';
    const DISC_CONTACT_SUPPLIER = 'messaging';
    const DISC_DEFAULT = 'local';
    const DISC_AVATARS = 'users';
    const DISC_PUBLIC = 'public';

    /**
     * @param $products
     * @param $userId
     * @return array
     */
    public function storeProducts($products, $userId)
    {
        $pathsImages = [];

        foreach ($products as $image) {
            $pathsImages[] = $this->storeProduct($image, $userId);
        }

        return $pathsImages;
    }

    /**
     * @param $image
     * @param $userId
     * @return string
     */
    public function storeProduct($image, $userId)
    {


        $path = $this->storeFile($image, $userId, self::DISC_PRODUCTS);

        $intervention = new Image();
        $intervention->make('uploads/images/' . self::DISC_PRODUCTS . '/' . $path)
            ->heighten(490)
            ->save('uploads/images/' . self::DISC_PRODUCTS . '/' . $path);
        return 'uploads/images/' . self::DISC_PRODUCTS . '/' . $path;
    }

    /**
     * @param UploadedFile $file
     * @param $userId
     * @return string
     */
    public function storeSupplierLogo(UploadedFile $file, $userId)
    {
        $partialPath = $this->storeFile($file, $userId, self::DISC_AVATARS);

        return 'uploads/images/' . self::DISC_AVATARS . '/' . $partialPath;
    }

    public function storeUserAvatar(UploadedFile $file, $userId)
    {
        $partialPath = $this->storeFile($file, $userId, self::DISC_AVATARS);

        return 'uploads/images/' . self::DISC_AVATARS . '/' . $partialPath;
    }

    public function storeBuyingRequest(UploadedFile $file, $userId)
    {
        $partialPath = $this->storeFile($file, $userId, self::DISC_BUYING_REQUEST);

        return 'uploads/images/' . self::DISC_BUYING_REQUEST . '/' . $partialPath;
    }

    public function storeContactSupplier(UploadedFile $file, $userId)
    {
        $partialPath = $this->storeFile($file, $userId, self::DISC_CONTACT_SUPPLIER);

        return 'uploads/files/' . self::DISC_CONTACT_SUPPLIER . '/' . $partialPath;
    }

    public function storeAttachmentMessage(UploadedFile $file, $userId)
    {
        $partialPath = $this->storeFile($file, $userId, self::DISC_CONTACT_SUPPLIER);

        return 'uploads/files/' . self::DISC_CONTACT_SUPPLIER . '/' . $partialPath;
    }

    public function removeProducts(array $productPaths)
    {
        foreach ($productPaths as $path) {
            $this->removeFile($path);
        }
    }

    public function storeFile(UploadedFile $file, $userId, $disc = self::DISC_DEFAULT)
    {
        return $file->storePubliclyAs('/' . $userId,
            uniqid($userId . '_' . time() . '_') . '.' . $file->getClientOriginalExtension(), $disc);
    }

    public function removeFile($path)
    {
        return File::delete($path);
    }

    public function downloadFile($path, $name)
    {
        $headers = [
            'Content-Disposition' => 'attachment;'
        ];

        return response()->download($path, $name, $headers);
    }
}