@extends('layouts.pages')
@section('title')
    Buying Requests
@endsection
@section('content')
    <section class="search-buying-request">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="search">
                        <span>{{trans('portal.search_buying_request')}}</span>
                        <div class="input-container">
                            <form action="">
                                <select name="type" id="tipoBusqueda">
                                    <option value="Buscar: ">{{trans('portal.search')}}:</option>
                                    <option>Nombre</option>
                                </select>
                                <input type="text" name="q" placeholder="{{trans('portal.search_products')}}" value="{{$search}}">
                                <button>{{trans('portal.search')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="buying-requests">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <a class="boton-azul pull-right submit-buying-request" href="{{action('User\BuyingRequestController@create')}}">{{trans('buyingRequest.submit_buying_request')}}</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    @foreach($buyingRequests as $buyingRequest)
                        <div class="row single-request">
                            @include('global-partials.buying-request-row')
                            <div class="col-xs-12 col-lg-3 buttons" >
                                @if($authUser)
                                    @if($myBuyingRequest->contains('id',$buyingRequest->id))
                                        <a href="{{action('User\BuyingRequestController@edit',$buyingRequest)}}" class=""><button class="contact">{{trans('products.edit')}}</button></a>
                                    @elseif($threads->contains('buyingRequest_id',$buyingRequest->id))
                                        <a href="{{route('user.messages.buying-request.index', ['buying-request' => $buyingRequest->id, 'thread' => $threads->where('buyingRequest_id',$buyingRequest->id)->first()->id])}}">
                                            <button class="contact">
                                                {{trans('menus.user.messages')}}
                                            </button>
                                        </a>
                                    @else
                                        <button class="contact" onclick="contactWithBuyer('{{$buyingRequest->id}}','{{$buyingRequest->name}}','{{asset($buyingRequest->image)}}','{{$buyingRequest->user->languages->implode(\Lang::locale().'_name',' ,')}}','{{$buyingRequest->description}}','{{$buyingRequest->start_date->format('d/m/Y')}}','{{$buyingRequest->end_date->format('d/m/Y')}}','{{$buyingRequest->user->names.' '.$buyingRequest->user->last_names}}','{{$buyingRequest->country->name}}')">{{trans('buyingRequest.contact_buyer')}}</button>
                                    @endif
                                    @if($savedBuyingRequest->contains('buyingRequest_id',$buyingRequest->id))
                                        <button class="save toggleSavedBuyingRequest active" data-buying-request-id="{{$buyingRequest->id}}">{{trans('portal.saved')}}</button>
                                    @else
                                        <button class="save toggleSavedBuyingRequest" data-buying-request-id="{{$buyingRequest->id}}">{{trans('buyingRequest.save_later')}}</button>
                                    @endif
                                @else
                                    <button class="contact" onClick="showAuth()">{{trans('buyingRequest.contact_buyer')}}</button>
                                    <button class="save toggleSavedBuyingRequest" data-buying-request-id="{{$buyingRequest->id}}">{{trans('buyingRequest.save_later')}}</button>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            {{$buyingRequests->appends(['q','type'])->links()}}
        </div>
    </section>
    @include('pages.includes.buyer-contact-popup',[
        'countries' => $allCountries,
        'measurements'=>$measurements,
        'paymentTerms' => $paymentTerms,
        'languages' => $languages
        ])
@endsection
@section('scripts')
    <script>
        var position = 0;

        $('#seleccionar-fotos').on('click',function(){
            $file = $('.file'+position);
            if($file.val()){
                $file.click();
            }else{
                position++;
                appendInputFile(position);
                $('.file'+position).click();
            }
        });

        $('.preview-files').on('mouseenter','div span i',function(){
            $(this).parent().parent().find('figure').fadeIn();
        }).on('mouseleave','div',function(){
            $(this).parent().parent().find('figure').fadeOut();
        });

        function previewFile(element){
            var $this = $(element);
            //console.log(position);

            if(validateFile($this)){
                var fileName = getFileName($this.val());

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.preview-files').append('<div class="file'+position+'"> <span><i class="fa fa-picture-o"></i><small>'+fileName+'</small></span> <figure><img src="'+e.target.result+'"></figure> <div class="close" onclick="removeFile('+position+')"><i class="fa fa-close"></i></div> </div>');
                };

                reader.readAsDataURL(element.files[0]);
                if(!position === 1){
                    position++;
                    appendInputFile(position)
                }
            }
        }

        function appendInputFile(position){
            $('#filesContainer').append($('<input/>').attr('type', 'file').attr('name', 'file').addClass('input-file file'+position).attr('onchange',"previewFile(this, "+position+")"));
        }

        function validateFile(input){

            var extencionesAceptadas = [
                'jpeg',
                'jpg',
                'png',
                'gif',
                'bmp',
                'pdf',
                'doc',
                'docx',
                'xls',
                'ppt',
                'pptx'
            ];

            var maxSize = 2000;

            var maxCount = 1;

            //console.log((1024 * maxSize),input[0].files[0].size);

            if(input[0].files[0].size > (1024 * maxSize)){
                //TODO: alerta de imagen muy grande
                alert('imagen muy grande');
                return false;
            }

            if($('#filesContainer input').length > maxCount){
                //TODO: maximo de imagenes permitido
                alert('maximo de imagenes permitido');
                return false;
            }

            var fileExtension = input.val().split('.').pop();

            if(!extencionesAceptadas.includes(fileExtension)){
                input.val('');
                alert('tipo de archivo no permitido');
                return false;
            }

            return true;
        }
        function removeOldFile(file){
            var $this = $(file);
            var position = $this.data('position');
            $('#filesContainer').append('<input type="hidden" name="filesToRemove[]" value="'+position+'">');
            $this.parent().remove();
        }

        function removeFile(position){
            $('.file'+position).remove();
        }

        function getFileName(fullPath){
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            return filename;
        }
    </script>

    <script>
        $(function(){

            var max_chars = 250;

            $('#message').keyup(function() {
                var chars = $(this).val().length;
                var diff = max_chars - chars;
                $('#contador').html(diff);
            });

        });

        function contactWithBuyer(br_id,product_name,product_photo,languages,description,start_date,end_date,buyer,country){
            var modal = $('#BuyerModal');
            modal.find('#buyingRequest_id').val(br_id);
            modal.find('#product_name').text(product_name);
            modal.find('#product_photo').attr('src',product_photo);
            modal.find('#input_product_photo').val(product_photo);
            modal.find('#languages').text(languages);
            modal.find('#description').text(description);
            modal.find('#input_description').val(description);
            modal.find('#start_date').text(start_date);
            modal.find('#input_start_date').val(start_date);
            modal.find('#close_date').text(end_date);
            modal.find('#input_end_date').val(end_date);
            modal.find('#buyer_name').text(buyer);
            modal.find('#input_buyer_name').val(buyer);
            modal.find('#buyer_country').text(country);
            modal.find('#input_country').val(country);
            modal.find('#message').val(' ');
            modal.modal();
        }

        $('.toggleSavedBuyingRequest').on('click',function(e){
            e.preventDefault();
            var buyingRequestId = $(this).data('buying-request-id');
            if(IS_AUTH){
                if($(this).hasClass('active')){
                    removeSavedBuyingRequest(buyingRequestId);
                    $(this).removeClass('active');
                    $(this).text('{{trans('buyingRequest.save_later')}}');
                }else{
                    savedBuyingRequest(buyingRequestId);
                    $(this).addClass('active');
                    $(this).text('{{trans('portal.saved')}}');
                }
            }else{
                showAuth();
            }
        });

        function savedBuyingRequest(buyingRequestId){
            var request = $.ajax({
                url: '{{route('api.saved-buying-request.create')}}',
                method: "POST",
                data: { buyingRequest_id : buyingRequestId,_token: _TOKEN},
                dataType: "json"
            });
            request.done(function( response ) {
                return response.response == 'ok' || response.response == 'exists';
            });
            request.fail(function( jqXHR, textStatus ) {
                return false;
            });
        }

        function removeSavedBuyingRequest(buyingRequestId){
            var request = $.ajax({
                url: '{{route('api.saved-buying-request.remove')}}',
                method: "POST",
                data: { buyingRequest_id : buyingRequestId,_token: _TOKEN},
                dataType: "json"
            });

            request.done(function( response ) {
                return (response.response == 'ok' || response.response == 'exists');
            });
            request.fail(function() {
                return false;
            });
        }
    </script>
@endsection