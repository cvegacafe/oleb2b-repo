<?php namespace App\Policies;

use App\Repositories\Administrator\Administrator;
use App\Repositories\Product\Product;
use App\Repositories\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

   /* public function before(Administrator $administrator, $ability)
    {
        if ($administrator) {
            return true;
        }

    }*/

    public function update(User $user,Product $product)
    {
        return $product->user_id == $user->id ;
    }

    public function destroy(User $user,Product $product)
    {
        return $product->user_id == $user->id ;
    }
}
