<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:21 PM
 */

use App\Repositories\Subcategory\SubcategoryRepository;

class SubcategoryService extends BaseService
{

    public function __construct(SubcategoryRepository $subcategoryRepository)
    {
        $this->mainRepo = $subcategoryRepository;
        parent::__construct();
    }

    public function getByCategoryId($categoryId)
    {
        return $this->mainRepo->getByColumn('category_id', $categoryId);
    }
}