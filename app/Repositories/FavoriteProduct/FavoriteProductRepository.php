<?php namespace App\Repositories\FavoriteProduct;

use App\Repositories\BaseRepository;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 2/11/2016
 * Time: 4:50 PM
 */
class FavoriteProductRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new FavoriteProduct();
        parent::__construct();
    }

    public function findByUserAndProduct($user_id, $product_id)
    {
        return $this->model->where('user_id', $user_id)->where('product_id', $product_id)->first();
    }

    public function getByColumn($column, $value)
    {
        return $this->model->with('product', 'product.currency', 'product.subcategory', 'product.deliveryTerms', 'product.country',
            'product.paymentTerms')
            ->where($column, $value)->get();
    }
}