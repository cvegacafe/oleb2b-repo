<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    private $categories;

    public function __construct(CategoryService $categoryService)
    {
        $this->categories = $categoryService;
    }

    public function getCategoriesBySupercategoryId(Request $request)
    {
        $supercategoryId = $request->get('supercategory_id');
        $categories = $this->categories->getBySupercategoryId($supercategoryId);
        foreach ($categories as $category) {
            switch ($request->get('language')) {
                case 'es':
                    $category['trans_name'] = $category->es_name;;
                    break;
                case 'en':
                    $category['trans_name'] = $category->en_name;;
                    break;
                case 'ru':
                    $category['trans_name'] = $category->ru_name;;
                    break;
                default:
                    $category['trans_name'] = trans('common.messages.without_translation', [], [], $request->get('language'));
            }
        }
        return response()->json($categories);
    }

}
