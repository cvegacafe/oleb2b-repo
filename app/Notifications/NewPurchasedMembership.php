<?php namespace App\Notifications;

use App\Repositories\Membership\Membership;
use App\Repositories\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewPurchasedMembership extends Notification
{
    use Queueable;

    private $user, $membership, $lang;

    /**
     * Create a new notification instance.
     * @param User $user
     * @param Membership $membership
     */
    public function __construct(User $user, Membership $membership)
    {
        $this->user = $user;
        $this->lang = app()->getLocale();
        $this->membership = $membership;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userName = $this->user->names;
        $membershipName = $this->membership->nameByLang($this->lang);
        if($this->membership->id == 1){
            return;
        }
        $actionUrl = route('user.dashboard');
        $emailResponse = 'banner@oleb2b.com';
        return (new MailMessage)->view('mails.new-purchased-membership',
            compact('userName', 'membershipName', 'actionUrl', 'emailResponse'))->subject(trans('mails.mail_purchase.subject').' - OleB2B.com');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
