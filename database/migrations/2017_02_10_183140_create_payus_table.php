<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_pol')->unique();
            $table->string('currency', 5);
            $table->boolean('is_test')->default(true);
            $table->timestamp('transaction_date');
            $table->string('cc_holder')->nullable();
            $table->string('description')->nullable();
            $table->decimal('value', 14, 2);
            $table->string('email_buyer');
            $table->string('response_message_pol');
            $table->string('transaction_id');
            $table->string('sign');
            $table->string('billing_address')->nullable();
            $table->string('payment_method_name')->nullable();
            $table->string('shipping_country', 5);
            $table->string('commision_pol_currency', 5);
            $table->decimal('commision_pol');
            $table->ipAddress('ip');
            $table->string('billing_city');
            $table->string('reference_sale');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payu');
    }
}
