@extends('layouts.admin3')
@section('content')
    <div class="container">
        <div class="row">
            @if($product->is_approved)
                <div class="alert alert-success">Producto Aprobado</div>
            @else
                <div class="alert alert-warning">Producto Sin Aprobar</div>
            @endif
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group col-md-12">
                            <label for="description"
                                   class="col-md-12 control-label">{{trans('products.product_photos')}}</label>
                            <div class="col-md-12">
                                @if(isset($product->pictures[0]))
                                    @foreach($product->pictures as $picture)
                                        <img src="{{asset($picture)}}" alt="" width="200px">
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Idioma</th>
                                <th>Nombre/Titulo</th>
                                <th>Descripcion</th>
                                <th>Color</th>
                                <th>Tipo</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">Español</th>
                                <td>{{$product->es_name}}</td>
                                <td>{{$product->es_description}}</td>
                                <td>{{$product->es_color}}</td>
                                <td>{{$product->es_type}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Ingles</th>
                                <td>{{$product->en_name}}</td>
                                <td>{{$product->en_description}}</td>
                                <td>{{$product->en_color}}</td>
                                <td>{{$product->en_type}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Ruso</th>
                                <td>{{$product->ru_name}}</td>
                                <td>{{$product->ru_description}}</td>
                                <td>{{$product->ru_color}}</td>
                                <td>{{$product->ru_type}}</td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <ul class="list-inline">
                                    <li><a href="{{action('Admin\ProductsController@edit',$product)}}"
                                           class="btn btn-info">Editar</a></li>
                                    <li>
                                        @if($product->is_approved)
                                            <form action="{{route('admin.product.disable',$product)}}" method="POST">
                                                <input type="hidden" name="_method" value="PUT">
                                                {{csrf_field()}}
                                                <button type="submit" class="btn btn-warning">Deshabilitar</button>
                                            </form>
                                        @else
                                            <form action="{{route('admin.product.enable',$product)}}" method="POST">
                                                <input type="hidden" name="_method" value="PUT">
                                                {{csrf_field()}}
                                                <button type="submit" class="btn btn-success">Habilitar</button>
                                            </form>
                                        @endif
                                    </li>
                                    <li>
                                        <form action="{{action('Admin\ProductsController@destroy',$product)}}"
                                              method="POST"
                                              onsubmit="return confirm('Seguro que desea eliminar {{$product->es_name}}')">
                                            <input type="hidden" name="_method" value="DELETE">
                                            {{csrf_field()}}
                                            <button type="submit" class="btn btn-danger">Eliminar</button>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
