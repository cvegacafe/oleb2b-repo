<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\FavoriteProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteProductsController extends Controller
{
    private $favoriteProductService = null;

    public function __construct(FavoriteProductService $favoriteProductService)
    {
        $this->favoriteProductService = $favoriteProductService;
    }

    public function allFavoriteCount()
    {
        $favorites = $this->favoriteProductService->getFavoritesByUser(Auth::id());
        return response()->json(['response' => 'ok', 'count' => $favorites->count()]);
    }
    
    public function create(Request $request)
    {
        $request['user_id'] = Auth::id();
        if ($this->favoriteProductService->exists($request['user_id'], $request->get('product_id'))) {
            return response()->json(['response' => 'exists', 'message' => 'ya existe']);
        }
        $result = $this->favoriteProductService->create($request->all());
        if ($result) {
            return response()->json(['response' => 'ok', 'message' => 'Agregado']);
        }

        return response()->json(['response' => 'error', 'message' => 'No se pudo agregar']);
    }

    public function remove(Request $request)
    {
        $user_id = Auth::id();
        $favorite = $this->favoriteProductService->getFavoriteProductId($request->product_id, $user_id);
        $result = $this->favoriteProductService->destroy($favorite->id);
        if ($result) {
            return response()->json(['response' => 'ok', 'message' => 'Removido']);
        }

        return response()->json(['response' => 'error', 'message' => 'No se pudo remover de tus favoritos']);
    }
}
