<?php namespace App\Services;
/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:20 PM
 */
use App\Repositories\Taxtypes\TaxtypeRepository;

class TaxTypeService extends BaseService
{

    public function __construct(TaxtypeRepository $taxtypeRepository)
    {
        $this->mainRepo = $taxtypeRepository;
        parent::__construct();
    }
}