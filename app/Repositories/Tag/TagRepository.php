<?php namespace App\Repositories\Tag;

use App\Repositories\BaseRepository;

class TagRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = new Tag();
        parent::__construct();
    }

    /**
     * @param $name
     * @return Tag
     */
    public function findOrCreate($name)
    {
        return $this->model->where('name', $name)->first() ?? $this->model->create(['name' => $name]);
    }
}