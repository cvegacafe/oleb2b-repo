@extends('layouts.pages')
@section('title')
    Home
@endsection
@section('content')
    <div class="buscador">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h1 class="titulo">{{trans('portal.main_slogan')}}</h1>
                    <div class="input-container">
                        <form action="{{route('portal.search.build')}}" method="POST">
                            {{ csrf_field() }}
                            <select id="tipoBusqueda">
                                <option value="Buscar: ">{{trans('portal.search')}}:</option>
                                <option>{{trans('portal.product')}}</option>
                                {{--        <option value="proveedor">{{trans('portal.supplier')}}</option>--}}
                            </select>
                            <input type="text" name="q" placeholder="{{trans('portal.search_products')}}">
                            <button>{{trans('portal.search')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="buying-requests" id="buying-requests">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="empty"><strong>404 {{trans('portal.error_404')}}
                            :</strong> {{trans('portal.error_404_text')}}</h3>
                </div>
            </div>

        </div>
    </section>
    <div class="registrate">
        <div class="centro">
            <h3>{{trans('portal.sale_online_is_easy')}}</h3>
            <button>{{trans('portal.join_free')}}</button>
        </div>
    </div>
@endsection
@section('styles')
    <style>
        mark {
            background: #ffffa3 !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{asset('assets/packages/mark.js/dist/jquery.mark.js')}}"></script>
    <script>
        function setFavoriteProduct(product_id, el) {
            var element = $(el);
            var request = $.ajax({
                url: "{{route('api.productFavorite.create')}}",
                method: "POST",
                data: {product_id: product_id, _token: '{{csrf_token()}}'},
                dataType: "json"
            });
            request.done(function (response) {
                if (response.response == 'ok' || response.response == 'exists') {
                    element.addClass('active');
                    element.text('{{trans('portal.saved')}}');
                    element.prop('onclick', null).off('click');
                } else {
                    //TODO: mensaje oportuno
                    alert('no se pudo agregar');
                }
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        }

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            console.log();

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        function mark(text) {

            // Read the keyword
            var keyword = text;

            // Determine selected options
            var options = {};
//            $("input[name='opt[]']").each(function() {
//                options[$(this).val()] = $(this).is(":checked");
//            });
            // Remove previous marked elements and mark
            // the new keyword inside the context
            $("#buying-requests").unmark({
                done: function () {
                    $("#buying-requests").mark(keyword, options);
                }
            });
        };

        $(document).ready(function () {
            var text = getUrlParameter('q');
            mark(text);
        });

    </script>
@endsection