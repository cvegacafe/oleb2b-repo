<?php namespace App\Jobs;

use App\Services\MessageService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SetMessagesViewed implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $threadsids;

    private $date;

    /**
     * Create a new job instance.
     *
     * @param array $threadsIds
     * @param Carbon $date
     */
    public function __construct(array $threadsIds, Carbon $date)
    {
        $this->threadsids = $threadsIds;

        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @param MessageService $messageService
     */
    public function handle(MessageService $messageService)
    {
        //$messageService->updateViewedMessagesByThread($this->date, $this->threadsids);
    }
}
