<?php

use App\Repositories\Measure\Measure;
use Illuminate\Database\Seeder;

class MeasurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Measure::create(['id'=>1,'en_name'=>'Kg','es_name'=>'Kilogramos','ru_name'=>'КГ']);
        Measure::create(['id'=>2,'en_name'=>'PCS','es_name'=>'Unidad','ru_name'=>'ШТ']);
        Measure::create(['id'=>3,'en_name'=>'TON','es_name'=>'Toneladas','ru_name'=>'ТОНН']);
        Measure::create(['id'=>4,'en_name'=>'METRIC TON','es_name'=>'Toneladas Métricas','ru_name'=>'МЕТР.ТОНН']);
        Measure::create(['id'=>5,'en_name'=>'Square Meters','es_name'=>'Metros Cuadrados','ru_name'=>'КВ.МЕТР']);
        Measure::create(['id'=>6,'en_name'=>'BOX','es_name'=>'Caja','ru_name'=>'КОРОБКА']);
        Measure::create(['id'=>7,'en_name'=>'PAIRS','es_name'=>'Pares','ru_name'=>'ПАРА']);
        Measure::create(['id'=>8,'en_name'=>'SET','es_name'=>'Juego','ru_name'=>'НАБОР']);
        Measure::create(['id'=>9,'en_name'=>'ROLL','es_name'=>'Rollo','ru_name'=>'РУЛОН']);
        Measure::create(['id'=>11,'en_name'=>'METERS','es_name'=>'Metros','ru_name'=>'МЕТРx']);
        Measure::create(['id'=>12,'en_name'=>'LITERS','es_name'=>'Litros','ru_name'=>'ЛИТР']);
        Measure::create(['id'=>13,'en_name'=>'20 FT CONTAINER','es_name'=>'Contenedor de 20 Pies','ru_name'=>'КОНТЕЙНЕР 20 ФУТОВ']);
        Measure::create(['id'=>14,'en_name'=>'40 FT CONTAINER','es_name'=>'Contenedor de 40 Pies','ru_name'=>'КОНТЕЙНЕР 40 ФУТОВ']);
        Measure::create(['id'=>15,'en_name'=>'40 HQ CONTAINER','es_name'=>'Contenedor de 40 Pies Cúbicos','ru_name'=>'КОНТЕЙНЕР 40 ФУТОВ HQ']);
        Measure::create(['id'=>16,'en_name'=>'45 FT CONTAINER','es_name'=>'CONTENEDOR DE 45 PIES','ru_name'=>'КОНТЕЙНЕР 45 ФУТОВ']);

    }
}
