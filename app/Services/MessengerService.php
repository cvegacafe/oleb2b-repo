<?php namespace App\Services;

use App\Notifications\SendEmailForNewMessage;
use App\Repositories\User\User;
use Carbon\Carbon;

class MessengerService
{
    private $notificationService = null;
    private $emailService = null;
    private $pusherService = null;
    private $messageService = null;

    /**
     * MessengerService constructor.
     * @param NotificationService $notificationService
     * @param EmailService $emailService
     * @param PusherService $pusherService
     * @param MessageService $messageService
     */
    public function __construct(
        NotificationService $notificationService,
        EmailService $emailService,
        PusherService $pusherService,
        MessageService $messageService
    ) {
        $this->notificationService = $notificationService;
        $this->emailService = $emailService;
        $this->pusherService = $pusherService;
        $this->messageService = $messageService;
    }

    /**
     * @param $toUserId
     * @param $membershipName
     */
    public function sendMessageSuccessPayment($toUserId, $membershipName)
    {
        $this->pusherService->sendToUser($toUserId,
            trans('alerts.messages.membership_pay_success', ['name' => $membershipName]),
            Carbon::now()->toDateTimeString(), 'fa fa-user', null);
    }

    /**
     * @param User $user
     * @param $lang
     * @param $url
     * @param bool $sendPusherNotification
     * @param bool $sendMail
     * @throws \Exception
     */
    public function sendMessageToSupplier(User $user, $lang, $url, $sendPusherNotification = true, $sendMail = true)
    {

        $newNotification = [
            'user_id' => $user->id,
            'icon' => 'fa fa-circle',
            'es_title' => 'Nuevo Mensaje!',
            'en_title' => 'New message',
            'ru_title' => 'новое сообщение',
            'url' => $url
        ];

        $notification = $this->notificationService->findNotificationByUrl($url);

        if (!$notification) {
            $notification = $this->notificationService->create($newNotification);
            if ($sendPusherNotification) {
                $this->pusherService->sendToUser($user->id, $newNotification[$lang . '_title'],
                    $notification->created_at,
                    'fa fa-user', $url);
            }

            if ($sendMail) {
                $user->notify(new SendEmailForNewMessage($user, $url));
            }
        }
    }

    /**
     * @param User $user
     * @param $lang
     * @param $url
     * @param bool $sendPusherNotification
     * @param bool $sendMail
     * @throws \Exception
     */
    public function sendMessageToBuyer(User $user, $lang, $url, $sendPusherNotification = true, $sendMail = true)
    {

        $newNotification = [
            'user_id' => $user->id,
            'icon' => 'fa fa-circle',
            'es_title' => 'Nuevo Mensaje!',
            'en_title' => 'New message',
            'ru_title' => 'новое сообщение',
            'url' => $url
        ];

        $notification = $this->notificationService->findNotificationByUrl($url);

        if (!$notification) {
            $notification = $this->notificationService->create($newNotification);
            if ($sendPusherNotification) {
                $this->pusherService->sendToUser($user->id, $newNotification[$lang . '_title'],
                    $notification->created_at,
                    'fa fa-user', $url);
            }

            if ($sendMail) {
                $user->notify(new SendEmailForNewMessage($user, $url));
            }
        }
    }
}