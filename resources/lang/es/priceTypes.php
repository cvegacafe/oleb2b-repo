<?php
return [
    \App\Repositories\Product\Product::TYPE_PRICE_RANGE => 'Rango de Precios',
    \App\Repositories\Product\Product::TYPE_PRICE_ASK => 'Preguntar al Vendedor',
    \App\Repositories\Product\Product::TYPE_PRICE_FIXED => 'Precio Fijo',
];