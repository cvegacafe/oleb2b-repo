<?php namespace App\Services;

use App\Repositories\Notifications\NotificationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;

class NotificationService extends BaseService
{

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->mainRepo = $notificationRepository;
        parent::__construct();
    }


    /**
     * get the last notifications not viewed from the auth user
     * @return mixed
     */
    public function getLastNotifications($userId)
    {
        return $this->mainRepo->getLastUnreadNotifications($userId, Lang::locale());
    }

    public function findNotificationByUrl($url)
    {
        return $this->mainRepo->firstByColumn('url', $url);
    }

    public function setNotificationViewed($url,$userId)
    {
        $notification = $this->mainRepo->firstByColumn('url', $url);

        if($notification && $notification->user_id == $userId){
            $notification->read_at = Carbon::now();
            return $notification->save();
        }

        return null;

    }

}
