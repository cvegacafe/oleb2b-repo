@extends('layouts.pages')
@section('title')
    Contact
@endsection
@section('content')
    <div class="container-fluid contact-top">
        <div class="col-xs-12">
            <h1>{{trans('menus.footer.privacy_politics')}}</h1>
        </div>
    </div>
    <section class="contact">
        <div class="container">
            <div class="row">
                <strong>Privacy Policy</strong>

                (Updated as of  August 2016)

                Oleb2b.com (the "Site") is an electronic commerce platform which is predominantly used by business entities to facilitate electronic commerce and such business use does not generally involve the collection of personal information of individuals. Oleb2b.com recognizes the importance of privacy as well as the importance of maintaining the confidentiality of personal information. This Privacy Policy applies to all products and services provided by us and sets out how we may collect, use and disclose information in relation to users of the Site.

                You may use our services and products via a mobile device either through mobile applications or mobile optimized websites. This Privacy Policy also applies to such use of our services and products.

                All capitalized terms not defined in this document shall have the meanings ascribed to them in the Terms of Use of the Site, which can be found  <u>HERE.</u>  If you are a user of the Website from Peru or from outside of Peru entity that you are contracting with is OleB2B.com S.A.C.

                &nbsp;

                &nbsp;
                <ol>
                    <li><strong> COLLECTION OF INFORMATION</strong></li>
                </ol>
                &nbsp;
                <ol>
                    <li>Your privacy is important to us and we have taken steps to ensure that we do not collect more information from you than is necessary for us to provide you with our services and to protect your account.</li>
                    <li>Information including, but not limited to, user name, address, phone number, fax number, email address, gender, date and/or year of birth and user preferences ("Registration Information") may be collected at the time of user registration on the Site.</li>
                    <li>We record and retain details of users’ activities on the Site. Information relating to such transactions including, but not limited to, the types and specifications of the goods, pricing and delivery information and any trade dispute records (“Activities Information”) may be collected when sale and purchase transactions are conducted on or facilitated through the Site.</li>
                    <li>From time to time, we collect information about our users and prospective users during trade shows, industry events and other functions. The information we may collect at these locations may include, but is not limited to, user name, address, phone number, fax number and email address ("Event Information").</li>
                    <li>We record and retain records of users' buying and browsing activities on our platform including but not limited to IP addresses, browsing patterns and buyer behavioral patterns. In addition, we gather statistical information about the Site and visitors to the Site including, but not limited to, IP addresses, browser software, operating system, software and hardware attributes, pages viewed, number of sessions and unique visitors (together "Browsing Information").</li>
                    <li>Registration Information, Account Information, Activities Information, Event Information and Browsing Information generally relate to business entities and are together referred to as business data (“Business Data”). Insofar and only insofar as they constitute personally identifiable data of living individuals, such information are together referred to as personal data (“Personal Data”).</li>
                    <li>It is mandatory for users of the Site to provide certain categories of Business Data and Personal Data (as specified at the time of collection). In the event that users do not provide any or sufficient Business Data and/or Personal Data marked as mandatory, we may not be able to complete the registration process or provide such users with our products or services.</li>
                </ol>
                &nbsp;
                <ol>
                    <li><strong> USE OF PERSONAL DATA</strong></li>
                </ol>
                &nbsp;

                If you provide any Personal Data to us, you are deemed to have authorized us to collect, retain and use that Personal Data for the following purposes:
                <ol>
                    <li>verifying your identity;</li>
                    <li>verifying your eligibility to register as a user of the Site;</li>
                    <li>processing your registration as a user, providing you with a log-in ID for the Site and maintaining and managing your registration;</li>
                    <li>providing you with customer service and responding to your queries, feedback, claims or disputes;</li>
                    <li>to facilitate communication between buyers and sellers on the Site;</li>
                    <li>performing research or statistical analysis in order to improve the content and layout of the Sites, to improve our product offerings and services and for marketing and promotional purposes;</li>
                    <li>subject to obtaining your consent in such form as may be required under the applicable law, we may use your name, phone number, residential address, email address and fax number ("Marketing Data") to provide notices, surveys, product alerts, communications and other marketing materials to you relating to goods and services offered by us on the Site including Advanced and Premiums membership, Free Members membership (each of Advanced and Premium membership, and Free Members membership, a “Membership” and collectively, the “Memberships”) the value added services ancillary to the Memberships, and other products and services offered by us from time to time to members of the Site;</li>
                    <li>if you voluntarily submit any information to the Site for publication on the Site through the publishing tools, including but not limited to, Company Profile, Product Catalog, Buying Requests, and any discussion forum, then you are deemed to have given consent to the publication of such information on the Site ("Voluntary Information"); and</li>
                    <li>Making such disclosures as may be required for any of the above purposes or as required by law or in respect of any claims or potential claims brought against us.</li>
                </ol>
                &nbsp;
                <ol>
                    <li><strong> DISCLOSURE OF PERSONAL DATA</strong></li>
                </ol>
                &nbsp;
                <ol>
                    <li>You further agree that we may disclose and transfer (whether within or outside the jurisdiction of the Oleb2b.com entity that you are contracting with) your Personal Data to service providers engaged by us to assist us with providing you with our services (including but not limited to data entry, database management, promotions, products and services alerts, delivery services, payment extension services, and membership authentication and verification services) ("Service Providers"). These Service Providers are under a duty of confidentiality to us and are only permitted to use your Personal Data in connection with the purposes specified at B.1 to B.9 above, and not for their own purposes (including direct marketing).</li>
                    <li>You agree that we may disclose and transfer (whether within or outside the jurisdiction of the Oleb2b.com entity that you are contracting with), for the purposes specified at B.1 to B.9 above, your Personal Data to other affiliated companies within the OleB2B.com S.A.C. group, which comprises a group of companies operating online in business-to-business commerce.</li>
                    <li>When necessary we may also disclose and transfer (whether within or outside the jurisdiction of the Oleb2b.com entity that you are contracting with) your Personal Data to our professional advisers, law enforcement agencies, insurers, government and regulatory and other organizations for the purposes specified at B.9 above.</li>
                    <li>Any Personal Data supplied by you will be retained by us and will be accessible by our employees, any Service Providers engaged by us and third parties referred to at C.2 and C.3 above, for or in relation to any of the purposes stated in B.1 to B.9 above.</li>
                    <li>All Voluntary Information may be made publicly available on the Site and therefore accessible by any internet user. Any Voluntary Information that you disclose to us becomes public information and you relinquish any proprietary rights (including but not limited to the rights of confidentiality and copyright) in such information. You should exercise caution when deciding to include personal or proprietary information in the Voluntary Information that you submit to us.</li>
                    <li>We may provide statistical information to third parties, but when we do so, we do not provide personally-identifying information without your permission.</li>
                    <li>We have established relationships with other parties and websites to offer you the benefit of products and services which we do not offer. We offer you access to these other parties and their websites either through the use of hyperlinks to these sites from the Sites or through offering "co-branded" sites in which both we and other parties share the same uniform resource locator, domain name or pages within a domain name on the Internet. In some cases you may be required to submit personal information to register or apply for products or services provided by such third parties or co-branded partners. This Privacy Policy does not apply to these third party sites or co-branded sites. The privacy policies of those other parties may differ from ours, and we have no control over the information that you submit to those third parties. You should read the relevant privacy policy for those third party sites and co-branded sites before responding to any offers, products or services advertised by those parties.</li>
                </ol>
                &nbsp;
                <ol>
                    <li><strong> RIGHT TO ACCESS/CORRECT PERSONAL DATA</strong></li>
                </ol>
                &nbsp;

                Under the applicable laws, you have the right of access to personal information held by us and to request correction of the information.
                If you have any questions regarding this Privacy Policy or if you wish to access or correct your Personal Data, you may send your request in writing to the following address:
                OleB2B.com S.A.C. 480 Francisco Graña, Lima 17, Peru.
                In accordance with the applicable laws, we reserve the right to charge you a reasonable fee for the processing of any data access or correction request.

                &nbsp;
                <ol>
                    <li><strong> COOKIES</strong></li>
                </ol>
                &nbsp;

                We use "cookies" to store specific information about you and track your visits to the Site. It is not uncommon for websites to use cookies to enhance identification of their users.

                A "cookie" is a small amount of data that is sent to your browser and stored on your computer's hard drive. A cookie can be sent to your computer's hard drive only if you access the Site using a computer. If you do not de-activate or erase the cookie, each time you use the same computer to access the Site, our web servers will be notified of your visit to the Site and in turn we may have knowledge of your visit and the pattern of your usage.

                Generally, we use cookies to identify you and enable us to 1) access your Registration Information or Account Information so you do not have to re-enter it; 2) gather statistical information about usage by users; 3) research visiting patterns and help target advertisements based on user interests; 4) assist our partners to track user visits to the Site; and 5) track progress and participation in promotions.

                You can determine if and how a cookie will be accepted by configuring the browser which is installed in the computer you are using to access the Site. If you choose, you can change those configurations. By setting your preferences in the browser, you can accept all cookies or you can choose to be notified when a cookie is sent or you can choose to reject all cookies. If you reject all cookies by choosing the cookie-disabling function in your browser, you may be required to re-enter information on the Site more often and certain features of the Site may be unavailable.

                &nbsp;
                <ol>
                    <li><strong> MINORS</strong></li>
                </ol>
                &nbsp;

                The Site and its content is not targeted to minors (those under the age of 18) and we do not intend to sell any of our products or services to minors. However, we have no way of distinguishing the age of individuals who access our Site. If a minor has provided us with personal information without parental or guardian consent, the parent or guardian should contact our Legal Department at the address set out in paragraph D above to remove the information.

                &nbsp;
                <ol>
                    <li><strong> SECURITY MEASURES</strong></li>
                </ol>
                &nbsp;

                We employ commercially reasonable security methods to prevent unauthorized access to the Site, to maintain data accuracy and to ensure the correct use of the information we hold.

                For registered users of the Site, your Registration Information and Account Information (if any) can be viewed and edited through your account, which is protected by a password. We recommend that you do not divulge your password to anyone. Our personnel will never ask you for your password in an unsolicited phone call or in an unsolicited email. If you share a computer with others, you should not choose to save your log-in information (e.g., user ID and password) on that shared computer. Remember to sign out of your account and close your browser window when you have finished your session.

                No data transmission over the internet or any wireless network can be guaranteed to be perfectly secure. As a result, while we try to protect the information we hold for you, we cannot guarantee the security of any information you transmit to us and you do so at your own risk.

                &nbsp;
                <ol>
                    <li><strong> CHANGES TO THIS PRIVACY POLICY</strong></li>
                </ol>
                &nbsp;

                Any changes to this Privacy Policy will be communicated by us posting an amended and restated Privacy Policy on the Site. Once posted on the Sites the new Privacy Policy will be effective immediately. You agree that any information we hold about you (as described in this Privacy Policy and whether or not collected prior to or after the new Privacy Policy became effective) will be governed by the latest version of the Privacy Policy.

                &nbsp;
                <ol>
                    <li><strong> YOUR FEEDBACK</strong></li>
                </ol>
                &nbsp;

                We welcome your input regarding our Privacy Policy and any comments on the services we provide to you. You may send us your comments and responses by post to: OleB2B.com S.A.C. 480 Francisco Graña, Lima 17, Peru.
            </div>
        </div>

    </section>
@endsection