<?php
return [
    'choose_a_conversation_to_show' => 'Escoja una conversacion para mostrar los mensajes',
    'all_messages' => 'Todos los Mensajes',
    'last_messages' => 'último mensaje',
    'attachment' => 'archivo adjunto',
    'write_message' => 'Escribe un mensaje',
    'order_details' => 'Detalle',
    'measure' => 'Unidad',
    'company' => 'Empresa',
    'no_messages' => 'Sin Mensajes',
    'new_message' => 'Tienes un nuevo mensaje',
    'we_have_sent_you_an_email' => '<strong>¡Atención!</strong> te hemos enviado un correo para validar tu correo.',
    'data_missing' => ' Mejora tu perfil de empresa ingresando ',
    'profile_images_missing' => ' algunas imágenes',
    'factory_details_images_missing' => ' los datos de tu empresa',
    'certifications_images_missing' => ' algunos de tus certificados',
    'here' => 'Desde Aquí',
    'send_again' => 'Enviar de nuevo'
];