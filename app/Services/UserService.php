<?php namespace App\Services;

/**
 * Created by PhpStorm.
 * User: Emmanuel
 * Date: 28/10/2016
 * Time: 1:14 PM
 */

use App\Library\Constants;
use App\Notifications\WelcomeToOleb2b;
use App\Repositories\Supplier\Supplier;
use App\Repositories\User\User;
use App\Repositories\User\UserRepository;

class UserService extends BaseService
{
    public function __construct()
    {
        $this->mainRepo = new UserRepository;
        parent::__construct();
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmail($email)
    {
        return $this->mainRepo->firstByColumn('email', $email);
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findByEmailTrashed($email)
    {
        return $this->mainRepo->firstByColumnTrashed('email', $email);
    }

    /**
     * @param Supplier $supplier
     * @param $exceptId
     * @return mixed
     */
    public function indexSubAccounts(Supplier $supplier, $exceptId)
    {
        $users = $supplier->users;

        $users->filter(function ($user) use ($exceptId) {
            return $user->id != $exceptId;
        });

        return $users;
    }

    /**
     * @param array $inputs
     * @param Supplier $supplier
     * @param User $userPrincipal
     * @return \Illuminate\Database\Eloquent\Model|mixed
     */
    public function createSubAccount(array $inputs, Supplier $supplier, User $userPrincipal)
    {

        $existsUser = $this->findByEmailTrashed($inputs['email']);
        $inputs['status'] = Constants::USER_STATUS_ACTIVE;
        $inputs['country_id'] = $userPrincipal->country_id;

        $password = null;
        if ($existsUser) {
            $existsUser->restore();
            $user = $existsUser;
        } else {
            $password = $inputs['password'];
            $inputs['password'] = bcrypt($password);
            $user = $this->create($inputs);
        }

        $currentMembership = $userPrincipal->activeMembership();

        $user->memberships()->attach($currentMembership->id, [
            'main_rotation_banners' => $currentMembership->main_rotation_banners,
            'inner_rotation_banners' => $currentMembership->inner_rotation_banners,
            'product_posting' => $currentMembership->product_posting,
            'amount' => 0,
            'frequency' => $currentMembership->pivot->frequency,
            'is_active' => true,
            'finish_at' => $currentMembership->pivot->finish_at
        ]);

        $user->suppliers()->attach($supplier->id, ['is_principal' => false]);

        if (isset($inputs['languages'])) {
            $user->languages()->attach($inputs['languages']);
        }

        if (isset($inputs['markets'])) {
            $user->markets()->attach($inputs['markets']);
        }

        $user->notify(new WelcomeToOleb2b($user, $password));

        return $user;
    }


    /**
     * @return mixed
     */
    public function premiumUsers()
    {
        return $this->mainRepo->premiumUsers();
    }

}