<?php namespace App\Repositories\Supercategory;

use App\Repositories\BaseRepository;

class SupercategoryRepository extends BaseRepository
{

    public function __construct()
    {
        $this->model = new Supercategory();
        parent::__construct();
    }

    public function all()
    {
        return $this->model->with('categories', 'categories.subcategories')->get();
    }

}