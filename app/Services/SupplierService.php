<?php namespace App\Services;

use App\Repositories\Supplier\SupplierRepository;

class SupplierService extends BaseService
{

    private $translateService,$fileService;

    /**
     * SupplierService constructor.
     * @param SupplierRepository $supplierRepository
     * @param TranslateService $translateService
     * @param FileService $fileService
     * @throws \Exception
     */
    public function __construct(
        SupplierRepository $supplierRepository,
        TranslateService $translateService,
        FileService $fileService
    ) {
        $this->mainRepo = $supplierRepository;
        $this->translateService = $translateService;
        $this->fileService = $fileService;
        parent::__construct();
    }

    /**
     * create supplier table
     * @param array $inputs
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $inputs)
    {
        $inputs['es_company_description'] = $this->translateService->transToSpanish($inputs['company_description']);
        $inputs['en_company_description'] = $this->translateService->transToEnglish($inputs['company_description']);
        $inputs['ru_company_description'] = $this->translateService->transToRussian($inputs['company_description']);
        return $this->mainRepo->create($inputs);
    }

    /**
     * update supplier table
     * @param $id
     * @param array $inputs
     * @return mixed
     */
    public function update($id,array $inputs)
    {
        $inputs['es_company_description'] = $this->translateService->transToSpanish($inputs['company_description']);
        $inputs['en_company_description'] = $this->translateService->transToEnglish($inputs['company_description']);
        $inputs['ru_company_description'] = $this->translateService->transToRussian($inputs['company_description']);
        return $this->mainRepo->update($id,$inputs);
    }

}