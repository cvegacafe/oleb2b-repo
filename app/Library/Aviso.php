<?php namespace App\Library;
/**
 * 1=success
 * 2=warning
 * 3=error
 * 4=info
 */
class Aviso
{

    const NOTIFICATION_SUCCESS = 'success';
    const NOTIFICATION_DANGER = 'error';
    const NOTIFICATION_INFO = 'info';
    const NOTIFICATION_WARNING = 'warning';

    public static function modelNotFound()
    {
        self::error('El registro al que desea acceder no ha sido encontrado.');
    }

    public static function exception()
    {
        self::error('Un error desconocido ha ocurrido', true);
    }

    public static function inUse($tipo = 'registros')
    {
        self::error('No se puede eliminar porque el registro que intenta eliminar esta siendo usado por otro(s) ' . $tipo);

    }

    public static function guardado($mensaje = 'Registro(s)  agregado(s) exitosamente ', $permanente = false)
    {
        self::success($mensaje, $permanente);
    }

    public static function noGuardado($mensaje = 'Registro(s) no pudo ser agregado(s) ', $permanente = false)
    {
        self::error($mensaje, $permanente);
    }

    public static function actualizado($mensaje = 'Registro(s) actualizado(s) exitosamente ')
    {
        self::success($mensaje);
    }

    public static function noActualizado($mensaje = 'Registro(s) no pudo ser actualizado(s) ')
    {
        self::success($mensaje);
    }

    public static function existe($mensaje = ' Este Registro ya existe ', $permanente = false)
    {
        self::error($mensaje, $permanente);
    }

    public static function eliminado($mensaje = 'Registro(s) eliminado(s) exitosamente ')
    {
        self::success($mensaje);
    }

    public static function noEliminado($mensaje = 'Registro(s) no pudo ser eliminado(s)')
    {
        self::success($mensaje);
    }

    public static function warning($mensaje, $permanente = false)
    {
        self::launch('warning', $mensaje, self::NOTIFICATION_WARNING, $permanente);
    }

    public static function success($mensaje, $permanente = false)
    {
        self::launch('success', $mensaje, self::NOTIFICATION_SUCCESS, $permanente);
    }

    public static function error($mensaje, $permanente = false)
    {
        self::launch('danger', $mensaje, self::NOTIFICATION_DANGER, $permanente);
    }

    private static function normal($mensaje, $permanente = false)
    {
        self::launch('info', $mensaje, self::NOTIFICATION_INFO, $permanente);
    }

    private static function launch($class = 'info', $mensaje, $tipo, $permanente = false)
    {
        session()->flash('aviso-msg', $mensaje);
        session()->flash('aviso-tipo', $tipo);
        session()->flash('aviso-title', $tipo);
    }
}
