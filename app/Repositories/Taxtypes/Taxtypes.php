<?php namespace App\Repositories\Taxtypes;

use App\Repositories\Country\Country;
use Illuminate\Database\Eloquent\Model;

class Taxtypes extends Model
{
    public $table = 'taxtypes';
    public $fillable = ['name'];

    public function countries()
    {
        return $this->belongsToMany(Country::class, 'country_taxtype', 'taxtype_id', 'country_id')->withPivot('digits');
    }
}
