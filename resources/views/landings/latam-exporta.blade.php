<!DOCTYPE html>
<html lang="es">
<head>
    <title>Exporta Más con Oleb2b</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('assets/landing/latam-exporta/css/styles.css')}}">
</head>

<body>

<header class="main-header">

    <div class="main-header--head">
        <div class="container">
            <a href="http://www.oleb2b.com/" target="_blank">
                <img src="{{ asset('assets/landing/latam-exporta/img/logo-blanco.png')}}" alt=""
                     class="main-header--logo">
            </a>
            <a href="{{url('/es/contact')}}"  class="no-link">
                <button type="button" class="btn-action btn-contact">CONTÁCTANOS</button>
            </a>
        </div>
    </div>

    <div class="main-header--body">

        <div class="banner-landing">

            <div class="super-capsula"></div>

            <div class="container">

                <div class="banner-landing__circle circle-md"></div>
                <div class="banner-landing__circle circle-sm"></div>

                <img src="{{ asset('assets/landing/latam-exporta/img/divices-banner.png')}}" alt=""
                     class="banner-landing__img">

                <div class="main-header--text">

                    <h1 class="title">Tus productos en<br>
                        todo el Mundo<br>
                    </h1>
                    <p class="decription">
                        ¡Primer <strong>portal B2B</strong> para Exportadores
                        de <strong>America Latina y Rusia</strong>!
                        <span class="capsula capsula-baner-1"></span>
                        <span class="capsula capsula-baner-2"></span>
                    </p>
                    <a href="{{url('/es?show=register')}}" class="no-link">
                        <button class="btn-action banner__btn-action btn--azul">Regístrate Hoy</button>
                    </a>
                </div>
            </div>

        </div>

    </div>

</header>

<section class="featured">

    <div class="container">

        <div class="featured__head">
            <h2 class="featured__title title-lines">¿Qué te ofrecemos?</h2>
            <p class="featured__decription">Beneficios para mejorar y hacer crecer tu negocio a nivel Mundial</p>
        </div>

        <div class="featured-grid">

            <div class="featured__item">
                <div class="featured__item-img">
                    <img src="{{ asset('assets/landing/latam-exporta/img/ico-ventas.png')}}" alt="">
                </div>
                <div class="featured__item-decription">
                    <h3 class="featured__item-title">Ventas</h3>
                    <p>Compradores de todo el mundo van a ver tus productos</p>
                </div>
            </div>

            <div class="featured__item">
                <div class="featured__item-img">
                    <img src="{{ asset('assets/landing/latam-exporta/img/ico-productos.png')}}" alt="">
                </div>
                <div class="featured__item-decription">
                    <h3 class="featured__item-title">Productos</h3>
                    <p>Encuentra proveedores de calidad para tu negocio</p>
                </div>
            </div>

            <div class="featured__item">
                <div class="featured__item-img">
                    <img src="{{ asset('assets/landing/latam-exporta/img/ico-accesos.png')}}" alt="">
                </div>
                <div class="featured__item-decription">
                    <h3 class="featured__item-title">Visibilidad global</h3>
                    <p>Tendrás acceso al Mercado Global y al Mercado Ruso a un Click</p>
                </div>
            </div>

            <div class="featured__item">
                <div class="featured__item-img">
                    <img src="{{ asset('assets/landing/latam-exporta/img/ico-promociones.png')}}" alt="">
                </div>
                <div class="featured__item-decription">
                    <h3 class="featured__item-title">Promoción constante</h3>
                    <p>Promocionamos al rededor del mundo todos los productos que ofrecemos</p>
                </div>
            </div>

            <div class="featured__item">
                <div class="featured__item-img">
                    <img src="{{ asset('assets/landing/latam-exporta/img/ico-membresias.png')}}" alt="">
                </div>
                <div class="featured__item-decription">
                    <h3 class="featured__item-title">Membresías</h3>
                    <p>Paga solo lo que necesitas, tenemos planes para todo volumen de negocio</p>
                </div>
            </div>

        </div>

        <a href="{{url('/es?show=register')}}"  class="no-link">
            <button class="btn-action featured-btn-action btn--azul btn-bing">¡REGISTRATE AHORA!</button>
        </a>

    </div>

</section>

{{--<section class="map">--}}
{{--<div class="container">--}}

{{--<h2 class="map__title title-lines">¡Fácil, Gratis, Efectivo!<span>¡Latam Exporta!</span></h2>--}}
{{--<img src="{{ asset('assets/landing/latam-exporta/img/mapa.png')}}" alt="" class="map__img">--}}
{{----}}
{{--</div>--}}
{{--</section>--}}

{{--<section class="divices">--}}

{{--<div class="container">--}}
{{--<div class="divices-box">--}}

{{--<div class="divices-box__main">--}}
{{--<img src="{{ asset('assets/landing/latam-exporta/img/divices-minimal.png')}}" alt="" class="divices-box__img">--}}
{{--<div class="divices-box__text">--}}
{{--<h4 class="divices-box__title">Multiplataforma</h4>--}}
{{--<p class="divices-box__decription">Available on all devices. TV, desktop, tablet and mobile.</p>--}}
{{--<div class="capsula"></div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<button type="" class="btn-action divices-box__btn-action btn--azul">Subscribete por 3 meses gratis</button>--}}

{{--</div>--}}
{{--</div>--}}

{{--</section>--}}

<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="card-faq-answer">

                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <div class="card-faq-answer__question">
                        <h4>¿Qué es OleB2B.com?</h4>
                    </div>
                    <div class="card-faq-answer__description">
                        <p>Somos un portal de comercio electrónico B2B (Business to Business) para exportadores de America Latina y Rusia. Buscamos promover la economía de la región y entablar sólidas relaciones internacionales entre los diversos países asociados</p>
                    </div>

                </div>

                <div class="card-faq-answer">

                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <div class="card-faq-answer__question">
                        <h4>¿Cómo hago a vender en OleB2B.com?</h4>
                    </div>
                    <div class="card-faq-answer__description">
                        <ul class="list-guion">
                            <li>Regístrate.</li>
                            <li>Completa la información de tu Empresa.</li>
                            <li>Publica tus productos</li>
                        </ul>
                    </div>

                </div>
                <div class="card-faq-answer">

                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <div class="card-faq-answer__question">
                        <h4>¿Cómo OleB2B puede convertirse en mi socio estratégico en ventas B2B?</h4>
                    </div>
                    <div class="card-faq-answer__description">
                        <p>B2B significa Business to Business, es decir, venta de empresa a empresa. Nuestro modelo de negocio no contempla un cobro de comisión por la venta, queremos conectarte con la mayor cantidad de clientes posible.</p>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="card-faq-answer card-steps">

                    <div>
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                        <div class="card-faq-answer__question">
                            <h4>¿De dónde pueden ser mis compradores?</h4>
                        </div>
                        <div class="card-faq-answer__description">
                            <p>Llevamos a cabo campañas de marketing en diversos países del mundo que tienen interés en comprar productos en nuestra región. Los compradores que puedes encontrar pueden ser de cualquier parte del mundo ya que nuestro portal está disponible para todos los países del globo.</p>
                        </div>
                    </div>

                </div>

                <div class="card-faq-answer">

                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <div class="card-faq-answer__question">
                        <h4>¿Tengo que pagar para registrarme?</h4>
                    </div>
                    <div class="card-faq-answer__description">
                        <p>Solo vas a pagar cuando lo requieras, ofrecemos Membresías Básicas para que puedas iniciar sin pagar nada y membresías mucho más completas para que puedas gestionar equipos enteros de venta.</p>
                    </div>

                </div>
            </div>
        </div>


    </div>
</section>

<div style="display: block; clear: both;"></div>

<footer class="footer">
    <div class="container">

        <div class="footer__logo">
            <a href="http://www.oleb2b.com/" target="_blank" class="footer__link">
                <img src="{{ asset('assets/pages/img/logo.png')}}" alt="">
            </a>
        </div>

        <p class="footer__legal">© 2017 Oleb2b.com. All Rights & Copyrights Reserved.</p>

    </div>
</footer>

</body>

</html>
